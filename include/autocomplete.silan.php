<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include (dirname(__FILE__)."/class/Autocomplete.class.php");
$autocomplete = new Autocomplete();
$dataKey = (object)$_REQUEST["data"];
switch ($dataKey->opcion) {
    case "hilosTenidos":
        $autocomplete->sqlHilos($dataKey);
        break;
    case "productos":
        $autocomplete->sqlProductos($dataKey);
        break;
    case "formulasTelas": // utilizado también en pólizas
        $autocomplete->sqlFormulasTelas($dataKey);
        break;
    case "piezas":
        $autocomplete->sqlPiezas($dataKey);
        break;
    default :
//        $combos->getAllWidgetProductList()
        break;
}
echo json_encode($autocomplete->arrayElementos);
