<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once (dirname(__FILE__)."/class/Combos.class.php");
$combos = new Combos();
if(isset($_REQUEST["data"])){

    $datakey = (object)$_REQUEST["data"];
    switch ($datakey->widget){
        case "product":
            echo json_encode($combos->getImageproduct($datakey->keyproduct));
            break;
        case "destino":
            $combos->inputDestinoTarimas($datakey->value);
            //echo json_encode($combos->msgout);
            break;
        default:
            break;
    }
}