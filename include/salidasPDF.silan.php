<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
ini_set('display_errors', 1);error_reporting(E_ALL);
require (dirname(__FILE__)."/lib/fpdf181/fpdf.php");
require (dirname(__FILE__)."/class/SalidasPDF.class.php");
class PDF extends FPDF{
    protected $reportes;
    function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
        parent::__construct($orientation, $unit, $size);
        $this->reportes = new SalidasPDF();
    }
            
    function Header() {
        $this->SetFont('Arial','B',16);
        $this->SetXY(10, 10);
        $this->Cell(196, 10, 'Reporte de salidas por folio', 0, 1, 'C');
        $this->SetFont('Arial','B',10);
//        $this->SetXY(10, 20);
        $this->Cell(196, 5, $this->reportes->getFechaSalida(), 0, 1, 'R');
//        $this->SetXY(10, 25);
        $this->Cell(196, 5, 'Folio Salida: '.$this->reportes->getFolioSalida(), 0, 1, 'L');
    }
    
    function tituloColumnas() {
        $this->SetFont('Arial','BU',9);
        $this->SetXY(10, 35);
        $this->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
        $this->Cell(22, 5, 'Referencia', 0, 0, 'C');
        $this->Cell(69, 5, 'Color', 0, 0, 'C');
        $this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
        $this->Cell(14, 5, 'Etiqueta', 0, 0, 'C');
        $this->Cell(16, 5, 'Metros', 0, 0, 'C');
        $this->Cell(16, 5, 'Kilos', 0, 1, 'C');
    }
    
    function contenidoTabla() 
    {
        $this->SetFont('Arial','BU',8);
        $this->SetXY(10, 45);
        $this->Cell(24, 5, $this->reportes->getRecordSalida(0,'articulo'), 0, 1, 'C');
        
        $granTotal              = 0;
        $granTotalPiezas        = 0;
        $totalKilos             = 0;
        $totalMetros            = 0;
        $ultimoArticulo         = null;
        $totalPiezasArticulo    = 0;
        $totalKilosArticulo     = 0;
        $totalMetrosArticulo    = 0;
        
        foreach($this->reportes->records as $rec)
        {
            if( $ultimoArticulo == null )
                $ultimoArticulo = $rec->articulo;
            
            //##verificar si el arituclo cambio
            if( $ultimoArticulo != $rec->articulo )
            {
                $this->SetY($this->GetY() + 5);
                $this->SetXY(10, $this->GetY());
                $this->SetFont('Arial','B',8);
                $this->Cell(134, 5, '', 0, 0, 'R');
                $this->Cell(30, 5, $totalPiezasArticulo . ' piezas', 0, 0, 'L');
                $this->Cell(16, 5, number_format($totalMetrosArticulo,2), 'T', 0, 'R');
                $this->Cell(16, 5, number_format($totalKilosArticulo,2), 'T', 1, 'R');
                $this->SetY($this->GetY() + 5);

                $this->SetFont('Arial','BU',9);
                $this->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
                $this->Cell(22, 5, 'Referencia', 0, 0, 'C');
                $this->Cell(69, 5, 'Color', 0, 0, 'C');
                $this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
                $this->Cell(14, 5, 'Etiqueta', 0, 0, 'C');
                $this->Cell(16, 5, 'Metros', 0, 0, 'C');
                $this->Cell(16, 5, 'Kilos', 0, 1, 'C');
                $this->SetY($this->GetY()+5);
                
                //##gardar articulo
                $ultimoArticulo         = $rec->articulo;
                //##reiniciar contadores de articulo
                $totalPiezasArticulo    = 0;
                $totalKilosArticulo     = 0;
                $totalMetrosArticulo    = 0;
            }
            $totalPiezasArticulo++;
            $totalKilosArticulo     += $rec->kilos;
            $totalMetrosArticulo    += $rec->metros;
            $granTotalPiezas++;
            $totalKilos             += $rec->kilos;
            $totalMetros            += $rec->metros;
            
            $this->SetFont('Arial','',8);
            $this->Cell(24, 5, $rec->articulo, 0, 0, 'C');
            $this->Cell(22, 5, $rec->referencia, 0, 0, 'C');
            $this->Cell(69, 5, $rec->color, 0, 0, 'C');
            $this->Cell(35, 5, $rec->poliza, 0, 0, 'C');
            $this->Cell(14, 5, $rec->etiqueta, 0, 0, 'C');
            $this->Cell(16, 5, $rec->metros, 0, 0, 'R');
            $this->Cell(16, 5, $rec->kilos, 0, 1, 'R');
        }
        //##corte para el ultimo articulo
        $this->SetY($this->GetY() + 5);
        $this->SetXY(10, $this->GetY());
        $this->SetFont('Arial','B',8);
        $this->Cell(134, 5, '', 0, 0, 'R');
        $this->Cell(30, 5, $totalPiezasArticulo . ' piezas', 0, 0, 'L');
        $this->Cell(16, 5, number_format($totalMetrosArticulo,2), 'T', 0, 'R');
        $this->Cell(16, 5, number_format($totalKilosArticulo,2), 'T', 1, 'R');
        $this->SetY($this->GetY() + 5);
    
        
        $this->SetY($this->GetY()+5);
        $this->SetXY(10,$this->GetY());
        $this->SetFont('Arial','B',8);
        $this->Cell(134, 5, 'Gran total general:', 0, 0, 'L');
        $this->Cell(30, 5, number_format($granTotalPiezas).' piezas', 0, 0, 'L');
        $this->Cell(16, 5, number_format($totalMetros,2), 'TB', 0, 'R');
        $this->Cell(16, 5, number_format($totalKilos,2), 'TB', 1, 'R');
        /*
        $this->SetFont('Arial','BU',8);
        $this->SetXY(10, 45);
        $this->Cell(24, 5, $this->reportes->getRecordSalida(0,'articulo'), 0, 1, 'C');
        $granTotalPiezas= 0;
        $tMismaClave= 0;
        
        for($row=0, $size=$this->reportes->getTotalRecords(); $row<=$size; $row++){
            if(is_null($this->reportes->getRecordSalida($row,'articulo')) && is_null($this->reportes->getRecordSalida($row,'etiqueta'))){
                $this->SetY($this->GetY()+5);
                $this->SetXY(10,$this->GetY());
                $this->SetFont('Arial','B',8);
                $this->Cell(134, 5, 'Gran total general:', 0, 0, 'L');
                $this->Cell(30, 5, number_format($granTotalPiezas).' piezas', 0, 0, 'L');
                $this->Cell(16, 5, number_format($this->reportes->getRecordSalida($row,'totmts'),2), 'TB', 0, 'R');
                $this->Cell(16, 5, number_format($this->reportes->getRecordSalida($row,'totkilos'),2), 'TB', 1, 'R');
                continue;
            } elseif(is_null($this->reportes->getRecordSalida($row,'etiqueta'))){
                $this->SetY($this->GetY()+5);
                $this->SetXY(10, $this->GetY());
                $this->SetFont('Arial','B',8);
                $this->Cell(134, 5, '', 0, 0, 'R');
                $this->Cell(30, 5, $tMismaClave.' piezas', 0, 0, 'L');
                $this->Cell(16, 5, number_format($this->reportes->getRecordSalida($row,'totmts'),2), 'T', 0, 'R');
                $this->Cell(16, 5, number_format($this->reportes->getRecordSalida($row,'totkilos'),2), 'T', 1, 'R');
                $this->SetY($this->GetY()+5);
                $tMismaClave= 0;

                $this->SetFont('Arial','BU',9);
                $this->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
                $this->Cell(22, 5, 'Referencia', 0, 0, 'C');
                $this->Cell(69, 5, 'Color', 0, 0, 'C');
                $this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
                $this->Cell(14, 5, 'Etiqueta', 0, 0, 'C');
                $this->Cell(16, 5, 'Metros', 0, 0, 'C');
                $this->Cell(16, 5, 'Kilos', 0, 1, 'C');
                $this->SetY($this->GetY()+5);
                continue;
            }
            $this->SetFont('Arial','',8);
            $this->Cell(24, 5, $this->reportes->getRecordSalida($row,'articulo'), 0, 0, 'C');
            $this->Cell(22, 5, $this->reportes->getRecordSalida($row,'referencia'), 0, 0, 'C');
            $this->Cell(69, 5, $this->reportes->getRecordSalida($row,'color'), 0, 0, 'C');
            $this->Cell(35, 5, $this->reportes->getRecordSalida($row,'poliza'), 0, 0, 'C');
            $this->Cell(14, 5, $this->reportes->getRecordSalida($row,'etiqueta'), 0, 0, 'C');
            $this->Cell(16, 5, $this->reportes->getRecordSalida($row,'metros'), 0, 0, 'R');
            $this->Cell(16, 5, $this->reportes->getRecordSalida($row,'kilos'), 0, 1, 'R');

            $tMismaClave+= 1;
            $granTotalPiezas+= 1;
            
        }
        */
    }
}
$pdf = new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetMargins(10, 20, 9.9);
$pdf->tituloColumnas();
$pdf->contenidoTabla();
$filePDF = '../tmp/repSalidas';
$pdf->Output('F',$filePDF.'.pdf');
echo './tmp/repSalidas.pdf';