<?php
session_start();
class rsSettingsMenu {
	private $_conn = "";
	public $outData = "";
	public $group = "";
	
	public function __construct($conn=""){
		include_once("../rsini/conn.sql.php");
		$this->_conn = $conn;
		$this->group = $_SESSION["group"];
	}

	public function getlinksUserMenu($idUser=""){
		$sql = "SELECT uf.*, fu.menu
				FROM $this->group.USUARIOS_FUNCIONES uf, $this->group.FUNCIONES fu 
				WHERE fu.id_funcion = uf.id_funcion AND uf.ID_USUARIO = $idUser
				ORDER BY fu.menu";
		$result = db2_exec($this->_conn, $sql);
		$list = array();
		while($row = db2_fetch_object($result)){
			array_push($list, $row);
		};
		$_SESSION["usuario"]["funciones"] = $list;
		$this->outData = json_encode($list);
		$this->dbClose();
	}

	public function getlinksAdminMenu($idUser=""){
		$sql = "SELECT menu
				FROM $this->group.FUNCIONES
				WHERE mostrar = 1
				ORDER BY menu";
		$result = db2_exec($this->_conn, $sql);
		$list = array();
		while($row = db2_fetch_object($result)){
			$arr = (object)array("MENU"=>$row->MENU, "PERMISOS"=>'LEB');
			array_push($list, $arr);
		};
		$_SESSION["usuario"]["funciones"] = $list;
		$this->outData = json_encode($list);
		$this->dbClose();
	}

	private function dbClose(){
		db2_close($this->_conn);
	}
}
$links = new rsSettingsMenu();
($_SESSION["usuario"]["tipo_usuario"] == 'U')
	? $links->getlinksUserMenu($_SESSION["usuario"]["id_usuario"])
	: $links->getlinksAdminMenu($_SESSION["usuario"]["id_usuario"]);
echo $links->outData;
