<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/OrdenesCompra.class.php");
$orden_compra = new OrdenesCompra();
$dataKey = (object)$_REQUEST["data"];
//echo "event== ".($dataKey->event);exit();

switch ($dataKey->event) {
    case 'insert':
        $orden_compra->insertOrdenComp($dataKey);
        break;
    case 'edit':
        $orden_compra->editOrdenComp($dataKey);
        break;
    case 'update':
        $orden_compra->updateOrdenCompra($dataKey);
        break;
    case 'delete':
        $orden_compra->deleteOrdenCompra($dataKey);
        break;
    case 'listFunctions':
        $orden_compra->getLabelFunctions();
        break;
    case 'delImage':
        $orden_compra->deleteImage($dataKey);
        break;
    default:
        break;
}
echo json_encode($orden_compra->msgout);