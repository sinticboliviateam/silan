<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
require (dirname(__FILE__)."/lib/fpdf181/fpdf.php");
require (dirname(__FILE__)."/class/repEntradasPDF.class.php");

class PDF extends FPDF{
    protected $reportes;
    function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
        parent::__construct($orientation, $unit, $size);
        $this->reportes = new entradasPDF();
    }
            
    function Header() {
        $this->SetFont('Arial','B',16);
        $this->SetXY(10, 10);
//$fecha= "<br>".$this->reportes->getFechaIni();
//echo $fecha;exit();
        //$this->Cell(196, 10, 'Reporte de entradas del '.$this->reportes->getFechaIni(), 0, 1, 'C');
        $this->Cell(196, 10, 'Reporte de entradas', 0, 1, 'C');
        $this->SetFont('Arial','B',10);
//        $this->SetXY(10, 20);
        $this->Cell(196, 5, $this->reportes->getFechaSalida(), 0, 1, 'R');
//        $this->SetXY(10, 25);
        //$this->Cell(196, 5, 'Folio Entrada: '.$this->reportes->getFolioSalida(), 0, 1, 'L');
        $this->Cell(196, 5, 'Periodo del: '.$this->reportes->getFechaIni().' al '.$this->reportes->getFechaFin(), 0, 1, 'L');
    }
    
    function tituloColumnas() {
        $this->SetFont('Arial','BU',9);
        $this->SetXY(10, 35);
        $this->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
        $this->Cell(22, 5, 'Referencia', 0, 0, 'C');
        $this->Cell(40, 5, 'Color', 0, 0, 'C');
        $this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
        $this->Cell(14, 5, 'Etiqueta', 0, 0, 'C');
        $this->Cell(16, 5, 'Metros', 0, 0, 'C');
        $this->Cell(16, 5, 'Kilos', 0, 0, 'C');
        $this->Cell(13, 5, 'Fecha entrada',0,1,'L');
    }
    
    function contenidoTabla() {
        $this->SetFont('Arial','BU',8);
        $this->SetXY(10, 45);
        $this->Cell(24, 5, $this->reportes->getRecordEntrada(0,'articulo'), 0, 1, 'C');
        $totalRegistros= ($this->reportes->getTotalRecords())-1;
        $tMismaClave= 0;

        for($row=0, $size=$this->reportes->getTotalRecords(); $row<=$size; $row++){
            if(is_null($this->reportes->getRecordEntrada($row,'poliza')) && is_null($this->reportes->getRecordEntrada($row,'etiqueta'))){
                $this->SetY($this->GetY()-5);
                $this->SetXY(10, $this->GetY());
                $this->SetFont('Arial','B',8);
                //$this->Cell(135, 5, 'Gran total: Total general:', 0, 0, 'L');
                $this->Cell(105, 5, 'Gran total: Total general:', 0, 0, 'L');
                $this->Cell(30, 5, $totalRegistros.' piezas', 0, 0, 'L');
                $this->Cell(16, 5, number_format($this->reportes->getRecordEntrada($row,'totmts'),2), 0, 0, 'R');
                $this->Cell(16, 5, $this->reportes->getRecordEntrada($row,'totkilos'), 0, 1, 'R');
                continue;
            } elseif(is_null($this->reportes->getRecordEntrada($row,'etiqueta'))){
                $this->SetY($this->GetY()+5);
                $this->SetXY(105, $this->GetY());
                $this->SetFont('Arial','B',8);
                $this->Cell(40, 5, $tMismaClave.' piezas', 0, 0, 'L');
                $this->Cell(16, 5, $this->reportes->getRecordEntrada($row,'totmts'), 'T', 0, 'R');
                $this->Cell(16, 5, $this->reportes->getRecordEntrada($row,'totkilos'), 'T', 1, 'R');
                $this->SetY($this->GetY()+5);
                $tMismaClave= 0;
                continue;
            }
            $this->SetFont('Arial','',8);
            $this->Cell(24, 5, $this->reportes->getRecordEntrada($row,'articulo'), 0, 0, 'C');
            $this->Cell(22, 5, $this->reportes->getRecordEntrada($row,'referencia'), 0, 0, 'C');
            $this->Cell(40, 5, $this->reportes->getRecordEntrada($row,'color'), 0, 0, 'C');
            $this->Cell(35, 5, $this->reportes->getRecordEntrada($row,'poliza'), 0, 0, 'C');
            $this->Cell(14, 5, $this->reportes->getRecordEntrada($row,'etiqueta'), 0, 0, 'C');
            $this->Cell(16, 5, $this->reportes->getRecordEntrada($row,'metros'), 0, 0, 'R');
            $this->Cell(16, 5, $this->reportes->getRecordEntrada($row,'kilos'), 0, 0, 'R');
            $this->Cell(13, 5, $this->reportes->getRecordEntrada($row,'fechaEntrada'), 0, 1, 'L');
            $tMismaClave+= 1;
            
        }
    }
}
$pdf = new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetMargins(10, 20, 9.9);
$pdf->tituloColumnas();
$pdf->contenidoTabla();
$filePDF = '../tmp/repEntradas';
$pdf->Output('F',$filePDF.'.pdf');
echo './tmp/repEntradas.pdf';