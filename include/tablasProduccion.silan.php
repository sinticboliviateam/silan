<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/TablasProduccion.class.php");
$tablaProduccion = new TablasProduccion();
$option = (object)$_REQUEST["data"];
switch ($option->opt) {
    case 'formulasTablas':
        $tablaProduccion->setTablaFormulas($option->filter);
        break;
    default:
        break;
}
