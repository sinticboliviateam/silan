<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
    define('DB_HOST_SYS', '127.0.0.1');
    define('DB_PORT_SYS', '3306');
    define('DB_NAME_SYS', 'silan');
    define('DB_USER_SYS', 'admindb');
    define('DB_PASS_SYS', '[admindb]');
    
    define('DB_COOKIESESSION_SYS', false);
    define('DB_CHARSET', 'utf-8');
    define('DB_COOKIESESSIONLIFE', 180); //Vida de la Cookie de Session. (Seg)
    