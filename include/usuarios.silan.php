<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/Usuarios.class.php");
$usuario = new Usuarios();
$dataKey = (object)$_REQUEST["data"];

switch ($dataKey->event) {
    case 'insert':
        $usuario->insertUser($dataKey);
        break;
    case 'edit':
        $usuario->editUser($dataKey);
        break;
    case 'update':
        $usuario->updateUser($dataKey);
        break;
    case 'delete':
        $usuario->deleteUser($dataKey);
        break;
    case 'listFunctions':
        $usuario->getLabelFunctions();
        break;
    case 'delImage':
        $usuario->deleteImage($dataKey);
        break;
    default:
        break;
}
echo json_encode($usuario->msgout);