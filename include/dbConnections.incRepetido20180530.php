<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/

    require_once(dirname(__FILE__)."/config.inc.php");
    class Modelo
    {
        protected $_db_sys;
        private $debug = false;

        public function __construct()
        { 
            $this->_db_sys = new mysqli(DB_HOST_SYS, DB_USER_SYS, DB_PASS_SYS, DB_NAME_SYS);
            if ($this->_db_sys->connect_errno){
                echo "Fallo al conectar a MySQL: ". $this->_db_sys->connect_error; 
                return;
            }
            $this->_db_sys->set_charset(DB_CHARSET);
            $this->_db_sys->query("SET NAMES utf8");
            $this->_db_sys->query("SET CHARACTER SET utf8");
        }
        
        public function getDateFormat($date="",$month=0){
            $dateconvert = strtotime($date);
            $data = array(
                "monthNames" => array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"),
                "monthNamesShort" => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic")
            );
            $nameMonth = ($month == 0) ? $data["monthNames"][date("n",$dateconvert)-1] : $data["monthNamesShort"][date("n",$dateconvert)-1];
            return date("d",$dateconvert)." ".$nameMonth." ".date("Y",date($dateconvert));
        }

        // Función para el formato de el número de poliza
        public function formatoNumeroPoliza($no_poliza) {
            return str_pad($no_poliza, 3, '0', STR_PAD_LEFT);
        }

        public function formatoNumeroPieza($no_pieza) {
            return $no_pieza;
        }

        public function formatoAnnPoliza($ann) {
            return substr($ann, -2);
        }

        public function formatoClaveProducto($clave_producto) {
            return $clave_producto;
        }

        public function formatoMesPoliza($mes) {
            return str_pad($mes, 2, '0', STR_PAD_LEFT);
        }

        // Función para la construcción de la semilla de la clave de la póliza y la pieza
        public function semillaPoliza($clave_producto, $ann, $mes) {
            $clave_producto_formato = $this->formatoClaveProducto($clave_producto);
            $ann_formato = $this->formatoAnnPoliza($ann);
            $mes_formato = $this->formatoMesPoliza($mes);
            return $clave_producto_formato . '-' . $mes_formato . '-' . $ann_formato;
        }

        public function construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza) {
            $semilla = $this->semillaPoliza($clave_producto, $ann, $mes);
            $no_poliza_formato = $this->formatoNumeroPoliza($no_poliza);
            return $semilla . '-' . $no_poliza_formato;
        }


        public function construyeClavePieza($clave_producto, $ann, $mes, $no_poliza, $no_pieza) {
            $clavePoliza = $this->construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza);
            $no_pieza_formato = $this->formatoNumeroPieza($no_pieza);
            return $clavePoliza . '-' . $no_pieza_formato;
        }

        public function obtieneSiguientePoliza($clave_producto, $ann, $mes)
        {
            // 1234567890123456
            // 077803-08-16-003
            $siguiente = 1;

            $semilla = $this->semillaPoliza($clave_producto, $ann, $mes);
            $sql = "SELECT * FROM piezas WHERE clave_pieza LIKE '" . $semilla. "%'";
            if ($this->debug) echo '<br> sql:' . $sql;
            $result = $this->_db_sys->query($sql);
            while($row = $result->fetch_assoc()){
                if ($this->debug) echo '<br> clave_pieza DB: ' . $row['clave_pieza'];
                $partes = preg_split("/-/", $row['clave_pieza']);
                $no_poliza = intval($partes[3]);
                if ($this->debug) echo '<br> no_poliza DB: ' . $no_poliza;
                if ($no_poliza >= $siguiente) {
                    $siguiente = $no_poliza + 1;
                }
            }

            return $siguiente;
        }
        
    } 
