<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();

include (dirname(__FILE__)."/class/menu.class.php");
$menu = new menu();
($_SESSION["datauser"]["tipoUsuario"] == 'U')
    ? $menu->getlinksUserMenu($_SESSION["datauser"]["idUsuario"])
    : $menu->getlinksAdminMenu();
echo json_encode($menu->msgout);