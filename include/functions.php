<?php
/**
 * Convert decimal values for database
 * @param float float
 * @return string the formatted value using decimal sepparator (.)
 **/
function sb_float_db($float)
{
	//$res = preg_match('/[-+]?[0-9]*[\.|,]?[0-9]*/', $float, $match);
	$res = preg_match('/^\d+[\.|,]\d+$/', trim($float));
	if( $res )
	{
		return str_replace(',', '.', $float);
	}
	return $float;
}
/**
 * Check if a value is float
 **/
function sb_is_float($value)
{
	$value = trim($value);
	if( empty($value) )
		return false;
	if( strstr($value, '-') || strstr($value, ':') )
		return false;
	if( !preg_match('/[0-9]/', $value{0}) )
		return false;
	//return preg_match('/[-+]?[0-9]*[\.|,]?[0-9]*/', $value);
	//return preg_match('/[0-9]*[\.|,]?[0-9]*/', $value);
	return preg_match('/^\d+[\.|,]\d+$/', $value);
}
function sb_is_int($str)
{
	return preg_match('/^\d+$/', $str);
}
/**
 * Check if the string is a datetime
 *
 * @param string $str
 * @return boolean
 */
function sb_is_datetime($str)
{
	//return (strstr($str, '-') || strstr($str, '/')) && strstr($str, ':') && strtotime($str);
	return strtotime($str);
}
function sb_fill_zeros($str, $length = 6)
{
	$zeros = '';
	$str_length = strlen(trim($str));
	for($i = 0; $i < ($length - $str_length); $i++)
	{
		$zeros .= '0';
	}

	return $zeros . trim($str);
}
function sb_str2camelcase($str)
{
	$str = trim(preg_replace('[^a-zA-Z0-0-]', '_', trim($str)), '_');
	
	return str_replace('_', '', ucwords($str, '_'));
}
function sb_verificar_impresora($ip, $port = 80, $timeout = 5)
{
	//$port = '80';
	$errno = '';
	$errstr = '';
	
	//try
	//{
		if( !($s = @fsockopen($ip, $port, $errno, $errstr, $timeout)) )
			throw new Exception('El equipo de impresion no se encuentra activo o es inaccesible');
	//}
	//catch(Exception $e)
	//{
	//	throw new Exception('El equipo de impresion no se encuentra activo o es inaccesible');
	//}
	//$s = null;

	return true;
}
function console_log($data){
            echo '<script>';
            echo 'console.log('.json_encode($data).')';
            echo '</script>';
}
function sb_json_normalize_obj($obj)
{
	$data = array();
	$members = get_object_vars($obj);
	/*
	print_r($members);
	foreach($members as $member)
	{
		$data[$member] = $obj->$member;
	}
	*/
	$data = $members;
	
	if( method_exists($obj, 'jsonSerialize') )
	{
		$data = array_merge($data, (array)$obj->jsonSerialize());
	}
	foreach($data as $key => $value)
	{
		if( is_array($value) )
			$data[$key] = sb_json_normalize_array($value);
		elseif( is_object($value) )
			$data[$key] = sb_json_normalize_obj($value);
		else
			$data[$key] = utf8_encode($value);
	}
	return $data;
}
function sb_json_normalize_array($array)
{
	$data = array();
	foreach($array as $key => $item)
	{
		if( is_object($item) )
		{
			$data[$key] = sb_json_normalize_obj($item);
		}
		elseif( is_array($item) )
		{
			$data[$key] = sb_json_normalize_array($item);
		}
		else
		{
			$data[$key] = utf8_encode($item);
		}
	}
	return $data;
}
function sb_json_encode($obj)
{
	$res = array();
	if( is_array($obj) )
	{
		foreach($obj as $key => $item)
		{
			if( is_object($item) )
			{
				$res[$key] = sb_json_normalize_obj($item);
			}
			elseif( is_array($item) )
			{
				$res[$key] = sb_json_normalize_array($item);
			}
			else
			{
				$res[$key] = utf8_encode($item);
			}
		
		}
	}
	elseif( is_object($obj) )
	{
		$res = sb_json_normalize_obj($obj);
	}
	else 
		$res = null;
	return json_encode($res, JSON_UNESCAPED_UNICODE);
}
function sb_format_date($date, $format = null, $from_format = null)
{
	$date_format = 'Y-m-d';
	if( defined('DATE_FORMAT') )
	{
		$date_format = DATE_FORMAT;
	}
	if( $format )
	{
		$date_format = $format;
	}
	if( is_numeric($date) )
		return date("$date_format", $date);

	$date = str_replace('/', '-', $date);
	$the_date = $from_format ? DateTime::createFromFormat($from_format, $date) : new DateTime($date);
	return $the_date ? $the_date->format($date_format) : null;
}
function sb_format_time($time, $format = null)
{
	if( !is_numeric($time) )
	{
		$time = strtotime(str_replace('/', '-', trim($time)));
	}
	$time_format = 'H:i:s';
	if( defined('DATE_FORMAT') )
	{
		$date_format = DATE_FORMAT;
	}
	if( defined('TIME_FORMAT') )
	{
		$time_format = TIME_FORMAT;
	}
	if( $format )
	{
		$time_format = $format;
	}
	//$date_time = strtotime($date);

	return date("$time_format", $time);
}
function sb_format_datetime($date, $format = null)
{
	$date_format = 'Y-m-d';
	$time_format = 'H:i:s';
	if( defined('DATE_FORMAT') )
	{
		$date_format = DATE_FORMAT;
	}
	if( defined('TIME_FORMAT') )
	{
		$time_format = TIME_FORMAT;
	}
	$the_format = "$date_format $time_format";
	if( $format != null )
	{
		$the_format = $format;
	}
	$date_time = is_numeric($date) ? $date : strtotime($date);

	return date($the_format, $date_time);
}