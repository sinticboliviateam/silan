<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/TablasSistemas.class.php");
$tablaSistemas = new TablasSistemas();
$option = (object)$_REQUEST["data"];
switch ($option->opt) {
    case 'usuarios':
        $tablaSistemas->setTablaUsuarios($option->filter);
        break;
    case 'productos':
        $tablaSistemas->setTablaProductos($option->filter);
        break;
    case 'polizas':
        $tablaSistemas->tablaPolizas($option->filter);
        break;
    case 'reporteSalidas':
        $tablaSistemas->tablaSalidasFolio();
        break;
    case 'ordenesComp':
        $tablaSistemas->setTablaOrdenesComp($option->filter);
        break;
    case 'tarimas':
        $tablaSistemas->setTablaTarimas($option->filter);
        break;
    default:
        break;
}
