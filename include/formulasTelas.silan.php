<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/FormulasTelas.class.php");
$formulas = new FormulasTelas();
$dataKey = (object)$_REQUEST["data"];
switch ($dataKey->event) {
    case "getdata":
        $formulas->getdata($dataKey);
        break;
    case 'insertDatos':
        $formulas->insertaDatos($dataKey);
        break;
    case 'editFormula':
        $formulas->editFormula($dataKey);
        break;
    case 'updateTecnicos':
        $formulas->updateTecnicos($dataKey);
        break;
    case 'delete':
        break;
    case 'updateFormula':
        $formulas->updateTecnicos($dataKey);
        $formulas->actualizaTablasTab($dataKey,"urdimbre");
        $formulas->actualizaTablasTab($dataKey,"trama");
        $formulas->actualizaTablasTab($dataKey,"orillo");
        $formulas->msgout = array("error"=>0, "msg"=>"", "action"=>"null", "title"=>"");
        break;
    case 'delImage':
        break;
    default:
        break;
}
echo json_encode($formulas->msgout);