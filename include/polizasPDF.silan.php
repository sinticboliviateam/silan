<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
require (dirname(__FILE__)."/lib/fpdf181/fpdf.php");
class PDF extends FPDF{
    function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
        parent::__construct($orientation, $unit, $size);
        include(dirname(__FILE__)."/class/PolizasPDF.class.php");
        $this->datosPoliza = new PolizasPDF();
    }

    function Header() {
        $this->SetFont('Arial','B',10);
        $this->SetXY(30, 15);
        $this->Cell(120, 10, str_pad("ARTICULO",12,' ',STR_PAD_RIGHT).$this->datosPoliza->getClaveArticulo(), 0, 0, 'L');
        $this->Cell(40,5,"Fecha",0,2,'C');
        $this->SetFont('Arial','');
        $this->Cell(40,5,$this->datosPoliza->getFechaNumerica(),0,1,'C');
        $this->SetX(30);
        $this->SetFont('Arial','B',10);
        $this->Cell(120,10,str_pad("POLIZA",10,' ',STR_PAD_RIGHT).$this->datosPoliza->getnumeroPoliza(), 0, 0, 'L');
    }
    
    function tituloColumnas() {
        $this->SetFont('Arial','B',10);
        $this->SetXY(15, 35);
        $this->Cell(63.9, 6, 'URDIDO', 0, 0, 'C');
        $this->Cell(61, 6, 'CURSO DE URDIDO', 0, 0, 'C');
        $this->Cell(61, 6, 'CURSO DE TRAMA', 0, 1, 'C');
    }
    
    function columnaUrdido() {
        $this->datosPoliza->setCalculaNoFajas();
        $this->datosPoliza->setCalculaAnchoFajas();
        $this->datosPoliza->setTablaUrdido();
        $this->SetFont('Arial','',9);
        $this->SetXY(15, 43);
        $this->Cell(34.9, 5, 'HILOS DE FONDO', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getHilosFondo(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'HILOS DE ORILLOS', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getHilosOrillos(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'HILOS TOTALES', 'B', 0);
        $this->Cell(29, 5, $this->datosPoliza->getHilosTotales(), 'B', 1, 'R');
        $this->Cell(34.9, 5, 'ANCHO DE TELA', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getAnchoTela(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'No. CABOS', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getnumeroCabos(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'No. FAJAS', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getnumeroFajas(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'ANCHO DE FAJAS', 'B', 0);
        $this->Cell(29, 5, $this->datosPoliza->getAnchoFajas(), 'B', 1, 'R');
        $this->SetFont('Arial','B',9);
        $this->Cell(63.9, 8, 'HILOS POR CURSO DE URDIMBRE', 0, 1, 'C');
        $this->SetFont('Arial','',9);
        for($hilo=0; $hilo<=14; $hilo++){
            $this->Cell(34.9, 5, $this->datosPoliza->getDatoHiloUrdido($hilo,'letra'), 0, 0);
            $this->Cell(29, 5, $this->datosPoliza->getDatoHiloUrdido($hilo,'total'), 0, 1, 'R');
        }
        $this->Cell(34.9, 5, 'HILOS TOTALES', 'B', 0);
        $this->Cell(29, 5, $this->datosPoliza->getTotalUrdido(), 'B', 1, 'R');
        $this->Cell(63.9, 8, $this->datosPoliza->getLeyendaUrdido(), 0, 1);
        $this->SetFont('Arial','B',9);
        $this->Cell(63.9, 10, 'TEJIDO', 0, 1, 'C');
        $this->SetFont('Arial','',9);
        $this->Cell(34.9, 5, 'PASADAS EN 2"', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getPasadas2(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'PEINE', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getnumeroRepaso(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'REPASO EN PEINE', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getRepasoPeine(), 0, 1, 'R');
        $this->Cell(34.9, 5, 'DIBUJO', 0, 0);
        $this->Cell(29, 5, $this->datosPoliza->getDibujo(), 0, 1, 'R');
    }
    
    function columnaCursoUrdido() {
        $this->SetFont('Arial','',9);
        $this->SetXY(78.9, 43);
        for($j=0; $j<=$this->datosPoliza->getTotalCursoUrdimbre(); $j++){
            $dato = $this->datosPoliza->getDatoCursoUrdimbre($j);
            $this->Cell(20, 5, $dato, 0, 0, 'R');
            $this->Cell(41, 5, $this->datosPoliza->getNombreColorUrdimbre($dato), 0, 0, 'L');
            $this->SetXY(78.9,$this->GetY()+5);
        }
        $this->Cell(20, 5, '----------', 0, 0, 'R');
        $this->SetXY(78.9,$this->GetY()+5);
        $this->Cell(20, 5, $this->datosPoliza->getTotalUrdido(), 0, 1, 'R');
    }
    
    function columnaCursoTrama() {
        $this->SetFont('Arial','',9);
        $this->SetXY(139.9, 43);
        for($j=0; $j<=$this->datosPoliza->getTotalCursoTrama(); $j++){
            $dato = $this->datosPoliza->getDatoCursoTrama($j);
            $this->Cell(20, 5, $dato, 0, 0, 'R');
            $this->Cell(41, 5, $this->datosPoliza->getNombreColorTrama($dato), 0, 0, 'L');
            $this->SetXY(139.9,$this->GetY()+5);
        }
        $this->Cell(20, 5, '----------', 0, 0, 'R');
        $this->SetXY(139.9,$this->GetY()+5);
        $this->Cell(20, 5, $this->datosPoliza->getTotalTrama(), 0, 1, 'R');
    }
}
$pdf = new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetMargins(15, 20, 20);
$pdf->tituloColumnas();
$pdf->columnaUrdido();
$pdf->columnaCursoUrdido();
$pdf->columnaCursoTrama();
$filePDF = '../tmp/temp';
$pdf->Output('F',$filePDF.'.pdf');
echo './tmp/temp.pdf';