<?php
$user = $_REQUEST["user"];
$password = $_REQUEST["password"];
require_once dirname(__FILE__) . '/conn.php';
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$tokens = explode('/', $url);
$group = strtoupper($tokens[sizeof($tokens)-2]);
$msg = "";
if( !verifyUser($user,$password, $group, $conn ) )
{
	http_response_code(401);
	$msg = "ACCESO NO AUTORIZADO";
}else{
	session_start();
	$_SESSION["group"] = getGroup();
	$_SESSION["sensores"] = getSensores($group,$conn);
}
$json = array('msg'=>$msg);
echo json_encode($json);
