<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/Tacome.class.php");
$tacome = new Tacome();
$keyTacome = (object)$_REQUEST["data"];

switch ($keyTacome->evento) {
    case 'usuarioRegistro':
        $tacome->msgout = array(
            "fechaRegistro" => $tacome->fechaActual_conFormato(),
            "usuarioRegistro" => $tacome->nombreUsuario()
        );
        break;
    case 'nuevoRegistro':
        $tacome->datosNuevoRegistro($keyTacome->keyproduct);
        break;
    case "insertaTacome":
        $tacome->insertaTacome($keyTacome);
        break;
    default:
        print_r($keyPolizas);
        break;
}
echo json_encode($tacome->msgout);