<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/dbConnections.inc.php");
$phpcomm = new Modelo();
$date = new DateTime(); //this returns the current date time
$result = $date->format('Y-m-d H:i:s');
$strdate = $phpcomm->getDateFormat($result, 1);
$_SESSION["datauser"]["fechaEstatus"] = $result;
echo json_encode(array("date"=>$strdate, "name"=>utf8_encode($_SESSION["datauser"]["nombreCompleto"])));
        