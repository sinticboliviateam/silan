<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
if (isset($_FILES["file"])){
    $file = $_FILES["file"];
    $nombre = $file["name"];
    $tipo = $file["type"];
    $ruta_provisional = $file["tmp_name"];
    $size = $file["size"];
    $dimensiones = getimagesize($ruta_provisional);
    $width = $dimensiones[0];
    $height = $dimensiones[1];
    $carpeta = dirname(__FILE__)."/../".$_SESSION["datauser"]["rutaImagen"];
    if($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png'){
        echo "Error, el archivo no es una imagen";
    } elseif ($size > 1024*1024) {
        echo "Error, el tamaño máximo permitido es un 1MB";
        return false;
    } else {
        $src = $carpeta.$nombre;
        move_uploaded_file($ruta_provisional, $src);
        echo(".".$_SESSION["datauser"]["rutaImagen"].$nombre);
    }
    $_SESSION["record"]["imagen"] = $nombre;

//    else if ($width > 500 || $height > 500)
//    {
//        echo "Error la anchura y la altura maxima permitida es 500px";
//    }
//    else if($width < 60 || $height < 60)
//    {
//        echo "Error la anchura y la altura mínima permitida es 60px";
//    }
}