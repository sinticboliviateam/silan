<?php
/* 
 * 20180810 por Manuel Luna, Juan Marcelo Áviles
 * v1.0
 * 
*/
session_start();
include (dirname(__FILE__)."/class/Productos.class.php");
$producto = new Productos();
$dataKey = (object)$_REQUEST["data"];

switch ($dataKey->event) {
    case 'insert':
        $producto->insertProd($dataKey);
        break;
    case 'edit':
        $producto->editProd($dataKey);
        break;
    case 'update':
        $producto->updateProd($dataKey);
        break;
    case 'delete':
        $producto->deleteProd($dataKey);
        break;
    case 'listFunctions':
        $producto->getLabelFunctions();
        break;
    case 'delImage':
        $producto->deleteImage($dataKey);
        break;
    default:
        break;
}
echo json_encode($producto->msgout);