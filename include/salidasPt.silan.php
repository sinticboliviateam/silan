<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/SalidasPt.class.php");
$salidas = new SalidasPt();
$keySalidas = (object)$_REQUEST["data"];
switch ($keySalidas->evento) {
    case 'usuarioRegistro':
        $salidas->msgout = array(
            "fechaRegistro" => $salidas->fechaActual_conFormato(),
            "usuarioRegistro" => $salidas->nombreUsuario()
        );
        break;
    case 'listaPiezasSinSalida':
        $salidas->listaPiezasSinSalida();
        break;
    case 'validacionExisteCliente':
        $salidas->validacionExisteCliente($keySalidas);
        break;
    case 'salidaPiezas':
        $salidas->salidaPiezas($keySalidas);
        break;
    default:
        print_r($keyEntradas);
        break;
}
echo json_encode($salidas->msgout);