<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class TablasSistemas extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function setTablaUsuarios($filter="") {
        $rows = "";
        $sql = "SELECT usu.idUsuario, usu.nombreCompleto, usu.usuario, usu.rfc, usu.puesto, 
                est.nombre nombreEstatus, DATE_FORMAT(usu.ultimoAcceso, '%d-%m-%Y %H:%i:%s') ultimoAcceso
                FROM ".DB_NAME_SYS.".usuarios usu INNER JOIN ".DB_NAME_SYS.".estatus est ON est.idEstatus = usu.idEstatus
                WHERE usu.nombreCompleto LIKE '%".$filter."%' OR
                      usu.usuario LIKE '%".$filter."%' OR
                      usu.rfc LIKE '%".$filter."%' OR
                      usu.puesto LIKE '%".$filter."%' OR
                      usu.ultimoAcceso LIKE '%".$filter."%'
                ORDER BY nombreCompleto";
        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idUsuario"].">";
            $rows.= "<td class='td-col06-col01'>".utf8_encode($record["nombreCompleto"]);
            $rows.= "<td class='td-col06-col02'>".utf8_encode($record["usuario"]);
            $rows.= "<td class='td-col06-col03'>".mb_strtoupper($record["rfc"], 'UTF-8');
            $rows.= "<td class='td-col06-col04'>".utf8_encode($record["puesto"]);
            $rows.= "<td class='td-col06-col05'>".utf8_encode($record["nombreEstatus"]);
            $rows.= "<td class='td-col06-col06'>".$record["ultimoAcceso"];
            $rows.= "</td></tr>";
        }
        echo $rows;
    }

    public function setTablaProductos($filter="") {
        $rows = "";
        $sql = "SELECT prod.id_artref, prod.clave_prod, prod.claveAnterior, prod.sku, prod.nombreProducto, prod.color,
                        prod.marca, est.nombre nombreEstatus
                FROM ".DB_NAME_SYS.".artref prod INNER JOIN ".DB_NAME_SYS.".estatus est ON est.idEstatus = prod.idEstatus
                WHERE prod.clave_prod LIKE '%".$filter."%' OR
                      prod.claveAnterior LIKE '%".$filter."%' OR
                      prod.sku LIKE '%".$filter."%' OR
                      prod.nombreProducto LIKE '%".$filter."%' OR
                      prod.color LIKE '%".$filter."%' OR
                      prod.marca LIKE '%".$filter."%' OR
                      est.nombre LIKE '%".$filter."%'
                ORDER BY prod.clave_prod;";

        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["id_artref"].">";
            $rows.= "<td class='td-col06-col01'>".utf8_encode($record["clave_prod"]);
            $rows.= "<td class='td-col06-col02'>".utf8_encode($record["claveAnterior"]);
            $rows.= "<td class='td-col06-col03'>".utf8_encode($record["sku"]);
            $rows.= "<td class='td-col06-col04'>".utf8_encode($record["nombreProducto"]);
            $rows.= "<td class='td-col06-col05'>".utf8_encode($record["color"]);
            $rows.= "<td class='td-col06-col06'>".utf8_encode($record["marca"]);
            $rows.= "<td class='td-col06-col07'>".utf8_encode($record["nombreEstatus"]);
            $rows.= "</td></tr>";
        }
        echo $rows;
    }

    public function setTablaOrdenesComp($filter="") { //20180430, ml
        $rows = "";
//echo "filter= $filter";exit();
        $sql1 = "SELECT usu.idUsuario, usu.nombreCompleto, usu.usuario, usu.rfc, usu.puesto, 
                est.nombre nombreEstatus, DATE_FORMAT(usu.ultimoAcceso, '%d-%m-%Y %H:%i:%s') ultimoAcceso
                FROM ".DB_NAME_SYS.".usuarios usu INNER JOIN ".DB_NAME_SYS.".estatus est ON est.idEstatus = usu.idEstatus
                WHERE usu.nombreCompleto LIKE '%".$filter."%' OR
                      usu.usuario LIKE '%".$filter."%' OR
                      usu.rfc LIKE '%".$filter."%' OR
                      usu.puesto LIKE '%".$filter."%' OR
                      usu.ultimoAcceso LIKE '%".$filter."%'
                ORDER BY nombreCompleto";

        $sql = "SELECT oc.idOrdenCompra, oc.ordenCompra, oc.fechaRegistro, oc.fechaEntrega, oc.archivotxt
                FROM silan.ordenescompra oc
                WHERE oc.ordenCompra LIKE '%{$filter}%' 
                or oc.fechaRegistro LIKE '%{$filter}%'
                or oc.archivotxt LIKE '%{$filter}%'
                ORDER BY oc.fechaRegistro desc, oc.ordenCompra desc";

        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idOrdenCompra"].">";
            
            $rows.= "<td class='td-col06-col01'>".$record["ordenCompra"] . '</td>';
            $rows.= "<td class='td-col06-col02'>".$record["fechaRegistro"] . '</td>';
            $rows.= "<td class='td-col06-col04'>".$record['fechaEntrega'] . '</td>';
            $rows.= "<td class='td-col06-col01'>".$record["archivotxt"] . '</td>';
            $rows.= "</tr>";
        }
        echo $rows;
    }

    public function setTablaTarimas($filter="") { //20180430, ml
        
        $rows = "";

        $sql = "SELECT t.idtarima, t.noTarima, t.fechaSalida, oc.ordenCompra, t.fechaSalida, 
                (select count(*) from piezas p where p.idTarima =t.idTarima) piezas,
                (select sum(metros) mts from piezas where idTarima =t.idTarima) metros, e.nombre estatus,
                (select IF( COUNT(DISTINCT(substring_index(p.clavepieza,'-',1))) > 1 ,'Multiple',substring_index(p.clavepieza,'-',1))) SKU
                FROM silan.tarimas t
                LEFT JOIN estatus e ON e.idEstatus = t.idEstatus
                LEFT JOIN piezas p ON p.idTarima = t.idTarima
                LEFT JOIN ordenescompra oc ON oc.idOrdenCompra = t.idOrdenCompra
                ".$filter." ORDER BY t.noTarima desc";

        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idTarima"].">";
            $rows.= "<td class='td-col06-col01'>".$record["noTarima"];
            $rows.= "<td class='td-col06-col01'>".$record["ordenCompra"];
            $rows.= "<td class='td-col06-col02'>".$record["fechaSalida"];
            $rows.= "<td class='td-col06-col02'>".$record["piezas"];
            $rows.= "<td class='td-col06-col02'>".$record["metros"];
            $rows.= "<td class='td-col06-col02'>".$record["estatus"];
            $rows.= "<td class='td-col06-col04'>".utf8_encode($record["SKU"]);
            //$rows.= "<td class='td-col06-col04'>".utf8_encode($record["nombre"]);
            $rows.= "</td></tr>";
        }
        echo $rows;
    }

    public function tablaPolizas($filter="") {
        $sql = "SELECT pol.idPoliza, pol.noPoliza, pol.fechaFiltro, pol.noPiezas,
                    (SELECT COUNT(idPiezas) FROM ".DB_NAME_SYS.".piezas pza 
                     WHERE pza.idPoliza = pol.idPoliza AND pza.fechaEntrada IS NOT NULL ORDER BY pza.idPoliza) terminadas, sta.nombre
                FROM ".DB_NAME_SYS.".polizas pol, ".DB_NAME_SYS.".estatus sta
                WHERE sta.idEstatus = pol.idEstatus AND
                    (pol.noPoliza LIKE '%".$filter."%' OR
                     pol.fechaFiltro LIKE '%".$filter."%' OR
                     pol.noPoliza LIKE '%".$filter."%' OR
                     pol.noPiezas LIKE '%".$filter."%' OR
                     (SELECT COUNT(idPiezas) FROM ".DB_NAME_SYS.".piezas pza 
                        WHERE pza.idPoliza = pol.idPoliza AND pza.fechaEntrada IS NOT NULL ORDER BY pza.idPoliza) LIKE '%".$filter."%' OR
                     sta.nombre LIKE '%".$filter."%')
                ORDER BY pol.idPoliza DESC";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        //
            $totalPolizas= 0;
            $totalPiezas= 0;
            $totalTerminadas= 0;
        //
        $rows = "";
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idPoliza"].">";
            $rows.= "   <td class='td-col05-col01-p'>".$record["noPoliza"]."</td>";
            $rows.= "   <td class='td-col05-col02-p'>".$record["fechaFiltro"]."</td>";
            $rows.= "   <td class='td-col05-col03-p'>".$record["noPiezas"]."</td>";
            $rows.= "   <td class='td-col05-col04-p'>".$record["terminadas"]."</td>";
            $rows.= "   <td class='td-col05-col05-p'>".$record["nombre"]."</td>";
            $rows.= "</tr>";
            //
                $totalPolizas+= 1;
                $totalPiezas+= (int)$record["noPiezas"];
                $totalTerminadas+= (int)$record["terminadas"];
            //
        }
        /*/
            $rows.= "</tbody>";
            $rows.= "<tfoot class='tableHead'>";
            $rows.= "    <th style='color:#000;'>".$totalPolizas."</th>";
            $rows.= "    <td></td>";
            $rows.= "    <th style='color:#000;'>".number_format($totalPiezas)."</th>";
            $rows.= "    <th style='color:#000;'>".number_format($totalTerminadas)."</th>";
            $rows.= "    <th style='color:#000;'>".number_format($totalPiezas-$totalTerminadas)."</th>";
            $rows.= "</tfoot>";
        /*/
        echo $rows;
    }
    
    public function tablaSalidasFolio() {
        $rows = "";
        $sql = "SELECT DATE_FORMAT(fechaSalida, '%Y-%m-%d') fecha, documentoSalida, noCliente, COUNT(documentoSalida) total 
                FROM ".DB_NAME_SYS.".piezas 
                WHERE documentoSalida > 0 GROUP BY documentoSalida ORDER BY fechaSalida DESC";
        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["documentoSalida"].">";
            $rows.= "<td class='td-col04-col01'>".  $this->getDateFormat($record["fecha"], 0, false);
            $rows.= "<td class='td-col04-col02'>".$record["documentoSalida"];
            $rows.= "<td class='td-col04-col03'>".$record["noCliente"];
            $rows.= "<td class='td-col04-col04'>".$record["total"];
            $rows.= "</td></tr>";
        }
        echo $rows;
    }
}