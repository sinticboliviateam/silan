<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once (dirname(__FILE__)."/PolizasPDF.class.php");
class ValePolizasPDF extends PolizasPDF{
    const pulgada = 2.54;

    private $valePoliza;
    private $tituloMetrico;
    public $arrVale;

    public function __construct(){
        parent::__construct();
        $this->setsqlGrupos();
        $this->setKilosTotalesUrdimbre();
        $this->setKilosTotalesTrama();
    }
    
    private function setsqlGrupos() {
        $sql = "SELECT CASE calibre
                        WHEN '2/30' THEN 15000
                        WHEN '2/44' THEN 22000
                       END tituloMetrico
                FROM ".DB_NAME_SYS.".grupos WHERE codigo = SUBSTRING('".$this->getClaveProducto()."',-4,1) LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $rows = $result->fetch_object();
        $this->tituloMetrico = $rows->tituloMetrico;
    }
    
    private function setKilosTotalesUrdimbre() {
        $kgUrdimbre = $this->getHilosTotales() / $this->getTituloMetrico();
        $this->valePoliza->kilosUrdimbre = $this->getTotalMetro() * $kgUrdimbre;
    }
    
    private function setKilosTotalesTrama() {
        $pulgadasTrama = ($this->getPasadas2()/$this->getPulgadas(2))*100;
        $kgTrama = ((($pulgadasTrama*$this->getAnchoTela())/100)/$this->getTituloMetrico())*$this->getTotalMetro();
        $this->valePoliza->kilosTrama = $kgTrama;
    }
    
    public function creaValePolizaUrdido() {
        for($i=0, $size=$this->getTotalPorcentajeUrdido(); $i<=$size-1; $i++){
            $this->arrVale[$i]->claveHilo = $this->getClaveHilo("urdido",$i);
            $this->arrVale[$i]->colorHilo = $this->getColorHilo("urdido",$i);
            $this->arrVale[$i]->composicion = $this->getComposicionHilo("urdido",$i);
            $this->arrVale[$i]->titulo = $this->getTituloFraccion("urdido",$i);
            $this->arrVale[$i]->kgUrdimbre = $this->getKiloHilosUrdimbre($i);
            $this->arrVale[$i]->kgTrama = $this->getKiloHilosTrama(-1);
            $this->arrVale[$i]->kgTotalHilo = $this->arrVale[$i]->kgUrdimbre + $this->arrVale[$i]->kgTrama;
        }
    }
    
    public function creaValePolizaTrama() {
        $arr="";
        for($i=0, $size=$this->getTotalPorcentajeTrama(); $i<=$size-1; $i++){
            $arr[$i]->claveHilo = $this->getClaveHilo("trama",$i);
            $arr[$i]->colorHilo = $this->getColorHilo("trama",$i);
            $arr[$i]->composicion = $this->getComposicionHilo("trama",$i);
            $arr[$i]->titulo = $this->getTituloFraccion("trama",$i);
            $arr[$i]->kgUrdimbre = $this->getKiloHilosUrdimbre(-1);
            $arr[$i]->kgTrama = $this->getKiloHilosTrama($i);
            $arr[$i]->kgTotalHilo = $arr[$i]->kgUrdimbre + $arr[$i]->kgTrama;
        }
        $this->setValesPoliza($arr);
    }
    
    private function setValesPoliza($arr) {
        $arrTemp = $arr;
        foreach ($this->arrVale as $dato) {
            foreach ($arrTemp as $idx => $datoTemp) {
                if(strcmp($dato->claveHilo, $datoTemp->claveHilo) === 0){
                    $dato->kgTrama = $datoTemp->kgTrama;
                    $dato->kgTotalHilo+= $datoTemp->kgTrama;
                    unset($arr[$idx]);
                    break;
                }
            }
        }
        if(count($arr) >= 1){
            foreach ($arr as $dato){ 
                $this->arrVale[] = $dato;
            }
        }
    }
    
    private function getTituloMetrico() {
        return $this->tituloMetrico;
    }
    
    private function getPulgadas($numero) {
        return self::pulgada*$numero;
    }
    
    private function getKilosTotalesUrdimbre() {
        return $this->valePoliza->kilosUrdimbre;
    }
    
    private function getKilosTotalesTrama() {
        return $this->valePoliza->kilosTrama;
    }
    
    private function getClaveHilo($tipo,$idx) {
        return ($tipo == "urdido")
            ? $this->formulas->porcentajeUrdido[$idx]->claveHilo
            : $this->formulas->porcentajeTrama[$idx]->claveHilo;
    }
    
    private function getColorHilo($tipo,$idx) {
        return ($tipo == "urdido")
            ? $this->formulas->porcentajeUrdido[$idx]->color
            : $this->formulas->porcentajeTrama[$idx]->color;
    }
    
    private function getComposicionHilo($tipo,$idx) {
        return ($tipo == "urdido")
            ? $this->formulas->porcentajeUrdido[$idx]->composicion
            : $this->formulas->porcentajeTrama[$idx]->composicion;
    }
    
    private function getTituloFraccion($tipo,$idx) {
        if($tipo == "urdido"){
            return (property_exists($this->formulas->porcentajeUrdido[$idx], "calibre") ? $this->formulas->porcentajeUrdido[$idx]->calibre : "");
        } else {
            return (property_exists($this->formulas->porcentajeTrama[$idx], "calibre") ? $this->formulas->porcentajeTrama[$idx]->calibre : "");
        }
    }
    
    private function getKiloHilosUrdimbre($idx=-1) {
        return ($idx == -1)
             ? 0
             : ($this->formulas->porcentajeUrdido[$idx]->porcentaje * $this->getKilosTotalesUrdimbre())/100;
    }
    
    private function getKiloHilosTrama($idx=-1) {
        return ($idx == -1)
             ? 0
             : ($this->formulas->porcentajeTrama[$idx]->porcentaje * $this->getKilosTotalesTrama())/100;
    }
    
    public function getTotalHilosVale() {
        return count($this->arrVale);
    }
    
    public function getTotalPorcentajeUrdido() {
        return count($this->formulas->porcentajeUrdido);
    }
    
    public function getTotalPorcentajeTrama() {
        return count($this->formulas->porcentajeTrama);
    }
    
}