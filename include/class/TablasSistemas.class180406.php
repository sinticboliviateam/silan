<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class TablasSistemas extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function setTablaUsuarios($filter="") {
        $sql = "SELECT usu.idUsuario, usu.nombreCompleto, usu.usuario, usu.rfc, usu.puesto, 
                est.nombre nombreEstatus, DATE_FORMAT(usu.ultimoAcceso, '%d-%m-%Y %H:%i:%s') ultimoAcceso
                FROM ".DB_NAME_SYS.".usuarios usu INNER JOIN ".DB_NAME_SYS.".estatus est ON est.idEstatus = usu.idEstatus
                WHERE usu.nombreCompleto LIKE '%".$filter."%' OR
                      usu.usuario LIKE '%".$filter."%' OR
                      usu.rfc LIKE '%".$filter."%' OR
                      usu.puesto LIKE '%".$filter."%' OR
                      usu.ultimoAcceso LIKE '%".$filter."%'
                ORDER BY nombreCompleto";
        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idUsuario"].">";
            $rows.= "<td class='td-col06-col01'>".utf8_encode($record["nombreCompleto"]);
            $rows.= "<td class='td-col06-col02'>".utf8_encode($record["usuario"]);
            $rows.= "<td class='td-col06-col03'>".mb_strtoupper($record["rfc"], 'UTF-8');
            $rows.= "<td class='td-col06-col04'>".utf8_encode($record["puesto"]);
            $rows.= "<td class='td-col06-col05'>".utf8_encode($record["nombreEstatus"]);
            $rows.= "<td class='td-col06-col06'>".$record["ultimoAcceso"];
            $rows.= "</td></tr>";
        }
        echo $rows;
        $this->dbClose($result);
    }
    
    public function tablaPolizas($filter="") {
        $sql = "SELECT pol.idPoliza, pol.noPoliza, pol.fechaFiltro, pol.noPiezas,
                    (SELECT COUNT(idPiezas) FROM ".DB_NAME_SYS.".piezas pza 
                     WHERE pza.idPoliza = pol.idPoliza AND pza.fechaEntrada IS NOT NULL ORDER BY pza.idPoliza) terminadas, sta.nombre
                FROM ".DB_NAME_SYS.".polizas pol, ".DB_NAME_SYS.".estatus sta
                WHERE sta.idEstatus = pol.idEstatus AND
                    (pol.noPoliza LIKE '%".$filter."%' OR
                     pol.fechaFiltro LIKE '%".$filter."%' OR
                     pol.noPoliza LIKE '%".$filter."%' OR
                     pol.noPiezas LIKE '%".$filter."%' OR
                     (SELECT COUNT(idPiezas) FROM ".DB_NAME_SYS.".piezas pza 
                        WHERE pza.idPoliza = pol.idPoliza AND pza.fechaEntrada IS NOT NULL ORDER BY pza.idPoliza) LIKE '%".$filter."%' OR
                     sta.nombre LIKE '%".$filter."%')
                ORDER BY pol.idPoliza ";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idPoliza"].">";
            $rows.= "<td class='td-col05-col01-p'>".$record["noPoliza"];
            $rows.= "<td class='td-col05-col02-p'>".$record["fechaFiltro"];
            $rows.= "<td class='td-col05-col03-p'>".$record["noPiezas"];
            $rows.= "<td class='td-col05-col04-p'>".$record["terminadas"];
            $rows.= "<td class='td-col05-col05-p'>".$record["nombre"];
            $rows.= "</td></tr>";
        }
        echo $rows;
        $this->dbClose($result);
    }
    
    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}