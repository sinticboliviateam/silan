<?php
/* 
 * 20180810 por Manuel Luna, Juan Marcelo Áviles
 * v1.0
 * 
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class Productos extends Modelo
{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    

//*** INSERTA LOS DATOS DEL USUARIO
    private function preparaDatosInsert($data=array()) {
        $fechaEstatus = date("Y-m-d h:i:s");
        $values = "(NULL,".
                    "'".utf8_decode(mb_strtolower($data->usuario, 'UTF-8'))."',".
                    "'".utf8_decode(mb_strtolower($data->password,'UTF-8'))."',".
                    "'".utf8_decode($data->nombreCompleto)."',".
                    "'".$data->rfc."',".
                    "'".utf8_decode($data->puesto)."',".
                    "'".utf8_decode($data->telefonos)."',".
                    "'".utf8_decode($data->celulares)."',".
                    "'".utf8_decode($data->correos)."',".
                    $data->estatus.",".
                    "'".$_SESSION["datauser"]["nombreCompleto"]."',".
                    "'".$fechaEstatus."',".
                    "'','',". //** campos: ultimoAcceso, IPP
                    "'".$data->horario."',".
                    "'".$_SESSION["datauser"]["imagen"]."',".
                    "'".$data->tipoUsuario."')";
        return $values;
    }
    
    public function insertProd($data=array()) 
    {
//echo "antecitos de insertar"; exit();
        $values = $this->preparaDatosInsert($data);
        $sql = "INSERT INTO ".DB_NAME_SYS.".usuarios VALUES".$values;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $iduser = $this->_db_sys->insert_id;
        if(!$result){
            $this->msgout = array('error'=>3002, 'msg'=>"ERROR 3002. No se registró el producto.", "action"=>"reload", "title"=>"Catálogo de Usuarios");
            unset($_SESSION["datauser"]["imagen"]);
            $this->dbClose($result);
            return;
        }
        if(!empty($data->functions))
            $this->updateFunctionsUser($iduser, $data->functions);
        $this->msgout = array('error'=>0, 'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"Catálogo de Usuarios");
        unset($_SESSION["datauser"]["imagen"]);
        $this->dbClose($result);
    }
//*** EDITA LOS DATOS DEL PRODUCTO
    public function editProd($data=array()) {
        $sql = "SELECT * FROM ".DB_NAME_SYS.".artref WHERE id_artref = ".$data->key." LIMIT 1";
        $this->log($sql);
//echo "dentro de editProd <br/>";
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Error en dBase: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>3003, "msg"=>"ERROR 3003. $msg", "action"=>"reload", "title"=>"Catálogo de Usuarios");
            $this->dbClose($result);
            return false;
        }
        while($row = $result->fetch_assoc()){
            $fechaEstatus = $this->getDateFormat($row["fechaEstatus"], 1);
            $fechaRegistro = $this->getDateFormat($row["fechaRegistro"], 1);

            $dataresult["id_artref"]        = $row["id_artref"];
            $dataresult["clave_prod"]       = utf8_encode($row["clave_prod"]);
            $dataresult["claveAnterior"]    = utf8_encode($row["claveAnterior"]);
            $dataresult["sku"]              = utf8_encode($row["sku"]);
            $dataresult["nombreProducto"]   = utf8_encode($row["nombreProducto"]);
            $dataresult["nombreMercado"]    = utf8_encode($row["nombreMercado"]);
            $dataresult["color"]            = utf8_encode($row["color"]);
            $dataresult["referenciaColor"]  = $row["referenciaColor"];
            $dataresult["marca"]            = $row["marca"];
            $dataresult["ancho"]            = $row["ancho"];
            $dataresult["ruta"]             = $row["ruta"];
            $dataresult["composicion01"]    = utf8_encode($row["composicion01"]);
            $dataresult["composicion02"]    = utf8_encode($row["composicion02"]);
            $dataresult["fechaRegistro"]    = $fechaRegistro;
            $dataresult["idEstatus"]        = $row["idEstatus"];
            $dataresult["fechaEstatus"]     = $fechaEstatus;
            $dataresult["usuarioEstatus"]   = utf8_encode($row["usuarioEstatus"]);
        }
 //var_dump($dataresult);
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "result"=>$dataresult);
        $this->dbClose($result);
    }
    
    private function preparaDatosUpdate($data=array()) {
        $values = "usuario = '".utf8_decode(mb_strtolower($data->usuario, 'UTF-8'))."',".
                  "password = '".utf8_decode(mb_strtolower($data->password,'UTF-8'))."',".
                  "nombreCompleto = '".utf8_decode($data->nombreCompleto)."',".
                  "rfc = '".utf8_decode($data->rfc)."',".
                  "puesto = '".utf8_decode($data->puesto)."',".
                  "telefonos = '".$data->telefonos."',".
                  "celulares = '".$data->celulares."',".
                  "correos = '".$data->correos."',".
                  "idEstatus = ".$data->estatus.",".
                  "usuarioEstatus = '".utf8_decode($data->usuarioEstatus)."',".
                  "fechaEstatus = '".$_SESSION["datauser"]["fechaEstatus"]."',".
                  "horario = '".$data->horario."',".
                  "imagen = '".$_SESSION["datauser"]["imagen"]."',".
                  "tipoUsuario = '".$data->tipoUsuario."'";
        return $values;
    }
    public function updateProd($data=array()) {
        $values = $this->preparaDatosUpdate($data);
        $sql = "UPDATE ".DB_NAME_SYS.".usuarios SET ".$values." WHERE idUsuario = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if(!empty($data->functions))
            $this->updateFunctionsUser($data->key, $data->functions);
        $this->msgout = array("error"=>0);
    }
//*** BORRA LA IMAGEN DEL USUARIO
    public function deleteImage($data=array()) {
        $sql = "UPDATE ".DB_NAME_SYS.".usuarios SET imagen = '' WHERE idUsuario = ".$data->key." LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"");
    }
    
    public function deleteProd($data=array()){
        $idUser= $data->key;
        $sql = "DELETE FROM ".DB_NAME_SYS.".usuarios  WHERE idUsuario = ".$idUser." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        // Elimina las funciones relacionadas 
        $sql = "DELETE FROM ".DB_NAME_SYS.".usuariosfunciones WHERE idUsuario = ".$idUser;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $this->dbClose($result);
        $this->msgout = array("error"=>0,'msg'=>"El registro se eliminó correctamente.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
    }

    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}
