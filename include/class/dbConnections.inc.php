<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/config.inc.php");

class Modelo
{ 
	/**
	 * Instancia de la aplicacion
	 * 
	 * @var Applicacion
	 */
	protected	$app;
    private		$debug;
    protected 	$_db_sys;

    public function __construct()
    {
        $this->_db_sys = new mysqli(DB_HOST_SYS, DB_USER_SYS, DB_PASS_SYS, DB_NAME_SYS);
        if ($this->_db_sys->connect_errno){
            echo "Fallo al conectar a MySQL: ". $this->_db_sys->connect_error; 
            return;
        }
        $this->_db_sys->set_charset(DB_CHARSET);
        //$this->_db_sys->query("SET NAMES utf8");
        //$this->_db_sys->query("SET CHARACTER SET utf8");
    }
	public function SetApp($app){$this->app = $app;}
	public function Init()
	{
		
	}        
    public function dbCloseModelo($result="",$oMySql="") {
        $result->free();
        $oMySql->close();
    }
    
    
    public function getDateFormat($date="",$month=0,$hora=true,$conDia=false)
    {
        $date = str_replace('/', '-', $date);
        if(!strtotime($date)){ return ""; }
        $dateconvert = strtotime($date);
        $data = array(
            "monthNames" => array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"),
            "monthNamesShort" => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
            "diasSemana" => array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado")
        );
        $nameMonth = ($month == 0) ? $data["monthNames"][date("n",$dateconvert)-1] : $data["monthNamesShort"][date("n",$dateconvert)-1];
        $fecha = ($hora)
                ? date("d",$dateconvert)." ".$nameMonth." ".date("Y",date($dateconvert))." ".date("H",$dateconvert).":".date("i",$dateconvert).":".date("s",$dateconvert)
                : date("d",$dateconvert)." ".$nameMonth." ".date("Y",date($dateconvert));
        $getFecha = ($conDia) ? utf8_decode($data["diasSemana"][date("w",$dateconvert)])." ".$fecha : $fecha;
        return $getFecha;
    }
    
    public function log($data="") 
    {
        $data = $this->EscapeString($data);
        $nombre = isset($_SESSION["datauser"]) ? $_SESSION["datauser"]["nombreCompleto"] : '';
        $values = "(NULL, '".date("Y-m-d h:i:s")."','".$nombre."','".$data."')";
        $sql = "INSERT INTO ".DB_NAME_SYS.".log VALUES".$values;
        $this->_db_sys->query($sql);
    }
    
    public function getPathImage($type="") {
        switch ($type){
            case "user":
                $sql = "SELECT valor FROM ".DB_NAME_SYS.".generales WHERE llave = 'rutaImgUsuarios' LIMIT 1";
                $result = $this->_db_sys->query($sql);
                $dataresult = $result->fetch_assoc();
                $_SESSION["datauser"]["rutaImagen"] = $dataresult["valor"];
                unset($dataresult);
//                    $result->free_result;
                break;
            case "formulasTelas":
                $sql = "SELECT valor FROM ".DB_NAME_SYS.".generales WHERE llave = 'rutaImgFormulas' LIMIT 1";
                $result = $this->_db_sys->query($sql);
                $dataresult = $result->fetch_assoc();
//                    $result->free_result;
                return $dataresult["valor"];
        }
    }
    
    public function fechaActual_conFormato($fecha="") {
        return ($fecha == "")
            ? $this->getDateFormat(date("Y-m-d h:i:s"),1)
            : $this->getDateFormat($fecha,1);
    }
    public function nombreUsuario($nombre="") {
        return ($nombre == "")
            ? utf8_encode($_SESSION["datauser"]["nombreCompleto"])
            : utf8_encode($nombre);
    }
//****
    public function obtieneSiguientePoliza($clave_producto, $ann, $mes) {
        // 1234567890123456
        // 077803-08-16-003
        $siguiente = 1;

        $semilla = $this->semillaPoliza($clave_producto, $ann, $mes);
        $sql = "SELECT * FROM piezas WHERE clavePieza LIKE '" . $semilla. "%'";
        if ($this->debug) echo '<br> sql:' . $sql;
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            if ($this->debug) echo '<br> clave_pieza DB: ' . $row['clave_pieza'];
            $partes = preg_split("/-/", $row['clavePieza']);
            $no_poliza = intval($partes[3]);
            if ($this->debug) echo '<br> no_poliza DB: ' . $no_poliza;
            if ($no_poliza >= $siguiente) {
                $siguiente = $no_poliza + 1;
            }
        }
        return $siguiente;
    }
    
    public function semillaPoliza($clave_producto, $ann, $mes) {
        $clave_producto_formato = $this->formatoClaveProducto($clave_producto);
        $ann_formato = $this->formatoAnnPoliza($ann);
        $mes_formato = $this->formatoMesPoliza($mes);
        return $clave_producto_formato . '-' . $mes_formato . '-' . $ann_formato;
    }
    
    public function formatoClaveProducto($clave_producto) {
        return $clave_producto;
    }
    
    public function formatoAnnPoliza($ann) {
        return substr($ann, -2);
    }

    public function formatoMesPoliza($mes) {
        return str_pad($mes, 2, '0', STR_PAD_LEFT);
    }
    
    public function construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza) {
        $semilla = $this->semillaPoliza($clave_producto, $ann, $mes);
        $no_poliza_formato = $this->formatoNumeroPoliza($no_poliza);
        return $semilla . '-' . $no_poliza_formato;
    }
    
    public function formatoNumeroPoliza($no_poliza) {
        return str_pad($no_poliza, 3, '0', STR_PAD_LEFT);
    }

    public function formatoNumeroPieza($no_pieza) {
        return $no_pieza;
    }
    
    public function construyeClavePieza($clave_producto, $ann, $mes, $no_poliza, $no_pieza) {
        $clavePoliza = $this->construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza);
        $no_pieza_formato = $this->formatoNumeroPieza($no_pieza);
        return $clavePoliza . '-' . $no_pieza_formato;
    }
    public function Query($query)
    {
    	$res = $this->_db_sys->query($query);
    	if( !$res && $this->_db_sys->errno )
    		throw new Exception("MYSQL ERROR: {$this->_db_sys->error}, QUERY: $query");
    	return $res;
    }
    public function WrapField($field)
    {
    	return "`$field`";
    }
    public function EscapeString($str)
    {
    	return $this->_db_sys->real_escape_string($str);
    }
    public function SanitizeColumns($cols, $table_alias = null)
    {
    	if( !is_array($cols) )
    	{
    		$cols = array_map('trim', explode(',', $cols));
    	}
    	$scols = array();
    	foreach($cols as $col)
    	{
    		if( strstr($col, '(') || strstr($col, '*') )
    		{
    			$scols[] = $col;
    		}
    		else
    		{
    			if( $pos = stripos($col, ' as ') )
    			{
    				$_col = substr($col, 0, $pos);
    				$col_alias = substr($col, $pos);
    				//list($_col, $col_alias) = explode(' AS ', strtoupper($col));
    				list($alias, $__col) = explode('.', strtolower($_col));
    				$scols[] = sprintf("%s.%s %s", $this->WrapField($alias), $this->WrapField($__col), $col_alias);
    			}
    			elseif( strstr($col, '.') )
    			{
    				list($table_alias, $_col) = explode('.', $col);
    				$scols[] = sprintf("%s.%s", $this->WrapField($table_alias), $this->WrapField($_col));
    			}
    			else
    			{
    				$scols[] = $table_alias ? sprintf("%s.%s", $this->WrapField($table_alias), $this->WrapField($col)) :
    				$this->WrapField($col);
    			}
    
    		}
    	}
    	 
    	return $scols;
    }
    public function GetResults($query)
    {
    	if( !$query )
    		return [];
    	
    	$res = $this->Query($query);
    	if( !$res )
    		return [];
    	$items = [];
    	while( $row = $res->fetch_object() )
    	{
    		//$items[] = (object)array_map('utf8_encode', (array)$row);
            $items[] = $row;
    	}
    	$res->free_result();
    	return $items;
    }
    public function GetObject($query)
    {
    	if( !$query )
    		return null;
    	$res = $this->Query($query);	
    	
    	//return (object)array_map('utf8_encode', (array)$res->fetch_object());
        return $res->fetch_object();
    }
    public function Insert($table, $data)
    {
    	if( is_object($data) )
    		$data = get_object_vars($data);
    		
    	$columns	= array_keys($data);
    	$vals		= array_values($data);
    	$columns 	= $this->SanitizeColumns($columns);
    	$query 		= "INSERT INTO $table (".implode(',', $columns).") VALUES(";
    
    	foreach($vals as $index => $v)
    	{
    		$_v = trim($v);
    		if( sb_is_float($_v) )
    		{
    			$query .= sb_float_db(trim($_v)) . ',';
    		}
    		elseif( sb_is_datetime($_v) )
    		{
    			$_v = $this->EscapeString($_v);
    			$query .= "'$_v',";
    		}
    		elseif( sb_is_int($_v) )
    		{
    			$query .= "'$_v',";
    		}
    		elseif( strstr($_v, 'OP[') !== false )
    		{
    			$query .= str_replace(array('OP[', ']'), '', $_v) . ',';
    		}
    		elseif( $v === null )
    		{
    			$query .= "NULL,";
    		}
    		else
    		{
    			$_v = $this->EscapeString($_v);
    			$query .= "'$_v',";
    		}
    	}
    	$query = substr($query, 0, -1) . ");";
    	$this->Query($query);
    		
    	return $this->_db_sys->insert_id;;
    }
    /**
     * Actualiza un registra en la tabla
     * @param string $table Nombre de la tabla
     * @param array|object $data Datos a actualizar en la tabla
     * @param array $where Condiciones
     */
    public function Update($table, $data, array $where)
    {
    	$query = "UPDATE $table SET ";
    	foreach($data as $col => $val)
    	{
    		$_val = trim($val);
    		$wrappedCol = $this->WrapField($col);
    		if( sb_is_float($_val) )
    		{
    			$query .= sprintf("%s = %s,", $wrappedCol, sb_float_db($_val));
    		}
    		elseif( sb_is_datetime($_val) )
    		{
    			$_val = $this->EscapeString($_val);
    			$query .= "$wrappedCol = '$_val',";
    		}
    		elseif( sb_is_int($_val) )
    		{
    			$query .=  "$wrappedCol = '$_val',";
    		}
    		elseif( strstr($_val, 'OP[') !== false )
    		{
    			$query .= sprintf("%s = %s,", $wrappedCol, str_replace(array('OP[', ']'), '', $_val));
    		}
    		elseif( $val == null )
    		{
    			$query .= sprintf("%s = NULL,", $wrappedCol);
    		}
    		else
    		{
    			$_val = $this->EscapeString($val);
    			$query .= "$wrappedCol = '$_val',";
    		}
    	}
    	$query = substr($query, 0, -1);
    	$query .= " WHERE ";
    	foreach($where as $col => $val)
    	{
    		$wrappedCol = $this->WrapField($col);
    		$_val 		= $this->EscapeString($val);
    		$query 		.= "$wrappedCol = '$_val' AND ";
    	}
    	$query = substr($query, 0, -4);
    	$this->Query($query);
    }
    public function ObtenerValorGeneral($llave, $default = null)
    {
        $query = "SELECT * FROM generales WHERE llave = '$llave' LIMIT 1";
        $record = $this->GetObject($query);
        if( !$record )
            return $default;

        return $record->valor;
    }
    public function console_log($data){
        echo '<script>';
        echo 'console.log('.json_encode($data).')';
        echo '</script>';
    }
}
