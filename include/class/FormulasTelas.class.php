<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
require_once(dirname(__FILE__)."/menu.class.php");
class FormulasTelas extends Modelo{
    public $msgout = "";
    private $getDataResult = array();
    
    public function __construct(){
        parent::__construct();
        $this->menu = new menu();
    }

    public function getdata($data=array()){
    	$sql = "SELECT id_artref, clave_prod, nombreProducto, color, claveAnterior, nombreMercado, ruta 
                FROM ".DB_NAME_SYS.".artref
                WHERE clave_prod = '".$data->clave."' LIMIT 1";
        $this->log($sql);  //20180129 ml, para rastrear sentencia
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'ERROR: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>1001, "msg"=>"ERROR 1001. $msg", "action"=>"reload", "title"=>"Telas");
            $this->dbClose($result);
            return false;
        }
        $imgPath = $this->getPathImage("formulasTelas");
        $dataresult = $result->fetch_assoc();
        $dataresult["ruta"] = $imgPath.$dataresult["ruta"];             
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "getdata"=>$dataresult);
        $this->dbClose($result);
    }
//*** INSERTA LOS DATOS DE LA PESTAÑA DATOS
    private function preparaDatosInsert($data=array()) {
        $fechaEstatus = date("Y-m-d h:i:s");
        $trama = ($data->urdimbreTrama == "true" ? 'S' : 'N');
        $values = "(NULL,".
                    "'".$data->claveProducto."',".
                    $data->coloresProducto.",".
                    $data->coloresOrillo.",".
                    "'".$trama."',".
                    $data->estatus.",".
                    "'".$_SESSION["datauser"]["nombreCompleto"]."',".
                    "'".$fechaEstatus."',".
                    "DEFAULT,DEFAULT,DEFAULT,DEFAULT,". //** hilosFondo, hilosOrillo, hilosTotales, anchoTela
                    "DEFAULT,DEFAULT,DEFAULT,DEFAULT,". //** noCabos, pasadas2p, noRepasoPeine, repasoPeine
                    "'',". // tipoDibujo
                    "'',". // composicion
                    "DEFAULT, DEFAULT)"; // totalHilosUrdimbre
        return $values;
    }    
    public function insertaDatos($data=array()) {
        $values = $this->preparaDatosInsert($data);
        $sql = "INSERT INTO ".DB_NAME_SYS.".formulastelas VALUES".$values;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if(!$result){
            $this->msgout = array('error'=>1002, 'msg'=>"ERROR 1002. No se registraron los datos de la tela.", "action"=>"reload", "title"=>"Telas");
            $this->dbClose($result);
            return;
        }
        $this->msgout = array('error'=>0, 'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"Telas");
        $this->dbClose($result);
    }
//*** EDITA LOS DATOS DEL PRODUCTO SELECCIONADO
    public function editFormula($data=array()) {
        //art.color nombreColor,
        $sql = "SELECT telas.*, art.nombreProducto, art.claveAnterior, art.nombreMercado, art.color, art.ruta,
                CONCAT_WS(' - ', art.composicion01, composicion02) composicion, art.ruta imagen
                FROM ".DB_NAME_SYS.".formulastelas telas, ".DB_NAME_SYS.".artref art
                WHERE art.clave_prod = telas.claveProducto AND telas.idFormulas = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'ERROR: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>1011, "msg"=>"ERROR 1011. $msg", "action"=>"reload", "title"=>"Telas");
            $this->dbClose($result);
            return false;
        }
        $this->getDataResult = $result->fetch_assoc();
        $this->getDataResult["usuarioEstatus"] = utf8_encode($this->getDataResult["usuarioEstatus"]);
        $this->getDataResult["tipoDibujo"] = utf8_encode($this->getDataResult["tipoDibujo"]);
        $this->getDataResult["composicion"] = utf8_encode($this->getDataResult["composicion"]);
        $fechaEstatus = $this->getDateFormat($this->getDataResult["fechaEstatus"], 1);
        $this->getDataResult["fechaEstatus"] = $fechaEstatus;
        $result->free_result;
        $permisos = $this->menu->regresaPermisosUsuario();
        $autorizado = strpos($permisos, 'E');
        $this->getDataResult["autorizado"] = ($autorizado ? true : false);
        $imgPath = $this->getPathImage("formulasTelas");
        if($this->getDataResult["imagen"] !== "")
           $this->getDataResult["imagen"] = $imgPath.$this->getDataResult["imagen"];
        $this->obtenDatosUrdimbre($data->key);
        $this->obtenDatosTrama($data->key);
        $this->obtenDatosOrillo($data->key);
//        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$dataresult);
//        $this->dbClose($result);
    }
    private function obtenDatosUrdimbre($key=0) {
        $sql = "SELECT secuencia, dato FROM ".DB_NAME_SYS.".cursourdimbre WHERE idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        if($result->num_rows == 0){
            $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$this->getDataResult);
            $this->dbClose($result);
            return;
        }
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["cursoUrdimbre"] = $arrdata;
        $result->free_result;
        unset($arrdata);
//        $sql = "SELECT hilos.letra, hilos.idHilo, color.claveHilo, porcentaje.porcentaje
//                FROM ".DB_NAME_SYS.".hilostenido color, ".DB_NAME_SYS.".hilosurdimbre hilos, ".DB_NAME_SYS.".porcurdimbre porcentaje
//                WHERE color.idHilo = hilos.idHilo AND 
//                      porcentaje.idHilo = hilos.idHilo AND porcentaje.idFormulaTela = hilos.idFormulaTela AND
//                      hilos.idFormulaTela = ".$key;
        $sql = "SELECT porcentaje.letra, IFNULL(porcentaje.idHilo,'') idHilo,
                    IF(porcentaje.idHilo != 0, (SELECT color.claveHilo FROM ".DB_NAME_SYS.".hilostenido color WHERE color.idHilo = porcentaje.idHilo), '') claveHilo,
                    porcentaje.porcentaje
                FROM ".DB_NAME_SYS.".porcurdimbre porcentaje
                WHERE porcentaje.idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["colorHilosUrdimbre"] = $arrdata;
//        $this->msgout = array("error"=>0, "msg"=>"obtenDatosUrdimbre", "action"=>"", "title"=>"", "data"=>$this->getDataResult["hilosUrdimbre"]);
//        $this->dbClose($result);
    }
    private function obtenDatosTrama($key=0) {
        $sql = "SELECT secuencia, dato FROM ".DB_NAME_SYS.".cursotrama WHERE idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        if($result->num_rows == 0){
            $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$this->getDataResult);
            $this->dbClose($result);
            return;
        }
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["cursoTrama"] = $arrdata;
        $result->free_result;
        unset($arrdata);
        $sql = "SELECT porcentaje.letra, IFNULL(porcentaje.idHilo,'') idHilo,
                    IF(porcentaje.idHilo != 0, (SELECT color.claveHilo FROM ".DB_NAME_SYS.".hilostenido color WHERE color.idHilo = porcentaje.idHilo), '') claveHilo,
                    porcentaje.porcentaje
                FROM ".DB_NAME_SYS.".porctrama porcentaje
                WHERE porcentaje.idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["colorHilosTrama"] = $arrdata;
//        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$this->getDataResult);
//        $this->dbClose($result);
    }
    private function obtenDatosOrillo($key=0) {
        $sql = "SELECT secuencia, dato FROM ".DB_NAME_SYS.".cursoorillo WHERE idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        if($result->num_rows == 0){
            $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$this->getDataResult);
            $this->dbClose($result);
            return;
        }
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["cursoOrillo"] = $arrdata;
        $result->free_result;
        unset($arrdata);
        $sql = "SELECT porcentaje.letra, IFNULL(porcentaje.idHilo,'') idHilo,
                    IF(porcentaje.idHilo != 0, (SELECT color.claveHilo FROM ".DB_NAME_SYS.".hilostenido color WHERE color.idHilo = porcentaje.idHilo), '') claveHilo,
                    porcentaje.porcentaje
                FROM ".DB_NAME_SYS.".porcorillo porcentaje
                WHERE porcentaje.idFormulaTela = ".$key;
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_assoc()){
            $arrdata[] = $rows;
        }
        $this->getDataResult["colorHilosOrillo"] = $arrdata;
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "data"=>$this->getDataResult);
        $this->dbClose($result);
    }
//*** ACTUALIZA DATOS - TECNICOS
    public function preparaDatosTenicos($data=array()) {
        $trama = ($data->urdimbreTrama == "true" ? 'S' : 'N');
        $values = "noColores = ".$data->coloresProducto.",".
                  "noColoresOrillo = ".$data->coloresOrillo.",".
                  "urdimbreTramaIguales = '".$trama."',".
                  "idEstatus = ".$data->estatus.",".
                  "hilosFondo = ".$data->hilosFondo.",".
                  "hilosOrillo = ".$data->hilosOrillo.",".
                  "hilosTotales = ".$data->hilosTotales.",".
                  "anchoTela = ".$data->anchoTela.",".
                  "noCabos = ".$data->noCabos.",".
                  "pasadas2p = ".$data->pasadas2p.",".
                  "noRepasoPeine = ".$data->noRepasoPeine.",".
                  "repasoPeine = ".$data->repasoPeine.",".
                  "tipoDibujo = '".utf8_decode($data->tipoDibujo)."',".
                  "composicion = '".$data->composicion."'";
        return $values;
    }
    public function updateTecnicos($data=array()) {
        $values = $this->preparaDatosTenicos($data);
        $sql = "UPDATE ".DB_NAME_SYS.".formulastelas SET ".$values." WHERE idFormulas = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if(!$result){
            $this->msgout = array('error'=>1005, 'msg'=>"ERROR 1005. No se actualizaron los datos.", "action"=>"reload", "title"=>"Telas");
            $this->dbClose($result);
            return;
        }
        $result->free_result;
//        $this->msgout = array('error'=>0, 'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"Telas");
//        $this->dbClose($result);
    }
//*** ACTUALIZA CURSOS
    private function borraFormulaCurso($prefix="", $key=0) {
        $sql = "DELETE FROM ".DB_NAME_SYS.".curso".$prefix." WHERE idFormulaTela = $key";
        $this->log($sql);
        $this->_db_sys->query($sql);
        $sql = "DELETE FROM ".DB_NAME_SYS.".hilos".$prefix." WHERE idFormulaTela = $key";
        $this->log($sql);
        $this->_db_sys->query($sql);
        $sql = "DELETE FROM ".DB_NAME_SYS.".porc".$prefix." WHERE idFormulaTela = $key";
        $this->log($sql);
        $this->_db_sys->query($sql);
    }
    private function preparaDatosInsertaCursos($data=array(), $key=0) {
        for($i=0, $size=count($data->curso); $i<=$size-1; $i++){
            $value.= "(NULL,".
                    $key.",".
                    $data->curso[$i]["secuencia"].",".
                    "'".mb_strtoupper($data->curso[$i]["dato"])."'),";
        }
        return substr($value, 0, -1);
    }
//    private function preparaDatosInsertaHilos($data=array(), $key=0) {
//        for($i=0, $size=count($data->hilos); $i<=$size-1; $i++){
//            $value.= "(NULL,".
//                    $key.",".
//                    "'".$data->hilos[$i]["letra"]."',".
//                    $data->hilos[$i]["idHilos"].",".
//                    "'".$data->hilos[$i]["tipo"]."'),";
//        }
//        return substr($value, 0, -1);
//    }
    private function preparaDatosInsertaPorcent($data=array(), $key=0) {
        for($i=0, $size=count($data->porcentaje); $i<=$size-1; $i++){
            $value.= "(NULL,".
                    $key.",".
                    "'".$data->porcentaje[$i]["letra"]."',".
                    $data->porcentaje[$i]["idHilos"].",".
                    number_format($data->porcentaje[$i]["porcentaje"], 3)."),";
        }
        return substr($value, 0, -1);
    }
    public function insertaDatosCurso($table="", $values=""){
        $sql = "INSERT INTO ".DB_NAME_SYS.".$table VALUES".$values;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'ERROR: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>1305, "msg"=>"ERROR 1305. $msg", "action"=>"closemodal", "title"=>"Actualización del curso de fórmulas");
            $this->dbClose($result);
            return false;
        }
        if($this->_db_sys->affected_rows === -1){
            $this->msgout = array("error"=>1305, "msg"=>"<p>ERROR 1305. Consulte al Administrador del sistema.</p><p>".$this->_db_sys->error."</p>", "action"=>"closemodal", "title"=>"Actualización del curso de fórmulas");
            $this->dbClose($result, $this->_db_sys);
            return false;
        }
        $result->free_result;
        return true;
    }
    
    private function actualizaTotalesCursos($key=0, $campo="", $valor=0) {
        $sql = "UPDATE ".DB_NAME_SYS.".formulastelas SET $campo = $valor WHERE idFormulas = $key LIMIT 1";
        $formulas = $this->_db_sys->query($sql);
        $formulas->free_result;
    }
    
    public function actualizaTablasTab($data=array(), $prefix="") {
        $curso = $data->$prefix;
        if(count($curso["curso"]) == 0){
            $this->msgout = array("error"=>0, "msg"=>"", "action"=>"null", "title"=>"");
            return;
        }
        $this->borraFormulaCurso($prefix, $data->key);
        $this->insertaDatosCurso("curso".$prefix, $this->preparaDatosInsertaCursos((object)$data->$prefix, $data->key));
        $this->actualizaTotalesCursos($data->key, "totalUrdido", $data->urdimbre["totalCurso"]);
//        *** $this->insertaDatosCurso("hilos".$prefix, $this->preparaDatosInsertaHilos((object)$data->$prefix, $data->key));
        $this->insertaDatosCurso("porc".$prefix, $this->preparaDatosInsertaPorcent((object)$data->$prefix, $data->key));
        $this->actualizaTotalesCursos($data->key, "totalTrama", $data->trama["totalCurso"]);
        $this->_db_sys->close;
    }
//*** CERRAR DB
    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}