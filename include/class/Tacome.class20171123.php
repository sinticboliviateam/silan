<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class Tacome extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
//*** DATOS INICIALES
    public function datosNuevoRegistro($key=0) {
        $sql = "SELECT art.ruta img
                FROM ".DB_NAME_SYS.".piezas pzas, ".DB_NAME_SYS.".artref art 
                WHERE art.clave_prod = SUBSTRING_INDEX(pzas.clavePieza,'-',1) AND pzas.idPiezas = $key LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
    //******    
        $imgPath = $this->getPathImage("formulasTelas");
        $row["img"] = ($row["img"] != "" ? $imgPath.$row["img"] : "");
        $this->msgout = array("error" => "", "result" => $row);
    }
    //*** PREPARA VALORES PARA EL REGISTRO DE TACOME
    private function obtieneConsecutivoPiezas() {
        $sql = "SELECT ifnull(MAX(noPieza),0) maxnum FROM piezas";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        $result->free_result;
        return intval($row['maxnum']) + 1;
    }
    public function insertaTacome($datakey=array()) {
        $consecutivo = $this->obtieneConsecutivoPiezas();
        $fechaTacome = date("Y-m-d h:i:s");
        $usuarioTacome = $_SESSION["datauser"]["nombreCompleto"];
    //** INSERTA LA NUEVA POLIZA
        $sql = "UPDATE ".DB_NAME_SYS.".piezas SET
                    metros = $datakey->metros,
                    kilos = $datakey->kilos,
                    noPieza = $consecutivo,
                    etiquetaPt = 'I',
                    fechaTacome = '$fechaTacome',
                    usuarioTacome = '$usuarioTacome'
                WHERE idPiezas = $datakey->idPiezas LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $this->msgout = array("error" => 0);
        $result->free_result;
//        $this->imprimePieza($datakey);
    }
    
    public function reimprimePieza($datakey) {
        $clave_pieza = $datakey->clave_pieza;

        // Actualiza el registro con información Tacome
        $sql = "UPDATE piezas ";
        $sql .= "SET etiqueta = 'R' ";
        $sql .= "WHERE clave_pieza = '$clave_pieza'";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $dataresult = (object) array("error" => "");
        if ($this->_db_sys->query($sql) === TRUE) {
            if ($this->debug) echo "Record updated successfully";
        } else {
            if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
            $dataresult->error = "Error: " . $sql . "<br>" . $conn->error;
            return dataresult;
        }

        //return $dataresult;
        return $this->imprimePieza($datakey);
    }

    public function imprimePieza($datakey) {
        $clave_pieza = $datakey->clave_pieza;

        $dataresult = (object) array("error" => "");

        // recuperar datos de pieza de la db
        $sql = "SELECT * FROM piezas WHERE clave_pieza = '$clave_pieza' LIMIT 1";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);

        $pieza = $result->fetch_assoc();
        if ($pieza == NULL) {
            $dataresult->error = 'No se eoncontró la pieza ' . $clave_pieza;
            return $dataresult;            
        }

        // determinar clave_prod
        $partesClavePieza = preg_split('/-/', $clave_pieza);
        $clave_prod = $partesClavePieza[0];

        // Recuperar datos de producto de db
        $sql = "SELECT * FROM artref WHERE clave_prod = '$clave_prod' LIMIT 1";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);

        $artref = $result->fetch_assoc();
        if ($pieza == NULL) {
            $dataresult->error = 'No se eoncontró el producto ' . $clave_prod;
            return $dataresult;            
        }


        // Organizar datos de la etiqueta
        $printdata = (object) array( 'data' => array(
            "servicio" => 'etiqueta',
            "sku" => $artref['sku'],
            "marca" => $artref['marca'],
            "clave_prod" => $artref['clave_prod'],
            "color" => $artref['color'],
            "ancho" => $artref['ancho'],
            "composicion01" => $artref['composicion01'],
            "composicion02" => $artref['composicion02'],
            "metros" => $pieza['metros'],
            "clave_pieza" => $pieza['clave_pieza'],
            "numero" => $pieza['numero']
        ));

        $printdata = http_build_query($printdata);
        //var_dump($printdata);


        //$rutaServidorImpresion = 'localhost';
        $rutaServidorImpresion = '10.0.0.23'; // CAMBIAR: Direccion IP de ZebraPrint

        // Llamar servicio de impresiom
        $url = 'http://' . $rutaServidorImpresion . '/ZebraPrint/zebraprint.php';

        //echo $url;
        //var_dump($printdata);

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $printdata);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );
        curl_close( $ch );

        //echo ($response);
        $dataresult = (object) array("error" => "", "response" => $response);
        return $dataresult;

        // Evaluar el resultado de la impresion

        // Registrar impresion en db

    }
//*** CIERRA BASE DE DATOS
    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}
