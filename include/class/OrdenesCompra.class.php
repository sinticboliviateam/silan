<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class OrdenesCompra extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getLabelFunctions(){
        $section = array("win"=>"", "pro"=>"", "rep"=>"");
        $sql = "SELECT * FROM ".DB_NAME_SYS.".funciones WHERE mostrar = 1 ORDER BY orden";
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Error en dBase 3001: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>3001, "msg"=>"ERROR 3001. $msg", "action"=>"reload", "title"=>"Catálogo de OrdenesCompra");
            $this->dbClose($result);
            return false;
        }
        $win = ""; $pro = ""; $rep = "";
        while($row = $result->fetch_assoc()){
            $idFunction = $row["idFuncion"] + 100;
            if(strcmp($row["tipo"], "V") == 0 && substr($row["menu"], 2, 4) != "00"){
               $win.= "<div class='flex-modal-produccion block-info-tab-checkBox'>".
                      "<div class='div-oLabel-tab'><label class='oLabel-left'>".utf8_encode($row['funcion'])."</label></div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='L' style='width: 30px;'>Leer</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='E' style='width: 30px;'>Escribir</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='B' style='width: 30px;'>Borrar</div>".
                      "</div>";
            }
            if(strcmp($row["tipo"], "P") == 0 && substr($row["menu"], 2, 4) != "00"){
               $pro.= "<div class='flex-modal-produccion block-info-tab-checkBox'>".
                      "<div class='div-oLabel-tab'><label class='oLabel-left'>".utf8_encode($row['funcion'])."</label></div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='L' style='width: 30px;'>SI</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='E' style='width: 30px;'>NO</div>".
                      "</div>";
            }
        }
        $section["win"] = $win;
        $section["pro"] = $pro;
        $section["rep"] = $rep;
        $this->msgout = array("error"=>0, "dataresult"=>$section);
        $this->dbClose($result);
    }
//*** INSERTA LOS DATOS DE LA ORDEN DE COMPRA
    private function preparaDatosInsert($data=array()) {
        $fech= $data->fechaOrden;
        $date1 = explode("/", $fech);
        $fechaOrden= $date1[2] ."-".$date1[1]."-".$date1[0];
        $values = "(NULL,".
                    "'".utf8_decode($data->noOrdenCompra)."',".
                    "'".$fechaOrden."',".
                    $data->destinoOrden.")";
        return $values;
    }
    
    public function insertOrdenComp($data=array()) {
        $values = $this->preparaDatosInsert($data);
        $sql = "INSERT INTO ".DB_NAME_SYS.".ordenescompra (idOrdenCompra,ordenCompra,fechaORden,idDestino) VALUES".$values;
//echo "sql".$sql; exit();

        $this->log($sql);

        $result = $this->_db_sys->query($sql);
        //$iduser = $this->_db_sys->insert_id;
        if(!$result){
            $this->msgout = array('error'=>3002, 'msg'=>"ERROR 3002. No se registró la Orden de compra.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
            unset($_SESSION["datauser"]["imagen"]);
            $this->dbClose($result);
            return;
        }
//echo "result ".$result; exit();
        $this->msgout = array('error'=>0, 'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
        unset($_SESSION["datauser"]["imagen"]);
        $this->dbClose($result);

    }
//*** EDITA LOS DATOS DE LA ORDEN DE COMPRA
    public function editOrdenComp($data=array()) {
        $sql = "SELECT * FROM ".DB_NAME_SYS.".ordenescompra WHERE idOrdenCompra = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Error en dBase: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>3003, "msg"=>"ERROR 3003. $msg", "action"=>"reload", "title"=>"Tabla Órdenes de compra");
            $this->dbClose($result);
            return false;
        }
        while($row = $result->fetch_assoc()){

            //$fechaEstatus = $this->getDateFormat($row["fechaEstatus"], 1);
            //$fechaUltimoAcceso = $this->getDateFormat($row["ultimoAcceso"], 1);
        //****
            $date = explode("-", $row["fechaOrden"]);
            $dataresult["idOrdenCompra"] = $row["idOrdenCompra"];
            $dataresult["noOrdenCompra"] = utf8_encode($row["ordenCompra"]);
            $dataresult["fechaOrden"] = $date[2]."/".$date[1]."/".$date[0];
            $dataresult["destinoOrden"] = $row["idDestino"];
        }

        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "result"=>$dataresult);
        $this->dbClose($result);
    }
    
    private function preparaDatosUpdate($data=array()) {
        $fecha= $data->fechaOrden;
        $fecha= explode('/', $fecha);
        $fecha= $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
//echo "noOrdenCompra= ". $data->noOrdenCompra;exit();

        $values = "ordenCompra = '".utf8_decode($data->noOrdenCompra)."',".
                  "fechaOrden = '".$fecha."',".
                  "idDestino = '".$data->destinoOrden."'";
        return $values;
    }

    public function updateOrdenCompra($data=array()) {
//print_r($data);exit();
        $values = $this->preparaDatosUpdate($data);
        $sql = "UPDATE ".DB_NAME_SYS.".ordenescompra SET ".$values." WHERE idOrdenCompra = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);

        if(!empty($data->functions))
            $this->updateFunctionsUser($data->key, $data->functions);
        $this->msgout = array("error"=>0);
    }

    public function deleteOrdenCompra($data=array()){
        $values = $this->preparaDatosUpdate($data);
        $sql = "DELETE FROM ".DB_NAME_SYS.".ordenescompra  WHERE idOrdenCompra = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $this->msgout = array("error"=>0,'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
    }

    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}
