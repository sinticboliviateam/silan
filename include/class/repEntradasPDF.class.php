<?php
/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-04
 * SiLan v1.0
*/
include(dirname(__FILE__)."/dbConnections.inc.php");
//print_r($_GET);exit();
class EntradasPDF extends Modelo {
    //private $idSalida=0;
    private $fechaIni;
    private $fechaFin;
    private $records;

    public function __construct(){
        parent::__construct();
        //$this->idSalida = $_GET["keyRecord"];
        $this->fechaIni = $_GET["fechaIni"];
        $this->fechaFin = $_GET["fechaFin"];
        $this->SetSqlEntradas();
    }
     
    private function setSqlEntradas(){
        $sql= "SELECT idPiezas, SUBSTRING_INDEX(piezas.clavePieza,'-',1) articulo,
                    SUBSTRING(SUBSTRING_INDEX(piezas.clavePieza,'-',1),-3,3) referencia,
                    artref.color, 
                    SUBSTRING_INDEX(piezas.clavePieza,'-',4) poliza,
                    LPAD(SUBSTRING_INDEX(piezas.clavePieza,'-',-1), 2, '0') etiqueta,
                    piezas.metros metros, 
                    FORMAT(piezas.kilos,3) kilos,
                    SUM(piezas.metros) totmts,
                    SUM(piezas.kilos) totkilos,
                    fechaEntrada 
                FROM silan.piezas piezas, silan.artref artref 
                WHERE 
                    artref.clave_prod = SUBSTRING_INDEX(piezas.clavePieza,'-',1) and
                    (piezas.fechaEntrada BETWEEN '".$this->fechaIni." 00:00:00' AND '".$this->fechaFin." 23:59:59')
                GROUP BY articulo,idPiezas WITH ROLLUP;";

        $sql1 = "select count(*), sum(metros) metros, sum(kilos) kilos
                    from silan.piezas
                where 
                    SUBSTRING_INDEX(piezas.clavePieza,'-',1) = '1010E001%%' and  
                    (piezas.fechaEntrada BETWEEN '2018-01-01 00:00:00' AND '2018-01-31 23:59:59');";

        //echo $sql;exit();
        //$this->log($sql); 20180412 revisar no graba la consulta
        $result = $this->_db_sys->query($sql);
        
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->records = $arrdata;
    }

    public function getFechaIni() {
        //return $this->fechaIni;
        return $this->getDateFormat($this->fechaIni, 0, false, true); 
    }

    public function getFechaFin() {
        return $this->getDateFormat($this->fechaFin, 0, false, true); 
    }

    public function getFechaSalida() {
        return $this->getDateFormat($this->fechaSalida, 0, false, true);
    }
    
    public function getFolioSalida() {
        return $this->idSalida;
    }
    
    public function getTotalRecords() {
        return count($this->records);
    }
    
    public function getRecordEntrada($idx=0,$campo='') {
        return $this->records[$idx]->$campo;
    }
    
}