<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * 12dic2017 impresión para Zebra Z4Mimpre
*/
include(dirname(__FILE__)."/dbConnections.inc.php");
class PolizasPDF extends Modelo {
    private $polizas;
    protected $formulas;
    private $artRef;
    private $idPoliza;
    public function __construct(){
        parent::__construct();
        $this->idPoliza = $_GET["keyPoliza"];
        $this->setsqlPoliza();
        $this->setsqlFormulas();
        $this->setsqlArticulo();
        $this->setsqlPorcentajeUrdido();
        $this->setsqlCursoUrdimbre();
        $this->setsqlPorcentajeTrama();
        $this->setsqlCursoTrama();
    }
    
    private function setsqlPoliza() {
        $sql = "SELECT * FROM ".DB_NAME_SYS.".polizas WHERE idPoliza = ".$this->getkeyPoliza()." LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $rows = $result->fetch_object();
        $this->polizas = $rows;
        $result->free_result;
    }
    
    private function setsqlFormulas() {
        $sql = "SELECT * FROM ".DB_NAME_SYS.".formulastelas WHERE idFormulas = ".$this->getkeyFormulas()." LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $rows = $result->fetch_object();
        $this->formulas = $rows;
        $result->free_result;
    }
    
    private function setsqlArticulo() {
        $sql = "SELECT color, CONCAT_WS(' ',composicion01,composicion02) composicion FROM ".DB_NAME_SYS.".artref WHERE clave_prod = '".$this->getClaveProducto()."' LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $this->artRef = $result->fetch_object();
        /*while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }*/
        $this->polizas->articulo = str_pad($this->getClaveProducto(),12,' ',STR_PAD_RIGHT).$this->artRef->color;
        $result->free_result;
    }
    
    private function setsqlPorcentajeUrdido() {
        $sql = "SELECT porcentaje.*, hilos.*
                FROM ".DB_NAME_SYS.".porcurdimbre porcentaje, ".DB_NAME_SYS.".hilostenido hilos
                WHERE hilos.idHilo = porcentaje.idHilo AND porcentaje.idFormulaTela = ".$this->getkeyFormulas()." ORDER BY porcentaje.idPorcUrdimbre";
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->formulas->porcentajeUrdido = $arrdata;
        $result->free_result;
    }
    
    private function setsqlPorcentajeTrama() {
        $sql = "SELECT porcentaje.*, hilos.*
                FROM ".DB_NAME_SYS.".porctrama porcentaje, ".DB_NAME_SYS.".hilostenido hilos
                WHERE hilos.idHilo = porcentaje.idHilo AND porcentaje.idFormulaTela = ".$this->getkeyFormulas()." ORDER BY porcentaje.idPorcTrama";
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->formulas->porcentajeTrama = $arrdata;
        $result->free_result;
    }
    
    public function setCalculaNoFajas() {
        $noFajas = (int)($this->getHilosTotales() / $this->getnumeroCabos());
        $fajasCompletas = $noFajas * $this->getnumeroCabos();
        $hilosFaltantes = $this->getHilosTotales() - $fajasCompletas;
        $this->formulas->noFajas = $noFajas;
        $this->formulas->hilosFaltantes = $hilosFaltantes;
    }
    
    public function setCalculaAnchoFajas() {
        $hilosCentimetro = $this->getHilosTotales() / $this->getAnchoTela();
        $anchoFaja = $this->getnumeroCabos() / $hilosCentimetro;
        $this->formulas->anchoFajas = $anchoFaja;
    }
    
    public function setTablaUrdido() {
        $tablaUrdido = array(); $idx=-1;
        $rowsPorcentajes = count($this->formulas->porcentajeUrdido)-1;
        for($i=65;$i<=79;$i++){
            $idx++; $total = 0;
            if($idx <= $rowsPorcentajes){
                $total = ($this->formulas->porcentajeUrdido[$idx]->porcentaje * $this->getTotalUrdido()) / 100;
                $total= $total+ .5;
                if($total < 1) $total = 1;
            }
            $tablaUrdido[$idx]["letra"] = chr($i);
            $tablaUrdido[$idx]["total"] = (int)$total;
        }
        $this->formulas->tablaUrdido = $tablaUrdido;
    }
    
    public function setsqlCursoUrdimbre() {
        $sql = "SELECT dato FROM ".DB_NAME_SYS.".cursourdimbre WHERE idFormulaTela = ".$this->getkeyFormulas()." ORDER BY idCursoUrdimbre";
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->formulas->cursoUrdimbre = $arrdata;
        $result->free_result;
    }
    
    public function setsqlCursoTrama() {
        $sql = "SELECT dato FROM ".DB_NAME_SYS.".cursotrama WHERE idFormulaTela = ".$this->getkeyFormulas()." ORDER BY idCursoTrama";
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->formulas->cursoTrama = $arrdata;
        $result->free_result;
    }
    
    public function getkeyPoliza() {
        return $this->idPoliza;
    }
    
    public function getkeyFormulas() {
        return $this->polizas->idFormulas;
    }
    
    public function getnumeroPoliza() {
        return $this->polizas->noPoliza;
    }
    
    public function getFechaNumerica() {
        return date("d / m / Y");
    }
    
    public function getClaveProducto() {
        return $this->formulas->claveProducto;
    }
    
    public function getClaveArticulo() {
        return $this->polizas->articulo;
    }
    
    public function getHilosFondo() {
        return $this->formulas->hilosFondo;
    }
    
    public function getHilosOrillos() {
        return $this->formulas->hilosOrillo;
    }
    
    public function getHilosTotales() {
        return $this->formulas->hilosTotales;
    }
    
    public function getAnchoTela() {
        return $this->formulas->anchoTela;
    }
    
    public function getnumeroCabos() {
        return $this->formulas->noCabos;
    }
    
    public function getnumeroFajas() {
        return $this->formulas->noFajas." Fajas ".$this->formulas->hilosFaltantes." Hilos";
    }
    
    public function getAnchoFajas() {
        return number_format($this->formulas->anchoFajas, 2);
    }
    
    public function getTotalUrdido() {
        return $this->formulas->totalUrdido;
    }
    
    public function getTotalTrama() {
        return $this->formulas->totalTrama;
    }
    
    public function getDatoHiloUrdido($idx,$campo) {
        return ($campo == 'letra') 
            ? "HILOS COLOR ".$this->formulas->tablaUrdido[$idx]["letra"]
            : $this->formulas->tablaUrdido[$idx]["total"];
    }
    
    public function getTotalMetro() {
        return $this->polizas->noPiezas * $this->polizas->metros;
    }
    
    public function getLeyendaUrdido() {
        return $this->polizas->noPiezas." Piezas de ".$this->polizas->metros." c/u ".$this->getTotalMetro()." Metros";
    }
    
    public function getPasadas2() {
        return $this->formulas->pasadas2p;
    }
    
    public function getnumeroRepaso() {
        return $this->formulas->noRepasoPeine;
    }
    
    public function getRepasoPeine() {
        return $this->formulas->repasoPeine;
    }
    
    public function getDibujo() {
        return $this->formulas->tipoDibujo;
    }

    public function getTotalCursoUrdimbre() {
        return count($this->formulas->cursoUrdimbre)-1;
    }
    
    public function getDatoCursoUrdimbre($idx) {
        return $this->formulas->cursoUrdimbre[$idx]->dato;
    }
    
    public function getNombreColorUrdimbre($dato) {
        $letra = substr($dato, -1);
        for($idx=0, $size=count($this->formulas->porcentajeUrdido)-1; $idx<=$size; $idx++){
            if($this->formulas->porcentajeUrdido[$idx]->letra === $letra){
               return $this->formulas->porcentajeUrdido[$idx]->claveHilo." ".$this->formulas->porcentajeUrdido[$idx]->color; 
            }
        }
    }
    
    public function getTotalCursoTrama() {
        return count($this->formulas->cursoTrama)-1;
    }
    
    public function getDatoCursoTrama($idx) {
        return $this->formulas->cursoTrama[$idx]->dato;
    }
    
    public function getNombreColorTrama($dato) {
        $letra = substr($dato, -1);
        for($idx=0, $size=count($this->formulas->porcentajeTrama)-1; $idx<=$size; $idx++){
            if($this->formulas->porcentajeTrama[$idx]->letra === $letra){
               return $this->formulas->porcentajeTrama[$idx]->claveHilo." ".$this->formulas->porcentajeTrama[$idx]->color; 
            }
        }
    }

    public function getComposicionArticulo(){
        return $this->artRef->composicion;
    }

    public function getColorArticulo(){
        return $this->artRef->color;
    }

    public function getnumeroTelar(){
        return $this->polizas->noTelar;
    }

    public function getnumeroPiezas(){
        return $this->polizas->noPiezas;
    }
}