<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
include(dirname(__FILE__)."/dbConnections.inc.php");
class Autocomplete extends Modelo {
    public $arrayElementos = array();
   
    public function __construct(){
        parent::__construct();
    }
//*** LLENA AUTOCOMPLETE    
    public function sqlHilos($data=array()) {
        $sql = "SELECT idHilo clave, CONCAT(RPAD(claveHilo,12,' '),color) etiqueta
                FROM ".DB_NAME_SYS.".hilostenido
                WHERE CONCAT(RPAD(claveHilo,12,' '),color) LIKE '%".$data->term."%'
                ORDER BY claveHilo LIMIT ".$data->limit;
        $this->getDataAutocomplete($sql);
    }
    
    public function sqlProductos($data=array()) {
        $sql = "SELECT articulos.clave_prod clave, CONCAT(RPAD(articulos.clave_prod,12,' '),articulos.color) etiqueta
                FROM ".DB_NAME_SYS.".artref articulos LEFT OUTER JOIN ".DB_NAME_SYS.".formulastelas formulas
                    ON articulos.clave_prod = formulas.claveProducto
                WHERE formulas.claveProducto IS NULL AND CONCAT(RPAD(articulos.clave_prod,12,' '),articulos.color) LIKE '%".$data->term."%'
                ORDER BY articulos.clave_prod LIMIT ".$data->limit;            
        //$this->log($sql); // 20180129 ml, rastreo de sentencia
        $this->getDataAutocomplete($sql);
    }
    
    public function sqlFormulasTelas($data=array()) {
        $sql = "SELECT formulas.claveProducto clave, CONCAT(RPAD(articulos.clave_prod,12,' '),articulos.color) etiqueta
                FROM ".DB_NAME_SYS.".formulastelas formulas LEFT OUTER JOIN ".DB_NAME_SYS.".artref articulos
                    ON articulos.clave_prod = formulas.claveProducto
                WHERE formulas.claveProducto LIKE '%".$data->term."%'
                ORDER BY formulas.claveProducto LIMIT ".$data->limit;
        //$this->log($sql); // 20180129 ml, seguimiento sentencia
        $this->getDataAutocomplete($sql);
    }
    
    public function sqlPiezas($data=array()) {
        $sql = "SELECT piezas.idPiezas clave, piezas.clavePieza etiqueta
                FROM ".DB_NAME_SYS.".piezas, ".DB_NAME_SYS.".polizas
                WHERE polizas.idEstatus = 10 AND polizas.idPoliza = piezas.idPoliza AND 
                        piezas.usuarioTacome = '' AND piezas.clavePieza LIKE '%".$data->term."%'
                ORDER BY piezas.idPiezas LIMIT ".$data->limit;
        $this->getDataAutocomplete($sql);
    }   
    
    private function getDataAutocomplete($sql="") {
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Violación a la dBase: '.$this->_db_sys->error. "\n";
            $msgout = array("error"=>5001, "msg"=>"ERROR 5001. $msg", "action"=>"closemodal", "title"=>"Llenado de Autocomplete");
            $this->dbCloseModelo($result,$this->_db_sys);
            echo json_encode($msgout);
            return false;
        }
        while($row = $result->fetch_assoc()){
            array_push($this->arrayElementos, array(
                    "label" => utf8_encode($row["etiqueta"]),
                    "value" => $row["clave"]
                )
            );
        }
        $this->dbCloseModelo($result,$this->_db_sys);
    }
}
