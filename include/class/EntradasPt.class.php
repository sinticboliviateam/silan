<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class EntradasPt extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
//*** VALIDA LA CLAVE DEL DOCUMENTO
    public function validacionUnico($datakey) {
        $this->msgout = (object) array(
            "error" => 0,
            "unico" => 'NO'
        );

        if (!property_exists($datakey, 'campo') || !property_exists($datakey, 'valor')){
            $this->msgout = array(
                "error" => 2301, "msg" => 'ERROR => 2301. Se requiere "campo" y "valor"<br>', "action" => "closemodal", "title" => "Entradas Pt");
            return;
        }
        $campo = $datakey->campo;
        $valor = $datakey->valor;
        $sql = "SELECT $campo FROM ".DB_NAME_SYS.".piezas WHERE $campo = '$valor'";
        $result = $this->_db_sys->query($sql);
        $rows = $result->num_rows;
        if($rows == 0){
            $this->msgout->unico = 'SI';
        }
        $this->_db_sys->close;
    }
    
    public function listaPiezasSinEntrada() {
        $list = Array();
        $sql = "SELECT noPieza, clavePieza FROM ".DB_NAME_SYS.".piezas WHERE fechaEntrada IS NULL ORDER BY noPieza";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        $this->msgout = array("error" => 0, "list" => $list);
        $this->dbClose($result);
    }
    
    public function entradaPiezas($datakey) {
        $documentoEntrada = $datakey->documentoEntrada;
        $piezas = $datakey->lista_numero_pieza;
        $fechaEntrada = date('Y-m-d H:i:s');
        $usuarioEntrada = $_SESSION["datauser"]["nombreCompleto"];
        foreach ($piezas as $numero) {
            $sql = "UPDATE piezas ";
            $sql .= "SET documentoEntrada = '$documentoEntrada', fechaEntrada = '$fechaEntrada', usuarioEntrada = '$usuarioEntrada' ";
            $sql .= "WHERE noPieza = '$numero'";
            if($this->_db_sys->query($sql) === FALSE){
                $this->msgout = array("error"=>2401, "msg"=>"SQL: ".$sql."<br>Error: ".$this->_db_sys->error."<br>", "action"=>"closemodal", "Entradas Pt");
                break;
            } else {
                $this->msgout = array("error"=>0, "msg"=>"Se registró la entrada: ".$documentoEntrada, "action"=>"reload", "Entradas Pt");
            }               
        }
        $this->_db_sys->close;
    }
//*** CIERRA BASE DE DATOS
    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}
