<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class SalidasPt extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
//*** MUESTRA REGISTROS EXISTENTES
    public function listaPiezasSinSalida() {
        $list = Array();
        $sql = "SELECT noPieza, clavePieza FROM ".DB_NAME_SYS.".piezas ";
        $sql .= "WHERE fechaEntrada IS NOT NULL AND (fechaSalida IS NULL OR fechaSalida = '0000-00-00') ORDER BY 'noPieza'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        $this->msgout = array("error" => 0, "list" => $list);
        $this->dbClose($result);
    }
    
//*** VALIDA LA CLAVE DEL CLIENTE
    public function validacionExisteCliente($datakey) {
        $this->msgout = (object) array(
            "error" => 0,
            "existe" => 'NO'
        );
        
        if (!property_exists($datakey, 'cliente')){
            $this->msgout = array(
                "error" => 2401, "msg" => 'ERROR => 2401. Se requiere número de cliente<br>', "action" => "closemodal", "title" => "Salidas Pt");
            return;
        }
        $noCliente = $datakey->cliente;
        $sql = "SELECT noCliente FROM ".DB_NAME_SYS.".clientes WHERE noCliente = '$noCliente'";
        $result = $this->_db_sys->query($sql);
        $rows = $result->num_rows;
        if($rows != 0){
            $this->msgout->existe = 'SI';
        }
        $this->_db_sys->close();
    }
//*** GUARDA EL REGISTRO DE SALIDA    
    public function salidaPiezas($datakey) {
        $noCliente = $datakey->noCliente;
        $piezas = $datakey->lista_numero_pieza;
        $fechaSalida = date('Y-m-d H:i:s');
        $usuarioSalida = $_SESSION["datauser"]["nombreCompleto"];
        // Calcula documento_salida
        $sql = "SELECT ifnull(MAX(documentoSalida),0) ultimoDocumento FROM piezas";
        if ($this->debug) echo '<br>sql: ' . $sql;
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        $documentoSalida = intval($row['ultimoDocumento']) + 1;
        foreach ($piezas as $numero) {
            $sql = "UPDATE piezas ";
            $sql .= "SET documentoSalida = $documentoSalida, ";
            $sql .= "noCliente = $noCliente, "; 
            $sql .= "fechaSalida = '$fechaSalida', ";
            $sql .= "usuarioSalida = '$usuarioSalida'";
            $sql .= "WHERE noPieza = $numero";
            if($this->_db_sys->query($sql) === FALSE){
                $this->msgout = array("error"=>2501, "msg"=>"SQL: ".$sql."<br>Error: ".$this->_db_sys->error."<br>", "action"=>"closemodal", "title"=>"Salidas Pt");
                return;
            }            
        }
        $this->msgout = array("error"=>0, "msg"=>"Se registró la salida: ".$documentoSalida, "action"=>"reload", "title"=>"Salidas Pt");
        $this->dbClose($result);
    }   
//*** CIERRA BASE DE DATOS
    private function dbClose($result="") {
        $result->free_result();
        $this->_db_sys->close();
    }
}
