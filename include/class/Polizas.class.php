<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
require_once(dirname(__FILE__)."/menu.class.php");
class Polizas extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
        $this->menu = new menu();
    }
//*** DATOS INICIALES    
    public function datosNuevaPoliza($claveProducto='') {
        $ann = date('Y');
        $mes = date('m');
        $no_poliza = $this->obtieneSiguientePoliza($claveProducto, $ann, $mes);
        $siguientePoliza = $this->construyeClavePoliza($claveProducto, $ann, $mes, $no_poliza);
        $sql = "SELECT art.ruta img, formulas.idFormulas id, formulas.noCabos
                FROM ".DB_NAME_SYS.".artref art, ".DB_NAME_SYS.".formulastelas formulas 
                WHERE formulas.claveProducto = art.clave_prod AND art.clave_prod = '$claveProducto' LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
    //******
        $imgPath = $this->getPathImage("formulasTelas");
        $row["img"] = ($row["img"] != "" ? $imgPath.$row["img"] : "");
        $row["siguientePoliza"] = $siguientePoliza;
        $this->msgout = array("error" => "", "result" => $row);
        return $this->msgout;
    }
//*** PREPARA VALORES PARA EL REGISTRO DE POLIZAS Y PEZAS
    private function estableceValoresPoliza($datakey=array()) {
        $this->idPoliza = NULL;
        $this->idFormulas = $datakey->idFormulas;
        $this->noPoliza = NULL;
        $this->noPiezas = $datakey->noPiezas;
        $this->noTelar = $datakey->noTelar;
        $this->fechaRegistro = date('Y-m-d H:i:s');
        $this->usuarioRegistro = $_SESSION["datauser"]["nombreCompleto"];
        $this->idEstatus = 10; // VIGENTE => ESTATUS - CATALOGO(4)
        $this->usuarioEstatus = $_SESSION["datauser"]["nombreCompleto"];
        $this->fechaEstatus = date('Y-m-d H:i:s');
        $this->fechaFiltro = $this->getDateFormat(date("Y-m-d"),1);
    //***
        $this->metros = $datakey->metros;
        $this->claveProducto = $datakey->claveProducto;
        $this->mes = date("m");
        $this->ann = date("Y");
    }
    public function insertaPoliza($datakey=array()) {
        $this->estableceValoresPoliza($datakey);
        $tempPoliza = $this->obtieneSiguientePoliza($this->claveProducto, $this->ann, $this->mes);
        $this->noPoliza = $this->construyeClavePoliza($this->claveProducto, $this->ann, $this->mes, $tempPoliza);
    //** INSERTA LA NUEVA POLIZA
        $sql = "INSERT INTO ".DB_NAME_SYS.".polizas VALUES(
                    NULL, $this->idFormulas, '$this->noPoliza', $this->noPiezas, $this->metros, $this->noTelar,
                    '$this->fechaRegistro','$this->usuarioRegistro',$this->idEstatus,'$this->usuarioEstatus','$this->fechaEstatus','$this->fechaFiltro')";
        $this->log($sql);
        //$result = $this->_db_sys->query($sql);
        $result		= $this->Query($sql);
        $this->idPoliza = $this->_db_sys->insert_id;
        if(!$result)
        {
            $this->msgout = array('error'=>2002, 'msg'=>"ERROR 2002. No se registraron los datos de la póliza.", "action"=>"reload", "title"=>"Generación de polizas");
            $this->dbClose($result);
            return;
        }
        //$result->free_result;
    	//** GENERLA E INSERTA EL REGISTRO DE LAS PIEZAS
        for($p = 1; $p<=$this->noPiezas; $p++) 
        {
            $clavePieza = $this->noPoliza."-".$p;
            $this->insertaPieza($clavePieza);
        }
    }
    private function estableceValoresPiezas() {
        $this->idPiezas = "NULL";
        $this->kilos = "DEFAULT";
    }
    private function insertaPieza($clavePieza='') {
        $this->estableceValoresPiezas();
        $sql = "INSERT INTO ".DB_NAME_SYS.".piezas (idPiezas,idPoliza,clavePieza,metros)
                    VALUES($this->idPiezas,$this->idPoliza,'$clavePieza',$this->metros)";
        $result = $this->_db_sys->query($sql);
        if(!$result){
            $this->msgout = array('error'=>2001, 'msg'=>"ERROR 2001. No se registraron las piezas.", "action"=>"closemodal", "title"=>"Generación de pólizas");
            $this->dbClose($result);
            return;
        }
        $this->msgout = array("error"=>0, "msg"=>"Se creó la póliza: ".$this->noPoliza, "action"=>"closemodal", "title"=>"Generación de pólizas");
        $this->dbClose($result);
    }
//** EDITA LOS CAMPOS DE LA POLIZA
    public function editaDatosPoliza($key=0) {
        $sql = "SELECT pol.*, formulas.claveProducto, art.ruta img, est.nombre nombreEstatus, formulas.noCabos
                FROM ".DB_NAME_SYS.".polizas pol, ".DB_NAME_SYS.".formulastelas formulas, 
                ".DB_NAME_SYS.".artref art, ".DB_NAME_SYS.".estatus est 
                WHERE art.clave_prod = formulas.claveProducto AND formulas.idFormulas = pol.idFormulas AND 
                        est.idEstatus = pol.idEstatus AND pol.idPoliza = $key
                ORDER BY pol.idPoliza LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $dataresult = $result->fetch_assoc();
        $dataresult["fechaRegistro"] = $this->fechaActual_conFormato($dataresult["fechaRegistro"]);
        $dataresult["usuarioRegistro"] = $this->nombreUsuario($dataresult["usuarioRegistro"]);
        $dataresult["fechaEstatus"] = $this->fechaActual_conFormato();
        $dataresult["usuarioEstatus"] = $this->nombreUsuario();
        $permisos = $this->menu->regresaPermisosUsuario();
        $dataresult["autorizado"] = $this->menu->verificaAutorizacion($permisos);
        $imgPath = $this->getPathImage("formulasTelas");
        $dataresult["img"] = ($dataresult["img"] != "" ? $imgPath.$dataresult["img"] : "");
        $this->msgout = array("error" => 0, "data" => $dataresult);
        $this->dbClose($result);
    }
    
    public function cancelaDatosPoliza($key=0) {
        // 11 = CANCELADA => ESTATUS - CATALOGO(4)
        $sql = "UPDATE ".DB_NAME_SYS.".polizas SET idEstatus = 11 WHERE idPoliza = $key LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $this->msgout = array("error" => 0, "data" => "");
        $this->dbClose($result);
    }
//*** CIERRA BASE DE DATOS
    private function dbClose($result="") 
    {
    	/*
        if( $result && is_object($result) )
        	$result->free_result();
        $this->_db_sys->close();
        */
    }
}
