<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class loginAccess extends Modelo
{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getDatauserlogin($data=array()) {
//        $key = md5(mb_strtolower($data->username, 'UTF-8').mb_strtolower($data->password,'UTF-8'));
        $sql = "SELECT idUsuario, nombreCompleto, tipoUsuario, imagen 
                FROM ".DB_NAME_SYS.".usuarios 
                WHERE usuario = '".mb_strtolower($data->username, 'UTF-8')."' AND
                      password = '".mb_strtolower($data->password, 'UTF-8')."' LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Acceso denegado: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>1001, "msg"=>"ERROR 1001. $msg", "action"=>"reload", "title"=>"Control de acceso");
            $this->dbCloseError($result);
            return false;
        }
        if($result->num_rows == 0){
            $this->msgout = array("error"=>1002, "msg"=>"El usuario no se encuentra registrado.", "action"=>"reload", "title"=>"Control de acceso");
            $this->dbCloseError($result);
            return false;
        }
        $dataresult = $result->fetch_assoc();
        $_SESSION["datauser"] = $dataresult;
        $this->setUltimoAceeso();
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "path"=>"./inicio.php");
        echo json_encode($this->msgout);
        $this->dbClose($result);
    }
    
    private function setUltimoAceeso() 
    {
        $fechaUltimoAcceso = date("Y-m-d h:i:s");
        $sql = "UPDATE ".DB_NAME_SYS.".usuarios SET ultimoAcceso = '".$fechaUltimoAcceso."'
                WHERE idUsuario = ".$_SESSION["datauser"]["idUsuario"];
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if( !$result )
            return false;
        $_SESSION["datauser"]["ultimoAcceso"] = date("d-m-Y h:i:s");
        //$result->free_result();
    }
    
    private function dbClose($result="") {
        //$result->free_result;
        $this->_db_sys->close();
    }
    
    private function dbCloseError($result="") {
        //$result->free_result;
        $this->_db_sys->close();
        echo json_encode($this->msgout);
    }
}
