<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class Usuarios extends Modelo
{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getLabelFunctions(){
        $section = array("win"=>"", "pro"=>"", "rep"=>"");
        $sql = "SELECT * FROM ".DB_NAME_SYS.".funciones WHERE mostrar = 1 ORDER BY orden";
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Error en dBase 3001: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>3001, "msg"=>"ERROR 3001. $msg", "action"=>"reload", "title"=>"Catálogo de Usuarios");
            $this->dbClose($result);
            return false;
        }
        $win = ""; $pro = ""; $rep = "";
        while($row = $result->fetch_assoc()){
            $idFunction = $row["idFuncion"] + 100;
            if(strcmp($row["tipo"], "V") == 0 && substr($row["menu"], 2, 4) != "00"){
               $win.= "<div class='flex-modal-produccion block-info-tab-checkBox'>".
                      "<div class='div-oLabel-tab'><label class='oLabel-left'>".utf8_encode($row['funcion'])."</label></div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='L' style='width: 30px;'>Leer</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='E' style='width: 30px;'>Escribir</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='B' style='width: 30px;'>Borrar</div>".
                      "</div>";
            }
            if(strcmp($row["tipo"], "P") == 0 && substr($row["menu"], 2, 4) != "00"){
               $pro.= "<div class='flex-modal-produccion block-info-tab-checkBox'>".
                      "<div class='div-oLabel-tab'><label class='oLabel-left'>".utf8_encode($row['funcion'])."</label></div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='L' style='width: 30px;'>SI</div>".
                      "<div class='' style='width: 100px;'><input class='oInput' type='checkbox' name='func_".$idFunction."[]' value='E' style='width: 30px;'>NO</div>".
                      "</div>";
            }
        }
        $section["win"] = $win;
        $section["pro"] = $pro;
        $section["rep"] = $rep;
        $this->msgout = array("error"=>0, "dataresult"=>$section);
        $this->dbClose($result);
    }
//*** INSERTA LOS DATOS DEL USUARIO
    private function preparaDatosInsert($data=array()) {
        $fechaEstatus = date("Y-m-d h:i:s");
        $values = "(NULL,".
                    "'".utf8_decode(mb_strtolower($data->usuario, 'UTF-8'))."',".
                    "'".utf8_decode(mb_strtolower($data->password,'UTF-8'))."',".
                    "'".utf8_decode($data->nombreCompleto)."',".
                    "'".$data->rfc."',".
                    "'".utf8_decode($data->puesto)."',".
                    "'".utf8_decode($data->telefonos)."',".
                    "'".utf8_decode($data->celulares)."',".
                    "'".utf8_decode($data->correos)."',".
                    $data->estatus.",".
                    "'".$_SESSION["datauser"]["nombreCompleto"]."',".
                    "'".$fechaEstatus."',".
                    "'','',". //** campos: ultimoAcceso, IPP
                    "'".$data->horario."',".
                    "'".$_SESSION["datauser"]["imagen"]."',".
                    "'".$data->tipoUsuario."')";
        return $values;
    }
    
    public function insertUser($data=array()) 
    {
        $values = $this->preparaDatosInsert($data);
        $sql = "INSERT INTO ".DB_NAME_SYS.".usuarios VALUES".$values;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $iduser = $this->_db_sys->insert_id;
        if(!$result){
            $this->msgout = array('error'=>3002, 'msg'=>"ERROR 3002. No se registró el usuario.", "action"=>"reload", "title"=>"Catálogo de Usuarios");
            unset($_SESSION["datauser"]["imagen"]);
            $this->dbClose($result);
            return;
        }
        if(!empty($data->functions))
            $this->updateFunctionsUser($iduser, $data->functions);
        $this->msgout = array('error'=>0, 'msg'=>"El registro se actualizó correctamente.", "action"=>"reload", "title"=>"Catálogo de Usuarios");
        unset($_SESSION["datauser"]["imagen"]);
        $this->dbClose($result);
    }
//*** EDITA LOS DATOS DEL USUARIO
    public function editUser($data=array()) {
        $sql = "SELECT * FROM ".DB_NAME_SYS.".usuarios WHERE idUsuario = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Error en dBase: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>3003, "msg"=>"ERROR 3003. $msg", "action"=>"reload", "title"=>"Catálogo de Usuarios");
            $this->dbClose($result);
            return false;
        }
        while($row = $result->fetch_assoc()){
            $_SESSION["datauser"]["fechaEstatus"] = $row["fechaEstatus"];
            $_SESSION["datauser"]["ultimoAcceso"] = $row["ultimoAcceso"];
            $fechaEstatus = $this->getDateFormat($row["fechaEstatus"], 1);
            $fechaUltimoAcceso = $this->getDateFormat($row["ultimoAcceso"], 1);
        //****
            $dataresult["idUsuario"] = $row["idUsuario"];
            $dataresult["usuario"] = utf8_encode($row["usuario"]);
            $dataresult["password"] = utf8_encode($row["password"]);
            $dataresult["nombreCompleto"] = utf8_encode($row["nombreCompleto"]);
            $dataresult["rfc"] = utf8_encode($row["rfc"]);
            $dataresult["puesto"] = utf8_encode($row["puesto"]);
            $dataresult["telefonos"] = $row["telefonos"];
            $dataresult["celulares"] = $row["celulares"];
            $dataresult["correos"] = $row["correos"];
            $dataresult["idEstatus"] = $row["idEstatus"];
            $dataresult["usuarioEstatus"] = utf8_encode($row["usuarioEstatus"]);
            $dataresult["fechaEstatus"] = $fechaEstatus;
            $dataresult["ultimoAcceso"] = $fechaUltimoAcceso;
            $dataresult["ipp"] = $row["ipp"];
            $dataresult["horario"] = $row["horario"];
            $dataresult["imagen"] = $_SESSION["datauser"]["rutaImagen"].$row["imagen"];
            $dataresult["tipoUsuario"] = $row["tipoUsuario"];
        }
        $dataresult["functions"] = $this->getFunctionsUser($data->key);
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"", "result"=>$dataresult);
        $this->dbClose($result);
    }
    
    private function preparaDatosUpdate($data=array()) {
        $values = "usuario = '".utf8_decode(mb_strtolower($data->usuario, 'UTF-8'))."',".
                  "password = '".utf8_decode(mb_strtolower($data->password,'UTF-8'))."',".
                  "nombreCompleto = '".utf8_decode($data->nombreCompleto)."',".
                  "rfc = '".utf8_decode($data->rfc)."',".
                  "puesto = '".utf8_decode($data->puesto)."',".
                  "telefonos = '".$data->telefonos."',".
                  "celulares = '".$data->celulares."',".
                  "correos = '".$data->correos."',".
                  "idEstatus = ".$data->estatus.",".
                  "usuarioEstatus = '".utf8_decode($data->usuarioEstatus)."',".
                  "fechaEstatus = '".$_SESSION["datauser"]["fechaEstatus"]."',".
                  "horario = '".$data->horario."',".
                  "imagen = '".$_SESSION["datauser"]["imagen"]."',".
                  "tipoUsuario = '".$data->tipoUsuario."'";
        return $values;
    }
    public function updateUser($data=array()) {
        $values = $this->preparaDatosUpdate($data);
        $sql = "UPDATE ".DB_NAME_SYS.".usuarios SET ".$values." WHERE idUsuario = ".$data->key." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if(!empty($data->functions))
            $this->updateFunctionsUser($data->key, $data->functions);
        $this->msgout = array("error"=>0);
    }
//*** BORRA LA IMAGEN DEL USUARIO
    public function deleteImage($data=array()) {
        $sql = "UPDATE ".DB_NAME_SYS.".usuarios SET imagen = '' WHERE idUsuario = ".$data->key." LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $this->msgout = array("error"=>0, "msg"=>"", "action"=>"", "title"=>"");
    }
    
//*** ASIGNA NUEVAS FUNCIONES AL USUARIO
    private function updateFunctionsUser($iduser=0,$functions=array()){
        //** ELIMINA RELACION FUNCIONES => USUARIOS
        $sql = "DELETE FROM ".DB_NAME_SYS.".usuariosfunciones WHERE idUsuario = ".$iduser;
        $result = $this->_db_sys->query($sql);
        $values = "";
        foreach ($functions as $value) {
            if($value["item"] < 100) continue;
            $values.= ($values != "")
                ? ",(".($value["item"]-100).",".$iduser.",'".$value["val"]."')"
                : "(".($value["item"]-100).",".$iduser.",'".$value["val"]."')";
        }
        unset($result); unset($sql);
        //** INSERTA NUEVA RELACION FUNCIONES => USUARIOS
        $sql = "INSERT INTO ".DB_NAME_SYS.".usuariosfunciones VALUES".$values;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
    }

    private function getFunctionsUser($key=0){
        $element = (object)array();
        $funciones = array();
        $sql = "SELECT idFuncion, permisos
                FROM ".DB_NAME_SYS.".usuariosfunciones
                WHERE idUsuario = ".$key." ORDER BY idFuncion";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        if($result)
            while($row = $result->fetch_assoc()){
                $element = (object)array(func => $row["idFuncion"]+100, permiso => $row["permisos"]);
                array_push($funciones,$element);
            }
        return ($funciones);
    }

    public function deleteUser($data=array()){
        $idUser= $data->key;
        $sql = "DELETE FROM ".DB_NAME_SYS.".usuarios  WHERE idUsuario = ".$idUser." LIMIT 1";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        // Elimina las funciones relacionadas 
        $sql = "DELETE FROM ".DB_NAME_SYS.".usuariosfunciones WHERE idUsuario = ".$idUser;
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        $this->dbClose($result);
        $this->msgout = array("error"=>0,'msg'=>"El registro se eliminó correctamente.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
    }

    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}
