<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class menu extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getlinksUserMenu($idUser=""){
            $sql = "SELECT uf.*, fu.menu, fu.programa
                    FROM ".DB_NAME_SYS.".usuariosfunciones uf, ".DB_NAME_SYS.".funciones fu 
                    WHERE fu.idFuncion = uf.idFuncion AND uf.idUsuario = $idUser
                    ORDER BY fu.menu";
            $result = $this->_db_sys->query($sql);
            $list = array();
            while($row = $result->fetch_object()){
                    $arr = (object)array("menu"=>$row->menu, "programa"=>$row->programa, "permisos"=>$row->permisos);
                    array_push($list, $arr);
            };
            $_SESSION["datauser"]["funciones"] = $list;
            $this->msgout = array("error"=>0, "msg"=>"", "title"=>"", "dataresult"=>$list);
            $this->dbClose($result);
    }

    public function getlinksAdminMenu(){
        $sql = "SELECT menu, programa FROM ".DB_NAME_SYS.".funciones WHERE mostrar = 1 ORDER BY menu";
        $result = $this->_db_sys->query($sql);
        if ($this->_db_sys->errno != 0) {
            $msg = 'Violación a la dBase: '.$this->_db_sys->error. "\n";
            $this->msgout = array("error"=>2001, "msg"=>"ERROR 2001. $msg", "action"=>"reload", "title"=>"Configuración de menú del usuario");
            $this->dbCloseError($result);
            return false;
        }
        $list = array();
        while($row = $result->fetch_object()){
            $arr = (object)array("menu"=>$row->menu, "programa"=>$row->programa, "permisos"=>'LEB');
            array_push($list, $arr);
        };
        $_SESSION["datauser"]["funciones"] = $list;
//print_r($list);exit();
        $this->msgout = array("error"=>0, "msg"=>"", "title"=>"", "dataresult"=>$list);
        $this->dbClose($result);
    }
    
    public function regresaPermisosUsuario() {
        $arr = $_SESSION["datauser"]["funciones"];
        foreach ($arr as $key => $val) {
            if($val->menu == $_SESSION["datauser"]["menuaccess"]){ return($val->permisos); }
        }
    }
    
    public function verificaAutorizacion($permisos="") {
        $autoriza = strpos($permisos, 'E');
        return ($autoriza ? TRUE : FALSE);
    }

    private function dbClose($result="") {
        $result->free_result();
        $this->_db_sys->close();
    }
}
