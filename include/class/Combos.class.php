<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class Combos extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function getAllwidgetproduct() {
        $options = "<option value=0>Selecciona...</option>";
        $sql = "SELECT id_artref, clave_prod FROM ".DB_NAME_SYS.".artref ORDER BY 'idx_claveProducto'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["id_artref"].'">'.utf8_encode($row["clave_prod"]).'</option>';
        }
        echo $options;
        $this->dbCloseobject($result, $this->_db_sys);
    }

    public function selectMarca() 
    {
        $options = '<option value="ESCOLAR" checked>ESCOLAR</option>';
        $options.= '<option value="COLEGIAL">COLEGIA</option>';
        $options.= '<option value="ESCOCES">ESCOCES</option>';
        $options.= '<option value="FIORA">FIORA</option>';
        echo $options;
    }
    public function selectEstatusProducto($keycatalog=0) {
        $sql        = "SELECT idEstatus, nombre FROM ".DB_NAME_SYS.".estatus WHERE catalogo = $keycatalog ORDER BY orden";
        $result     = $this->_db_sys->query($sql);
        $options    = '';
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["idEstatus"].'">'.utf8_encode($row["nombre"]).'</option>';
        }
        echo $options;
        $this->dbCloseobject($result, $this->_db_sys);
    }
    
    public function getImageproduct($keyproduct=0) {
        $sql = "SELECT ruta FROM ".DB_NAME_SYS.".artref WHERE id_artref = $keyproduct LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        $dataresult = (!empty($row["ruta"]))
                ? (object) array("error" => "", "img" => $row["ruta"])
                : (object) array("error" => 'ERROR. El producto no cuenta con imágen.', "img" => '');
        return $dataresult;
    }
    
    public function selectTipoUsuario() {
        $options.= '<option value="U" checked>Usuario</option>';
        $options.= '<option value="A">Administrador</option>';
        echo $options;
    }
    
    public function selectEstatusUsuario($keycatalog=0) {
        $sql 		= "SELECT idEstatus, nombre FROM ".DB_NAME_SYS.".estatus WHERE catalogo = $keycatalog ORDER BY orden";
        $result 	= $this->_db_sys->query($sql);
        $options 	= '';
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["idEstatus"].'">'.utf8_encode($row["nombre"]).'</option>';
        }
        echo $options;
        $this->dbCloseobject($result, $this->_db_sys);
    }

    public function selectOrdenesCompra() {
        $sql 		= "SELECT idOrdenCompra, ordenCompra FROM ".DB_NAME_SYS.".ordenescompra WHERE archivotxt = 'No'";
        //echo $sql;
        $result 	= $this->_db_sys->query($sql);
        $options 	= '';
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["idOrdenCompra"].'">'.utf8_encode($row["ordenCompra"]).'</option>';
        }
        echo $options;
        $this->dbCloseobject($result, $this->_db_sys);
    }
    public function inputClienteTarimas($type = null) {
        $sql = "SELECT * FROM silan.clientes where noCliente = (SELECT valor FROM silan.generales g where g.llave = 'parisinaNumero')";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        if( $type == 'object' )
        	return $row;
        echo $row["nombre"];
        $this->dbCloseobject($result, $this->_db_sys);
    }
    public function inputDestinoTarimas($destino) {
        $sql = "SELECT d.nombre FROM silan.destinos d 
        LEFT JOIN silan.ordenescompra oc ON oc.idOrdenCompra = ".$destino." WHERE d.idDestino = oc.idDestino;";
        $result = $this->_db_sys->query($sql);

        $row = $result->fetch_assoc();
        echo utf8_encode($row['nombre']);
        $this->dbCloseobject($result, $this->_db_sys);
    }

    public function selectDestinosOC($keycatalog=0) {
        $sql1 = "SELECT idEstatus, nombre FROM ".DB_NAME_SYS.".estatus WHERE catalogo = $keycatalog ORDER BY orden";
        $sql = "select idDestino, nombre from ".DB_NAME_SYS.".destinos order by nombre";
        $result = $this->_db_sys->query($sql);
        $options = '';
        while($row = $result->fetch_assoc()){
            $options .= '<option value="'.$row["idDestino"].'">'.utf8_encode($row["nombre"]).'</option>';
        }
        echo $options;
        $this->dbCloseobject($result, $this->_db_sys);
    }

    public function getAllWidgetProductList() {
        $list = "";
        //$sql = "SELECT id_artref, clave_prod FROM " . DB_NAME_SYS . ".artref ORDER BY 'idx_claveProducto'";
        $sql = "SELECT id_artref, clave_prod FROM " . DB_NAME_SYS . ".artref ORDER BY clave_prod";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list .= utf8_encode($row["clave_prod"]) . ",";
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }
    
    private function dbCloseobject($result, $oMysqli) 
    {
        //$result->free_result();
        //$oMysqli->close();
    }
}
