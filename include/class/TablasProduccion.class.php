<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
require_once(dirname(__FILE__)."/dbConnections.inc.php");
class TablasProduccion extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
    
    public function setTablaFormulas($filter="") {
        $sql = "SELECT telas.idFormulas, telas.claveProducto, art.claveAnterior, art.nombreProducto, art.color, 
                est.nombre nombreEstatus
                FROM ".DB_NAME_SYS.".formulastelas telas, ".DB_NAME_SYS.".artref art, ".DB_NAME_SYS.".estatus est
                WHERE art.clave_prod = telas.claveProducto AND est.idEstatus = telas.idEstatus AND                
                      (telas.claveProducto LIKE '%".$filter."%' OR
                      art.claveAnterior LIKE '%".$filter."%' OR
                      art.nombreProducto LIKE '%".$filter."%' OR
                      art.color LIKE '%".$filter."%' OR
                      est.nombre LIKE '%".$filter."%')
                ORDER BY claveProducto";
        $this->log($sql);
        $result = $this->_db_sys->query($sql);
        while($record = $result->fetch_assoc()){
            $rows.= "<tr class='renglonesGrid' id=".$record["idFormulas"].">";
            $rows.= "<td class='td-col05-col01'>".$record["claveProducto"];
            $rows.= "<td class='td-col05-col02'>".$record["claveAnterior"];
            $rows.= "<td class='td-col05-col03'>".utf8_encode($record["nombreProducto"]);
            $rows.= "<td class='td-col05-col04'>".utf8_encode($record["color"]);
            $rows.= "<td class='td-col05-col05'>".utf8_encode($record["nombreEstatus"]);
            $rows.= "</td></tr>";
        }
        echo $rows;
        $this->dbClose($result);
    }
    
    private function dbClose($result="") {
        $result->free_result;
        $this->_db_sys->close;
    }
}