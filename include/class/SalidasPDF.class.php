<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
include(dirname(__FILE__)."/dbConnections.inc.php");
class SalidasPDF extends Modelo {
    private $idSalida=0;
    private $fechaSalida;
    public $records;

    public function __construct(){
        parent::__construct();
        $this->idSalida = $_GET["keyRecord"];
        $this->SetSqlFechaSalida();
        $this->SetSqlSalidas();
    }
    
    private function SetSqlFechaSalida() {
        $sql = "SELECT fechaSalida FROM ".DB_NAME_SYS.".piezas WHERE documentoSalida = ".$this->idSalida." LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_object();
        $this->fechaSalida = $row->fechaSalida;
    }
    
    private function SetSqlSalidas(){
        /*
        $sql = "SELECT
                    SUBSTRING_INDEX(piezas.clavePieza,'-',1) articulo, 
                    SUBSTRING_INDEX(SUBSTRING_INDEX(piezas.clavePieza,'-',-2),'-',1) referencia,
                    artref.color, 
                    SUBSTRING_INDEX(piezas.clavePieza,'-',4) poliza, 
                    LPAD(SUBSTRING_INDEX(piezas.clavePieza,'-',-1), 2, '0') etiqueta,
                    piezas.metros metros, 
                    FORMAT(piezas.kilos,3) kilos, 
                    SUM(piezas.metros) totmts,
                    SUM(piezas.kilos) totkilos
                FROM ".DB_NAME_SYS.".piezas piezas, ".DB_NAME_SYS.".artref artref
                WHERE artref.clave_prod = SUBSTRING_INDEX(piezas.clavePieza,'-',1) AND
                    piezas.documentoSalida = ".$this->idSalida." 
                GROUP BY articulo,etiqueta WITH ROLLUP";
            */
        $sql=   "SELECT
                    SUBSTRING_INDEX(piezas.clavePieza,'-',1) articulo, 
                    SUBSTRING_INDEX(SUBSTRING_INDEX(piezas.clavePieza,'-',-2),'-',1) referencia,
                    artref.color, 
                    SUBSTRING_INDEX(piezas.clavePieza,'-',4) poliza, 
                    LPAD(SUBSTRING_INDEX(piezas.clavePieza,'-',-1), 2, '0') etiqueta,
                    piezas.metros metros, 
                    FORMAT(piezas.kilos,3) kilos
                FROM silan.piezas piezas, silan.artref artref
                WHERE artref.clave_prod = SUBSTRING_INDEX(piezas.clavePieza,'-',1) AND
                    piezas.documentoSalida = ".$this->idSalida."
                order BY articulo";
        $result = $this->_db_sys->query($sql);
        while($rows = $result->fetch_object()){
            $arrdata[] = $rows;
        }
        $this->records = $arrdata;
    }

    public function getFechaSalida() {
        return $this->getDateFormat($this->fechaSalida, 0, false, true);
    }
    
    public function getFolioSalida() {
        return $this->idSalida;
    }
    
    public function getTotalRecords() {
        return count($this->records)-1;
    }
    
    public function getRecordSalida($idx=0,$campo='') {
        return $this->records[$idx]->$campo;
    }
    
}