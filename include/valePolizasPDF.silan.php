<?php
/* 
 * Libreria creada por: Manuel Luna / Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
require (dirname(__FILE__)."/lib/fpdf181/fpdf.php");
class PDF extends FPDF{
    public $vales;
    
    function __construct($orientation = 'P', $unit = 'mm', $size = 'A4') {
        parent::__construct($orientation, $unit, $size);
        require (dirname(__FILE__)."/class/ValePolizasPDF.class.php");
        $this->vales = new ValePolizasPDF();
    }
            
    function Header() {
        $this->SetFont('Arial','B',16);
        $this->SetXY(10, 20);
        $this->Cell(150, 10, 'LANERA MODERNA SA DE CV', 0, 0, 'C');
        $this->SetFont('Arial','B',10);
        $this->Cell(23,5,"Fecha: ",0,0,'L');
        $this->SetFont('Arial','',10);
        $this->Cell(23,5,$this->vales->getFechaNumerica(),0,1,'L');
        $this->SetFont('Arial','B',10);
        $this->SetXY(10, 30);
        $this->Cell(100, 10, 'VALE DE HILO DE LA POLIZA', 0, 0, 'C');
        $this->Cell(50, 10, $this->vales->getnumeroPoliza(), 0, 1, 'C');
    }
    
    function tituloColumnas() {
        $this->SetFont('Arial','B',10);
        $this->SetXY(10, 45);
        $this->Cell(28, 10, 'CLAVE', 0, 0, 'C');
        $this->Cell(28, 10, 'COLOR', 0, 0, 'C');
        $this->Cell(28, 10, 'COMPOSICION', 0, 0, 'C');
        $this->Cell(28, 10, 'TITULO', 0, 0, 'C');
        $this->Cell(28, 5, 'KG EN', 0, 2, 'C');
        $this->Cell(28, 5, 'URDIMBRE', 0, 0, 'C');
        $this->SetXY(150, 45);
        $this->Cell(28, 5, 'KG EN', 0, 2, 'C');
        $this->Cell(28, 5, 'TRAMA', 0, 0, 'C');
        $this->SetXY(178, 45);
        $this->Cell(28, 10, 'TOTAL KG', 0, 0, 'C');
    }
    
    function contenidoTabla() {
        $totalVale = 0;
        $this->vales->creaValePolizaUrdido();
        $this->vales->creaValePolizaTrama();
        $this->SetFont('Arial','',10);
        $this->SetXY(10, 55);
        for($j=0, $size=$this->vales->getTotalHilosVale(); $j<=$size-1; $j++){
            $this->Cell(28, 5, $this->vales->arrVale[$j]->claveHilo, 0, 0, 'L');
            $this->Cell(28, 5, $this->vales->arrVale[$j]->colorHilo, 0, 0, 'L');
            $this->Cell(28, 5, $this->vales->arrVale[$j]->composicion, 0, 0, 'L');
            $this->Cell(28, 5, $this->vales->arrVale[$j]->titulo, 0, 0, 'C');
            $this->Cell(28, 5, number_format($this->vales->arrVale[$j]->kgUrdimbre,2), 0, 0, 'R');
            $this->Cell(28, 5, number_format($this->vales->arrVale[$j]->kgTrama,2), 0, 0, 'R');
            $this->Cell(28, 5, number_format($this->vales->arrVale[$j]->kgTotalHilo,2), 0, 0, 'R');
            $totalVale+= number_format($this->vales->arrVale[$j]->kgTotalHilo,2);
            $this->SetXY(10,$this->GetY()+5);
        }
        $this->SetXY(178, $this->GetY());
        $this->Cell(28, 5, number_format($totalVale,2), 0, 0, 'R');
    }
}
$pdf = new PDF('P','mm','Letter');
$pdf->AddPage();
$pdf->SetMargins(10, 20, 9.9);
$pdf->tituloColumnas();
$pdf->contenidoTabla();
$filePDF = '../tmp/temp';
$pdf->Output('F',$filePDF.'.pdf');
echo './tmp/temp.pdf';