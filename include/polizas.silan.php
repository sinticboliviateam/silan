<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/Polizas.class.php");
$polizas = new Polizas();
$keyPolizas = (object)$_REQUEST["data"];
switch ($keyPolizas->evento) {
    case 'usuarioRegistro':
        $polizas->msgout = array(
            "fechaRegistro" => $polizas->fechaActual_conFormato(),
            "usuarioRegistro" => $polizas->nombreUsuario(),
            "estatus" => "Vigente",
            "fechaEstatus" => $polizas->fechaActual_conFormato(),
            "usuarioEstatus" => $polizas->nombreUsuario()
        );
        break;
    case 'nuevaPoliza':
        $polizas->datosNuevaPoliza($keyPolizas->keyproduct);
        break;
    case "insertaPoliza":
        $polizas->insertaPoliza($keyPolizas);
        break;
    case "editaPoliza":
        $polizas->editaDatosPoliza($keyPolizas->key);
        break;
    case "cancelaPoliza":
        $polizas->cancelaDatosPoliza($keyPolizas->key);
        break;
    default:
        print_r($keyPolizas);
        break;
}
echo json_encode($polizas->msgout);