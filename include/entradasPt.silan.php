<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
include (dirname(__FILE__)."/class/EntradasPt.class.php");
$entradas = new EntradasPt();
$keyEntradas = (object)$_REQUEST["data"];
switch ($keyEntradas->evento) {
    case 'usuarioRegistro':
        $entradas->msgout = array(
            "fechaRegistro" => $entradas->fechaActual_conFormato(),
            "usuarioRegistro" => $entradas->nombreUsuario()
        );
        break;
    case 'validacionUnico':
        $entradas->validacionUnico($keyEntradas);
        break;
    case 'listaPiezasSinEntrada':
        $entradas->listaPiezasSinEntrada();
        break;
    case 'entradasPt':
        $entradas->entradaPiezas($keyEntradas);
        break;
    default:
        print_r($keyEntradas);
        break;
}
echo json_encode($entradas->msgout);