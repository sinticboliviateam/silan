<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//ini_set('display_errors', 1);error_reporting(E_ALL);
require_once dirname(__FILE__) . '/functions.php';
include(dirname(__FILE__)."/class/PolizasPDF.class.php");
$datosPoliza = new PolizasPDF();
include("./lib/ZebraPrint/PrintSend.php");
include("./lib/ZebraPrint/PrintSendLPR.php");
try
{
	$ip = "10.0.0.111";
	sb_verificar_impresora($ip, 515);
	//die($ip);
	$lpr = new PrintSendLPR();
	$lpr->setHost($ip);//Put your printer IP here //20180219 Zebra
	//$lpr->setPort('9100'); //6101
	$archivo = '../tmp/labelZPLII.txt';
	$noTelar = $datosPoliza->getnumeroTelar();
	$composicion = $datosPoliza->getComposicionArticulo();
	$colorArticulo = $datosPoliza->getColorArticulo();
	$claveProducto = $datosPoliza->getClaveProducto();
	$noPoliza = $datosPoliza->getnumeroPoliza();
	$zpl = "";
	for($pzas=1; $pzas<=$datosPoliza->getnumeroPiezas(); $pzas++){ //
		$zpl.= "^XA\n";
		$zpl.= "^MMT\n";
		$zpl.= "^PW609\n";
		$zpl.= "^LL0203\n";
		$zpl.= "^LS0\n";
		$zpl.= "^FT146,172 ^A0N,28,28 ^FH\^FD".$noTelar." ^FS\n"; // NUMERO DE TELAR
		$zpl.= "^FT231,172 ^A0N,28,28 ^FH\^FD".$composicion." ^FS\n"; // COMPOSICION
		$zpl.= "^FT46,172 ^A0N,28,28 ^FH\^FDTelar: ^FS\n"; // LEYENDA
		$zpl.= "^FT231,120 ^A0N,28,28 ^FH\^FD".$colorArticulo." ^FS\n"; // COLOR
		$zpl.= "^FT45,120 ^A0N,28,28 ^FH\^FD".$claveProducto." ^FS\n"; // CLAVE DEL PRODUCTO
		$zpl.= "^FT45,66 ^A0N,28,28 ^FH\^FD".$noPoliza." ^FS\n"; // CLAVE DE LA POLIZA
		$zpl.= "^FT377,66 ^A0N,28,28 ^FH\^FD".$pzas." ^FS\n"; // NUMERO DE PIEZAS
		$zpl.= "^PQ1,0,1,Y ^XZ\n";
	}
	$myfile = fopen($archivo, "w") or die("Unable to open file!");
	fwrite($myfile, $zpl);
	fclose($myfile);
	//echo "<h1>PrintSendLPR example</h1>";
	$lpr->setData(file_get_contents($archivo));//Path to file, OR string to print.
	$res = $lpr->printJob("etiquetas");//If your printer has a built-in printserver, it might just accept anything as a queue name.
	echo "<h3>Debug</h3><pre>".$lpr->getDebug()."</pre>";
	http_response_code(200);
	header('Content-type: application/json');
	print json_encode(['status' => 'ok', 'mensaje' => 'Impresion enviada correctamente', 'res' => $res]);
}
catch(Exception $e)
{
	http_response_code(401);
	header('Content-type: application/json');
	print json_encode(['status' => 'error', 'error' => $e->getMessage()]);
}
