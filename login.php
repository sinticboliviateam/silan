<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
$_SESSION = array();
session_destroy();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>SiLan</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" media="screen" href="./css/login.css" />
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/login.class.js"></script>
        <script>
            $(document).ready(function(){
                $silan_login.formValidate();
                $silan_login.widgetEventForm();
                $("#username").focus();
            });
        </script>
    </head>
    <body>
        <div class="container-central" style="height: 515px;">
            <form id="formLogin" method="post">
                <div class="box-login">
                    <div class="data01">
                        <input id="username" name="username" class="ui-widget oInput lower" style="font-size: 1em; opacity:0.75;" type="text" required maxlength="15" placeholder="usuario" tabindex="1"></div>
                    <div class="data01">
                        <input id="password" name="password" class="ui-widget oInput lower" style="font-size: 1em; opacity:0.75;" type="password" required maxlength="25" placeholder="contraseña" tabindex="2"></div>
                </div>
            </form>
            <div id="modal-message"></div>
        </div>
    </body>
</html>