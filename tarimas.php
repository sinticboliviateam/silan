<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
require_once './include/combos.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Tarimas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/tarimas.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/tarimas.class.js"></script>
        <script src="./js/class/tarimasValidate.class.js"></script>
        <script>
            $(document).ready(function(){
                $setmenu.getLinksUser();
                Tarimas.ready();
//                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
            });
        </script>
    </head>
    <body>
        <audio id="error" src="sonidos/error.wav" autostart="false" ></audio>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Tarimas</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <button id="btnNuevaTarima" class=""></button>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col06-col01">No. Tarima</th>
                            <th class="th-col06-col02">Orden compra</th>
                            <th class="th-col06-col03">Fecha</th>
                            <th class="th-col06-col03">Piezas</th>
                            <th class="th-col06-col03">Mts.</th>
                            <th class="th-col06-col03">Estatus</th>
                            <th class="th-col06-col03">SKU</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div id="dialogTarima" style="display: none;">
                <form id="form-datos-tarima" action="" method="post">
                    <div class="flex-modal-tacome">
                        <div class="flex-modal-left-tab" style="width: 365px;">
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="noTarima" class="oLabel-left">No. Tarima</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="noTarima" name="noTarima" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="noOrdenCompra" class="oLabel-left">No. Orden compra</label></div>
                                <div class="div-dataInput-tab">
                                    <select id="noOrdenCompra" name="noOrdenCompra" class="oSelect-normal-tab" tabindex="13"><?php echo $combos->selectOrdenesCompra(); ?></select></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="cliente" class="oLabel-left">Cliente</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="cliente" name="cliente" class="oInput campoRequerido" data-auto="" type="text" value="<?php echo $combos->inputClienteTarimas(); ?>" required readonly tabindex="1"></div>
                            </div>

                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="destino" class="oLabel-left">Destino</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="destino" name="destino" class="oInput campoRequerido" data-auto="" type="text" value="" required readonly tabindex="1"></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab" style="">
                                    <label for="noPieza" class="oLabel-left">Número de pieza</label></div>
                                <div class="div-dataInput-tab" style="">
                                    <input id="noPieza" name="noPieza" class="oInput campoRequerido" type="number" value="" required tabindex="2"></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="fechaSalida" class="oLabel-left">Fecha de salida</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="fechaSalida" name="fechaSalida" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="usuarioSalida" class="oLabel-left">Usuario</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="usuarioSalida" name="usuarioSalida" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
                            </div>
                        </div>
                        <div class="flex-modal-left-tab" style="width: 365px;">
                            <div class="tabs-usuarios-botones" style="">
                                <div id="guardaRegistro" class=""></div>
                                <div class="cerrar-tabs"></div>
                            </div>
                            <hr>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="listaPiezas" class="oLabel-left">Claves de las piezas</label></div>
                                <div id="listaPiezas" class="div-oList"></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab" style="width: 100px;">
                                    <label for="no_piezas" class="oLabel-left">No. de piezas</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="no_piezas" name="no_piezas" class="oInput" style="text-align: right; font-weight: bold; width: 80px;" type="text" readonly value="0"></div>
                            </div>
                        </div>
                    </div>
                    <p>
						<button type="submit" id="btn-guardar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-disk"></span> Guardar
						</button>
						<a href="javascript:;" id="btn-borrar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-trash"></span> Borrar</a>
						<a href="javascript:;" id="btn-regresar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-arrowreturnthick-1-w"></span> Regresar
						</a>
					</p>
                </form>
            </div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
