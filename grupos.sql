/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.1.53-community-log : Database - silan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`silan` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `silan`;

/*Table structure for table `grupos` */

DROP TABLE IF EXISTS `grupos`;

CREATE TABLE `grupos` (
  `grupo` varchar(50) NOT NULL,
  `codigo` char(1) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `tipo2` varchar(100) NOT NULL,
  `composicion` varchar(100) NOT NULL,
  `calibre` varchar(10) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grupos` */

insert  into `grupos`(`grupo`,`codigo`,`nombre`,`tipo2`,`composicion`,`calibre`) values ('Especial','A','Dibujos especiales','Especial','Especial','Diversos'),('Fiora','C','Sarga Fiora','Sarga de acrílico','100% acrílico','2/30'),('Escolar','E','Escolar','Tafetán de acrílico','100% acrílico','2/30'),('Escocés','T','tafetán de poliéster acrrílico','Tafetán de poliéster acrílico','70% Poliéster 30% Acrílico','2/44'),('Colegial','X','Sarga de poliéster acrílico','Sarga de poliéster acrílico','70% Poliéster 30% Acrílico','2/44');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
