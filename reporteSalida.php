<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
require_once './include/class/menu.class.php';
$autPermisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02060000";
$permisos = $autPermisos->regresaPermisosUsuario();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Reporte de Salidas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/repSalidas.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/reporteSalidas.class.js"></script>
        <script>
            $(document).ready(function(){
                $setmenu.getLinksUser();
                reporteSalida.read();
//                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.75em"});
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            
            <div class="cleared" style="display: block; width: 50%;"></div>
            <div class="tableRecords" style="width: 50%;">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col04-col01">Fecha de salida</th>
                            <th class="th-col04-col02">Número de salida</th>
                            <th class="th-col04-col03">Número de cliente</th>
                            <th class="th-col04-col04">Total de piezas</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
