<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
//unset($_SESSION["record"]);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuarios</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/usuarios.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/usuarios.class.js"></script>
        <script src="./js/class/usuariosValidate.class.js"></script>
	<!--<script src="./js/telemetrik.js"></script>-->
<!--        <script src="./js/dateFormats.js"></script>-->

        <!--<script src="./cssmenu/script.js"></script>-->
        <!--<script src="./js/usuariosN.js"></script>-->
        <script type="text/javascript">
            $(document).ready(function() {
//                $("nav #menu").find(".active").removeClass();
//                var links = $("nav #menu > li");
//                $(links[3]).find("a").first().addClass("active");
                $setmenu.getLinksUser();
                $silan_usuarios.widgetEventUsuarios();
                $silan_usuarios._initform();
                $silan_usuarios.buscarRegistro();
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Usuarios</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <button id="btnNuevoUsuario" class=""></button>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col06-col01">Nombre</th>
                            <th class="th-col06-col02">Usuario</th>
                            <th class="th-col06-col03">RFC</th>
                            <th class="th-col06-col04">Puesto</th>
                            <th class="th-col06-col05">Estatus</th>
                            <th class="th-col06-col06">Último acceso</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogUser" style="display: none;">
                <div id="tabs-usuarios" style="width: 95%; height: auto; margin: 0 auto;">
                    <ul>
                        <li><a href="#tabs-datos-usuarios">Datos</a></li>
                        <li><a href="#tabs-funciones-usuarios">Funciones</a></li>
                    </ul>
                    <div id="tabs-datos-usuarios"></div>
                    <div id="tabs-funciones-usuarios"></div>     
                </div>
            </div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>