<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('html_errors', false);
ini_set('display_startup_errors', 1);

//echo "\$_SESSION['datauser']= ".array_key_exists("datauser", $_SESSION)."<br><br>";
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");


//var_dump($_SESSION);
//exit;

require_once './include/class/menu.class.php';
$autPerimisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02010000";
$permisos = $autPerimisos->regresaPermisosUsuario();
//echo '<pre>';
//print_r($_SESSION["datauser"]["funciones"]);
//echo '</pre>';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Telas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/formulasTelas.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/formulasTelas.class.js"></script>
        <script src="./js/class/formulasTelasValidate.class.js"></script> 
        <script src="./js/class/autocomplete.class.js"></script> 
        <script>
            $(document).ready(function(){
                $setmenu.getLinksUser();
                $silan_formulasTelas.widgetEventFormulas();
                $silan_formulasTelas._initformFormulas();
                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Producto</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <?php 
                        $autorizado = strpos($permisos, 'E');
                        if($autorizado){ ?> 
                            <div id="btnNuevoProducto" class="" style="display:none;"></div> 
                    <?php } ?>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col05-col01">Clave del producto</th>
                            <th class="th-col05-col02">Clave anterior</th>
                            <th class="th-col05-col03">Nombre del producto</th>
                            <th class="th-col05-col04">Color</th>
                            <th class="th-col05-col05">Estatus</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogFormulas" style="display: none;">
                <div id="tabs-formulas" style="width: 95%; height: auto; margin: 0 auto;">
                    <ul>
                        <li><a href="#tabs-datos-formula">Datos generales</a></li>
                        <li><a href="#tabs-tecnicos-formula">Datos técnicos</a></li>
                        <li><a href="#tabs-urdimbre-formula">Curso de urdimbre</a></li>
                        <li><a href="#tabs-trama-formula">Curso de trama</a></li>
                        <li><a href="#tabs-orillos-formula">Curso del orillo</a></li>
                    </ul>
                    <div id="tabs-datos-formula"></div>
                    <div id="tabs-tecnicos-formula" class="ocultaTabs"></div>
                    <div id="tabs-urdimbre-formula" class="ocultaTabs"></div>
                    <div id="tabs-trama-formula" class="ocultaTabs"></div>
                    <div id="tabs-orillos-formula" class="ocultaTabs"></div>
                </div>
            </div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
