<?php
?>
<div class="div-buscaRegistros">
	<div class="buscaRegistros">
		<h3>Piezas Anteriores</h3>
		<form action="index.php" method="get">
			<input type="hidden" name="mod" value="tacome" />
			<input type="hidden" name="task" value="ObtenerRegistros" />
			<input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" tabindex="1" value="<?php print $filtro; ?>">
			<span id="status-busca"></span>
		</form>
	</div>
	
</div>
<div class="cleared" style="display: block; width: 95%;"></div>
	<div class="tableRecords" style="height:400px;">
		<table id="tabla-piezas" class="">
		<thead class="tableHead">
		<tr class="">
			<th>Documento de Entrada</th>
			<th class="th-col06-col01">Clave Pieza</th>
			<th class="">Num. Poliza</th>
			<th class="">Kilos</th>
			<th class="">Metros</th>
			<th class="">Impresi&oacute:n</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<?php if( $escribir ): ?>
<div id="dialogTacome" style="display: none;">
	<form id="form-datos-tacome">
		<input type="hidden" id="idPieza" name="idPieza" value="" />
	    <div class="flex-modal-tacome">
	        <div class="flex-modal-left-tab" style="width: 365px;">
	            <div class="block-info-tab">
	                <div class="div-oLabel-tab">
	                    <label for="clavePieza" class="oLabel-left">Clave de la pieza</label></div>
	                <div class="div-dataInput-tab">
	                    <input id="clavePieza" name="clavePieza" class="oInput campoRequerido" data-auto="" type="text" value="" required maxlength="60" tabindex="1"></div>
	            </div>
	            <div class="block-info-tab">
	                <div class="div-oLabel-tab" style="width: 120px;">
	                    <label for="metros" class="oLabel-left">Metros por pieza</label></div>
	                <div class="div-dataInput-tab" style="width: 75px; text-align: right;">
	                    <input id="metros" name="metros" class="oInput campoRequerido" type="number" min="1" max="200" step="any" value="37.00" required tabindex="2"></div>
	            </div>
	            <div class="block-info-tab">
	                <div class="div-oLabel-tab" style="width: 120px;">
	                    <label for="kilos" class="oLabel-left">Kilos</label></div>
	                <div class="div-dataInput-tab" style="width: 75px;">
	                    <input id="kilos" name="kilos" class="oInput campoRequerido" type="number" min="0.500" max="50" step="any" value="1" required tabindex="3"></div>
	            </div>
	            <div class="block-info-tab">
	                <div class="div-oLabel-tab">
	                    <label for="fechaTacome" class="oLabel-left">Fecha tacome</label></div>
	                <div class="div-dataInput-tab">
	                    <input id="fechaTacome" name="fechaTacome" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
	            </div>
	            <div class="block-info-tab">
	                <div class="div-oLabel-tab">
	                    <label for="usuarioTacome" class="oLabel-left">Usuario</label></div>
	                <div class="div-dataInput-tab">
	                    <input id="usuarioTacome" name="usuarioTacome" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
	            </div>            
	        </div>
	        <div class="flex-modal-left-tab" style="width: 365px;">
	            <div class="tabs-usuarios-botones" style="">
	                
	                <?php if( $leer && $escribir): ?>
	                <div id="btn-imprimir-tacome" class=""></div>
	                <?php endif; ?>
	                <div class="cerrar-tabs"></div>
	            </div>
	            <hr>
	            <div class="baseImagen" style="margin-top: 10px;">
	                <div id="div-imagen" class="imagenRegistro"></div>
	            </div>
	        </div>
	    </div>
	</form>
</div><!-- id="dialogTacome" -->
<?php endif; ?>
<script src="modulos/tacome/js/antiguo.js"></script>
<script src="modulos/tacome/js/tacomeValidate.class.js"></script>
<script src="./js/class/autocomplete.class.js"></script>