<?php
class TacomeControlador extends Controlador
{
	/**
	 * 
	 * @var TacomeModelo
	 */
	protected	$tacomeModelo;
	/**
	 * 
	 * @var PiezasModelo
	 */
	protected	$piezasModelo;
	/**
	 * 
	 * @var menu
	 */
	protected	$autPerimisos;
	
	public function Init()
	{
		$this->tacomeModelo = $this->CargarModelo();
		$this->piezasModelo	= $this->CargarModelo(null, 'piezas');
		$this->autPerimisos = new menu();
	}
	public function TaskDefault()
	{
		$filtro		= filter_input(INPUT_GET, 'filter');
		$_SESSION["datauser"]["menuaccess"] = "02040000";
		$permisos 	= $this->autPerimisos->regresaPermisosUsuario();
		//var_dump(/*$_SESSION["datauser"]["funciones"],*/ $permisos);
		$leer		= strstr($permisos, 'L') ? true : false;
		$escribir	= strstr($permisos, 'E') ? true : false;
		$borrar		= strstr($permisos, 'B') ? true : false;
		
		$this->CargarVista('default', get_defined_vars());
	}
	public function TaskAntiguo()
	{

		$filtro		= filter_input(INPUT_GET, 'filter');
		$_SESSION["datauser"]["menuaccess"] = "02080500";
		$permisos 	= $this->autPerimisos->regresaPermisosUsuario();
		//var_dump(/*$_SESSION["datauser"]["funciones"],*/ $permisos);
		$leer		= strstr($permisos, 'L') ? true : false;
		$escribir	= strstr($permisos, 'E') ? true : false;
		$borrar		= strstr($permisos, 'B') ? true : false;
		
		$this->CargarVista('antiguo', get_defined_vars());
	}
	public function TaskObtenerRegistros()
	{
		try
		{
			$filter = filter_input(INPUT_GET, 'filter'); 
			$antiguo = isset($_GET['antiguo']) ? (int)$_GET['antiguo'] : 0;
			$piezas = !$antiguo ? $this->piezasModelo->ObtenerPiezaNoAsignadas($filter) : 
						$this->piezasModelo->ObtenerPiezasAntiguas($filter);;
			$this->ResponseJSON($piezas);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGuardar()
	{
		$data = (object)$_POST["data"];
		try
		{
			$this->tacomeModelo->Actualizar($data);
			//$this->tacomeModelo->ImprimiePieza($data);
			
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Los datos se guardaron correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskImprimir()
	{
		$clavePieza = filter_input(INPUT_GET, 'clave_pieza');
		try
		{
			if( !$clavePieza || empty($clavePieza) )
				throw new Exception('La clave de la pieza es invalida');
			$this->tacomeModelo->ImprimiePieza((object)['clavePieza' => $clavePieza]);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Impresion de pieza enviada correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}