<?php
require_once BASE_DIR . SB_DS . 'include' . SB_DS . 'lib' . SB_DS . 'tcpdf' . SB_DS . 'tcpdf.php';
class Etiqueta
{
	public function __construct()
	{
		
	}
	public function crea_etiqueta_pdf($etiqueta, $nombreImpresora = null)
	{
		extract($etiqueta);
	
		$prueba = true;
		$pantalla = true;
		$imprime = false;
	
		if ($prueba) 
		{
			$sku = '1807L29';
			$marca = 'ESCOLAR';
			$claveProducto = '00116T004';
			$descripcion = 'CAFE/BEIGE 04';
			$ancho = '1.50m';
			$longitud = '35.01';
			$composicion1 = '70% Poliester';
			$composicion2 = '30% Acrilico';
			$pagina = 'www.Lmtextil.com';
			$telefono = '(55)5604-5240';
			$consecutivo = '123456';
			$clavePieza = '00116A004-08-16-001-48';
		}
	
		//Add a custom size
		$width = 110;
		$height = 110;
		$pageLayout = array($height, $width);
		//$pageLayout = array($width, $height);
		//$pageLayout = 'A5';
		//$pdf = new TCPDF('p', 'pt', $pageLayout, true, 'UTF-8', false);
	
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		//$pdf = new TCPDF('p', PDF_UNIT, $pageLayout, true, 'UTF-8', false);
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('RSistemas');
		$pdf->SetTitle('Etiqueta Tacome');
		$pdf->SetSubject('SiLan');
		$pdf->SetKeywords('SiLan, Tacome');
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		/*
		 // set margins
		 $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
		 // set auto page breaks
		 $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		*/
	
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
	
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}
	
		// ---------------------------------------------------------
		// add a page
		$pdf->AddPage('P', $pageLayout);
	
		$pdf->SetFont('helvetica', 'b', 12);
		$pdf->Text(10, 30, $marca);
	
		$pdf->SetFont('helvetica', '', 10);
		$this->imprime($pdf, 'Color:', 10, 36, false);
		$this->imprime($pdf, $claveProducto, 23, 36, false);
		$this->imprime($pdf, $descripcion, 23, 40, false);
		$this->imprime($pdf, 'Ancho:', 10, 44, false);
		$this->imprime($pdf, $ancho, 23, 44, false);
	
		$pdf->SetFont('helvetica', 'b', 20);
		$this->imprime($pdf, $longitud, 55, 5, false);
		if ($longitud != '35') {
			$rectWidth = strlen($longitud) * 4;
			$pdf->RoundedRect(54.5, 5, $rectWidth, 10, 3.50, '1111', 'D', array('width' => 1));
		}
	
		$pdf->StartTransform();
		$pdf->Rotate(90, 40, 30);
		$pdf->SetFont('helvetica', '', 8);
		$this->imprime($pdf, $composicion1, 20, 63, true);
		$this->imprime($pdf, $composicion2, 20, 66, true);
		$this->imprime($pdf, $pagina, 20, 69, true);
		$this->imprime($pdf, $telefono, 20, 72, true);
		$pdf->SetFont('helvetica', '', 10);
		$this->imprime($pdf, $clavePieza, 20, 77, true);
		$pdf->StopTransform();
		// -----------------------------------------------------------------------------
		$pdf->SetFont('helvetica', '', 10);
	
		// define barcode style
		$style = array(
				'position' => '',
				'align' => 'C',
				'stretch' => false,
				'fitwidth' => true,
				'cellfitalign' => '',
				'border' => false,
				'hpadding' => 'auto',
				'vpadding' => 'auto',
				'fgcolor' => array(0,0,0),
				'bgcolor' => false, //array(255,255,255),
				'text' => true,
				'font' => 'helvetica',
				'fontsize' => 10,
				'stretchtext' => false
		);
	
		// PRINT VARIOUS 1D BARCODES
	
		// SKU
		$pdf->SetY(0);
		$pdf->StartTransform();
	
		$pdf->Translate(-5, 5);
		$pdf->write1DBarcode($sku, 'C128', '', '', '', 22, 0.4, $style, 'N');
	
		$pdf->StopTransform();
	
		// Clave Pieza
	
		$pdf->SetY(0);
		$pdf->StartTransform();
	
		$pdf->Rotate(90, 30, 30);
		$pdf->Translate(-3, 90);
		//$pdf->ScaleXY(80);
		//$pdf->Cell(0, 0, $clavePieza, 0, 1);
		$pdf->write1DBarcode($consecutivo, 'C128', '', '', '', 15, 0.4, $style, 'N');
	
		$pdf->StopTransform();
	
		ob_end_clean();
		//Close and output PDF document
	
		if( !$nombreImpresora ) 
		{
			$pdf->Output('etiqueta.pdf', 'I');
			return true;
		}
		$archivo 	= TMP_DIR . SB_DS . 'etiqueta.pdf';
		
		$pdf->Output($archivo, 'F');
		$output = shell_exec("lp -d $nombreImpresora $archivo");
		echo $output;
	}
	
	protected function imprime($pdf, $txt, $x, $y, $rot)
	{
		// print a message
		//$pdf->MultiCell(70, 50, $txt, 0, 'C', false, 1, $x, $y, true, 0, false, true, 0, 'T', false);
		$pdf->Text($x, $y, $txt);
	}
	public function crea_etiqueta_zpl($etiqueta, $ipImpresora, $puertoImpresora = null)
	{
		
	}	
}
