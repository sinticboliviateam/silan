<?php
class TacomeModelo extends Modelo
{
	/**
	 * @var PiezasModelo
	 */
	protected	$piezasModelo;
	
	public function Init()
	{
		$this->piezasModelo = $this->app->GetControllador()->CargarModelo(null, 'piezas');
	}
	protected function obtieneConsecutivoPiezas() 
	{
		$sql = "SELECT ifnull(MAX(noPieza),0) maxnum FROM piezas";
		$row = $this->GetObject($sql);
		
		return (int)$row->maxnum + 1;
	}
	public function Guardar($data)
	{
		$pieza = $this->piezasModelo->Obtener($datakey->idPiezas);
		//##verificamos que el registro de la pieza existe en la base de datos
		if( !$pieza )
			throw new Exception('La pieza no existe');
		if( (int)$pieza->noPieza > 0 )
			throw new Exception('La pieza ya esta asignada');
		$consecutivo = $this->obtieneConsecutivoPiezas();

		$fechaTacome 	= date("Y-m-d h:i:s");
		$usuarioTacome 	= $_SESSION["datauser"]["nombreCompleto"];
		
		//** INSERTA LA NUEVA POLIZA
		$data = [
				'metros'		=> $data->metros,
				'kilos'			=> $data->kilos,
				'noPieza' 		=> $consecutivo,
				'etiquetaPt' 	=> 'I',
				'fechaTacome' 	=> $fechaTacome,
				'usuarioTacome'	=> $usuarioTacome
		];
	}
	public function Actualizar($datakey=array()) 
	{
		$pieza = $this->piezasModelo->Obtener($datakey->idPiezas);
		//##verificamos que el registro de la pieza existe en la base de datos
		if( !$pieza )
			throw new Exception('La pieza no existe');
		$etiquetaPt		= 'I';
		$consecutivo 	= null;
		
		//##verificamos si la pieza ya tiene un consecutivo
		if( (int)$pieza->noPieza > 0 )
		{
			//##mantenemos el consecutivo
			$consecutivo	= $pieza->noPieza;
			$etiquetaPt 	= 'R';
		}
		else
		{
			//##validamos si necesitamos obtener un nuevo consecutivo
			$consecutivo = $this->obtieneConsecutivoPiezas();
		}
		
		$fechaTacome 	= date("Y-m-d h:i:s");
		$usuarioTacome 	= $_SESSION["datauser"]["nombreCompleto"];
		
		//** INSERTA LA NUEVA POLIZA
		$data = [
				'metros'		=> $datakey->metros,
				'kilos'			=> $datakey->kilos,
				'noPieza' 		=> $consecutivo,
				'etiquetaPt' 	=> $etiquetaPt,
				'fechaTacome' 	=> $fechaTacome,
				'usuarioTacome'	=> $usuarioTacome
		];
		$this->Update('piezas', $data, ['idPiezas' => $datakey->idPiezas]);
		
		return true;
	}
	public function ImprimiePieza($datakey)
	{
		//##verificar impresora
		//sb_verificar_impresora(IP_IMPRESORA, PUERTO_IMPRESORA);
		
		$clave_pieza	= $datakey->clavePieza;
		$dataresult 	= (object) array("error" => "");
		// recuperar datos de pieza de la db
		$sql = "SELECT * FROM piezas WHERE clavePieza = '$clave_pieza' LIMIT 1";
		
		$result = $this->Query($sql);
		$pieza	= $result->fetch_assoc();
		if ($pieza == NULL) {
			throw new Exception('No se eoncontró la pieza ' . $clave_pieza);
		}
		// determinar clave_prod
		$partesClavePieza 	= preg_split('/-/', $clave_pieza);
		$clave_prod 		= $partesClavePieza[0];
		
		// Recuperar datos de producto de db
		$sql 	= "SELECT * FROM artref WHERE clave_prod = '$clave_prod' LIMIT 1";
		$result = $this->Query($sql);
		$artref = $result->fetch_assoc();
		if ($pieza == NULL) {
			throw new Exception('No se eoncontró el producto ' . $clave_prod);
		}
		
		// Organizar datos de la etiqueta
		$etiqueta = (object) array( 		
				"servicio" 		=> 'etiqueta',
				"sku" 			=> $artref['sku'],
				"marca" 		=> $artref['marca'],
				"clave_prod" 	=> $artref['clave_prod'],
				"color" 		=> $artref['color'],
				"ancho" 		=> $artref['ancho'],
				"composicion01" => $artref['composicion01'],
				"composicion02" => $artref['composicion02'],
				"metros" 		=> $pieza['metros'],
				"clave_pieza" 	=> $pieza['clavePieza'],
				"numero" 		=> $pieza['noPieza']
		);
		
	
		$sku 			= $etiqueta->sku;
		$marca 			= $etiqueta->marca;
		$clave_prod 	= $etiqueta->clave_prod;
		$color 			= $etiqueta->color;
		$ancho 			= $etiqueta->ancho;
		$composicion01 	= $etiqueta->composicion01;
		$composicion02 	= $etiqueta->composicion02;
		$metros 		= $etiqueta->metros;
		$clave_pieza 	= $etiqueta->clave_pieza;
		$numero 		= $etiqueta->numero;
		$skuFormato 	= substr($sku, 0, 4) . ">6" . substr($sku, -3); // 1955L29 -> 1955>6L29
		
		$zpl = '';
		//$zpl .= "#CT~~CD,~CC^~CT~";
		//$zpl .= "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ";
		$zpl .= "^XA\n";
		$zpl .= "^MMT\n";
		$zpl .= "^PW831\n";
		$zpl .= "^LL0406\n";
		$zpl .= "^LS0\n";
		$zpl .= "^BY3,3,55^FT764,369^BCB,,Y,N\n";
		$zpl .= "^FD>:" . $numero . "^FS\n";
		$zpl .= "^FT69,231^A0N,37,48^FH\^FD" . $marca . "^FS\n";
		$zpl .= "^FT697,386^A0B,25,24^FH\^FD" . $clave_pieza . "^FS\n";
		$zpl .= "^FT554,383^A0B,25,24^FH\^FD" . $composicion01 . "^FS\n";
		$zpl .= "^FT585,383^A0B,25,24^FH\^FD" . $composicion02 . "^FS\n";
		$zpl .= "^FT616,383^A0B,25,24^FH\^FDwww.LMtextil.com^FS\n";
		$zpl .= "^FT647,383^A0B,25,24^FH\^FD(55) 5604-5240^FS\n";
		$zpl .= "^FT70,280^A0N,25,24^FH\^FDColor:   " . $clave_prod . "^FS\n";
		$zpl .= "^FT70,311^A0N,25,24^FH\^FD           " . $color . "^FS\n";
		$zpl .= "^FT70,342^A0N,25,24^FH\^FDAncho: " . $ancho . "m^FS\n";
		$zpl .= "^FT496,88^A0N,45,45^FH\^FD" . $metros . "^FS\n";
		$zpl .= "^BY4,3,87^FT31,112^BCN,,Y,N\n";
		$zpl .= "^FD>;" . $skuFormato . "^FS\n";
		
		if ($metros != 35) {
			$zpl .= "^FO486,40^GB123,65,8^FS\n";
		}
		$zpl .= "^PQ1,0,1,Y^XZ\n";
		
		//$filename = TMP_DIR . SB_DS . 'tacome_etiqueta.txt';
		//file_put_contents($filename, $zpl);
		//var_dump(IP_IMPRESORA);
		require_once BASE_DIR . SB_DS . "include/lib/ZebraPrint/PrintSend.php";
		require_once BASE_DIR . SB_DS . "include/lib/ZebraPrint/PrintSendLPR.php";
		$lpr 			= new PrintSendLPR();
		$lpr->setHost(strstr($pieza['documentoEntrada'], 'CA') ? IP_IMPRESORA_2 : IP_IMPRESORA);
		$lpr->setData($zpl);
		$res = $lpr->printJob("etiquetas"); //copia1
		$res = $lpr->printJob("etiquetas"); //copia2
		//var_dump($zpl);
		//print $lpr->getDebug();
		$this->Update('piezas', ['etiquetaPt' => 'I'], ['idPiezas' => $clave['idPiezas']]);
		return $res;
	}
}