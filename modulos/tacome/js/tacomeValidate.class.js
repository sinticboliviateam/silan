/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
var rSisTacomeValidate = {
    _init : function(){
        this.formValidateTacome();
    },
    formValidateTacome : function(){
        $("form#form-datos-tacome").validate({
            onfocusout: function(element) {
                if($(element).valid())
                    rSisTacome.parametros[$(element).attr("name")] = $(element).val();
            },
            rules : {
                "clavePieza" : { required: true },
                "metros" : { required: true, range : [1.00,200.00]},
                "kilos" : { required: true, range : [0.500,50.000]}
            },
            messages: {
                clavePieza : { required : "Este campo es obligatorio" },
                metros : { required : "Este campo es obligatorio", range : "Rango válido entre 37 hasta 200" },
                kilos : { required : "Este campo es obligatorio", range : "Rango válido entre 1 hasta 80" }
            },
            submitHandler: function(form)
            {
            	rSisTacomeValidate.obtenValores(this.currentElements);
                let idPieza = parseInt(jQuery('#idPieza').val());
                if( isNaN(idPieza) || !idPieza )
                {
                    $silan_msg.showmessagecondition("¿Se encuentra preparada la impresora?",
                    ()=>
                    {
                        rSisTacomeValidate.submitTacome(true);
                    }, 
                    () => 
                    {
                        console.log('cerrando');
                    });
                }
                else
                {
                    rSisTacomeValidate.submitTacome(false);
                }
            }
        });
    },
    obtenValores : function(elementos)
    {
        for(var i=0; i<=(elementos.length)-1; ++i)
        {
            rSisTacome.parametros[elementos[i].name] = elementos[i].value; 
        }
        let idPieza = isNaN(parseInt(jQuery('#idPieza').val())) ? 0 : parseInt(jQuery('#idPieza').val());
        rSisTacome.parametros.idPiezas = idPieza > 0 ? idPieza : rSisAutocomplete.key;
        rSisTacome.parametros.evento = "insertaTacome";
       
    },
    submitTacome : function(print)
    {
    	//var params = 'data=' + JSON.stringify(rSisTacome.parametros);
    	//console.log(rSisTacome.parametros);
    	var params = {'data': rSisTacome.parametros};
        //jQuery.post('./include/tacome.silan.php', params, function(data)
    	jQuery.post('index.php?mod=tacome&task=Guardar', params, function(data)
        {
    		$silan_msg.showmessage('Valores actualizado correctamente', 'closemodal', 'VALORES ACTUALIZADOS');
    		if( print )
            {
                //##enviar impresion
                jQuery.get('index.php?mod=tacome&task=Imprimir&clave_pieza=' + rSisTacome.parametros.clavePieza, () => {});
            }
        }, 'json').fail(function(data)
        {
        	alert(data.responseJSON.error || 'Error desconocido al guardar tacome');
        }).always(function(data)
        {
            location.reload();
        });
    }
};
