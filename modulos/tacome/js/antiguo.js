rSisTacome = {
    parametros : {},
    ready : function()
    {
        //this.cargaCampos();
    	this.AsignarEventos();
    	this.ObtenerPiezas();
    },
    AsignarEventos: function()
    {
    	jQuery('#btnNuevoTacome').click( (e) => /*this.cargaCampos()*/this.MostrarVentana() );
    	jQuery(document).on('click', '.tacome-pieza', (e) => 
    	{
    		let tr = e.currentTarget;
            let claveProd = tr.dataset.clavePieza.split('-')[0];
    		//this.cargaCampos();
    		jQuery('#idPieza').val(tr.dataset.id);
    		jQuery('#clavePieza').attr('readonly', true).val(tr.dataset.clavePieza);
    		jQuery('#metros').val(tr.dataset.metros);
    		jQuery('#kilos').val(tr.dataset.kilos);
    		jQuery('#fechaTacome').val(tr.dataset.fechaTacome);
            jQuery('#div-imagen').css('background-image', 'url(img/clavesprods/'+ claveProd +'.jpg)');
    		this.MostrarVentana();
    	});
    	jQuery(document).on('click', '.btn-imprimir', () => 
    	{
    		this.ImprimirPieza();
    	});
        jQuery('#filter').keyup( (e) => { this.Filtrar(e.currentTarget.value) });
    },
    ImprimirPieza: function(id)
    {
    	jQuery.get('index.php?mod=tacome&task=Imprimir');
    },
    ObtenerPiezas: function(keyword)
    {
        let params = 'mod=tacome&task=ObtenerRegistros&antiguo=1';
        if( keyword )
            params += '&filter=' + keyword;
        jQuery.get('index.php?' + params, (piezas) => 
        {
            this.LlenarRegistros(piezas);
        }).fail( (error) => 
        {
            alert(error.responseJSON.error || 'Error desconocido al obtener las piezas de tacome');
        });
    },
    Filtrar: function(keyword)
    {
        if( this.timeout )
            clearTimeout(this.timeout);
        
        this.timeout = setTimeout(() =>
        {
            this.ObtenerPiezas(keyword);
        }, 400)
    },
    LlenarRegistros: function(piezas, append)
    {
        append = append || false;
        
        let tbody = jQuery('#tabla-piezas tbody').get(0);
        if( !append )
            tbody.innerHTML = '';
        let columnas = [
            {'col': 'documentoEntrada'},
            {'col': 'clavePieza'},
            {'col': 'noPoliza'},
            {'col': 'kilos'},
            {'col': 'metros'},
            {'col': 'etiquetaPt'}
        ];
        for(let pieza of piezas)
        {
            let tr = document.createElement('tr');
            tr.className            = 'tacome-pieza';
            tr.dataset.id           = pieza.idPiezas;
            tr.dataset.clavePieza   = pieza.clavePieza;
            tr.dataset.metros       = pieza.metros;
            tr.dataset.kilos        = pieza.kilos;
            tr.dataset.fechaTacome  = pieza.fechaTacome;
            for(let columna of columnas)
            {
                let td  = document.createElement('td');
                td.className = 'columna-' + columna.col;
                if( columna.col == 'noPoliza' )
                {
                    td.innerHTML = '<span class="valor">'+ pieza.poliza.noPoliza +'</span>';
                }
                else if( columnas.col == 'etiquetaPt' )
                {
                    let texto = pieza.etiquetaPt == 'I' ? 'Impresa' : '';
                    td.innerHTML = '<span class="valor">'+ texto +'</span>';
                }
                else
                {
                    td.innerHTML = '<span class="valor">'+ pieza[columna.col] +'</span>';
                }
                tr.appendChild(td);
            }
            tbody.appendChild(tr);
        }
    },
    MostrarVentana: function()
    {
    	rSisTacome.datosRegistro();
        rSisTacome.eventoAutocomplete();
        rSisTacomeValidate._init();
    },
    cargaCampos : function()
    {
        $("div#dialogTacome").load("./tabs/datos-tacome.php", function(){
            rSisTacome.datosRegistro();
            rSisTacome.eventoAutocomplete();
            rSisTacomeValidate._init();
        });
    },
    datosRegistro : function(){
        $.post('./include/polizas.silan.php',
            {data: {evento:"usuarioRegistro"}}, function(data){
                $("input#fechaTacome").prop("value",data.fechaRegistro);
                $("input#usuarioTacome").prop("value",data.usuarioRegistro);
                rSisTacome.muestraDialog();
                rSisTacome.botones();
            },"json");
    },
    eventoAutocomplete : function(){
        $("input#clavePieza").focusin(function(){
            rSisAutocomplete.tacome(this.id);
        });
    },
    muestraDialog : function()
    {
        $("div#dialogTacome").dialog({
            modal: true,
            autoOpen: true,
            title: "Tacome",
            dialogClass: 'hide-close',
            width: 750,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true,
            close: function(event, ui)
            {
            	//window.location.replace('./inicio.php');
            	//window.location = 'index.php';
            	window.location.reload();
            }
        }).show();
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
    },
    botones : function()
    {
        $("div#guardaRegistro")
            .button({ 
                icons: { primary: "ui-icon-disk" },
                label: "Guardar"
            })
            .on("click",function(e)
            {
                e.preventDefault();
                $("form#form-datos-tacome").submit();
            });
        jQuery('#btn-imprimir-tacome').button({
        	icons: {primary: 'ui-icon-print'},
        	label: 'Imprimir'
        }).on('click', () => 
        {
        	let clave = jQuery('#clavePieza').val();
        	if( !clave )
        	{
        		alert('Clave de pieza invalida');
        		return false;
        	}
        	jQuery.get('index.php?mod=tacome&task=Imprimir&clave_pieza=' + clave, () => 
        	{
        		
        	}).fail( (data) => 
        	{
        		console.log(data.responseJSON);
        		alert(data.responseJSON.error || 'Ocurrio un error an imprimir la pieza');
        	});
        });
        $("div.cerrar-tabs")
            .button({ 
                icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                label: "Cerrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("div#dialogTacome").dialog("close");
            });
    }
};
jQuery(function()
{
	rSisTacome.ready();
});

