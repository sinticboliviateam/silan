<?php
class OrdenescompraModelo extends Modelo
{
	public function Obtener($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de orden de compra invalido');
		$query = "SELECT * FROM ordenescompra WHERE idOrdenCompra = $id LIMIT 1";

		return $this->GetObject($query);
	}
	public function Gurdar($orden)
	{
		if( isset($orden->idOrdenCompra) && (int)$orden->idOrdenCompra )
			return $this->Insertar($orden);

		return $this->Actualizar($orden);
	}
	public function Insertar($data)
	{

        $fechaEntrega = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data->fechaEntrega)));
        $record = array(
            'ordenCompra'   => trim($data->noOrdenCompra),
            'fechaEntrega' => $fechaEntrega,
            'fechaRegistro' => date('Y-m-d H:i:s'),
            'archivotxt'    => 'No'
        );

        $id = $this->Insert('ordenescompra', $record);
        $this->msgout = array('error'=>0, 'msg'=>"El registro se inserto correctamente.", "action"=>"reload", "title"=>"tabla de Órdenes de compra");
        //unset($_SESSION["datauser"]["imagen"]);
        //$this->dbClose($result);
        return $id;
	}
	public function Actualizar($data)
	{
		if( !(int)$data->idOrdenCompra )
			throw new Exception('Identificador de orden de compra invalido, no se puede actualizar');
		$ordenAntigua = $this->Obtener($data->idOrdenCompra);
		if( !$ordenAntigua )
			throw new Exception('La orden de compra no existe, no se puede actualizar');
		if( $ordenAntigua->archivotxt != 'No' )
			throw new Exception('No puede actualizar la orden de compra');

        $fechaEntrega = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data->fechaEntrega)));
        $record = array(
            'ordenCompra'   => trim($data->noOrdenCompra),
            'fechaEntrega' => $fechaEntrega,
        );

        $id = $this->Update('ordenescompra', $record, ['idOrdenCompra' => $data->idOrdenCompra]);

        /*
        if(!empty($data->functions))
            $this->updateFunctionsUser($data->key, $data->functions);
        $this->msgout = array("error"=>0);
    	*/
	}
	public function ObtenerTarimas($orden)
	{
		if( !$orden || !$orden->idOrdenCompra )
			throw new Exception('Orden de compra invalido, ser espera un objeto y su identificador');

		$query = "SELECT * FROM tarimas WHERE idOrdenCompra = {$orden->idOrdenCompra} ORDER BY idTarima ASC";
		$items = $this->GetResults($query);

		return $items;
	}
	public function Borrar($id)
	{
		$orden = $this->Obtener($id);
		if( !$orden )
			throw new Exception('La orden de compra no existe');
		if( $orden->archivotxt != 'No' )
			throw new Exception('No se puede borrar la orden de compra');
		$tarimas = $this->ObtenerTarimas($orden);

		if( count($tarimas) )
			throw new Exception('La orden de compra ya tiene tarimas asignadas, no es posible borrarla');
		//$this->Delete('ordenescompra', ['idOrdenCompra' => $orden->idOrdenCompra]);
		$query = "DELETE FROM ordenescompra WHERE idOrdenCompra = {$orden->idOrdenCompra} LIMIT 1";
		$this->Query($query);
		return true;
	}
}