<?php
class OrdenescompraControlador extends Controlador
{
	/**
	 * 
	 * @var OrdenesModelo
	 */
	protected $modelo;
	public function Init()
	{
		$this->modelo = $this->CargarModelo();
	}
	public function TaskBuildbarcode()
	{
		$id		= '';
		$ean13	= $this->modelo->BuildEAN13('200', '0', $id);
		
	}
	public function TaskDefault()
	{
		try
		{
			$this->CargarVista('default');
		}
		catch(Exception $e)
		{
			print (['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskObtener()
	{
		try
		{
			$id = (int)$_GET['id'];
			$orden = $this->modelo->Obtener($id);
			$this->ResponseJSON(['status' => 'ok', 'orden' => $orden]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGuardar()
	{
		try
		{
			$data = (object)$_POST['data'];//json_decode(file_get_contents('php://input'));
			if( !$data )
				throw new Exception('Datos de la orden de compra invalidos');
			$this->modelo->Insertar($data);
			$this->ResponseJSON(['status' => 'ok', 'mensage' => 'La orden de compra se creo correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskActualizar()
	{
		try
		{
			$data = (object)$_POST['data'];//json_decode(file_get_contents('php://input'));
			$this->modelo->Actualizar($data);
			$this->ResponseJSON(['status' => 'ok', 'mensage' => 'La orden de compra se actualizo correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskBorrar()
	{
		try
		{
			$id = $_GET['id'];
			$this->modelo->Borrar($id);
			$this->ResponseJSON(['status' => 'ok', 'mensage' => 'La orden de compra se borro correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}