<script src="modulos/ordenescompra/js/ordenesCompra.class.js"></script>
<script src="modulos/ordenescompra/js/ordenesCompraValidate.class.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $setmenu.getLinksUser();
                $("#fechaEntrega").datepicker($.datepicker.regional[ "es" ] );
                $("#fechaEntrega").datepicker().datepicker('setDate', new Date());
                //$silan_ordenesComp.widgetEventOrdenesComp();
                $silan_ordenesComp._initform();
                $silan_ordenesComp.buscarRegistro();
            });
        </script>
<div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Órdenes de compra</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <button id="btnNuevaOrden" class=""></button>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            
                            <th class="th-col06-col01">Orden compra</th>
                            <th class="th-col06-col02">Fecha Registro</th>
                            <th class="th-col06-col03">Fecha Entrega</th>
                            <th class="th-col06-col03">Archivo TXT</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogOrdenCompra" style="display: none;">
                <div id="tabs-ordenComp" style="width: 95%; height: auto; margin: 0 auto;">
                        <form id="form-ordenComp" enctype="multipart/form-data" method="post">
                            <input type="hidden" id="idOrdenCompra" name="idOrdenCompra" value="" />
                            <div class="flex-modal-ordenesComp">
                                <div class="flex-modal-left-tab" style="width: 365px;">
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="noOrdenCompra" class="oLabel-left">No. orden de compra</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="noOrdenCompra" name="noOrdenCompra" class="oInput" type="text" value="" 
                                                required maxlength="60" tabindex="1"></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="fechaRegistro" class="oLabel-left">Fecha Registro</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="fechaRegistro" name="fechaRegistro" class="oInput" type="text" tabindex="2" readonly 
                                                value="<?php print date('d M Y H:i:s') ?>"></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="fechaEntrega" class="oLabel-left">Fecha Entrega</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="fechaEntrega" name="fechaEntrega" class="oInput" type="text" tabindex="2" readonly value=""></div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="ordenes-compra-botones" style="clear:both;width:100%;">
                                        <div id="guardar-ordenes-compra" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-disk"></span><span class="ui-button-icon-space"> </span>Guardar</div>
                                        <div id="borrar-ordenes-compra" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-trash"></span><span class="ui-button-icon-space"> </span>Borrar</div>
                                        <div id="cerrar-ordenes-compra" class="cerrar-tabs ui-button ui-corner-all ui-widget">
                                            <span class="ui-button-icon ui-icon ui-icon-arrowreturnthick-1-w"></span>
                                            <span class="ui-button-icon-space"> </span>
                                            Regresar
                                        </div>
                                        <a href="javascript:;" id="btn-imprimir-csv" class="ui-button ui-corner-all ui-widget" style="display:none;">
                                            <span class="ui-button-icon ui-icon ui-icon-print"></span>
                                            Archivo TXT
                                        </a>
                            </div>
                        </form>
 
                </div>
            </div>