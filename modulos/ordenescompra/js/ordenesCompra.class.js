/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-30
 * SiLan v1.0
*/
function ordenesComp()
{
	var $this		= this;
    var datauser 	= {
        event : ""
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  datauser : datauser[field];
    };
    
    this.setobjvar = function(field, value){
        datauser[field] = value;
    };
    
    this.clearobjvar = function(){
        datauser = {
            event : ""
        };
    };
//*** INICIALIZA LOS ELEMENTOS DOM DEL FORM
    this._initform = function(){
        $silan_ordenesComp.getRecordsTableOrdenesComp();
        $silan_ordenesComp.showdialogOrdenCompra();
        $silan_validOrdenCompra.formValidateDatos();
        jQuery('#btn-imprimir-csv').click(this.OnBtnConstruirCSVClicked);
    };
    
//*** OPCIONES PARA BUSCAR REGISTROS DENTRO DEL GRID
    this.getRecordsTableOrdenesComp = function(){
        var filter = $("#filter").val();
//        filter = filter.replace(/\s/g,"");
        $.post('./include/tablasSistemas.silan.php',
            {data: {opt:"ordenesComp", filter:filter}}, function(data){
                $("#tbl01-records-busqueda").append(data);
                $("#status-busca").html("");
            }).always(function(){
                $silan_ordenesComp.widgetEventOrdenesComp();
            });
    };
    
    this.buscarRegistro = function(){
        $("#filter").keyup(function(){
            $(".renglonesGrid").remove();
            $("#status-busca").html("Buscando...");
            $silan_ordenesComp.getRecordsTableOrdenesComp();
        });
    };
//*** BOTONES FORM PRINCIPAL
    this.widgetEventOrdenesComp = function(){
        $("button#btnNuevaOrden")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validOrdenCompra.setobjvar("event","insert");
                //$("#dialogOrdenCompra").dialog("open");
                $("#dialogOrdenCompra").dialog().dialog("open");
            });
            
        $("tr.renglonesGrid").on("click",function(e){
            e.preventDefault();
            //console.log("click en row");    //<-- debug
            $silan_validOrdenCompra.setobjvar("event","edit");
            $silan_validOrdenCompra.setobjvar("idOrdenCompra",this.id);
            $("#dialogOrdenCompra").dialog().dialog("open");
            $.get('index.php?mod=ordenescompra&task=Obtener&id=' + this.id, 
                function(data)
                {
                    //$silan_msg.showmessage(data.msg, data.action, data.title);
                },"json")
                .fail(function(data)
                {
                    alert(data.responseJSON.error);
                    //alert("ERROR 0 renglonesGrid.-> "+data);
                })
                .always(function(data)
                {
                    //console.log(data.result);    //<-- debug
                    $silan_ordenesComp.editDataRegistro(data.orden);
                });
        });

        $("#guardar-ordenes-compra")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();

                if($silan_validOrdenCompra.getobjvar("event") == 'edit') {
                    $silan_validOrdenCompra.setobjvar("event","update");
                }

                $("#form-ordenComp").submit();
            });
            
        $("#borrar-ordenes-compra")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                var seguir =true;

                var cancel =function(modal){
                    //console.log(modal);
                }
               
                var aceptar =function(modal){
                    console.log(modal)
                    $silan_validOrdenCompra.setobjvar("event","delete");
                    var params = 'mod=ordenescompra&task=Borrar&id=' + $silan_validOrdenCompra.getobjvar('idOrdenCompra');
                    $.get('index.php?' + params, function(data)
                    {
                        //if(data.error !== 0)
                        $("#dialogOrdenCompra").dialog("close");
                        location.reload();
                            // $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json")
                    .fail(function(data)
                    {
                        //alert("ERROR 0 renglonesGrid.-> "+data);
                        alert(data.responseJSON.error);
                    })
                    .always(function(data)
                    { 
                        //$silan_ordenesComp.editDataRegistro(data);
                    });
                }
                $silan_msg.showmessagecondition('Estas seguro desear borrar este registro?', aceptar, cancel, 'Borrar') 
            }); 
            
        $(".cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#dialogOrdenCompra").dialog("close");
                location.reload();
            });

        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
    };

    this.showdialogOrdenCompra = function(){
        $("#dialogOrdenCompra").dialog({
            modal: true,
            autoOpen: false,
            title: "Ordenes de compra",
            dialogClass: 'hide-close',
            width: 1140,
            maxHeight: 600,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
        $(".ordenes-compra-botones").css({"font-size":"1.4em"});
    };
    
//*** EDITA LOS DATOS DE lA ORDEN DE COMPRA SELECCIONADA
    this.editDataRegistro = function(data)
    {
    	$("#idOrdenCompra").prop("value", data.idOrdenCompra);
        $("#noOrdenCompra").prop("value", data.ordenCompra);
        $("#fechaRegistro").prop("value", data.fechaRegistro);
        $("#fechaEntrega").prop("value", data.fechaEntrega);
        jQuery('#btn-imprimir-csv').css('display', 'inline-block');
    };
    this.OnBtnConstruirCSVClicked = function()
    {
    	var id = parseInt(jQuery('#idOrdenCompra').val());
    	if( !id || isNaN(id) )
    	{
    		alert('Identificador de orden de compra invalido');
    		return false;
    	}
    	var url = 'index.php?mod=tarimas&task=Ordenes.ConstruirCSV&id=' + id;
    	
    	
    	jQuery.get(url, function(res)
    	{
    		if( res && res.status == 'ok' )
    		{
    			alert(res.mensaje);
    			window.location = 'index.php?mod=tarimas&task=Ordenes.DescargarCSV&id=' + id;
    		}
    		else
    		{
    			alert(res.error || 'Error desconocido al enviar la impresion');
    		}
    	}).fail(function(error)
    	{
    		alert('Ocurrio un error al tratar de imprimir el codigo de barras');
    	});
    	
    	return false;
    };
}
$silan_ordenesComp = new ordenesComp();