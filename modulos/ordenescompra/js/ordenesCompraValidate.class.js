/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-30
 * SiLan v1.0
*/
function ordenesCompValidate()
{
    var $this = this;

    var dataform = {
        event : "usuarios",
        functions : new Array()
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    
    this.clearobjvar = function(){
        dataform = {
            event : "usuarios"
        };
    };
    
    this.formValidateDatos = function(){

        $("#form-ordenComp").validate({

            rules : {
                noOrdenCompra : { required : true },
                fechaEntrega : { required : true },
                //destinoOrden : { required : true }
            },
            messages: {
                noOrdenCompra : { required : "Este campo es obligatorio" },
                fechaEntrega : { required : "Este campo es obligatorio" },
                //destinoOrden : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form)
            {
                if(!$silan_validOrdenCompra.validaCampos()){
                    $silan_msg.showmessage("Existen datos obligatorios.",
                                            "closemodal","Tabla de órdenes de compra.");
                    return;
                }
                var endpoint = 'index.php?mod=ordenescompra&task=Guardar';
                if( $this.getobjvar("event") == 'update' )
                {
                    endpoint = 'index.php?mod=ordenescompra&task=Actualizar';
                }
                
                //$.post('./include/ordenesCompra.php',
                $.post(endpoint,
                    {
                        data: $silan_validOrdenCompra.getobjvar()
                    }, 
                    function(data)
                    {
                        (data.status !== 'ok' )
                            ? $silan_msg.showmessage(data.error, data.action, 'ERROR')
                            : location.reload();
                    },
                    "json")
                    .fail(function(data)
                    {
                        alert(data.responseJSON.error);
                        //alert("ERROR LINE 53 formValidate.-> "+data);
                    });
            }
        });
        return false;
    };
    
    this.validaCampos = function(){
        if($("#noOrdenCompra").val() === "" ||
            $("#fechaEntrega").val() === "" /*88||
            $("#destinoOrden").val() === "" */
        ){
            return false;
        }
        $silan_validOrdenCompra.setobjvar("noOrdenCompra", $("#noOrdenCompra").val());
        $silan_validOrdenCompra.setobjvar("fechaEntrega", $("#fechaEntrega").val());
        //$silan_validOrdenCompra.setobjvar("destinoOrden", $("#destinoOrden").val());
        
        return true;
    };
    
}
$silan_validOrdenCompra = new ordenesCompValidate();