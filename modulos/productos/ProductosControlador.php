<?php
class ProductosControlador extends Controlador
{
	/**
	 * 
	 * @var ProductosModelo
	 */
	protected $productosModelo;

	public function Init()
	{
		$this->productosModelo = $this->CargarModelo();
		$this->autPerimisos = new menu();
	}
	public function TaskDefault()
	{
		require_once BASE_DIR . SB_DS . "include/class/Combos.class.php";
		$combos = new Combos();
		$_SESSION["datauser"]["menuaccess"] = "02010200";
		$permisos 	= $this->autPerimisos->regresaPermisosUsuario();
		$leer		= strstr($permisos, 'L') ? true : false;
		$escribir	= strstr($permisos, 'E') ? true : false;
		$borrar		= strstr($permisos, 'B') ? true : false;

		$this->CargarVista('default', get_defined_vars());
	}
	public function TaskGet()
	{
		try
		{
			$id = isset($_GET['id']) ? $_GET['id'] : null;
			if(!$id)
				throw new Exception('Identificador de producto invalido');
			$product = $this->productosModelo->Obtener($id);
			$product->fechaEstatus = $this->productosModelo->getDateFormat($product->fechaEstatus);
			$product->fechaRegistro = $this->productosModelo->getDateFormat($product->fechaRegistro);
			$this->ResponseJSON(['status' => 'ok', 'producto' => $product]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGuardar()
	{
		try
		{
			if( !isset($_POST) || !count($_POST) )
				throw new Exception('No existen datos para la solicitud');

			$data = (object)$_POST;
			
			$mensaje = 'El registro fue creado correctamente';
			if( !isset($data->id_artref) || !$data->id_artref )
			{
				$this->productosModelo->Crear($data);
			}
			else
			{
				$this->productosModelo->Actualizar($data);
				$mensaje = 'El registro fue actualizado correctamente';
			}
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => $mensaje]);	
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskBorrarImagen()
	{     // 20180810 falta corregir esta parte del código
		try
		{ 
			$id = isset($_GET['id']) ? (int)$_GET['id'] : null;
			if( !$id )
				throw new Exception('Identificador de producto invalido');
			$this->productosModelo->BorrarImagen($id);
			
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Imagen borrada']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
		
	}
	public function TaskBorrar()
	{     // 20180810 falta corregir esta parte del código
		try
		{ 
			$id = isset($_GET['id']) ? (int)$_GET['id'] : null;
			if( !$id )
				throw new Exception('Identificador de producto invalido');
			$this->productosModelo->Borrar($id);
			
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Producto borrado']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
		
	}
}