<?php
class ProductosModelo extends Modelo
{
	public function Obtener($id)
	{
		$id = (int)$id;
		$query = "SELECT * FROM artref WHERE id_artref = $id LIMIT 1";
		
		return $this->GetObject($query);
	}
	public function ObtenerPor($column, $valor)
	{
		$query = "SELECT * FROM artref WHERE `$column` = '$valor' LIMIT 1";

		return $this->GetObject($query);
	}
	public function Crear($producto)
	{
		/* 20180807 area de validación de datos
		if( !isset($producto->password) || empty($producto->password) )
			throw new Exception('Debe asignar una contraseña para el usuario');
		*/

		if( empty($producto->clave_prod))
			throw new Exception('Falta la clave del producto');
		$query = "SELECT id_artref FROM silan.artref WHERE clave_prod ='$producto->clave_prod' LIMIT 1";
		$row = $this->GetObject($query);

		if( $row )
			throw new Exception("la clave $producto->clave_prod ya existe !!");
		if( empty($producto->ancho) || !(int)$producto->ancho )
			throw new Exception('El ancho es obligatorio y debe ser un numero mayor a cero');

		$producto->id_artref		= null;
		$producto->clave_prod		= utf8_decode( trim($producto->clave_prod));
		$producto->claveAnterior	= utf8_decode( trim($producto->claveAnterior));
		$producto->sku 				= utf8_decode( trim($producto->sku));
		$producto->nombreProducto	= utf8_decode( trim($producto->nombreProducto));
		$producto->nombreMercado	= utf8_decode( trim($producto->nombreMercado));
		$producto->color 			= utf8_decode( trim($producto->color));  // 20180825 falta convertir solo mayúsculas sin acentos
																		     // por lo pronto trim
		$producto->referenciaColor	= trim($producto->referenciaColor);
		$producto->marca 			= trim($producto->marca);
		$producto->ancho 			= $producto->ancho;
		$producto->composicion01	= utf8_decode( strtoupper(trim($producto->composicion01)));
		$producto->composicion02	= utf8_decode(strtoupper(trim($producto->composicion02)));
		//$producto->fechaRegistro	= '';    //   default workbenchcurrent_timestamp
		$producto->idEstatus		= $producto->idEstatus;
		$producto->fechaEstatus		= date('Y-m-d H:i:s');
		$producto->usuarioEstatus	= utf8_decode($_SESSION['datauser']['nombreCompleto']);
		if( isset($producto->imagen) )
		{
			$imagen = (object)$producto->imagen;
			
			$ext = substr($imagen->archivo, strrpos($imagen->archivo, '.') + 1);
			$archivo_imagen = "{$producto->clave_prod}.{$ext}";
			$producto->ruta = $archivo_imagen;
			file_put_contents(PRODUCTOS_DIR . SB_DS . $archivo_imagen, base64_decode( $imagen->data ));
			unset($producto->imagen);
		}
		$id = $this->Insert('artref', $producto);
		
		$nuevoProducto = $this->Obtener($id);
		// ml, determinar si para productos es necesaria la línea siguiente	
		//unset($_SESSION["record"]["imagen"]);
		return $nuevoProducto;
	}

	public function Actualizar($producto)
	{
		if( !$producto->id_artref )
			throw new Exception('Identificador de usuario invalido');
		
		$productoG = $this->Obtener($producto->id_artref);
		if( !$productoG )
			throw new Exception('El producto no existe');
		if( empty($producto->ancho) || !(int)$producto->ancho )
			throw new Exception('El ancho es obligatorio y debe ser un numero mayor a cero');

		//$productoG->clave_prod 		= $producto->clave_prod;
		$productoG->claveAnterior 	= $producto->claveAnterior;
		$productoG->sku 			= $producto->sku;
		$productoG->nombreProducto 	= utf8_decode($producto->nombreProducto);
		$productoG->nombreMercado 	= utf8_decode($producto->nombreMercado);
		$productoG->color			= utf8_decode($producto->color);   // mayúsculas sin acentos
		$productoG->referenciaColor = utf8_decode($producto->referenciaColor);
		$productoG->marca 			= $producto->marca;
		$productoG->ancho			= $producto->ancho;
		//$productoG->ruta			= $producto->ruta;
		$productoG->composicion01	= strtoupper($producto->composicion01); // mayúsculas sin acentos
		$productoG->composicion02	= strtoupper($producto->composicion02); // mayúsculas sin acentos
		//$productoG->fechaRegistro	= no se le hace nada
		if( $productoG->idEstatus != $producto->idEstatus )
		{
			$productoG->idEstatus 		= (int)$producto->idEstatus;
			$productoG->fechaEstatus	= date('Y-m-d H:i:s');
			$productoG->usuarioEstatus	= utf8_decode($_SESSION['datauser']['nombreCompleto']);
		}
		
		//20180702 ml, falta verificar cambio de estatus entonces guardar fechaEstatus y usuarioEstatus
		if( isset($producto->imagen) )
		{
			$imagen = (object)$producto->imagen;
			
			$ext = substr($imagen->archivo, strrpos($imagen->archivo, '.') + 1);
			$archivo_imagen = "{$productoG->clave_prod}.{$ext}";
			$productoG->ruta = $archivo_imagen;
			file_put_contents(PRODUCTOS_DIR . SB_DS . $archivo_imagen, base64_decode( $imagen->data ));
		}
		
		$this->Update('artref', $productoG, ['id_artref' => $productoG->id_artref]);
		
		return $productoG;
	}
	public function BorrarImagen($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de producto invalido');
		$producto = $this->Obtener($id);
		if( !$producto )
			throw new Exception('El producto no existe');
		//##borrar imagen 
		if( $producto->ruta && is_file(PRODUCTOS_DIR . SB_DS . $producto->ruta) )
		{
			unlink(PRODUCTOS_DIR . SB_DS . $producto->ruta);
		}
		//##actualizar registro
		$this->Update('artref', ['ruta' => null], ['id_artref' => $producto->id_artref]);

		return true;
	}
	public function Borrar($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de producto invalido');
		$producto = $this->Obtener($id);
		if( !$producto )
			throw new Exception('El producto no existe');
		$query = "SELECT COUNT(claveProducto) as total FROM formulastelas
					WHERE claveProducto = '$producto->clave_prod'";
		$res = $this->GetObject($query);
		//print_r($res);
		if( (int)$res->total > 0 )
			throw new Exception('No puede borrar el producto, contiene referencias');
		//die(__FILE__);
		//##borrar imagen
		if( $producto->ruta && is_file(PRODUCTOS_DIR . SB_DS . $producto->ruta) )
		{
			unlink(PRODUCTOS_DIR . SB_DS . $producto->ruta);
		}
		$query = "DELETE FROM artref WHERE id_artref = $producto->id_artref LIMIT 1";
		$this->Query($query);

		return true;
	}
	public function ObtenerMarcas()
	{
		$query = "select id_artref,marca 
					from artref 
					where marca is not null and marca <> ''
					group by marca";
		return $this->GetResults($query);
	}
}