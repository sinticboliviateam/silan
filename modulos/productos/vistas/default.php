<?php
?>
<script type="text/javascript">
$(document).ready(function() {
	//$("nav #menu").find(".active").removeClass();
	//var links = $("nav #menu > li");
	//$(links[3]).find("a").first().addClass("active");
	$silan_productos._initform();
	$silan_productos.buscarRegistro();
});
</script>
<script src="modulos/productos/js/productos.class.js"></script>
<!-- <script src="modulos/productos/js/productosValidate.class.js"></script> -->
<div class="div-buscaRegistros">
	<div class="buscaRegistros">
		<h3>Productos</h3>
		<input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
		<span id="status-busca"></span>
	</div>
	<div class="boton-nuevoRegistro">
		<?php if( $escribir ): ?>
		<button id="btnNuevoProducto" class=""></button>
		<?php endif; ?>
	</div>
</div>
<div class="cleared" style="display: block; width: 95%;"></div>
<div class="tableRecords">
	<table class="">
	<thead class="tableHead">
		<tr class="">
		<th class="th-col06-col01">Clave</th>
		<th class="th-col06-col02">Clave anterior</th>
		<th class="th-col06-col03">SKU</th>
		<th class="th-col06-col04">Nombre</th>
		<th class="th-col06-col05">Color</th>
		<th class="th-col06-col06">Marca</th>
		<th class="th-col06-col07">Estatus</th>
	</tr>
	</thead>
	<tbody id="tbl01-records-busqueda">
	<tr class="" id="">
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
	</tbody>
	</table>
</div>
<div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
<div id="dialogProducto" style="display: none;">
	<div id="tabs-productos" style="width: 95%; height: auto; margin: 0 auto;">
		<ul>
			<li><a href="#tabs-datos-productos">Datos</a></li>
			<li><a href="#tabs-mas-productos">Mas</a></li>
		</ul>
		<div id="tabs-datos-productos">
			<form id="form-tab-datos-productos" enctype="multipart/form-data" method="post">
				<input type="hidden" id="id_artref" name="id_artref" value="0" />
			    <div class="flex-modal-produccion">
			        <div class="flex-modal-left-tab" style="width: 365px;">
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="clave_prod" class="oLabel-left">Clave</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="clave_prod" name="clave_prod" class="oInput" type="text" value="" required maxlength="20" tabindex="1"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="claveAnterior" class="oLabel-left">Clave anterior</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="claveAnterior" name="claveAnterior" class="oInput" type="text" maxlength="20" tabindex="2"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="sku" class="oLabel-left">SKU</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="sku" name="sku"  class="oInput" type="text" maxlength="12" tabindex="3"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="nombreProducto" class="oLabel-left">Nombre</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="nombreProducto" name="nombreProducto" class="oInput" type="text"  maxlength="30" tabindex="4"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="nombreMercado" class="oLabel-left">Nombre mercado</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="nombreMercado" name="nombreMercado" class="oInput" type="text"  maxlength="30" minlength="3" tabindex="5"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="color" class="oLabel-left">Color</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="color" name="color" class="oInput" type="text" maxlength="20" tabindex="6"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="referenciaColor" class="oLabel-left">Referencia</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="referenciaColor" name="referenciaColor" class="oInput" type="text" maxlength="3" tabindex="7"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="marca" class="oLabel-left" tabindex="8">Marca</label></div>
			                <div class="div-dataInput-tab">
			                    <select id="marca" name="marca" class="oSelect-normal-tab" required tabindex="8">
			                    	<?php echo $combos->selectMarca(); ?></select></div>
			            </div>
			        </div>

			        <div class="flex-modal-left-tab" style="width: 365px;">
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="ancho" class="oLabel-left">Ancho</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="ancho" name="ancho" class="oInput" required type="number" step="0.01" min="0.01" max="2.50" placeholder='0.00' tabindex="9"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="ruta"  class="oLabel-left">Imagen</label>
			                </div>
			                <div class="div-dataInput-tab">
			                    <input id="ruta" name="ruta" class="oInput" type="text" tabindex="10" readonly>
			                </div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="composicion01" class="oLabel-left">Composición 1</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="composicion01" name="composicion01"class="oInput" type="text" tabindex="11"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="composicion02" class="oLabel-left">Composición 2</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="composicion02" name="composicion02" class="oInput" type="text" tabindex="12"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="fechaRegistro" class="oLabel-left">Fecha registro</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="fechaRegistro" name="fechaRegistro" class="oInput" type="text" tabindex="13" readonly></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="idEstatus" class="oLabel-left">Estatus</label></div>
			                <div class="div-dataInput-tab">
			                    <select id="idEstatus" name="idEstatus" class="oSelect-normal-tab" tabindex="14"><?php echo $combos->selectEstatusProducto(6); ?></select></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="fechaEstatus" class="oLabel-left">Fecha estatus</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="fechaEstatus" name="fechaEstatus" class="oInput" type="text" tabindex="15" readonly value="<?php echo $combos->getDateFormat(date("d-m-Y h:i:s"), 1); ?>"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="usuarioEstatus" class="oLabel-left">Usuario estatus</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="usuarioEstatus" name="usuarioEstatus" class="oInput" type="text" tabindex="16" readonly value="<?php echo ($_SESSION["datauser"]["nombreCompleto"]); ?>"></div>
			            </div>
			        </div>

			        <div class="flex-modal-left-tab" style="width: 365px; padding: 0px;">
			            <div class="tabs-productos-botones">
			            	<?php if( $escribir ): ?>
			                <div id="guardar-datos-producto"></div>
			            	<?php endif; ?>
			                <?php if( $borrar ): ?>
			                <div id="borrar-datos-producto"></div>
			            	<?php endif; ?>
			                <div id="cerrar-tabs-producto" class="cerrar-tabs"></div>
			            </div>
			            <hr>
			            <div class="baseImagen" style="margin-top: 10px;">
			                <div id="div-imagen" class="imagenRegistro"></div>
			            </div>
			            <div class="tabs-productos-botones" style="justify-content: right; margin-top: 10px;">
			                <input type="file" name="file" id="fileupload" style="display: none;">
			                <button id="btnAgregaImagen" type="button" style="margin-right: 15px;"></button>
			                <button id="btnEliminaImagen" type="button"></button>
			            </div>
			        </div>
			    </div>
			</form>
		</div>
		<div id="tabs-mas-productos"></div>     
	</div>
</div>