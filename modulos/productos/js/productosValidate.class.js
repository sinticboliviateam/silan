/* 
 * 20180810 por Manuel Luna, Juan Marcelo Áviles
 * v1.0
 * 
*/
function productosValidate(){
    var dataform = {
        event : "productos"
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    
    this.clearobjvar = function(){
        dataform = {
            event : "productos"
        };
    };
    
    this.formValidateDatos = function(){
        $("#form-tab-datos-productos").validate({
            onfocusout: function(element) { 
                var context = $(element).val();
                if($(element).attr("name") === "clave")
                    $(element).val(context.replace(/\s/g,""));
                ($(element).valid())
                    $silan_validProductos.setobjvar($(element).attr("name"),$(element).val());
            },
            rules : {
                repeatPassword : { required : true, equalTo : "#password" }
            },
            messages: {
                claveProducto : { required : "Este campo es obligatorio" },
                marca : { required : "Este campo es obligatorio" },
                ancho : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form)
            {
                if(!$silan_validProductos.validFieldsProdData()){
                    $silan_msg.showmessage("Existen datos obligatorios dentro de la pestaña Datos.",
                                            "closemodal","Catálogo de productos");
                    $("#tabs-productos").tabs("option", "active", 0);
                    return;
                }
                //$.post('./include/productos.silan.php', {data:$silan_validProductos.getobjvar()}, function(data)
                $.post('index.php?mod=productos&task=guardar', {data:$silan_validProductos.getobjvar()}, function(data)
                {
                	$silan_msg.showmessage(data.mensaje || 'Registro guardado', 'reload', data.title || 'COMPLETADO');
                        
                },"json").fail(function(data){
                    //alert("ERROR 0 formValidate.-> "+data);
                	$silan_msg.showmessage(data.responseJSON.error || 'Error desconocido al guardar el registro', 'closemodal', data.title || 'ERROR');
                });
            }
        });
    };
    
    this.validFieldsProdData = function(){
        if($("#clave").val() === "" ||
            $("#marca").val() === "" ||
            $("#ancho").val() === ""){
            return false;
        }

        $silan_validProductos.setobjvar("clave_prod", $("#clave_prod").val());
        $silan_validProductos.setobjvar("claveAnterior", $("#claveAnterior").val());
        $silan_validProductos.setobjvar("sku", $("#sku").val());
        $silan_validProductos.setobjvar("nombreProducto", $("#nombreProducto").val());
        $silan_validProductos.setobjvar("nombreMercado", $("#nombreMercado").val());
        $silan_validProductos.setobjvar("color", $("#color").val());
        $silan_validProductos.setobjvar("referenciaColor", $("#referenciaColor").val());
        $silan_validProductos.setobjvar("marca", $("#marca").val());
        $silan_validProductos.setobjvar("ancho", $("#ancho").val());
        $silan_validProductos.setobjvar("ruta", $("#ruta").val());
        $silan_validProductos.setobjvar("composicion01", $("#composicion01").val());
        $silan_validProductos.setobjvar("composicion02", $("#composicion02").val());
        //$silan_validProductos.setobjvar("fechaRegistro", $("#fechaRegistro").val());
        $silan_validProductos.setobjvar("idEstatus", $("#idEstatus").val());
        $silan_validProductos.setobjvar("fechaEstatus", $("#fechaEstatus").val());
        $silan_validProductos.setobjvar("usuarioEstatus", $("#usuarioEstatus").val());
        
        return true;
    };
    
}
$silan_validProductos = new productosValidate();