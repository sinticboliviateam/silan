/* 
 * 20180810 por Manuel Luna, Juan Marcelo Áviles
 * v1.0
 * 
*/
function productos()
{
    var $this = this;
    var dataprod = {
        event : ""
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataprod : dataprod[field];
    };
    
    this.setobjvar = function(field, value){
        dataprod[field] = value;
    };
    
    this.clearobjvar = function(){
        dataprod = {
            event : ""
        };
    };
//*** INICIALIZA LOS ELEMENTOS DOM DEL FORM
    this._initform = function(){
        //$silan_productos.buscarRegistro();
        $("#tabs-productos").tabs();
        this.getRecordsTableProd();
        this.widgetEventProductos();
        this.widgetEventsTabsProductos();
        $("#form-tab-datos-productos").submit(this.GuardarProducto);
        jQuery('#fileupload').change(this.OnimagenSeleccionada);
    };
    this.NuevoProducto = function()
    {
        document.querySelector('#id_artref').value = 0;
        document.querySelector("#form-tab-datos-productos").reset();
        this.showDialogProd();

    };
    this.OnimagenSeleccionada = function()
    {
        let files = this.files;
        var reader = new FileReader();
        reader.onload = (e) => 
        {
            let img = new Image();
            img.src = reader.result;
            img.dataset.filename = files[0].name;
            img.style.width = '100%';
            img.style.height = '100%';
            jQuery('#div-imagen').html('').append(img);
        };
        reader.readAsDataURL(this.files[0]);
    };
 
//*** OPCIONES PARA BUSCAR REGISTROS DENTRO DEL GRID
    this.getRecordsTableProd = function(){
        var filter = $("#filter").val();
//        filter = filter.replace(/\s/g,"");
        $.post('./include/tablasSistemas.silan.php',
            {data: {opt:"productos", filter:filter}}, function(data){
                $("#tbl01-records-busqueda").append(data);
                $("#status-busca").html("");
            }).always(function(){
                
            });
    };
    
    this.buscarRegistro = function(){
        $("#filter").keyup(function(){
            $(".renglonesGrid").remove();
            $("#status-busca").html("Buscando...");
            $silan_productos.getRecordsTableProd();
        });
    };
//*** BOTONES FORM PRINCIPAL
    this.widgetEventProductos = function()
    {
        
        $("button#btnNuevoProducto")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                $this.NuevoProducto();
                
            });
         
        jQuery(document).on('click', "tr.renglonesGrid", function(e)
        {
            
            $.get('index.php?mod=productos&task=get&id=' + this.id, function(data)
            {
                //console.log(data);
                if(data.error !== 0)
                    $silan_msg.showmessage(data.msg,data.action,data.title);
            })
            .fail(function(data)
            {
                alert("ERROR 90 renglonesGrid.-> "+data);
            })
            .always(function(data)
            {    
                $this.editdataprod(data.producto);
            });
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
    };
    this.GuardarProducto = function()
    {
        var data = jQuery(this).serialize();
        var imageInput = document.querySelector('#fileupload');
        if( imageInput.files.length > 0 )
        {
            let img = document.querySelector('#div-imagen img');
            if( img )
            {
                var parts = img.src.split(',');
                data += '&imagen[archivo]='+ img.dataset.filename+'&imagen[data]=' + encodeURIComponent(parts[1]);
            }   

        }
        jQuery.post('index.php?mod=productos&task=guardar', data, function(res)
        {
            if( res.status == 'ok')
            {
                $silan_msg.showmessage(res.mensaje, 'reload', 'Producto Guardado');

            }
        })
        .fail(function(data)
        {
            
            $silan_msg.showmessage(data.responseJSON.error, 'closemodal', 'ERROR');
        });
        return false;
    };
    this.EliminarImagen = function()
    {
        document.querySelector('#div-imagen').innerHTML = '';
        let inputId = document.querySelector('#id_artref');
        if( inputId && inputId.value )
        {
            jQuery.get('index.php?mod=productos&task=BorrarImagen&id=' + inputId.value, function(res)
            {
                if( res.status == 'ok')
                {
                    $silan_msg.showmessage(res.mensaje, 'closemodal', 'Completado');

                }
            });
        }
    };
    this.BorrarProducto = function()
    {
        let inputId = document.querySelector('#id_artref');
        if( !inputId.value )
            return false;
        jQuery.get('index.php?mod=productos&task=borrar&id=' + inputId.value, function(res)
        {
             if( res.status == 'ok')
            {
                $silan_msg.showmessage(res.mensaje, 'reload', 'Borrado Completado');
            }
        })
        .fail(function(res)
        {
            $silan_msg.showmessage(res.responseJSON.error, 'closemodal', 'Error Borrado');
        });
    };
//*** BOTONES FORM TABS
    this.widgetEventsTabsProductos = function(){
    //*** PRODUCTOS
        $("#estatus").on("change", function(e){
            e.preventDefault();
            $.post('./include/phpcomm.php',
                {}, function(data){
                    $("#fechaEstatus").prop("value", data.date);
                    $("#usuarioEstatus").prop("value", data.name);
                },"json").fail(function(data){
                    alert("ERROR 0 renglonesGrid.-> "+data);
                });
        });
        
        $("#guardar-datos-producto")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#form-tab-datos-productos").submit();
            });
            
        $("#borrar-datos-producto")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_msg.showmessagecondition('Estas seguro desear borrar este registro?', 
                    $this.BorrarProducto, (modal) => {}, 'Borrar');
                
            });
            
        $(".cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#dialogUser").dialog("close");
                location.reload();
            });
            
        $("#btnAgregaImagen")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Agregar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#fileupload").trigger("click");
            });
            
        $("#btnEliminaImagen")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Eliminar"
            })
            .on("click",function(e){
                $this.EliminarImagen();
                return false;
            });
        
    };
    this.showDialogProd = function(){
        $("#dialogProducto").dialog({
            modal: true,
            autoOpen: false,
            title: "Actualización de Productos",
            dialogClass: 'hide-close',
            width: 1140,
            maxHeight: 600,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
        $(".tabs-productos-botones").css({"font-size":"1.4em"});
        jQuery('#dialogProducto').dialog("open");
    };
    
//*** EDITA LOS DATOS DEL PRODUCTO SELECCIONADO
    this.editdataprod = function(data)
    {
        //console.log(data);
        for(var col in data)
        {
            var id = '#' + col;

            //console.log(id, col);
            $(id).val(data[col]);    
        }
        if( data.ruta )
        {    
            var img = new Image();
            img.src = '/SiLan/img/clavesprods/' + data.ruta;
            img.style.width = '100%';
            img.style.height = '100%';
            jQuery('#div-imagen').html('').append(img);
        }
        /*
    	$silan_validProductos.setobjvar("id_artref", data.idUsuario)
        $("#nombreCompleto").prop("value", data.nombreCompleto);
        $("#usuario").prop("value", data.usuario);
        $("#password").prop("value", data.password);
        $("#repeatPassword").prop("value", data.password);
        $("#rfc").prop("value", data.rfc);
        $("#tipoUsuario").prop("value", data.rfc);
//        $("input[name=tipoUsuario][value='"+data.tipoUsuario+"']").prop("checked", true); CASO: OPTION o CHECKBOX
        $("select[name=tipoUsuario]").prop("value", data.tipoUsuario);
        $("#puesto").prop("value", data.puesto);
        $("#horario").prop("value", data.horario);
        $("#correos").prop("value", data.correos);
        $("#celulares").prop("value", data.celulares);
        $("#telefonos").prop("value", data.telefonos);
        $("#ultimoAcceso").prop("value", data.ultimoAcceso);
        $("select[name=estatus]").prop("value", data.idEstatus);
//        $("input[name=estatus][value="+data.estatus+"]").prop("checked", true); CASO: OPTION o CHECKBOX
        $("#fechaEstatus").prop("value", data.fechaEstatus);
        $("#usuarioEstatus").prop("value", data.usuarioEstatus);
        
        $('#div-imagen').css({"background":"url('"+data.imagen+"') center no-repeat", "background-size": "contain"});
        //$silan_productos.editUserFunctions(data.functions);
        */
        this.showDialogProd();
        
        //$silan_validProductos.setobjvar("event","update");
    };
}
$silan_productos = new productos();