<?php
?>
<div id="piezas-container">
	<h2>Piezas</h2>
	<div class="div-buscaRegistros">
		<form action="" method="get">
			<input type="hidden" name="mod" value="piezas" />
			<div>
				<input type="text" id="keyword" name="keyword" value="" autocomplete="off" />
			</div>
		</form>
	</div><br/>
	<div class="tableRecords">
		<table id="table-items">
		<thead class="tableHead">
		<tr>
			<th>ID</th>
			<th>Clave</th>
			<th>Metros</th>
			<th>Kilos</th>
			<th>Nro. Pieza</th>
			<th>Acciones</th>
		</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	</div>
	<p class="pager">
		
	</p>
	<br/><br/>
</div>
<div id="dialog-impresion"><p>Se encuentra preparada la impresora?</p></div>
<script src="modulos/piezas/js/piezas.js"></script>