/**
 * 
 */
class Piezas
{
	constructor()
	{
		this.SetEvents();
	}
	Obtener(keyword)
	{
		let params = 'mod=piezas&task=ObtenerItems';
		if( keyword )
			params += '&keyword=' + keyword;
		jQuery.get('index.php?' + params, (res) => 
		{
			this.Fill(res.items);
			this.BuildPager(res.totalPages, res.totalItems);
		}).fail( (error) => 
		{
			alert(error.responseJSON.error || 'Ocurrio un error al obtener las piezas');
		});
	}
	Fill(items)
	{
		let tbody = jQuery('#table-items tbody');
		tbody.html('');
		for(let item of items)
		{
			let row = document.createElement('tr');
			let td1	= document.createElement('td');
			let td2	= document.createElement('td');
			let td3	= document.createElement('td');
			let td4	= document.createElement('td');
			let td5	= document.createElement('td');
			let tdAcciones	= document.createElement('td');
			
			let	btnPrint		= document.createElement('a');
			btnPrint.href 		= 'javascript:;';
			btnPrint.className 	= 'button';
			btnPrint.title		= 'Imprimir Etiqueta';
			btnPrint.innerHTML	= 'Imprimir';
			btnPrint.addEventListener('click', (e) => this.PrintLabel(item));
			td1.innerHTML 		= item.idPiezas;
			td2.innerHTML 		= item.clavePieza;
			td3.innerHTML 		= item.metros;
			td4.innerHTML		= item.kilos;
			td5.innerHTML		= item.noPieza;
			tdAcciones.appendChild(btnPrint);
			
			row.appendChild(td1);
			row.appendChild(td2);
			row.appendChild(td3);
			row.appendChild(td4);
			row.appendChild(td5);
			row.appendChild(tdAcciones);
			tbody.append(row);
		}
	}
	PrintLabel(item)
	{
		function EnviarImpresion(id)
		{
			jQuery.get('index.php?mod=piezas&task=imprimir&id=' + id, function(res)
			{
				console.log(res);
			});
		}
		let dlg = jQuery("#dialog-impresion");
		dlg.dialog({
            modal: true,
            autoOpen: true,
            title: "Impresion de Etiqueta",
            dialogClass: 'hide-close',
            //width: 750,
            resizable: false,
            //position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true,
            close: function(event, ui)
            {
            },
            buttons: [
                      {
                    	  text: 'Cancelar',
                    	  icon: '',
                    	  click: () => {dlg.dialog('close');},
                    	  
                      },
                      {
                    	  text: 'Imprimir',
                    	  icon: '',
                    	  click: () => 
                      	  {
                      		  dlg.dialog('close');
                      		  EnviarImpresion(item.idPiezas);
                      	  }
                      }
            ]
        }).show();
	}
	SetEvents()
	{
		let timeOut = null;
		let keyword	= jQuery('#keyword');
		keyword.keyup( (e) => 
		{
			if( timeOut )
				clearTimeout(timeOut);
			timeOut = setTimeout( () => 
			{
				this.Obtener(keyword.val());
			}, 500);
		});
	}
	BuildPager()
	{
		
	}
}
jQuery(function()
{
	window.piezas = new Piezas();
	piezas.Obtener();
});