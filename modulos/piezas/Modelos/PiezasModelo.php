<?php
class PiezasModelo extends Modelo
{
	public function Obtener($id)
	{
		if( !$id )
			throw new Exception($id);
		
		$query = "SELECT * FROM piezas WHERE idPiezas = $id LIMIT 1";
		
		return $this->GetObject($query);
	}
	public function ObtenerTodos(array $args, &$totalPages, &$totalItems)
	{
		$defaultLimit 	= 50;
		$args['limit'] 	= isset($args['limit']) ? $args['limit'] : $defaultLimit;
		$pagina			= isset($args['pagina']) ? (int)$args['pagina'] : 1;
		if( $pagina <= 0 )
			$pagina = 1;
		
		$limit			= $args['limit'];
		$offset 		= 0;
		$query = "SELECT * FROM piezas ";
		$query .= "WHERE 1 = 1 ";
		$query .= "AND fechaEntrada IS NOT NULL AND fechaSalida IS NULL ";
		if( $pagina )
		{
			$queryc 		= "SELECT COUNT(idPiezas) as records FROM piezas";
			$row		= $this->GetObject($queryc);
			$totalItems = $row->records;
			$totalPages = ceil($totalItems / $limit);
			$offset		= $pagina == 1 ? 0 : (($pagina - 1) * $limit);
		}
		if( isset($args['keyword']) && $args['keyword'] )
		{
			$keyword = trim($args['keyword']);
			/*
			if( stristr(strtoupper($keyword), 'NRO') )
			{
				$pieza = (int)str_replace('NRO', '', strtoupper($keyword));
				$query .= "AND noPieza LIKE '%$pieza%' ";
			}
			*/
			if( $keyword )
			{
				$query .= "AND (idPiezas LIKE '%{$keyword}%' OR noPieza LIKE '%$keyword%' OR metros LIKE '%$keyword%') ";
			}
			
		}
		$query .= "ORDER BY idPiezas DESC ";
		$query .= "LIMIT $offset, $limit";
		return $this->GetResults($query);
	}
	public function Imprimir($pieza)
	{
		$date = date('d/m/Y');
		$metros = sb_fill_zeros(str_replace('.', '', $pieza->metros), 5);
		$zpl = "";
		//zpl="CT~~CD,~CC^~CT~";
		$zpl.= "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ\n";
		$zpl.= "^XA\n";
		$zpl.= "^MMT\n";
		$zpl.= "^PW831\n";
		$zpl.= "^LL0405\n";
		$zpl.= "^LS0\n";
		$zpl.= "^FT87,235^A0N,39,38^FH\^FDEscolar^FS\n";
		$zpl.= "^FT86,313^A0N,34,33^FH\^FD{$date}^FS\n";
		$zpl.= "^FT479,351^A0N,31,31^FH\^FDML^FS\n";
		$zpl.= "^BY4,2,128^FT123,161^BEN,,Y,N\n";
		$zpl.= "^FD2000001874004^FS\n";
		$zpl.= "^BY3,3,94^FT522,323^BCN,,Y,N\n";
		//$zpl.= "^FD>;0500>60^FS\n";
		$zpl.= "^FD>;{$metros}^FS\n";
		$zpl.= "^PQ1,0,1,Y^XZ\n";
		require_once BASE_DIR . SB_DS . "include/lib/ZebraPrint/PrintSend.php";
		require_once BASE_DIR . SB_DS . "include/lib/ZebraPrint/PrintSendLPR.php";
		$lpr 			= new PrintSendLPR();
		$lpr->setHost(IP_IMPRESORA);
		$lpr->setData($zpl);
		$res = $lpr->printJob("etiquetas");
		return $res;
	}
	public function ObtenerPoliza($id)
	{
		$query = "SELECT * FROM polizas WHERE idPoliza = $id LIMIT 1";
		
		return $this->GetObject($query);
	}
	/**
	 * Obtienes las piezas sin asignacion
	 * 
	 * @return Ambigous <multitype:, multitype:unknown >
	 */
	public function ObtenerPiezaNoAsignadas($filter = null)
	{
		$query = "SELECT * FROM piezas WHERE noPieza > 0 and documentoEntrada = '' ";
		if( $filter )
		{
			$query .= "AND (clavePieza LIKE '%$filter%' OR kilos LIKE '%$filter%' OR metros LIKE '%$filter%') ";
		}
		$query .= "order by clavePieza";
		//var_dump($query);
		$piezas = $this->GetResults($query);
		for($i = 0; $i < count($piezas); $i++)
		{
			$piezas[$i]->poliza = $this->ObtenerPoliza($piezas[$i]->idPoliza);
		}
		
		return $piezas;
	}
	public function ObtenerPiezasAntiguas($filter = null)
	{
		$query = "SELECT * FROM piezas WHERE fechaEntrada = '2019-04-14 00:01:00' ";
		if( $filter )
		{
			$query .= "AND (documentoEntrada LIKE '%$filter%' OR clavePieza LIKE '%$filter%' OR kilos LIKE '%$filter%' OR metros LIKE '%$filter%') ";
		}
		$query .= "order by clavePieza";
		//var_dump($query);
		$piezas = $this->GetResults($query);
		for($i = 0; $i < count($piezas); $i++)
		{
			$piezas[$i]->poliza = $this->ObtenerPoliza($piezas[$i]->idPoliza);
		}
		
		return $piezas;
	}
}