<?php
class PiezasControlador extends Controlador
{
	/**
	 * 
	 * @var PiezasModelo
	 */
	protected	$modelo;
	
	public function Init()
	{
		$this->modelo = $this->CargarModelo();
	}
	public function TaskDefault()
	{
		try
		{
			$this->CargarVista('default');
		}
		catch(Exception $e)
		{
			print (['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskObtenerItems()
	{
		try
		{
			$keyword 	= isset($_GET['keyword']) ? $_GET['keyword'] : null;
			$pagina		= isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
			$args		= ['keyword' => $keyword, 'pagina' => $pagina];
			$items 		= $this->modelo->ObtenerTodos($args, $pages, $totalItems);
			$this->ResponseJSON(['items' => $items, 'totalPages' => (int)$pages, 'totalItems' => (int)$totalItems]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
		
	}
	public function TaskImprimir()
	{
		try
		{
			$id = isset($_GET['id']) ? (int)$_GET['id'] : null;
			if( !$id )
				throw new Exception('Identificador de pieza invalido');
			$pieza = $this->modelo->Obtener($id);
			if( !$pieza )
				throw new Exception('La pieza no existe');
			$this->modelo->Imprimir($pieza);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'La pieza se envio a la impresora correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}