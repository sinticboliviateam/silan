<?php
class UsuariosControlador extends Controlador
{
	/**
	 * 
	 * @var UsuariosModelo
	 */
	protected $usuariosModelo;
	
	public function Init()
	{
		$this->usuariosModelo = $this->CargarModelo();
	}
	public function TaskDefault()
	{
		$this->CargarVista('default');
	}
	public function TaskGuardar()
	{
		try
		{
			if( !isset($_REQUEST['data']) )
				throw new Exception('No existen datos para la solicitud');
			$data = (object)$_REQUEST["data"];
			unset($data->event);
			$data->idEstatus = $data->estatus;
			unset($data->estatus);
			$mensaje = 'El usuario fue creado correctamente';
			if( !isset($data->idUsuario) || !$data->idUsuario )
			{
				$this->usuariosModelo->Crear($data);
			}
			else
			{
				$this->usuariosModelo->Actualizar($data);
				$mensaje = 'El usuario fue acutlizado correctamente';
			}
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => $mensaje]);	
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskUploadImage()
	{
		try
		{
			if ( !isset($_FILES["file"]) || $_FILES["file"]['size'] <= 0 )
				throw new Exception('Archivo invalid');
			$file 			= $_FILES["file"];
			$nombre 		= $file["name"];
			$tipo 			= $file["type"];
			$ruta_provisional = $file["tmp_name"];
			$size 			= $file["size"];
			$dimensiones	= getimagesize($ruta_provisional);
			$width 			= $dimensiones[0];
			$height 		= $dimensiones[1];
			$carpeta 		= USUARIOS_DIR; //"/../".$_SESSION["datauser"]["rutaImagen"];
			$url			= USUARIOS_URL;
			if($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png'){
				throw new Exception("Error, el archivo no es una imagen");
			} elseif ($size > 1024*1024) {
				throw new Exception("Error, el tamaño máximo permitido es un 1MB");
			} 
			else 
			{
				$src = $carpeta . SB_DS . $nombre;
				move_uploaded_file($ruta_provisional, $src);
				$url .= '/' . $nombre;
			}
			$_SESSION["record"]["imagen"] = $nombre;
			
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => '', 'url' => $url]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
		
	}
}