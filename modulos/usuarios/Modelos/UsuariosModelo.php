<?php
class UsuariosModelo extends Modelo
{
	public function Obtener($id)
	{
		$id = (int)$id;
		$query = "SELECT * FROM usuarios WHERE idUsuario = $id LIMIT 1";
		
		return $this->GetObject($query);
	}
	public function Crear($usuario)
	{
		if( !isset($usuario->password) || empty($usuario->password) )
			throw new Exception('Debe asignar una contraseña para el usuario');
		if( isset($usuario->repeatPassword) )
		{
			unset($usuario->repeatPassword);
		}
		if( empty($usuario->rfc) )
			throw new Exception('El RFC esta vacio');
		$query = "SELECT idUsuario FROM usuarios WHERE rfc = '$usuario->rfc' LIMIT 1";
		$row = $this->GetObject($query);
		if( $row )
			throw new Exception('El RFC ya existe');

		$usuario->idUsuario 		= null;
		$usuario->ultimoAcceso 		= null;
		$usuario->usuario			= trim($usuario->usuario);
		$usuario->password			= trim($usuario->password);
		$usuario->fechaEstatus		= date('Y-m-d H:i:s');
		$usuario->imagen			= isset($_SESSION["record"]["imagen"]) ? $_SESSION["record"]["imagen"] : '';
		$usuario->usuarioEstatus	= utf8_decode($_SESSION['datauser']['nombreCompleto']);
		$usuario->nombreCompleto	= utf8_decode($usuario->nombreCompleto);
		$usuario->puesto			= utf8_decode($usuario->puesto);
		
		$funciones = null;
		if( isset($usuario->functions) )
		{
			$funciones = $usuario->functions;
			unset($usuario->functions);
		}
		$id = $this->Insert('usuarios', $usuario);
		
		$nuevoUsuario = $this->Obtener($id);	
		if( $funciones )
		{
			$this->AsignarFunciones($nuevoUsuario, $funciones);
		}	
		unset($_SESSION["record"]["imagen"]);
		return $nuevoUsuario;
	}
	public function Actualizar($usuario)
	{
		if( !$usuario->idUsuario )
			throw new Exception('Identificador de usuario invalido');
		
		$usuarioG = $this->Obtener($usuario->idUsuario);
		if( !$usuarioG )
			throw new Exception('El usuario no existe');
		$usuarioG->nombreCompleto 	= utf8_decode($usuario->nombreCompleto);
		$usuarioG->usuario 			= utf8_decode($usuario->usuario);
		$usuarioG->password 		= utf8_decode($usuario->password);
		$usuarioG->rfc 				= utf8_decode($usuario->rfc);
		$usuarioG->tipoUsuario 		= utf8_decode($usuario->tipoUsuario);
		$usuarioG->puesto			= utf8_decode($usuario->puesto);
		$usuarioG->horario 			= utf8_decode($usuario->horario);
		$usuarioG->correos 			= utf8_decode($usuario->correos);
		$usuarioG->celulares		= utf8_decode($usuario->celulares);
		$usuarioG->telefonos		= utf8_decode($usuario->telefonos);
		$usuarioG->idEstatus		= (int)$usuario->idEstatus;
		//20180702 ml, falta verificar cambio de estatus entonces guardar fechaEstatus y usuarioEstatus
		$usuarioG->imagen			= isset($_SESSION["record"]["imagen"]) ? $_SESSION["record"]["imagen"] : $usuarioG->imagen; 
		$funciones = null;
		if( isset($usuario->functions) )
		{
			$funciones = $usuario->functions;
			unset($usuario->functions);
		}
		
		$this->Update('usuarios', $usuarioG, ['idUsuario' => $usuarioG->idUsuario]);
		$this->AsignarFunciones($usuarioG, $funciones);
		unset($_SESSION["record"]["imagen"]);
		return $usuarioG;
	}
	public function AsignarFunciones($usuario, $funciones)
	{
		if( !$usuario )
			throw new Exception('El usuario es invalido, no se pueden adicionar las funciones');
		if( !is_array($funciones) || !count($funciones) )
			return false;

		//** ELIMINA RELACION FUNCIONES => USUARIOS
		$sql = "DELETE FROM usuariosfunciones WHERE idUsuario = {$usuario->idUsuario}";
		$result = $this->Query($sql);
		$values = "";
		foreach ($funciones as $value) 
		{
			if( empty($value['item']) || $value["item"] < 100) continue;
			$values.= ($values != "") ? ",(".($value["item"]-100).",".$usuario->idUsuario.",'".$value["val"]."')" : 
										"(".($value["item"]-100).",".$usuario->idUsuario.",'".$value["val"]."')";
		}
		unset($result); unset($sql);
		if( empty($values) )
			return false;
		//** INSERTA NUEVA RELACION FUNCIONES => USUARIOS
		$sql = "INSERT INTO usuariosfunciones VALUES".$values;
		$result = $this->Query($sql);
	}
}