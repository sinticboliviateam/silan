<?php
?>
<script type="text/javascript">
$(document).ready(function() {
	//$("nav #menu").find(".active").removeClass();
	//var links = $("nav #menu > li");
	//$(links[3]).find("a").first().addClass("active");
	$silan_usuarios.widgetEventUsuarios();
	$silan_usuarios._initform();
	$silan_usuarios.buscarRegistro();
});
</script>
<script src="modulos/usuarios/js/usuarios.class.js"></script>
<script src="modulos/usuarios/js/usuariosValidate.class.js"></script>
<div class="div-buscaRegistros">
	<div class="buscaRegistros">
		<h3>Usuarios</h3>
		<input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
		<span id="status-busca"></span>
	</div>
	<div class="boton-nuevoRegistro">
		<button id="btnNuevoUsuario" class=""></button>
	</div>
</div>
<div class="cleared" style="display: block; width: 95%;"></div>
<div class="tableRecords">
	<table class="">
	<thead class="tableHead">
		<tr class="">
		<th class="th-col06-col01">Nombre</th>
		<th class="th-col06-col02">Usuario</th>
		<th class="th-col06-col03">RFC</th>
		<th class="th-col06-col04">Puesto</th>
		<th class="th-col06-col05">Estatus</th>
		<th class="th-col06-col06">Último acceso</th>
	</tr>
	</thead>
	<tbody id="tbl01-records-busqueda">
	<tr class="" id="">
		<td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
	</tbody>
	</table>
</div>
<div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
<div id="dialogUser" style="display: none;">
	<div id="tabs-usuarios" style="width: 95%; height: auto; margin: 0 auto;">
		<ul>
			<li><a href="#tabs-datos-usuarios">Datos</a></li>
			<li><a href="#tabs-funciones-usuarios">Funciones</a></li>
		</ul>
		<div id="tabs-datos-usuarios"></div>
		<div id="tabs-funciones-usuarios"></div>     
	</div>
</div>