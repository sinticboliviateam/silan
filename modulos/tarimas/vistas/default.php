<?php
$autPerimisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02080200";
$permisos = $autPerimisos->regresaPermisosUsuario();
$escribir   = strstr($permisos, 'E') ? true : false;
$borrar   = strstr($permisos, 'B') ? true : false;
?>
<script src="modulos/tarimas/js/tarimas.class.js"></script>
<script src="modulos/tarimas/js/tarimasValidate.class.js"></script>
<script>
$(document).ready(function(){
	Tarimas.ready();
	$(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
	$("#fechaSalida").datepicker($.datepicker.regional[ "es" ] );
	jQuery('#filter').focus();
});
var estatusNuevo = <?php print json_encode($estatusNuevo); ?>;
</script>
<div class="div-buscaRegistros">
	<div class="buscaRegistros">
		<h3>Tarimas</h3>
		<form action="index.php" method="get">
			<input type="hidden" name="mod" value="tarimas" />
			<input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" tabindex="1" value="<?php print $filtro; ?>">
			<span id="status-busca"></span>
		</form>
	</div>
    <?php if( $escribir ): ?>
	<div class="boton-nuevoRegistro">
		<button id="btnNuevaTarima" class="">Nuevo</button>
	</div>
    <?php endif; ?>
</div>
<div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col06-col02">Orden compra</th>
                            <th class="th-col05-col01">No. Tarima</th>
                            
                            <th class="th-col06-col03">Fecha</th>
                            <th class="th-col05-col04">Destino</th>
                            <th class="th-col06-col03">Clave Producto</th>
                            <th class="th-col06-col03">Piezas</th>
                            <th class="th-col06-col03">Mts.</th>
                            <th class="th-col06-col03">Estatus</th>
                            
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                    <?php $total_metros = 0; $total_piezas = 0; foreach($tarimas as $t): ?>
                    <?php 
                    $total_metros += $t->metros;
                    $total_piezas += $t->total_piezas;
                    ?>
                    <tr class="renglonesGrid" id="" data-id="<?php print $t->idtarima?>">
                        <td><?php print $t->ordenCompra; ?></td>
                        <td><?php print $t->noTarima; ?></td>
                        
                        <td><?php print $t->fechaSalida; ?></td>
                        <td><?php printf("%d - %s", $t->clave_destino, utf8_encode($t->nombre_destino));?></td>
                        <td><?php print $t->total_sku > 1 ? 'Multiple' : explode('-', $t->clavePieza)[0]; ?></td>
                        <td><?php print $t->total_piezas; ?></td>
                        <td><?php print $t->metros; ?></td>
                        <td><?php print $t->estatus; ?></td>
                        
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot class="tableHead">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <th style="color:#000;"><?php print $total_piezas; ?></th>
                        <th style="color:#000;"><?php print $total_metros; ?></th>
                        <td></td>
                    </tfoot>
                </table>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div id="dialogTarima" style="display: none;">
                <form id="form-datos-tarima" action="" method="post">
                	<input type="hidden" id="idTarima" name="idTarima" value="" />
                    <div class="flex-modal-tacome">
                        <div class="flex-modal-left-tab" style="width: 365px;">
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="noTarima" class="oLabel-left">No. Tarima</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="noTarima" name="noTarima" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="noOrdenCompra" class="oLabel-left">No. Orden compra</label></div>
                                <div class="div-dataInput-tab">
                                    <select id="noOrdenCompra" name="noOrdenCompra" class="oSelect-normal-tab" tabindex="13">
                                    	<option value="">-- orden de compra --</option>
                                    	<?php echo $combos->selectOrdenesCompra(); ?>
                                    </select></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="cliente" class="oLabel-left">Cliente</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="cliente" name="cliente" class="oInput campoRequerido" data-auto="" data-id="<?php print $cliente['idClientes']; ?>"type="text" 
                                    	value="<?php echo $cliente['nombre']; ?>" required readonly tabindex="1">
								</div>
                            </div>

                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="destino" class="oLabel-left">Destino</label></div>
                                <div class="div-dataInput-tab">
                                    <select id="destino" name="destino" class="oSelect-normal-tab">
                                        <option value="">-- destino --</option>
                                        <?php print $combos->selectDestinosOC(); ?>
                                    </select>
                                    <!--
                                    <input id="destino" name="destino" class="oInput campoRequerido" data-auto="" type="text" value="" required readonly tabindex="1">
                                    -->
                                </div>

                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab" style="">
                                    <label for="noPieza" class="oLabel-left">Número de pieza</label></div>
                                <div class="div-dataInput-tab" style="">
                                    <input id="noPieza" name="noPieza" class="oInput campoRequerido" type="text" value="" tabindex="2"></div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="fechaSalida" class="oLabel-left">Fecha de salida</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="fechaSalida" name="fechaSalida" value="<?php print date('d/m/Y'); ?>" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly >
                                </div>
                            </div>
                         
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="fechaSalida" class="oLabel-left">Estatus</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="estatus" name="estatus" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value="">
                                </div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="fechaSalida" class="oLabel-left">Fecha Estatus</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="fechaEstatus" name="fechaEstatus" value="<?php print date('d/m/Y'); ?>" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly >
                                </div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="usuarioSalida" class="oLabel-left">Usuario Estatus</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="usuarioSalida" name="usuarioSalida" class="oInput" style="text-align: center; font-weight: bold;" 
                                    	type="text" readonly 
                                        value="<?php print utf8_encode($_SESSION['datauser']['nombreCompleto']); ?>"></div>
                            </div>
                        </div>
                        <div class="flex-modal-left-tab" style="width: 365px;">
                            
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab">
                                    <label for="listaPiezas" class="oLabel-left">Claves de las piezas</label>
								</div>
                                <div id="listaPiezas" class="div-oList">
                                	<table></table>
                                </div>
                            </div>
                            <div class="block-info-tab">
                                <div class="div-oLabel-tab" style="width: 100px;">
                                    <label for="no_piezas" class="oLabel-left">No. de piezas</label></div>
                                <div class="div-dataInput-tab">
                                    <input id="no_piezas" name="no_piezas" class="oInput" style="text-align: right; font-weight: bold; width: 80px;" type="text" readonly value="0"></div>
                            </div>
                        </div>
                    </div>
                    <p>
                        <?php if( $escribir ): ?>
						<button type="submit" id="btn-guardar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-disk"></span> Guardar
						</button>
                        <?php endif; ?>
                        <?php if( $borrar ): ?>
						<a href="javascript:;" id="btn-borrar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-trash"></span> Borrar</a>
                        <?php endif; ?>
						<a href="javascript:;" id="btn-regresar" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-arrowreturnthick-1-w"></span> Regresar
						</a>
						<a href="javascript:;" id="btn-imprimir-barcode" class="ui-button ui-widget ui-corner-all">
							<span class="ui-button-icon ui-icon ui-icon-print"></span> Imprimir Codigo de Barras
						</a>
					</p>
                </form>
            </div>