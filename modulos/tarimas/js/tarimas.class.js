/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna / frank
 * MEXICO, 2017
 * SiLan v1.0
 * Modificaciones: 2017-03-17
*/

var Tarimas = 
{
    numero_pieza: 			null,
    listaPiezasSinSalida: 	[],
    mapaPiezas: 			{},
    focus: 					false,
    debug: 					false,
    piezas: 				[],
    ready: function() 
    {
        if (this.debug) console.log('Tarimas.ready()');
        Tarimas.init();
        Tarimas.buscarRegistro();
        Tarimas.SetEvents();
    },
    buscarRegistro: function()
    {
    	/*
        $("#filter").keyup(function(){
            $(".renglonesGrid").remove();
            $("#status-busca").html("Buscando...");
            Tarimas.getRecordsTarimas();
            
        });
        */
    },
    init: function()
    {
    	SalidaPieza.init();
        Numero_Pieza.init();
        NumeroCliente.init();
        $("div#dialogTarima").dialog({
            modal: true,
            autoOpen: !true,
            title: "Tarimas",
            dialogClass: 'hide-close',
            width: 750,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
    },

    actualizaPieza: function() {
        var numero_pieza =  Numero_Pieza.val();
        if(numero_pieza !== "" && Salidas_pt.numero_pieza !== numero_pieza) {
            if (this.debug) console.log('actualizaPieza: ' + numero_pieza);
            if (Salidas_pt.existeNumero_Pieza(numero_pieza)) {
                Salidas_pt.numero_pieza = numero_pieza;
            } else {
                $silan_msg.showmessage('Clave de pieza inválida: ' + numero_pieza,"closemodal","Salidas Pt");
                Salidas_pt.numero_pieza = null;
            }
        }
    },
    existeNumero_Pieza: function(numero_pieza) {
        if (this.debug) console.log('existeNumero_Pieza: ' + numero_pieza);
        return ($.inArray(numero_pieza, Salidas_pt.listaPiezasSinSalida) !== -1);
    },

    valida: function() {
        if (this.debug) console.log('valida');
        var focus = false;
        var valido = true;

        if (!NumeroCliente.valida()) {
            if(!focus) focus = $(NumeroCliente.id);
            valido = false;
        }

        if (!ListaPiezas.valida()) {
            if(!focus) focus = $(ListaPiezas.id);
            valido = false;
        }

        if (!valido) {
            if (focus) focus.focus();
        }

        return valido;
    },
    OnBtnNuevoClicked: function()
    {
    	
    	jQuery('#form-datos-tarima').get(0).reset();
    	jQuery('#idTarima').val('');
    	jQuery('#noOrdenCompra').attr('disabled', false);
        jQuery('#destino').val('');
    	jQuery('#estatus').val(estatusNuevo.nombre);
    	jQuery('#listaPiezas table').html('');
    	jQuery('#btn-imprimir-barcode').css('display', 'none');
    	jQuery("div#dialogTarima").dialog().dialog('open');
    	jQuery('#listaPiezas table').html('');
    	Tarimas.piezas = [];
    },
    OnFormSubmit: function()
    {
    	var tarima = {
    			idtarima: 		jQuery('#idTarima').val(),
    			noTarima: 		0,
    			idOrdenCompra: 	jQuery('#noOrdenCompra').val(),
    			idCliente: 		jQuery('#cliente').get(0).dataset.id,
                idDestino:      jQuery('#destino').val(),
    			fechaSalida: 	jQuery('#fechaSalida').val(),
    			idEstatus: 		null,
    			fechaEstatus: 	'',
    			usuarioEstatus:	'',
    			piezas:			Tarimas.piezas
    	};
    	try
    	{
    		if( !tarima.idOrdenCompra )
    			throw 'Debe seleccionar un numero orden';
    		if( !tarima.idCliente )
    			throw 'Identificador de cliente invalido';
            if( !tarima.idDestino )
                throw 'Debe seleccionar un destino';

    		if( !tarima.piezas || tarima.piezas.length <= 0 )
    			throw 'Debe ingresar almenos una pieza para la tarima';
    		
        	jQuery.post('index.php?mod=tarimas&task=guardar', JSON.stringify(tarima), function(res)
        	{
                console.log(res);
        		if( res && res.status == 'ok' )
        		{
        			alert(res.mensaje);
        			window.location.reload();
        		}
        		else
        		{
        			alert(res.error || 'Error desconocido al tratar de guardar la tarima');
        		}
        	}).fail(function(data)
        	{
                console.log(data.responseJSON);
        		alert("Ocurrio un error desconocido al tratar de guardar la tarima");
		    });
    	}
    	catch(e)
    	{
    		alert(e);
    	}
    	
    	return false;
    },
    OnBtnBorrarClicked: function()
    {
    	var params = 'mod=tarimas&task=Borrar&id=' + jQuery('#idTarima').val();
    	jQuery.get('index.php?' + params, function(res)
    	{
    		if( res.status == 'ok' )
    		{
    			alert(res.mensaje);
                window.location.reload();
    		}
    		else
    		{
    			alert(res.error || 'Error desconocido al borrar la tarima');
    		}
    	}).fail(function(data)
    	{
    		//console.log(data);
    		alert(data.responseJSON.error || 'Error desconocido al borrar la tarima');
    	});
    },
    OnBtnRegresarClicked: function()
    {
    	jQuery("#dialogTarima").dialog().dialog('close');
    	return false;
    },
    SetEvents: function()
    {
    	jQuery("#noOrdenCompra").change(function(e){
            jQuery.post('./include/combos.php', 
                {data:{widget:'destino',value:$("#noOrdenCompra").val()}}, 
                function(data){},"json").always(function(data)
                {
                    $("#destino").val(data.responseText);
                });
		});
		    
		$("tr.renglonesGrid").on("click",function(e){
		    e.preventDefault();
		    
		    //$silan_validOrdenCompra.setobjvar("event","edit");
		    //$silan_validOrdenCompra.setobjvar("key",this.id);
		    
		    jQuery.get('index.php?mod=tarimas&task=obtener&id=' + this.dataset.id, function(tarima)
		    {
		    	if( tarima )
		    		Tarimas.Editar(tarima);
		    	
		    })/*.fail(function(data)
            {
                console.log(data);
		            alert("ERROR 0 renglonesGrid.-> "+data);
		    })/*.always(function(data){
		            
		    })*/;
		    
		});
    	jQuery('#btnNuevaTarima').click(Tarimas.OnBtnNuevoClicked);
    	jQuery('#form-datos-tarima').submit(Tarimas.OnFormSubmit);
    	jQuery('#btn-borrar').click(Tarimas.OnBtnBorrarClicked);
    	jQuery('#btn-regresar').click(Tarimas.OnBtnRegresarClicked);
    	jQuery(document).on('click', '#listaPiezas .pieza', Tarimas.BorrarPieza);
    	jQuery('#btn-imprimir-barcode').click(Tarimas.ImprimirCodigoDeBarras);
    },
    Editar: function(tarima)
    {
    	Tarimas.piezas = [];
    	jQuery('#listaPiezas table').html('');
    	jQuery('#noOrdenCompra').attr('disabled', true);
    	var form 					= jQuery('#form-datos-tarima').get(0);
    	form.idTarima.value			= tarima.idtarima;
    	form.noTarima.value 		= tarima.noTarima;
    	form.noOrdenCompra.value 	= tarima.orden.idOrdenCompra;
    	form.destino.value			= tarima.idDestino;
    	form.fechaSalida.value		= tarima.fechaSalida;
        form.usuarioSalida.value    = tarima.usuarioEstatus;
    	for(var i in tarima.piezas)
    	{
    		Tarimas.AdicionarPieza(tarima.piezas[i]);
    	}
    	jQuery('#btn-imprimir-barcode').css('display', 'inline-block');
    	jQuery("#dialogTarima").dialog().dialog('open');
    },
    AdicionarPieza: function(pieza)
    {
    	if( Tarimas.piezas.length >= 45 )
    	{
    		alert('No puede adicionar mas de 45 tarimas');
    		return false;
    	}
    	if( Tarimas.PiezaExiste(pieza.noPieza) )
    	{
    		var sound = document.getElementById("error");
            sound.play();
        	
    		return false;
    	}
    	var index = Tarimas.piezas.push(pieza) - 1;
    	jQuery('#listaPiezas table').append('<tr class="pieza" data-id="'+ pieza.idPiezas+'" data-idx="'+ index +'">'+ 
    									'<td>' + pieza.noPieza + '</td><td>' + pieza.clavePieza + '</td><td>' + pieza.metros + ' Mts.</td></tr>');
    	jQuery('#no_piezas').val(Tarimas.piezas.length);
    },
    BorrarPieza: function(e)
	{
    	var index	= e.currentTarget.dataset.idx;
    	Tarimas.piezas.splice(parseInt(index), 1);
    	var items = Tarimas.piezas; 
    	Tarimas.piezas = [];
	    //this.remove();
	    jQuery('#listaPiezas table').html('');
	    for(var i in items)
	    {
	    	Tarimas.AdicionarPieza(items[i]);
	    }
	},
	
    PiezaExiste: function(noPieza)
    {
    	if( !noPieza )
    		return false;
    	if( Tarimas.piezas.length <= 0 )
    		return false;
    	var existe = false;
    	for(var i in Tarimas.piezas)
    	{
    		if( Tarimas.piezas[i].noPieza == noPieza )
    		{
    			existe = true;
    			break;
    		}
    	}
    	
    	return existe;
    },
    ImprimirCodigoDeBarras: function()
    {
    	var form 					= jQuery('#form-datos-tarima').get(0);
    	if( !form.idTarima.value )
    		return false;
    	jQuery.get('index.php?mod=tarimas&task=ImprimirCodigoBarras&id=' + form.idTarima.value, function(res)
    	{
    		if( res.status == 'ok' )
    			alert(res.mensaje);
    	}).fail(function(res)
    	{
    		alert(res.responseJSON.error || 'Error desconocido al enviar la impresion');
    	});
    	return false;
    }
};

var Numero_Pieza = {
    id: 'input#noPieza',
    debug: true,
    init: function() 
    {
        $(Numero_Pieza.id).empty();
        $(Numero_Pieza.id).keydown(function(event) 
        {
            if ( event.which === 13 ) 
            {
                if (this.debug) console.log('enter: ' + $("#numero_pieza").val());
                event.preventDefault();
                Numero_Pieza.Obtener();
            }
        });
    },
    valida: function() 
    {
        if (Salidas_pt.numero_pieza === null) {
            alert('Clave de pieza inválida');
            return false;            
        }
        return true;
    },
    val: function() 
    {
        return $(Numero_Pieza.id).val();
    },
    Obtener: function()
    {
    	jQuery.post('index.php?mod=tarimas&task=obtenerpieza&noPieza=' + Numero_Pieza.val(), function(pieza)
        {
    		if( pieza )
    		{
    			
    			$(Numero_Pieza.id).val('');
    			Tarimas.AdicionarPieza(pieza);
    		}
        }).fail(function(data)
        {
        	
        	alert(data.responseJSON.error || 'Error desconocido al tratar de obtener la pieza');
        	//console.log("ERROR 0 Nuemro_Pieza.init() -> "+data);
        });
    }
};

var NumeroCliente = {
    id: 'input#noCliente',
    valido: false,
    debug: false,
    init: function() {
        $(this.id).on('change',function(){
            $.post('./include/salidasPt.silan.php',
                {data: {evento:"validacionExisteCliente", cliente:NumeroCliente.val()}}, function(data){
                    if(data.error !== 0)
                        $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    console.log("ERROR 0 NumeroCliente.init() -> "+data);
                }).always(function(data){
                    (data.existe === 'NO' ? NumeroCliente.noExiste() : NumeroCliente.existe() );
            });
        });
    },
    limpia: function() {
        $("#" + this.id).val('');
        this.valido = false;
    },
    valida: function() {
        var valido = true;

        if (!NumeroCliente.valido) {
            valido = false;
            alert('Número de cliente inválido');
        }

        return valido;
    },
    val: function() {
        return $(NumeroCliente.id).val();
    },
    existe: function() {
        if (NumeroCliente.debug) console.log('existe');
        NumeroCliente.valido = true;
    },
    noExiste:function() {
        if (NumeroCliente.debug) console.log('noExiste');
        NumeroCliente.valido = false;
        $silan_msg.showmessage("El cliente número " + NumeroCliente.val() + " no existe","closemodal","Salidas Pt");
        $(NumeroCliente.id).val('').focus();
    }

};

var SalidaPieza = {
    id: "salidaPieza",
    debug: false,
    init: function(){
        $(SalidaPieza.id).on("click", function(e){
            if (!Salidas_pt.valida()) return;
            Salidas_pt.guarda();
        });
        
        $("div#guardaRegistro")
            .button({ 
                icons: { primary: "ui-icon-disk" },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                if (!Salidas_pt.valida()) return;
                Salidas_pt.guarda();
            });
            
        $("div.cerrar-tabs")
            .button({ 
                icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                label: "Cerrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("div#dialogSalidasPt").dialog("close");
            });
    }
};

