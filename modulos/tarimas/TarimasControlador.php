<?php
require_once BASE_DIR . SB_DS . '/include/combos.php';
class TarimasControlador extends Controlador
{
	/**
	 * 
	 * @var TarimasModelo
	 */
	protected $modelo;
	
	public function Init()
	{
		$this->modelo 	= $this->CargarModelo();
	}
	public function TaskDefault()
	{
		$filtro			= filter_input(INPUT_GET, 'filter');
		$combos 		= new Combos();
		$cliente		= $combos->inputClienteTarimas('object');
		$tarimas		= $this->modelo->ObtenerTodas($filtro);
		$estatusNuevo	= $this->modelo->ObtenerEstatusNuevo();
		$this->CargarVista('default', get_defined_vars());
	}
	public function TaskGuardar()
	{
		$data = json_decode(file_get_contents('php://input'));
		
		try
		{
			$mensaje = 'Tarima creada correctamente';
			$tarima = null;
			
			if( !$data->idtarima )
			{
				$tarima = $this->modelo->Crear($data);
			}
			else
			{
				$tarima		= $this->modelo->Actualizar($data);
				$mensaje 	= 'Tarima actualizada correctamente';
			}
			
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => $mensaje, 'tarima' => $tarima]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskBorrar()
	{
		$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
		try
		{
			if( !$id )
				throw new Exception('Indentificador de tarima invalido');
			$this->modelo->Borrar($id);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Tarima borrada correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskObtenerPieza()
	{
		$noPieza = (int)filter_input(INPUT_GET, 'noPieza');
		try 
		{
			if( !$noPieza )
				throw new Exception('Numero de pieza invalido');
			$modelo = $this->CargarModelo('Piezas');
			$pieza = $modelo->ObtenerPor('noPieza', $noPieza);
			if( $pieza->idTarima )
				throw new Exception('La pieza ya esta asignada en otra tarima');
			$this->ResponseJSON($pieza);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskObtener()
	{
		$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

		try
		{
			if( !$id )
				throw new Exception('Indentificador de tarima invalido');
			$tarima = $this->modelo->Obtener($id, true);
			//xxxxxxxx
			if( $tarima->idEstatus == $this->modelo->ObtenerEstatusCancelado()->idEstatus )
				throw new Exception('La tarima tiene un estatus cancelado, no puedo editarla');
			//die(json_encode(utf8_encode($tarima->usuarioEstatus)));
			
			//die(json_encode($tarima, JSON_UNESCAPED_UNICODE));
			$this->ResponseJSON($tarima);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskImprimirCodigoBarras()
	{
		try
		{
			$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
			if( !$id )
				throw new Exception('Identificador de tarima invalido');
			$tarima = $this->modelo->Obtener($id);
			if( !$tarima )
				throw new Exception('La tarima no existe');
			$this->modelo->ImprimirCodigoDeBarras($tarima);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'La impresion fue enviada correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}