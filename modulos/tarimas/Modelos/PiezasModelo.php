<?php
class PiezasModelo extends Modelo
{
	public function Obtener($id)
	{
		$id 		= (int)$id;
		$wrappedCol = $this->WrapField('idPiezas');
		$query 		= "SELECT * FROM piezas WHERE $wrappedCol = $id  LIMIT 1";
		$res 		= $this->Query($query);
	
		if( !$res )
			return null;
		return $res->fetch_object();
	}
	public function ObtenerPor($column, $valor, $simple = true)
	{
		$wrappedCol = $this->WrapField($column);
		$query 		= "SELECT * FROM piezas WHERE $wrappedCol = '$valor' ";
		if( $simple )
			$query .= "LIMIT 1";
		
		$res = $this->Query($query);
		
		if( !$res )
			return null;
		return $res->fetch_object();
	}
}