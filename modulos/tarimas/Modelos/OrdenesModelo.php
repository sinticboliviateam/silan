<?php
class OrdenesModelo extends Modelo
{
	/**
	 *
	 * @var TarimasModelo
	 */
	protected	$modeloTarimas;
	
	public function Init()
	{
		$ctrl					= $this->app->GetControllador();
		$this->modeloTarimas 	= $ctrl->CargarModelo('Tarimas', 'tarimas');
	}
	/**
	 * Obtiene una orden de compra
	 * @param int $id Identificador de la orden de compra
	 */
	public function Obtener($id)
	{
		$query = "SELECT * FROM ordenescompra WHERE idOrdenCompra = $id LIMIT 1";
		$res	= $this->Query($query);
		if( !$res )
			return null;
		$orden 			= $res->fetch_object();
		//$orden->destino = $this->ObtenerDestino($orden->idDestino);
		$orden->tarimas = $this->ObtenerTarimas($orden->idOrdenCompra);
		
		return $orden;
	}
	public function ObtenerTodas($contarPiezas = false)
	{
		$query = "SELECT o.* ";
		if( $contarPiezas )
		{
			$idTarimas = "select t.idtarima from tarimas t where t.idOrdenCompra = o.idOrdenCompra";

			$query .= ", (select count(p.idPiezas) from piezas p where p.idTarima IN($idTarimas)) as piezas ";
		}
		$query .= "FROM ordenescompra o
					ORDER BY fechaRegistro DESC";

		return $this->GetResults($query);
	}
	/**
	 * 
	 * @param unknown $orden
	 */ 
	public function ObtenerTarimas($ordenId)
	{
		$query = "SELECT * FROM tarimas WHERE idOrdenCompra = $ordenId ORDER BY idtarima";
		$items = [];
		$res	= $this->Query($query);
		if( !$res )
			return $items;
		while( $row = $res->fetch_object() )
		{
			$row->piezas = $this->modeloTarimas->ObtenerPiezas($row->idtarima);
			$row->destino =  $this->ObtenerDestino($row->idDestino);
			$items[] = $row;
		}
		
		return $items;
	}
	public function ObtenerDestino($id)
	{
		$query 	= "SELECT * FROM destinos WHERE idDestino = $id LIMIT 1";
		$res	= $this->Query($query);
		if( !$res )
			return null;
		
		return $res->fetch_object();
	}
	public function BuildEAN13($country, $company, $id)
	{
		$fl 		= strlen(trim($country) . trim($company));
		$code 		= trim($country) . trim($company) . str_pad($id, 12 - $fl, '0');
		$weightflag = true;
		$sum = 0;
		// Weight for a digit in the checksum is 3, 1, 3.. starting from the last digit.
		// loop backwards to make the loop length-agnostic. The same basic functionality
		// will work for codes of different lengths.
		for ($i = strlen($code) - 1; $i >= 0; $i--)
		{
		$sum += (int)$code[$i] * ($weightflag ? 3 : 1);
		$weightflag = !$weightflag;
		}
		$code .= (10 - ($sum % 10)) % 10;
		return $code;
	}
	/**
	 * Build a base64 image buffer from ean13 code
	 * 
	 * @param string $ean13
	 * @return string The image base64 buffer
	 */
	public function EAN13ToImage($ean13)
	{
		require_once BASE_DIR . SB_DS . 'include' . SB_DS . 'lib' . SB_DS . 'php-barcode' . SB_DS . 'BarcodeGenerator.php';
		require_once BASE_DIR . SB_DS . 'include' . SB_DS . 'lib' . SB_DS . 'php-barcode' . SB_DS . 'BarcodeGeneratorPNG.php';
		
		$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
		return base64_encode($generatorPNG->getBarcode($ean13, $generatorPNG::TYPE_EAN_13));
		
	}
	/**
	 * Envia la impresion de codigo de barras a una impresora remota
	 * 
	 * @param string $ean13
	 * @param string $printerIP
	 */
	public function ImprimirEAN13($ean13, $printerIP)
	{
		sb_verificar_impresora($printerIP);
		include(BASE_DIR . SB_DS . 'include' . SB_DS . "lib/ZebraPrint/PrintSend.php");
		include(BASE_DIR . SB_DS . 'include' . SB_DS . "lib/ZebraPrint/PrintSendLPR.php");
		$lpr = new PrintSendLPR();
		$lpr->setHost($printerIP);//Put your printer IP here //20180219 Zebra
		$zpl 	= "^XA\n";
		$zpl	.= "^MMT\n";
		$zpl	.= "^PW609\n";
		$zpl	.= "^LL0203\n";
		$zpl	.= "^LS0\n";
		//$zpl	.= "^FT146,172 ^A0N,28,28 ^FH\^FD".$noTelar." ^FS\n"; // NUMERO DE TELAR
		//$zpl	.= "^FT231,172 ^A0N,28,28 ^FH\^FD".$composicion." ^FS\n"; // COMPOSICION
		//$zpl	.= "^FT46,172 ^A0N,28,28 ^FH\^FDTelar: ^FS\n"; // LEYENDA
		//$zpl	.= "^FT231,120 ^A0N,28,28 ^FH\^FD".$colorArticulo." ^FS\n"; // COLOR
		$zpl	.= "^FT45,120 ^A0N,28,28 ^FH\^FD".$ean13." ^FS\n"; // CLAVE DEL PRODUCTO
		//$zpl	.= "^FT45,66 ^A0N,28,28 ^FH\^FD".$noPoliza." ^FS\n"; // CLAVE DE LA POLIZA
		//$zpl	.= "^FT377,66 ^A0N,28,28 ^FH\^FD".$pzas." ^FS\n"; // NUMERO DE PIEZAS
		$zpl	.= "^PQ1,0,1,Y ^XZ\n";
		
		$lpr->setData($zpl);//Path to file, OR string to print.
		echo $lpr->printJob("barcode_queue");
	}
	public function ConstruirCSV($orden)
	{
		$estadoCancelado = $this->modeloTarimas->ObtenerEstatusCancelado();
		//print_r($orden);
		//$filename = TMP_DIR . SB_DS . sprintf("orden-compra-%d-%d.txt", $orden->idOrdenCompra, $orden->ordenCompra);
		$filename = TMP_DIR . SB_DS . sprintf("%d.txt", $orden->ordenCompra);

		//$cabeceras = ['Clave Provvedor', 'Orden Compra', 'Destino', 'SKU', 'LPN', 'Total Metros', 'Total Piezas', 'Mts'];
		//print_r($orden);
		$fh = fopen($filename, 'w');
		//fputcsv($fh, $cabeceras);
		$proveedor = 'PN132';
		$buffer			= "";

		foreach($orden->tarimas as $tarima)
		{
			//##verificar el estado de la tarima
			if( $tarima->idEstatus == $estadoCancelado->idEstatus ) continue;
			$skus = $this->modeloTarimas->ObtenerSKUS($tarima);
			
			$total_piezas 	= count($tarima->piezas);
			$lpn			= $proveedor . sb_fill_zeros($tarima->noTarima, 8);
			
			foreach($skus as $sku)
			{
				$total_metros 	= 0;
				$piezas_sku 	= 0;
				$destino 		= $tarima->destino ? $tarima->destino->clave : '';
				//##calcular total de metros
				foreach($tarima->piezas as $pieza)
				{
					if( strstr($pieza->clavePieza . '-', $sku->clave_prod) )
					{
						$total_metros += $pieza->metros;
						$piezas_sku++;
					}
				}
				$fields = [
						$proveedor,
						$orden->ordenCompra,
						utf8_encode($destino),
						$sku->sku,
						$lpn,
						sb_fill_zeros(str_replace([',', '.'], '', number_format($total_metros, 2)), 6),
						$piezas_sku,
						'Metros'
				];
/*
fwrite( $fh,"totalSKUs = $totalSKUs"."\n");
fwrite( $fh,"renActual = $renActual"."\n");
fwrite( $fh,"tarimaActual = $tarimaActual"."\n");
fwrite( $fh,"totalTarimas = $totalTarimas"."\n");
				fwrite($fh, implode(',', $fields) . (($renActual < $totalTarimas) ? "\n" : ""));
*/
				$buffer .= implode(',', $fields) . "\r\n";
			}			
		}
		fwrite($fh, rtrim($buffer, "\r\n"));
		fclose($fh);
		//readfile($filename);die();
		$this->Update('ordenescompra', ['archivotxt' => 'Si'], ['idOrdenCompra' => $orden->idOrdenCompra]);
		return $filename;
	}
	public function ObtenerPoliza($id)
	{
		$query 	= "SELECT * FROM polizas WHERE idPoliza = $id LIMIT 1";
		$res 	= $this->Query($query);
		if( !$res )
			return null;
		$poliza = $res->fetch_object();
		//##obtener formular
		$query = "SELECT * FROM formulastelas WHERE idFormulas = $poliza->idFormulas LIMIT 1";
		if( $res = $this->Query($query) )
			$poliza->formula = $res->fetch_object();
		return $poliza;
	}
	public function ObtenerFormulaSKU($claveProducto)
	{
		$query 	= "SELECT * FROM artref WHERE clave_prod = '$claveProducto'";
		$res	= $this->Query($query);
		$items	= [];
		if( !$res )
			return $items;
		while( $row = $res->fetch_object() )
			$items[] = $row;
		
		return $items;
	}
	
}