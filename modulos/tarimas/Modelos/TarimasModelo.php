<?php
class TarimasModelo extends Modelo
{
	/**
	 * 
	 * @var OrdenesModelo
	 */
	protected $ordenesModelo;
	
	public function Init()
	{
		$ctrl					= $this->app->GetControllador();
		$this->ordenesModelo 	= $ctrl->CargarModelo('Ordenes');
	}
	public function Obtener($id, $orden = false)
	{
		$id		= (int)$id;
		$query 	= "SELECT * FROM tarimas WHERE idTarima = $id LIMIT 1";
		$tarima	= $this->GetObject($query);
		if( !$tarima )
			return null;
		
		if( $orden )
		{
			$tarima->orden	= $this->ObtenerOrden($tarima->idOrdenCompra);
		}
		$query = "SELECT * FROM destinos WHERE idDestino = $tarima->idDestino LIMIT 1";
		$tarima->destino = $this->Query($query)->fetch_object();
		//##obtener las piezas de la tarima
		$tarima->piezas = $this->ObtenerPiezas($tarima->idtarima);
		$tarima->estatus = $this->ObtenerEstatus($tarima->idEstatus);
		return $tarima;
	}
	public function ObtenerTodas($filtro = null)
	{
		$sql = "select o.ordenCompra, t.idtarima, t.noTarima, t.fechaSalida, e.nombre estatus,
						d.clave as clave_destino, d.nombre as nombre_destino,
						p.clavePieza,
						( select count(p1.idPiezas) from piezas p1 where p1.idTarima = t.idtarima ) total_piezas,
						sum(p.metros) metros
						,count(distinct(artref.sku)) total_sku
					from tarimas t
					join ordenescompra o on (o.idOrdenCompra = t.idOrdenCompra)
					join estatus e on (e.idEstatus = t.idEstatus)
					left join destinos d on d.idDestino = t.idDestino
					left join piezas p on (p.idTarima = t.idtarima)
					left join artref on (artref.clave_prod = substring_index(p.clavePieza, '-', 1))
					where 1 = 1
					{filtro}
					group by t.idtarima desc, t.noTarima desc 
					{having}";
					
		$filtroSql 	= '';
		$having 	= '';
		if( $filtro )
		{
			
			if( stristr($filtro, "oc") )
			{
				$ordenCompra 	= (int)str_replace(['oc', 'OC'], '', $filtro);
				//$filtroSql		= "AND o.ordenCompra LIKE '%$ordenCompra%'";
				$filtroSql		= "AND o.ordenCompra = $ordenCompra";
			}
			else
			{
				$having .= "HAVING (t.noTarima LIKE '%$filtro%' ";
				$having .= "OR t.fechaSalida LIKE '%$filtro%' ";
				$having .= "OR d.clave LIKE '%{$filtro}%' ";
				$having .= "OR d.nombre LIKE '%{$filtro}%' ";
				$having .= "OR metros LIKE '%$filtro%' ";
				$having .= "OR total_piezas LIKE '%$filtro%' ";
				//$having .= "OR SKU LIKE '%$filtro%' ";
				$having .= "OR e.nombre LIKE '%$filtro%') ";
			}	
			
		}
		$sql = str_replace(['{filtro}', '{having}'], [$filtroSql, $having], $sql);
		$res = $this->Query($sql);
		//var_dump($sql);
		$items = [];
		if( !$res )
			return $items;
		
		while( $row = $res->fetch_object() )
		{
			//$row->orden		= $this->ObtenerOrden($row->idOrdenCompra);
			//$row->piezas 	= $this->ObtenerPiezas($row->idtarima);
			//$row->estatus 	= $this->ObtenerEstatus($row->idEstatus);
			//$row->orden		= $this->ObtenerOrden($row->idOrdenCompra);
			$items[] 		= $row;
		}
		
		return $items;
	}
	public function ObtenerPiezas($idTarima)
	{
		$query = "SELECT * FROM piezas WHERE idTarima = $idTarima";
		$res = $this->Query($query);
		if( !$res )
		{
			return [];
		}
		$items = [];
		while( $pieza = $res->fetch_object() )
			$items[] = $pieza;
		return $items;
	}
	public function ObtenerPorOrden($id)
	{
		
	}
	public function ObtenerOrden($id)
	{
		$query = "SELECT * FROM ordenescompra WHERE idOrdenCompra = $id LIMIT 1";
		
		$res = $this->_db_sys->query($query);
		if( !$res )
			return null;
		$orden = $res->fetch_object();
		if( !$orden )
			return null;
		//$query = "SELECT * FROM destinos WHERE idDestino = $orden->idDestino LIMIT 1";
		//$orden->destino = $this->Query($query)->fetch_object();
		return $orden;
	}
	public function Crear($tarima)
	{
		$piezas = $tarima->piezas;
		unset($tarima->piezas);
		//##obtener el siguiente numero de tarima
		$query 	= "SELECT MAX(noTarima) as numero FROM tarimas";
		$res	= $this->Query($query);
		$noTarima = 1;
		if( !$res )
			$noTarima = 1;
		else
		{
			$max = $res->fetch_object();
			if( $max->numero <= 0 )
				$noTarima = 1;
			else
				$noTarima = (int)$max->numero + 1;
		}
		$tarima->idtarima		= null;
		$tarima->noTarima 		= $noTarima;
		$tarima->fechaSalida 	= date('Y-m-d', strtotime(str_replace('/', '-', $tarima->fechaSalida)));
		$tarima->idEstatus		= $this->ObtenerEstatusNuevo()->idEstatus;
		$tarima->fechaEstatus	= date('Y-m-d');
		$tarima->usuarioEstatus	= $_SESSION['datauser']['nombreCompleto'];//utf8_decode($_SESSION['datauser']['nombreCompleto']);
		
		$id = $this->Insert('tarimas', $tarima);
		if( !$id )
			throw new Exception('Ocurrio un error al crear la tarima');
		
		foreach($piezas as $pieza)
		{
			$this->Update('piezas', 
				[
					'documentoSalida' => $tarima->noTarima,
					'fechaSalida' => date('Y-m-d H:i:s', strtotime($tarima->fechaSalida)),
					'usuarioSalida' => $tarima->usuarioEstatus,
					'noCliente' => $this->ObtenerValorGeneral('parisinaNumero'),
					'idTarima' => $id
				], 
				['idPiezas' => $pieza->idPiezas]);
		}
		return $this->Obtener($id);
	}
	public function Actualizar($tarima)
	{
		if( !$tarima->idtarima )
			throw new Exception('Identificador de tarima invalido');
		
		//##obtener la tarima guardada anteriormente
		$tarimaG = $this->Obtener($tarima->idtarima, true);
		if( !$tarimaG )
			throw new Exception('La tarima no existe');
		if( $tarimaG->orden->archivotxt != 'No' )
			throw new Exception('No es posible actualizar la tarima, ya se emitio el archivo TXT');
		//##quitar asignacion de piezas guardadas
		foreach($tarimaG->piezas as $pieza)
		{
			$this->Update('piezas', ['idTarima' => null], ['idPiezas' => $pieza->idPiezas]);
		}
		//##asignar las nuevas piezas
		foreach($tarima->piezas as $pieza)
		{
			$this->Update('piezas', ['idTarima' => $tarimaG->idtarima], ['idPiezas' => $pieza->idPiezas]);
		}
		//##actualizar los datos de la tarima
		$tarimaG->idDestino = $tarima->idDestino;
		//TODO: falta modificar datos de tarima
		unset($tarimaG->piezas, $tarimaG->estatus, $tarimaG->destino);
		$tarimaG->fechaSalida = date('Y-m-d', strtotime(str_replace('/', '-', $tarima->fechaSalida)));
		$this->Update('tarimas', $tarimaG, ['idtarima' => $tarimaG->idtarima]);
		return $this->Obtener($tarimaG->idtarima);
		
	}
	/**
	 * Quita las asociaciones de las piezas de la tarima
	 * 
	 * @param int $id
	 * @return mixed
	 */
	public function Borrar($id)
	{
		//$query = "DELETE FROM tarimas WHERE idtarima = $id LIMIT 1";
		$estatusCancelado = $this->ObtenerEstatusCancelado();
		//##quitar asociaciones de las piezas de tarima
		$tarima = $this->Obtener($id, true);
		if( !$tarima )
			throw new Exception('La tarima no existe');
		if( $tarima->orden->archivotxt != 'No' )
			throw new Exception('No es posible borrar la tarima, ya se emitio el archivo TXT');
		//print_r($tarima);
		foreach($tarima->piezas as $pieza)
		{
			$this->Update('piezas', 
				[
					'documentoSalida' 	=> '',
					'noCliente'			=> '',
					'fechaSalida'		=> null,
					'usuarioSalida'		=> '',
					'idTarima'			=> null
				], 
				['idPiezas' => $pieza->idPiezas]);
		} 
		//##cambiar el estatus del la tarima
		$this->Update('tarimas', ['idEstatus' => $estatusCancelado->idEstatus], ['idtarima' => $id]);
	}
	public function ObtenerEstatusNuevo()
	{
		$query = "select * from estatus where catalogo = 5 and orden = 1";
		
		if( !$res = $this->Query($query) )
			return null;
		
		return $res->fetch_object();
	}
	public function ObtenerEstatusCancelado()
	{
		$query = "select * from estatus where catalogo = 5 and orden = 3";
	
		if( !$res = $this->Query($query) )
			return null;
	
		return $res->fetch_object();
	}
	public function ObtenerEstatus($id)
	{
		$query = "select * from estatus where idEstatus = $id LIMIT 1";
		
		if( !$res = $this->Query($query) )
			return null;
		
		return $res->fetch_object();
	}
	/**
	 * Obtiene todos los SKU's utilizados dentro de una tarima
	 * @param object $tarima
	 * @return array
	 */
	public function ObtenerSKUS($tarima)
	{
		$query = "SELECT * 
					FROM artref 
					WHERE 1 = 1
					AND clave_prod IN( 
						SELECT substring_index(p.clavePieza, '-', 1) as clave_prod
						FROM piezas p
						WHERE 1 = 1
						AND p.idTarima = $tarima->idtarima
						GROUP by clave_prod
					)";
		
		$res = $this->Query($query);
		if( !$res )
			return [];
		
		$items = [];
		while( $row = $res->fetch_object() )
			$items[] = $row;
		
		return $items;
	}
	public function ImprimirCodigoDeBarras($tarima)
	{
		if( !$tarima )
			throw new Exception('Objeto tarima');
		if( !$tarima->piezas )
			throw new Exception('La tarima no tiene piezas');
		
		$ean13 = $this->ordenesModelo->BuildEAN13('200', '00', sb_fill_zeros($tarima->noTarima, 7));
		$codigo = 'PN132' . sb_fill_zeros($tarima->noTarima, 8);
		$printerIP = IP_IMPRESORA_2;
		sb_verificar_impresora($printerIP);
		include(BASE_DIR . SB_DS . 'include' . SB_DS . "lib/ZebraPrint/PrintSend.php");
		include(BASE_DIR . SB_DS . 'include' . SB_DS . "lib/ZebraPrint/PrintSendLPR.php");
		$lpr = new PrintSendLPR();
		$lpr->setHost($printerIP);//Put your printer IP here //20180219 Zebra
		/*
		$zpl 	= "^XA\n";
		$zpl	.= "^MMT\n";
		$zpl	.= "^PW609\n";
		$zpl	.= "^LL0203\n";
		$zpl	.= "^LS0\n";
		//$zpl	.= "^FT146,172 ^A0N,28,28 ^FH\^FD".$noTelar." ^FS\n"; // NUMERO DE TELAR
		//$zpl	.= "^FT231,172 ^A0N,28,28 ^FH\^FD".$composicion." ^FS\n"; // COMPOSICION
		//$zpl	.= "^FT46,172 ^A0N,28,28 ^FH\^FDTelar: ^FS\n"; // LEYENDA
		//$zpl	.= "^FT231,120 ^A0N,28,28 ^FH\^FD".$colorArticulo." ^FS\n"; // COLOR
		$zpl	.= "^FT45,120 ^A0N,28,28 ^FH\^FD".$ean13." ^FS\n"; // CLAVE DEL PRODUCTO
		//$zpl	.= "^FT45,66 ^A0N,28,28 ^FH\^FD".$noPoliza." ^FS\n"; // CLAVE DE LA POLIZA
		//$zpl	.= "^FT377,66 ^A0N,28,28 ^FH\^FD".$pzas." ^FS\n"; // NUMERO DE PIEZAS
		$zpl	.= "^PQ1,0,1,Y ^XZ\n";
		*/
		$total_piezas = 0;
		$sumaMetros = 0;
		foreach($tarima->piezas as $pieza)
		{
			$sumaMetros += (float)$pieza->metros;
			$total_piezas++;
		}
		$skus = $this->ObtenerSKUS($tarima);
		$sku = '';
		if( $skus && count($skus) ) 
		{
			$sku = count($skus) > 1 ? 'Multiple SKU' : $skus[0]->sku;
		}
		$ordenCompra = $this->ObtenerOrden($tarima->idOrdenCompra);

		$zpl = '';
		/*
		//$zpl .= "#CT~~CD,~CC^~CT~";
		//$zpl .= "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ";
		$zpl .= "^XA\n";
		$zpl .= "^MMT\n";
		$zpl .= "^PW831\n";
		$zpl .= "^LL0406\n";
		$zpl .= "^LS0\n";
		$zpl .= "^BY3,3,55^FT764,369^BCB,,Y,N\n";
		$zpl .= "^FD>:" . $ean13 . "^FS\n";
		$zpl .= "^FT69,231^A0N,37,48^FH\^FD" . '[marca]' . "^FS\n";
		$zpl .= "^FT697,386^A0B,25,24^FH\^FD" . '[clave_pieza]' . "^FS\n";
		//$zpl .= "^FT554,383^A0B,25,24^FH\^FD" . $composicion01 . "^FS\n";
		//$zpl .= "^FT585,383^A0B,25,24^FH\^FD" . $composicion02 . "^FS\n";
		$zpl .= "^FT616,383^A0B,25,24^FH\^FDwww.LMtextil.com^FS\n";
		$zpl .= "^FT647,383^A0B,25,24^FH\^FD(55) 5604-5240^FS\n";
		$zpl .= "^FT70,280^A0N,25,24^FH\^FDColor:   " . '[clave_prod]' . "^FS\n";
		$zpl .= "^FT70,311^A0N,25,24^FH\^FD           " . '[color]' . "^FS\n";
		$zpl .= "^FT70,342^A0N,25,24^FH\^FDAncho: " . $sumaMetros . "m^FS\n";
		//$zpl .= "^FT496,88^A0N,45,45^FH\^FD" . $metros . "^FS\n";
		$zpl .= "^BY4,3,87^FT31,112^BCN,,Y,N\n";
		$zpl .= "^FD>;" . '[SKU]' . "^FS\n";
		//if ($metros != 35) {
		//	$zpl .= "^FO486,40^GB123,65,8^FS\n";
		//}
		
		$zpl .= "^PQ1,0,1,Y^XZ\n";
		*/
		$zpl .= "^XA\n".
				"^MMT\n".
				"^PW831\n".
				"^LL0405\n".
				"^LS0\n".
				"^BY4,3,101^FT73,139^BCN,,Y,N\n".
				//"^FD>:PN>51320000010>62^FS\n".
				"^FD>:{$codigo}^FS\n".
				"^FT87,235^A0N,39,38^FH\^FDEscolar^FS\n".
				"^FT280,236^A0N,39,38^FH\^FD{$sku}^FS\n".
				"^FT363,345^A0N,34,33^FH\^FD{$total_piezas} piezas^FS\n".
				"^FT558,345^A0N,34,33^FH\^FD{$sumaMetros}  Mts.^FS\n".
				
				"^FT424,283^A0N,34,33^FH\^FDSuc.  {$tarima->destino->clave} {$tarima->destino->nombre}^FS\n".
				"^FT82,289^A0N,39,38^FH\^FDOrden  {$ordenCompra->ordenCompra}  ^FS\n".				
				"^PQ1,0,1,Y^XZ\n";

		$lpr->setData($zpl);//Path to file, OR string to print.
		echo $lpr->printJob("barcode_queue");
		
	}
}