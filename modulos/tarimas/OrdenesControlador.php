<?php
class OrdenesControlador extends Controlador
{
	/**
	 * 
	 * @var OrdenesModelo
	 */
	protected 	$modelo;
	
	
	public function Init()
	{
		$this->modelo 			= $this->CargarModelo('Ordenes');
		
	}
	public function TaskBuildbarcode()
	{
		try
		{
			$id		= isset($_GET['id']) ? $_GET['id'] : null;
			if( !$id )
				throw new Exeption('Identificador invalido');
			$ean13	= $this->modelo->BuildEAN13('200', '0', $id);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}

		
	
	}
	public function TaskImprimirEan13()
	{
		$id = (int)filter_input(INPUT_GET, 'id');	
		try
		{
			if( !$id )
				throw new Exception('Identificador de orden de compra invalido');
			
			$this->modelo->ImprimirEAN13($ean13);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Impresion enviada correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
		
	}
	public function TaskConstruirCSV()
	{
		$id = (int)filter_input(INPUT_GET, 'id');
		try
		{
			if( !$id )
				throw new Exception('Identificador de orden de compra invalido');
			$orden = $this->modelo->Obtener($id);
			if( !$orden )
				throw new Exception('La orden de compra no existe');
			if( !$orden->tarimas || !count($orden->tarimas) )
				throw new Exception('La orden de compra aun no tiene tarimas asignadas');
			$csv = $this->modelo->ConstruirCSV($orden);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Contruccion de archivo CSV completada']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskDescargarCSV()
	{
		$id = (int)filter_input(INPUT_GET, 'id');
		try
		{
			if( !$id )
				throw new Exception('Identificador de orden de compra invalido');
			$orden = $this->modelo->Obtener($id);
			if( !$orden )
				throw new Exception('La orden de compra no existe');
			
			//$filename 	= TMP_DIR . SB_DS . sprintf("orden-compra-%d-%d.csv", $orden->idOrdenCompra, $orden->ordenCompra);
			$filename 	= TMP_DIR . SB_DS . sprintf("%d.txt", $orden->ordenCompra);
			if( !is_file($filename) )
				throw new Exception('El archivo CSV no existe');
			$size 		= filesize($filename);
			Header('Content-Description: File Transfer');
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Length: ' . $size);
			header('Content-Disposition: attachement; filename="'.basename($filename).'"');
			readfile($filename);
			die();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
			//$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}