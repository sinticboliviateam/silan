<?php
class DestinosControlador extends Controlador
{
	protected $modelo;

	public function Init()
	{
		$this->modelo = $this->CargarModelo();
	}
	public function TaskDefault()
	{
		$_SESSION["datauser"]["menuaccess"] = '02080000';//"02080300";
		try
		{
			$this->CargarVista('default');
		}
		catch(Exception $e)
		{
			print (['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskFiltrar()
	{
		try
		{

			$filtro = isset($_GET['filtro']) ? trim($_GET['filtro']) : null;
			$items = $this->modelo->ObtenerTodos($filtro);
			$this->ResponseJSON(['status' => 'ok', 'items' => $items]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGuardar()
	{
		try
		{
			$data = json_decode(file_get_contents('php://input'));
			if( !$data )
				throw new Exception('Datos invalidos');
			$mensaje = 'Destino creado';
			if( isset($data->idDestino) && (int)$data->idDestino )
			{
				$this->modelo->Actualizar($data);
				$mensaje = 'Destino actualizado';
			}
			else
			{
				$this->modelo->Crear($data);
			}
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => $mensaje]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskEditar()
	{
		try
		{
			$id = $_GET['id'];
			$destino = $this->modelo->Obtener($id);
			$this->ResponseJSON(['status' => 'ok', 'destino' => $destino]);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskBorrar()
	{
		try
		{
			$id = $_GET['id'];
			$this->modelo->Borrar($id);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Destino borrado correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}