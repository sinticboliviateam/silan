<?php
class DestinosModelo extends Modelo
{
	public function Obtener($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de destino invalido');
		$query = "SELECT * FROM destinos WHERE idDestino = $id LIMIT 1";

		return $this->GetObject($query);
	}
	public function ObtenerTodos($filter = null)
	{
		$query = "SELECT * FROM destinos WHERE 1 = 1 {where} ORDER BY idDestino DESC";
		$where = '';
		if( $filter )
		{
			$where = "AND clave LIKE '%{$filter}%' " .
						"OR nombre LIKE '%{$filter}%' ".
						"OR direccion LIKE '%{$filter}%' ";
		}
		$query = str_replace('{where}', $where, $query);

		return $this->GetResults($query);
	}
	public function Crear($destino)
	{
		if( empty($destino->clave) )
			throw new Exception('No se puede crear el destino, clave invalida');
		if( empty($destino->nombre) )
			throw new Exception('No se puede crear el destino, nombre invalido');
		$destino->idDestino = null;
		$destino->nombre = utf8_decode(trim($destino->nombre));
		$destino->direccion = utf8_decode(trim($destino->direccion));

		$id = $this->Insert('destinos', $destino);

		return $this->Obtener($id);
	}
	public function Actualizar($destino)
	{
		if( !$destino || !isset($destino->idDestino) || !(int)$destino->idDestino )
			throw new Exception('Destino invalido, no se puede actualizar');
		$destinoG = $this->Obtener($destino->idDestino);
		if( !$destinoG )
			throw new Exception('El destino no existe, no se puede actualizar');
		$destinoG->clave = $destino->clave;
		$destinoG->nombre = utf8_decode(trim($destino->nombre));
		$destinoG->direccion = utf8_decode(trim($destino->direccion));

		$this->Update('destinos', $destinoG, ['idDestino' => $destinoG->idDestino]);

		return $destinoG;
	}
	public function Borrar($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de destino invalido, no se puede borrar');
		$destino = $this->Obtener($id);
		if( !$destino )
			throw new Exception('El destino no existe, no se puede borrar');
		$query = "SELECT COUNT(*) as num FROM tarimas WHERE idDestino = {$destino->idDestino}";
		$count = $this->GetObject($query);
		if( $count && $count->num > 0 )
			throw new Exception('No esposible borrar el destino, contiene referencias en tarimas');

		$query = "DELETE FROM destinos WHERE idDestino = {$destino->idDestino} LIMIT 1";
		$this->Query($query);

		return true;
	}
}