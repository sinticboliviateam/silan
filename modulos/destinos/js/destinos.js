function Destinos()
{
	var $this = this;
	var $dialogo = $("#dialogDestino");
	var $form		= $("#form-destino");
	var $tabla		= $('#tabla-destinos');
	this.Init = function()
	{
		$this.InitBotones();
		$this.InitEventos();
	}
	this.InitEventos = function()
	{
		jQuery('#filter').keyup( function(e)
		{
			$this.ObtenerRegistros(this.value, true);
		});
		$(document).on("click", "tr.renglonesGrid",function(e){
        	
            e.preventDefault();
            
            $.get('index.php?mod=destinos&task=Editar&id=' + this.dataset.id, 
                function(data)
                {
                    
                },"json")
                .fail(function(data)
                {
                    alert(data.responseJSON.error);
                    //alert("ERROR 0 renglonesGrid.-> "+data);
                })
                .always(function(data)
                {
                    $this.Editar(data.destino);
                });
        });
		$form.submit($this.EnviarFormulario);
	}
	this.ObtenerRegistros = function(filtro, borrar)
	{
		jQuery.get('index.php?mod=destinos&task=Filtrar&filtro=' + filtro, function(res)
		{
			$this.InsertarRegistros(res.items, borrar);
		})
		.fail(function(e)
		{
			alert(e.responseJSON.error);
		});
	}
	this.InsertarRegistros = function(items, borrar)
	{
		var $tbody = $tabla.find('tbody');
		if( borrar )
			$tbody.html('');
		for(var i in items)
		{

			var destino = items[i];

			var tr = '<tr class="renglonesGrid" data-id="' + destino.idDestino + '">' +
						'<td>' + destino.clave + '</td>'+
						'<td>' + destino.nombre + '</td>'+
						'<td>' + destino.direccion + '</td>' +
					'</tr>';
			
			$tbody.append(tr);
		}
	}
	this.EnviarFormulario = function(e)
	{
		e.preventDefault();
		if( this.clave.value.length <= 0 )
		{
			alert('Debe ingresar una clave para el destino');
			return false;
		}
		if( this.nombre.value.length <= 0 )
		{
			alert('Debe ingresar un nombre para el destino');
			return false;
		}
		var id = isNaN(parseInt(this.idDestino.value)) ? 0 : parseInt(this.idDestino.value);
		var data = {
			idDestino: id,
			clave: this.clave.value,
			nombre: this.nombre.value,
			direccion: this.direccion.value
		};
		jQuery.post('index.php?mod=destinos&task=Guardar', JSON.stringify(data), function(res)
		{
			window.location.reload();
		})
		.fail( function(res) 
		{
			alert(res.responseJSON.error || 'Ocurrio un error desconocido');
		});
		return false;
	}
	this.Editar = function(destino)
	{
		$form.get(0).idDestino.value = destino.idDestino;
		$form.get(0).clave.value = destino.clave;
		$form.get(0).nombre.value = destino.nombre;
		$form.get(0).direccion.value = destino.direccion;
		$dialogo.dialog().dialog("open");
	}
	this.InitBotones = function()
	{
		$("button#btnNuevoDestino")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                
                $dialogo.dialog().dialog("open");
            });

        $("#guardar-destino")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $form.submit();
            });
            
        $("#borrar-destino")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                var seguir =true;

                var cancel =function(modal){
                    //console.log(modal);
                }
               
                var aceptar =function(modal){
                    var params = 'mod=destinos&task=Borrar&id=' + $form.get(0).idDestino.value;
                    $.get('index.php?' + params, function(data)
                    {
                        $dialogo.dialog("close");
                        location.reload();
                        
                    },"json")
                    .fail(function(data)
                    {
                        
                        alert(data.responseJSON.error);
                    })
                    .always(function(data)
                    { 
                        
                    });
                }
                $silan_msg.showmessagecondition('Estas seguro desear borrar este registro?', aceptar, cancel, 'Borrar') 
            }); 
            
        $(".cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $dialogo.dialog("close");
                location.reload();
            });

        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
	}
}