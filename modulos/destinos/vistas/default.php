<script src="modulos/destinos/js/destinos.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $setmenu.getLinksUser();
                var obj = new Destinos();
                obj.Init();
                obj.ObtenerRegistros('');
            });
        </script>
<div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Destinos</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <button id="btnNuevoDestino" class=""></button>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table id="tabla-destinos" class="">
                    <thead class="tableHead">
                        <tr class="">   
                            <th class="th-col06-col01">Clave</th>
                            <th class="th-col06-col02">Nombre</th>
                            <th class="th-col06-col03">Direccion</th>
                            
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogDestino" style="display: none;">
                <div id="tabs-ordenComp" style="width: 95%; height: auto; margin: 0 auto;">
                        <form id="form-destino" enctype="multipart/form-data" method="post">
                            <input type="hidden" id="idDestino" name="idDestino" value="" />
                            <div class="flex-modal-ordenesComp">
                                <div class="flex-modal-left-tab" style="width: 365px;">
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="clave" class="oLabel-left">Clave</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="clave" name="clave" class="oInput" type="text" value="" 
                                                required maxlength="60" tabindex="1"></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="nombre" class="oLabel-left">Nombre</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="nombre" name="nombre" class="oInput" type="text" tabindex="2" 
                                                required value=""></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="direccion" class="oLabel-left">Direcci&oacute;n</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="direccion" name="direccion" class="oInput" type="text" tabindex="3" value=""></div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="ordenes-compra-botones" style="clear:both;width:100%;">
                                        <div id="guardar-destino" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-disk"></span><span class="ui-button-icon-space"> </span>Guardar</div>
                                        <div id="borrar-destino" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-trash"></span><span class="ui-button-icon-space"> </span>Borrar</div>
                                        <div id="cerrar-destino" class="cerrar-tabs ui-button ui-corner-all ui-widget">
                                            <span class="ui-button-icon ui-icon ui-icon-arrowreturnthick-1-w"></span>
                                            <span class="ui-button-icon-space"> </span>
                                            Regresar
                                        </div>
                                       
                            </div>
                        </form>
 
                </div>
            </div>