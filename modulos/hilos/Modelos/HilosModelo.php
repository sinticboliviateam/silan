<?php
class HilosModelo extends Modelo
{
	public function ObtenerTodos($filter = null)
	{
		$query = "SELECT * FROM hilostenido ";
		if( $filter && !empty($filter) )
		{
			$query .= "WHERE claveHilo LIKE '%{$filter}%' OR color LIKE '%{$filter}%' OR titulo LIKE '%{$filter}%' OR metrosK LIKE '%{$filter}%' ";
		}
		$query .= "ORDER by claveHilo ASC";

		return $this->GetResults($query);
	}
	public function Obtener($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de hilo invalido');
		$query = "SELECT * FROM hilostenido WHERE idHilo = $id LIMIT 1";

		return $this->GetObject($query);
	}
	public function Borrar($id)
	{
		if( !(int)$id )
			throw new Exception('Identificador de hilo invalido');
		$hilo = $this->Obtener($id);
		if( !$hilo )
			throw new Exception('El hilo no existe');
		$query = "SELECT count(idPorcTrama) as ref FROM porctrama WHERE idHilo = $hilo->idHilo";
		$res = $this->getObject($query);
		if( $res && (int)$res->ref > 0)
			throw new Exception('No puede borrar el hilo, conteine referencias a las tramas');
		
		$query = "SELECT count(idPorcUrdimbre) as ref FROM porcurdimbre WHERE idHilo = $hilo->idHilo";
		$res = $this->getObject($query);
		if( $res && (int)$res->ref > 0)
			throw new Exception('No puede borrar el hilo, conteine referencias a las urdimbres');

		$query = "DELETE FROM hilostenido WHERE idHilo = $hilo->idHilo LIMIT 1";
		$this->Query($query);
		return true;
	}
	public function Guardar($hilo)
	{
		if( isset($hilo->idHilo) && (int)$hilo->idHilo )
			$this->Actualizar($hilo);
		else
			$this->Insertar($hilo);
	}
	public function Insertar($hilo)
	{
		if( !$hilo )
			throw new Exception('Hilo o datos invalido');
		if( isset($hilo->idHilo) && $hilo->idHilo )
			$hilo->idHilo = null;
		if( !isset($hilo->claveHilo) || !$hilo->claveHilo )
			throw new Ecxception('Clave de hilo invalida');
		//##verificar si la clave existe
		$res = $this->GetObject("SELECT idHilo FROM hilostenido WHERE claveHilo = '{$hilo->claveHilo}'");
		if( $res )
			throw new Exception('La clave del hilo ya existe');

		$hilo->color = strtoupper($hilo->color);
		$hilo->metrosK	= (int)$hilo->metrosK;//number_format((float)$hilo->metrosK, 2, '.', '');

		$id = $this->Insert('hilostenido', $hilo);

		return $this->Obtener($id);
	}
	public function Actualizar($hilo)
	{
		if( !$hilo )
			throw new Exception('Hilo o datos invalido');
		if( !isset($hilo->idHilo) && !(int)$hilo->idHilo )
			throw new Exception('Identificador de hilo invalido');
		$hiloG = $this->Obtener($hilo->idHilo);
		if( !$hiloG )
			throw new Exception('El hilo no existe');
		//$hiloG->claveHilo 	= $hilo->claveHilo;
		$hiloG->color 		= strtoupper($hilo->color);
		$hiloG->titulo		= $hilo->titulo;
		$hiloG->metrosK		= (int)$hilo->metrosK;//number_format((float)$hilo->metrosK, 2, '.', '');
		
		$this->Update('hilostenido', $hiloG, ['idHilo' => $hiloG->idHilo]);

		return $this->Obtener($hiloG->idHilo);
	}
}