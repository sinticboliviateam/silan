<?php
class HilosControlador extends Controlador
{
	/**
	 * 
	 * @var HilosModelo
	 */
	protected $hilosModelo;

	public function Init()
	{
		$this->hilosModelo = $this->CargarModelo();
		$this->autPerimisos = new menu();
	}
	public function TaskDefault()
	{
		//die(__METHOD__);
		require_once BASE_DIR . SB_DS . "include/class/Combos.class.php";
		$combos = new Combos();
		$_SESSION["datauser"]["menuaccess"] = "02010100";
		$permisos 	= $this->autPerimisos->regresaPermisosUsuario();
		$leer		= strstr($permisos, 'L') ? true : false;
		$escribir	= strstr($permisos, 'E') ? true : false;
		$borrar		= strstr($permisos, 'B') ? true : false;

		$this->CargarVista('default', get_defined_vars());
	}
	public function TaskObtenerHilos()
	{
		try
		{
			$filter = filter_input(INPUT_GET, 'filter'); 
			$hilos = $this->hilosModelo->ObtenerTodos($filter);
			$this->ResponseJSON($hilos);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGet()
	{
		try
		{
			$id = isset($_GET['id']) ? $_GET['id'] : null;
			if(!$id)
				throw new Exception('Identificador de hilo invalido');
			$hilo = $this->hilosModelo->Obtener($id);
			$this->ResponseJSON($hilo);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskGuardar()
	{
		try
		{
			$data = file_get_contents('php://input');
			if( !$data )
				throw new Exception('Datos invalidos');
			$hilo = json_decode($data);
			if( !$hilo || !is_object($hilo) )
				throw new Exception('Datos invalido, se espera un objeto');
			
			$mensaje = 'El registro fue creado correctamente';
			if( !isset($hilo->idHilo) || !(int)$hilo->idHilo )
			{
				$this->hilosModelo->Insertar($hilo);
			}
			else
			{
				$this->hilosModelo->Actualizar($hilo);
				$mensaje = 'El registro fue actualizado correctamente';
			}
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => $mensaje]);	
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
	public function TaskBorrar()
	{
		try
		{
			$id = isset($_GET['id']) ? $_GET['id'] : null;
			if(!$id)
				throw new Exception('Identificador de hilo invalido');
			$this->hilosModelo->Borrar($id);
			$this->ResponseJSON(['status' => 'ok', 'mensaje' => 'Hilo borrado correctamente']);
		}
		catch(Exception $e)
		{
			$this->ResponseJSON(['status' => 'error', 'error' => $e->getMessage()]);
		}
	}
}