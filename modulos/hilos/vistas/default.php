<?php
?>
<script src="modulos/hilos/js/hilos.js"></script>
<!-- <script src="modulos/productos/js/productosValidate.class.js"></script> -->
<div><br/><br/></div>
<div class="div-buscaRegistros">
	<div class="buscaRegistros">
		<h3>Hilos</h3>
		<input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
		<span id="status-busca"></span>
	</div>
	<div class="boton-nuevoRegistro">
		<?php if( $escribir ): ?>
		<button id="btnNuevoHilo" class=""></button>
		<?php endif; ?>
	</div>
</div>
<div class="cleared" style="display: block; width: 95%;"></div>
<div class="tableRecords" style="height:400px;">
	<table id="tabla-hilos" class="">
	<thead class="tableHead">
	<tr class="">
		<th class="th-col06-col01">Clave Hilo</th>
		<th class="th-col06-col02">Color</th>
		<th class="th-col06-col03">Titulo</th>
		<th class="th-col06-col04">Metros K</th>
	</tr>
	</thead>
	<tbody id="tbl01-records-busqueda">
	<tr class="" id="">
		<td></td><td></td><td></td><td></td>
	</tr>
	</tbody>
	</table>
</div>
<div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
<div id="dialogHilo" style="display: none;">
	<div id="tabs-hilos" style="width: 95%; height: auto; margin: 0 auto;">
		<ul>
			<li><a href="#tabs-datos-hilo">Datos</a></li>
			<li><a href="#tabs-mas-hilo">Mas</a></li>
		</ul>
		<div id="tabs-datos-hilo">
			<form id="form-tab-datos-hilo" enctype="multipart/form-data" method="post">
				<input type="hidden" id="idHilo" name="idHilo" value="0" />
			    <div class="flex-modal-produccion">
			        <div class="flex-modal-left-tab" style="width: 365px;">
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="claveHilo" class="oLabel-left">Clave</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="claveHilo" name="claveHilo" class="oInput" type="text" value="" required maxlength="20" tabindex="1"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="color" class="oLabel-left">Color</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="color" name="color" class="oInput" type="text" maxlength="20" tabindex="2"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="titulo" class="oLabel-left">Titulo</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="titulo" name="titulo" class="oInput" type="text" maxlength="12" tabindex="3"></div>
			            </div>
			            <div class="block-info-tab">
			                <div class="div-oLabel-tab">
			                    <label for="metrosK" class="oLabel-left">Metros K</label></div>
			                <div class="div-dataInput-tab">
			                    <input id="metrosK" name="metrosK" class="oInput" type="text"  maxlength="30" tabindex="4"></div>
			            </div>
			        </div>

			        <div class="flex-modal-left-tab" style="width: 365px;">
			            
			        </div>

			        <div class="flex-modal-left-tab" style="width: 365px; padding: 0px;">
			            <div class="tabs-productos-botones">
			            	<?php if( $escribir ): ?>
			                <button id="guardar-datos-hilo" type="button"></button>
			            	<?php endif; ?>
			                <?php if( $borrar ): ?>
			                <button id="borrar-datos-hilo" type="button"></button>
			            	<?php endif; ?>
			                <div id="cerrar-tabs-hilo" class="cerrar-tabs"></div>
			            </div>
			            <hr>
			            <div class="baseImagen" style="margin-top: 10px;">
			                
			            </div>
			            <div class="tabs-productos-botones" style="justify-content: right; margin-top: 10px;">
			                
			            </div>
			        </div>
			    </div>
			</form>
		</div>
		<div id="tabs-mas-hilo"></div>     
	</div>
</div>