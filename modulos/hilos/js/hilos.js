function ModHilos()
{
	var $this = this;
	var timeout = null;

	this.init = function()
	{
		$("#dialogHilo").dialog({
            modal: true,
            autoOpen: false,
            title: "Actualización de Hilo",
            dialogClass: 'hide-close',
            width: 1140,
            maxHeight: 600,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
        $(".tabs-productos-botones").css({"font-size":"1.4em"});
        $("#tabs-hilos").tabs();

		$("button#btnNuevoHilo")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                $this.NuevoHilo();
                
            });
		$("#guardar-datos-hilo")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                //document.querySelector("#form-tab-datos-hilo").submit();
                $this.GuardarHilo();
            });
            
        $("#borrar-datos-hilo")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_msg.showmessagecondition('Estas seguro desear borrar este registro?', 
                    $this.BorrarHilo, (modal) => {}, 'Borrar');
                
            });
            
        $(".cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#dialogHilo").dialog("close");
                location.reload();
            });
		$(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
		this.setEvents();
		this.ObtenerHilos();
	};
	this.setEvents = function()
	{
		jQuery('#filter').keyup(function(e)
		{
			$this.Filtrar(this.value);
		});
		jQuery(document).on('click', "tr.renglonesGrid", function(e)
        {
            $.get('index.php?mod=hilos&task=get&id=' + this.dataset.id, function(hilo)
            {
                $this.EditarHilo(hilo);
            })
            .fail(function(data)
            {
                alert("ERROR 90 renglonesGrid.-> "+data);
            });
        });
        document.querySelector('#form-tab-datos-hilo').addEventListener('submit', function(e)
        {
        	e.preventDefault();
        	//$this.GuardarHilo();
        	return false;
        });
	};
	this.NuevoHilo = function()
	{
		document.querySelector('#form-tab-datos-hilo #claveHilo').value = 0;
		document.querySelector('#form-tab-datos-hilo').reset();
		$("#dialogHilo").dialog("open");
	};
	this.EditarHilo = function(hilo)
	{
		document.querySelector('#form-tab-datos-hilo').reset();
		for(let prop in hilo)
		{
			document.querySelector('#' + prop).value = hilo[prop];
		}
		$("#dialogHilo").dialog("open");
	};
	this.LlenarTabla = function(hilos, append)
	{
		let tablaBody = document.querySelector('#tabla-hilos tbody');
		if( !append )
			tablaBody.innerHTML = '';

		for(let hilo of hilos)
		{
			try
			{
				let row 		= document.createElement('tr');
				let tdClave 	= document.createElement('td');
				let tdColor 	= document.createElement('td');
				let tdTitulo 	= document.createElement('td');
				let tdMetrosK 	= document.createElement('td');

				row.className 		= 'renglonesGrid';
				row.dataset.id 		= hilo.idHilo;
				tdClave.innerHTML	= '<span>' + hilo.claveHilo + '</span>';
				tdColor.innerHTML 	= '<span>' + hilo.color + '</span>';
				tdTitulo.innerHTML 	= '<span>' + hilo.titulo + '</span>';
				tdMetrosK.innerHTML = '<span>' + hilo.metrosK + '</span>';

				row.appendChild(tdClave);
				row.appendChild(tdColor);
				row.appendChild(tdTitulo);
				row.appendChild(tdMetrosK);
				tablaBody.appendChild(row);	
			}
			catch(e)
			{
				console.log(e);
			}
		}
	};
	this.ObtenerHilos = function()
	{
		jQuery.get('index.php?mod=hilos&task=ObtenerHilos', function(res)
		{
			$this.LlenarTabla(res);
		})
		.fail(function(res)
		{
			silan_msg.showmessage(res.error, 'closemodal', 'Error');
		});
	};
	this.Filtrar = function(filtro)
	{
		if( timeout )
			clearTimeout(timeout);
		if( !filtro || filtro.length <= 0 )
		{
			$this.ObtenerHilos();
			return false;
		}	

		timeout = setTimeout(function()
		{
			jQuery.get('index.php?mod=hilos&task=ObtenerHilos&filter=' + filtro, function(res)
			{
				$this.LlenarTabla(res);
			});
		}, 400);
	};
	this.GuardarHilo = function()
	{
		let hilo = {};
		let form = document.querySelector('#form-tab-datos-hilo');
		try
		{
			let inputs = form.querySelectorAll('input');
			for(let input of inputs)
			{
				hilo[input.name] = input.value.trim();
			}
			jQuery.post('index.php?mod=hilos&task=guardar', JSON.stringify(hilo), function(res)
			{
				if( res.status == 'ok' )
					$silan_msg.showmessage(res.mensaje, 'reload', 'Completado');

			}).fail(function(res)
			{
				console.log(res);
				$silan_msg.showmessage(res.responseJSON.error, 'closemodal', 'Error');
			});
		}
		catch(e)
		{
			console.log(e);
		}
		
		return false;
	};
	this.BorrarHilo = function()
	{
		let id = document.querySelector('#idHilo').value;
		jQuery.get('index.php?mod=hilos&task=borrar&id=' + id, function(res)
		{
			window.location.reload();
		})
		.fail(function(res)
		{
			console.log(res);
			silan_msg.showmessage(res.error || 'Ocurrio un error al borrar el registro', 'closemodal', 'Error');
		});
	};
}
jQuery(function()
{
	window.modHilos = new ModHilos();
	modHilos.init();
});