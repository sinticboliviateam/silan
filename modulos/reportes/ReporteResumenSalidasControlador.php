<?php
class ReporteResumenSalidasControlador extends Controlador
{
	/**
	 * 
	 * @var ReporteResumenSalidasModelo
	 */
	protected	$reporteSalidasModelo;
	
	public function Init()
	{
		$this->reporteSalidasModelo = $this->CargarModelo('ReporteSalidas');
	}
	public function TaskDefault()
	{
		$this->CargarVista('resumen-salidas');
	}
	public function TaskResumen()
	{
		try
		{
			//$fechaInicial 		= filter_input(INPUT_POST, 'fechaInicial');
			//$fechaFinal 		= filter_input(INPUT_POST, 'fechaFinal');
			$documentoSalida	= (int)filter_input(INPUT_POST, 'documentoSalida');
			
			$items = $this->reporteSalidasModelo->ObtenerResumen(/*$fechaInicial, $fechaFinal, */$documentoSalida);
			$pdf = $this->reporteSalidasModelo->ConstruirPDF($items);
			$pdf->Output('I');
			die();
		}
		catch(Exception $e)
		{
			die('ERROR:' . $e->getMessage());
		}
	}
}