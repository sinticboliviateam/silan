<?php
class ReporteKardexControlador extends Controlador
{
	protected	$productosModelo;
	protected	$reporteKardexModelo;

	public function Init()
	{
		$this->productosModelo		= $this->CargarModelo('Productos', 'productos');
		$this->reporteKardexModelo 	= $this->CargarModelo('ReporteKardex');
	}
	public function TaskDefault()
	{
		$this->CargarVista('reporte-kardex');
	}
	public function TaskConstruir()
	{
		try 
		{
			$clave_prod = filter_input(INPUT_POST, 'clave_prod');	
			if( !$clave_prod )
				throw new Exception('Clave de producto invalida');
			//print_r($this->productosModelo);
			$producto = $this->productosModelo->ObtenerPor('clave_prod', $clave_prod);
			if( !$producto )
				throw new Exception('El producto no existe');
			$entradas 	= $this->reporteKardexModelo->ObtenerEntradas($producto->id_artref);
			$salidas 	= $this->reporteKardexModelo->ObtenerSalidas($producto->id_artref);
			$pdf		= $this->reporteKardexModelo->Construir($producto, $entradas, $salidas);
			$pdf->Output(sprintf("reporte-kardex-%s.pdf", $producto->clave_prod), 'I');
			die;
		} catch (Exception $e) {
			die('ERROR:' . $e->getMessage());
		}
	}
}