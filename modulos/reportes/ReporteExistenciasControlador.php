<?php
class ReporteExistenciasControlador extends Controlador
{
	protected	$productosModelo;
	protected	$reporteExistenciasModelo;

	public function Init()
	{
		$ctrl				= $this->app->GetControllador();
		$this->productosModelo = $ctrl->CargarModelo('Productos', 'productos');
		$this->reporteExistenciasModelo = $ctrl->CargarModelo('ReporteExistencias');
	}
	public function TaskDefault()
	{
		$marcas = $this->productosModelo->ObtenerMarcas();
		$this->CargarVista('reporte-existencias', get_defined_vars());
	}
	public function TaskConstruir()
	{
		try
		{
			$marca = filter_input(INPUT_POST, 'marca');
			$titulo = 'Existencias de Productos';
			$items = $this->reporteExistenciasModelo->ObtenerExistencias($marca);
			$pdf = $this->reporteExistenciasModelo->ConstruirPDF($items, $titulo);
			$pdf->Output('existencias.pdf', 'I');
			die();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
}