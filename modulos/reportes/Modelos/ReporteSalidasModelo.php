<?php
class ReporteSalidasModelo extends Modelo
{
	/**
	 * 
	 * @var Controlador
	 */
	protected 	$ctrl;
	protected	$productosModelo;
	protected	$ordenesModelo;

	public function Init()
	{
		$this->ctrl				= $this->app->GetControllador();
		$this->productosModelo 	= $this->ctrl->CargarModelo('Productos', 'productos');
		$this->ordenesModelo	= $this->ctrl->CargarModelo('Ordenes', 'tarimas');

	}
	/**
	 * Obtiene el resumen de salidas de un articulo
	 * 
	 * @param Object $articulo
	 * @param date $fechaInicio
	 * @param date $fechaFin
	 */
	public function ObtenerResumen(/*$fechaInicial = null, $fechaFinal = null, */$documentoSalida)
	{
		/*
		if( !$producto )
			throw new Exception('Producto invalido');
		if( !(int)$producto->id_artref )
			throw new Exception('Identificador de producto invalido');
		*/
		//if( !$fechaInicial )
		//	throw new Exception('La fecha inicial es requerida');
		if( !(int)$documentoSalida )
			throw new Exception('El documento de salida es invalido');

		//$fechaInicial 	= sb_format_date($fechaInicial, 'Y-m-d');
		//$fechaFinal 	= $fechaFinal ? sb_format_date($fechaFinal, 'Y-m-d') : null;
		
		$query = "SELECT p.documentoSalida, art.clave_prod, art.color, /*art.nombreProducto, p.idPiezas, p.clavePieza*/
					SUM(p.metros) as metros, SUM(p.kilos) as kilos, COUNT(p.documentoSalida) as piezas, art.marca
				FROM piezas p
				JOIN artref art on art.clave_prod = SUBSTRING_INDEX(p.clavePieza, '-', 1)
				WHERE 1 = 1
				AND p.fechaSalida is not null
				{where}
				GROUP BY art.clave_prod, art.color, art.marca ";
		
		$where = '';
		/*
		if( $fechaInicial )
			$where .= "AND p.fechaSalida >= '$fechaInicial' ";
		
		if( $fechaFinal )
		{
			$where .= "AND p.fechaSalida <= '$fechaFinal' ";
		}
		*/
		if( (int)$documentoSalida )
		{
			$where .= "AND p.documentoSalida = $documentoSalida ";
		}
		$query = str_replace('{where}', $where, $query);
		//var_dump($query);
		$results = $this->GetResults($query);
		
		return $results;
	}
	/**
	 * 
	 * @param array $items
	 * @return FPDF
	 */
	public function ConstruirPDF($items)
	{
		require BASE_DIR . SB_DS . "include/lib/fpdf181/fpdf.php";
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetMargins(10, 20, 9.9);
		//##contruir cabecera
		$pdf->SetFont('Arial','B',16);
		$pdf->SetXY(10, 10);
		$pdf->Cell(196, 10, sprintf("Resumen de salidas del folio %d", $items[0]->documentoSalida), 0, 1, 'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(196, 5, $this->getDateFormat(date('Y-m-d H:i:s')), 0, 1, 'C');
		$pdf->Cell(196, 5, '' , 0, 1, 'L');
		//##columnas
		$pdf->SetFont('Arial','BU',9);
		$pdf->SetXY(10, 35);
		$pdf->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
		//$this->Cell(22, 5, 'Referencia', 0, 0, 'C');
		$pdf->Cell(69, 5, 'Color', 0, 0, 'C');
		//$this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
		$pdf->Cell(14, 5, 'Metros', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Piezas', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Marca', 0, 1, 'C');

		$pdf->SetFont('Arial', 'BU', 8);
		$pdf->SetXY(10, 45);
		//$pdf->Cell(24, 5, 'articulo', 0, 1, 'C');
		$totalMetros = 0;
		$totalKilos = 0;
		$totalPiezas = 0;

		$pdf->SetFont('Arial','',8);
		foreach($items as $item)
		{
			$pdf->Cell(24, 5, $item->clave_prod, 0, 0, 'L');
			//$this->Cell(22, 5, $rec->referencia, 0, 0, 'C');
			$pdf->Cell(69, 5, $item->color, 0, 0, 'C');
			//$this->Cell(35, 5, $item->poliza, 0, 0, 'C');
			$pdf->Cell(14, 5, $item->metros, 0, 0, 'C');
			$pdf->Cell(16, 5, $item->kilos, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->piezas, 0, 0, 'C');
			$pdf->Cell(16, 5, $item->marca, 0, 1, 'R');

			$totalMetros 	+= $item->metros;
			$totalKilos 	+= $item->kilos;
			$totalPiezas 	+= $item->piezas;
		}
		$pdf->Ln();
		//##totales
		$pdf->Cell(93, 5, 'Gran Total', 0, 0, 'L');
		//$pdf->Cell(69, 5, '', 0, 0, 'C');
		$pdf->Cell(14, 5, number_format($totalMetros, 2, '.', ','), 'TB', 0, 'C');
		$pdf->Cell(16, 5, number_format($totalKilos, 3, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(16, 5, $totalPiezas, 'TB', 0, 'C');
		$pdf->Cell(16, 5, '', 0, 1, 'R');
		return $pdf;
	}
	public function ObtenerSalidasPorOrdenCompra($ordenCompra)
	{
		if( !$ordenCompra )
			throw new Exception('Orden de compra invalida');
		$tarimas = $this->ordenesModelo->ObtenerTarimas($ordenCompra->idOrdenCompra);
		//##obtener las piezas de las tarimas
		$idTarimas = '';
		foreach($tarimas as $tarima)
		{
			$idTarimas .= $tarima->idtarima . ",";
		}

		$idTarimas = rtrim($idTarimas, ',');
		$query = "SELECT a.clave_prod, a.color, a.referenciaColor, s.*, t.noTarima 
					FROM piezas_salidas s 
					JOIN artref a ON a.id_artref = s.id_producto
					JOIN tarimas t ON t.idtarima = s.idTarima
					WHERE s.idTarima IN($idTarimas) ORDER BY clave_prod ASC";

		$items = $this->GetResults($query);

		return $items;
	}
	public function ConstruirPdfSalidas($ordenCompra, $items)
	{
		require BASE_DIR . SB_DS . "include/lib/fpdf181/fpdf.php";
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetMargins(10, 20, 9.9);
		//##contruir titulo
		$pdf->SetFont('Arial','B',16);
		$pdf->SetXY(10, 10);
		$pdf->Cell(196, 10, sprintf("Salida de Piezas de la Orden de Compra %d", $ordenCompra->ordenCompra), 0, 1, 'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(196, 5, 'Emitido: ' . $this->getDateFormat(date('Y-m-d H:i:s')), 0, 1, 'C');
		$pdf->Cell(196, 5, '' , 0, 1, 'L');
		//##cabeceras
        $pdf->SetFont('Arial','BU',9);
        $pdf->SetXY(10, 35);
        $pdf->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
        $pdf->Cell(22, 5, 'Referencia', 0, 0, 'C');
        $pdf->Cell(45, 5, 'Color', 0, 0, 'C');
        $pdf->Cell(35, 5, utf8_decode('Clave Pieza'), 0, 0, 'C');
        $pdf->Cell(16, 5, 'Metros', 0, 0, 'C');
        $pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
    	$pdf->Cell(14, 5, 'Tarima', 0, 1, 'C');
    
        $pdf->SetFont('Arial','BU',8);
        $pdf->SetXY(10, 45);
        //$pdf->Cell(24, 5, 'Articulo', 0, 1, 'C');
        
        $granTotal              = 0;
        $granTotalPiezas        = 0;
        $totalKilos             = 0;
        $totalMetros            = 0;
        $ultimoArticulo         = null;
        $totalPiezasArticulo    = 0;
        $totalKilosArticulo     = 0;
        $totalMetrosArticulo    = 0;
        
        foreach($items as $rec)
        {
            if( $ultimoArticulo == null )
                $ultimoArticulo = $rec->clave_prod;
            
            //##verificar si el arituclo cambio
            if( $ultimoArticulo != $rec->clave_prod )
            {
                $pdf->SetY($pdf->GetY() + 5);
                $pdf->SetXY(10, $pdf->GetY());
                $pdf->SetFont('Arial','B',8);
                $pdf->Cell(24, 5, '', 0, 0, 'R');
                $pdf->Cell(22, 5, '', 0, 0, 'R');
                $pdf->Cell(45, 5, '', 0, 0, 'R');
                $pdf->Cell(35, 5, $totalPiezasArticulo . ' piezas', 'T', 0, 'L');
                $pdf->Cell(16, 5, number_format($totalMetrosArticulo,2), 'T', 0, 'R');
                $pdf->Cell(16, 5, number_format($totalKilosArticulo,2), 'T', 0, 'R');
                $pdf->Cell(14, 5, '', 0, 1, 'C');
                $pdf->SetY($pdf->GetY() + 5);
                
                $pdf->SetFont('Arial','BU',9);
                $pdf->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
                $pdf->Cell(22, 5, 'Referencia', 0, 0, 'C');
                $pdf->Cell(45, 5, 'Color', 0, 0, 'C');
                $pdf->Cell(35, 5, utf8_decode('Clave Pieza'), 0, 0, 'C');
                $pdf->Cell(16, 5, 'Metros', 0, 0, 'C');
                $pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
                $pdf->Cell(14, 5, 'Tarima', 0, 1, 'C');
                $pdf->SetY($pdf->GetY() + 5);
                
                //##gardar articulo
                $ultimoArticulo         = $rec->clave_prod;
                //##reiniciar contadores de articulo
                $totalPiezasArticulo    = 0;
                $totalKilosArticulo     = 0;
                $totalMetrosArticulo    = 0;
            }
            $totalPiezasArticulo++;
            $totalKilosArticulo     += $rec->kilos;
            $totalMetrosArticulo    += $rec->metros;
            $granTotalPiezas++;
            $totalKilos             += $rec->kilos;
            $totalMetros            += $rec->metros;
            
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(24, 5, $rec->clave_prod, 0, 0, 'C');
            $pdf->Cell(22, 5, $rec->referenciaColor, 0, 0, 'C');
            $pdf->Cell(45, 5, $rec->color, 0, 0, 'C');
            $pdf->Cell(35, 5, $rec->clavePieza, 0, 0, 'C');
            $pdf->Cell(16, 5, $rec->metros, 0, 0, 'R');
            $pdf->Cell(16, 5, $rec->kilos, 0, 0, 'R');
            $pdf->Cell(14, 5, $rec->noTarima, 0, 1, 'C');
        }
        //##corte para el ultimo articulo
        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetXY(10, $pdf->GetY());
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(24, 5, '', 0, 0, 'R');
        $pdf->Cell(22, 5, '', 0, 0, 'R');
        $pdf->Cell(45, 5, '', 0, 0, 'R');
        $pdf->Cell(35, 5, $totalPiezasArticulo . ' piezas', 'T', 0, 'L');
        $pdf->Cell(16, 5, number_format($totalMetrosArticulo,2), 'T', 0, 'R');
        $pdf->Cell(16, 5, number_format($totalKilosArticulo,2), 'T', 0, 'R');
        $pdf->Cell(14, 5, '', 0, 1, 'C');

        $pdf->SetY($pdf->GetY() + 5);
    
        
        $pdf->SetY($pdf->GetY() + 5);
        $pdf->SetXY(10, $pdf->GetY());
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(91, 5, 'Gran total general:', 0, 0, 'L');
        $pdf->Cell(35, 5, number_format($granTotalPiezas).' piezas', 'TB', 0, 'L');
        $pdf->Cell(16, 5, number_format($totalMetros,2), 'TB', 0, 'R');
        $pdf->Cell(16, 5, number_format($totalKilos,2), 'TB', 0, 'R');
        $pdf->Cell(14, 5, '', 0, 1, 'C');
        return $pdf;
    }
}