<?php
class ReporteExistenciasModelo extends Modelo
{
	protected $productosModelo;

	public function Init()
	{
		$ctrl				= $this->app->GetControllador();
		$this->productosModelo 	= $ctrl->CargarModelo('Productos', 'productos');
	}
	public function ObtenerExistencias($marca = null)
	{
		$items = [];
		$query = "SELECT art.clave_prod, art.color, art.marca,
					SUM(p.metros) as metros, SUM(p.kilos) as kilos, COUNT(p.documentoSalida) as piezas
				FROM piezas p
				JOIN artref art on art.clave_prod = SUBSTRING_INDEX(p.clavePieza, '-', 1)
				WHERE 1 = 1
				AND p.fechaEntrada is not null and fechaSalida is null ";
		if( $marca )
		{
			
			$query .= "AND art.marca = '$marca' ";
			$query .= "GROUP BY art.clave_prod, art.color, art.marca
					ORDER BY art.clave_prod ASC";

			$items = $this->GetResults($query);
		}
		else
		{
			$marcas = $this->productosModelo->ObtenerMarcas();
			foreach($marcas as $marca)
			{
				$mquery = $query . "AND art.marca = '$marca->marca' ".
							"GROUP BY art.clave_prod, art.color, art.marca " .
							"ORDER BY art.clave_prod ASC";
				$items += $this->GetResults($mquery);
			}
		}
		
		

		return $items;
	}
	/**
	 * 
	 * @param array $items
	 * @return FPDF
	 */
	public function ConstruirPDF($items, $titulo = null, $group = false) 
	{
		$titulo = $titulo ? $titulo : "Resumen de entradas";

		require BASE_DIR . SB_DS . "include/lib/fpdf181/fpdf.php";
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetMargins(10, 20, 9.9);
		//##contruir cabecera
		$pdf->SetFont('Arial','B',16);
		$pdf->SetXY(10, 10);
		$pdf->Cell(196, 10, $titulo, 0, 1, 'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(196, 5, 'Emitido el ' . $this->getDateFormat(date('Y-m-d H:i:s')), 0, 1, 'C');
		$pdf->Cell(196, 5, '' , 0, 1, 'L');
		//##columnas
		$pdf->SetFont('Arial','BU',9);
		$pdf->SetXY(10, 35);
		$pdf->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
		//$this->Cell(22, 5, 'Referencia', 0, 0, 'C');
		$pdf->Cell(69, 5, 'Color', 0, 0, 'C');
		//$this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
		$pdf->Cell(14, 5, 'Metros', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Piezas', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Marca', 0, 1, 'C');

		$pdf->SetFont('Arial', 'BU', 8);
		$pdf->SetXY(10, 45);
		//$pdf->Cell(24, 5, 'articulo', 0, 1, 'C');
		
		$pdf->SetFont('Arial','',8);
		$gTotalMetros = 0;
		$gTotalKilos = 0;
		$gTotalPiezas	= 0;

		$totalMetros = 0;
		$totalKilos = 0;
		$totalPiezas	= 0;
		$marcaAnterior = null;
		$showgranTotal	= false;

		foreach($items as $item)
		{
			if( $marcaAnterior != null && $marcaAnterior != $item->marca )
			{
				$pdf->Ln();
				$pdf->Cell(24, 5, $marcaAnterior, 'TB', 0, 'C');
				$pdf->Cell(69, 5, '', 'TB', 0, 'C');
				$pdf->Cell(14, 5, number_format((float)$totalMetros, 2, '.', ','), 'TB', 0, 'R');
				$pdf->Cell(16, 5, number_format((float)$totalKilos, 3, '.', ','), 'TB', 0, 'R');
				$pdf->Cell(16, 5, $totalPiezas, 'TB', 0, 'C');
				$pdf->Cell(16, 5, '', 0, 1, 'C');
				$pdf->Ln();
				$totalMetros = 0;
				$totalKilos = 0;
				$totalPiezas	= 0;
				$showgranTotal = true;
			}
			$pdf->Cell(24, 5, $item->clave_prod, 0, 0, 'L');
			//$this->Cell(22, 5, $rec->referencia, 0, 0, 'C');
			$pdf->Cell(69, 5, $item->color, 0, 0, 'C');
			//$this->Cell(35, 5, $item->poliza, 0, 0, 'C');
			$pdf->Cell(14, 5, $item->metros, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->kilos, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->piezas, 0, 0, 'C');
			$pdf->Cell(16, 5, $item->marca, 0, 1, 'R');
			$totalMetros += $item->metros;
			$totalKilos += $item->kilos;
			$totalPiezas += $item->piezas;
			$gTotalMetros += $item->metros;
			$gTotalKilos += $item->kilos;
			$gTotalPiezas += $item->piezas;

			$marcaAnterior = $item->marca;
		}
		$pdf->Ln();
		$pdf->Cell(24, 5, $showgranTotal ? $marcaAnterior : 'Gran total', 'TB', 0, 'C');
		$pdf->Cell(69, 5, '', 'TB', 0, 'C');
		$pdf->Cell(14, 5, number_format((float)$totalMetros, 2, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(16, 5, number_format((float)$totalKilos, 3, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(16, 5, $totalPiezas, 'TB', 0, 'C');
		$pdf->Cell(16, 5, '', 0, 1, 'C');
		if( $showgranTotal )
		{
			$pdf->Ln();
			$pdf->Cell(24, 5, 'Gran total', 'TB', 0, 'C');
			$pdf->Cell(69, 5, '', 'TB', 0, 'C');
			$pdf->Cell(14, 5, number_format((float)$gTotalMetros, 2, '.', ','), 'TB', 0, 'R');
			$pdf->Cell(16, 5, number_format((float)$gTotalKilos, 3, '.', ','), 'TB', 0, 'R');
			$pdf->Cell(16, 5, $gTotalPiezas, 'TB', 0, 'C');
			$pdf->Cell(16, 5, '', 0, 1, 'C');
		}
		return $pdf;
	}
	public function ObtenerPorFecha($idProducto, $fechaLimite)
	{
		$query = "SELECT COUNT(idPiezas) as existencias
					FROM piezas_entradas 
					WHERE id_producto = $idProducto 
					AND DATE(fechaEntrada) <= '$fechaLimite'";

		$res = $this->GetObject($query);

		return (int)$res->existencias;
	}
}