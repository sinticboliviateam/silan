<?php
class ReporteKardexModelo extends Modelo
{
	protected	$reporteExistenciasModelo;

	public function Init()
	{
		$ctrl				= $this->app->GetControllador();
		$this->reporteExistenciasModelo 	= $ctrl->CargarModelo('ReporteExistencias');

	}
	public function ObtenerEntradas($idProducto)
	{
		$query = "SELECT * FROM piezas_entradas WHERE id_producto = $idProducto ORDER BY fechaEntrada ASC";

		return $this->GetResults($query);
	}
	public function ObtenerSalidas($idProducto)
	{
		$query = "SELECT * FROM piezas_salidas WHERE id_producto = $idProducto ORDER BY fechaSalida ASC";

		return $this->GetResults($query);	
	}
	public function Construir($producto, $entradas, $salidas, $titulo = '')
	{
		$titulo = $titulo ? $titulo : "Kardex";

		require BASE_DIR . SB_DS . "include/lib/fpdf181/fpdf.php";
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetMargins(10, 20, 9.9);
		//##contruir cabecera
		$pdf->SetFont('Arial','B',16);
		$pdf->SetXY(10, 10);
		$pdf->Cell(196, 10, $titulo, 0, 1, 'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(196, 5, sprintf("clave producto: %s", $producto->clave_prod), 0, 0, 'L');
		$pdf->Cell(196, 5, 'Emitido el ' . $this->getDateFormat(date('Y-m-d H:i:s')), 0, 1, 'R');
		//##columnas
		$pdf->SetFont('Arial','BU', 8);
		$pdf->SetXY(10, 35);
		$pdf->Cell(40, 5, utf8_decode('Fecha entrada'), 0, 0, 'C');
		$pdf->Cell(15, 5, 'Movto', 0, 0, 'C');
		$pdf->Cell(15, 5, 'Documento', 0, 0, 'C');
		$pdf->Cell(25, 5, utf8_decode('Cantidad'), 0, 0, 'C');
		$pdf->Cell(14, 5, 'Existencia', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Metros', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
		$pdf->Cell(35, 5, 'Clave Pieza', 0, 1, 'C');
		

		$pdf->SetFont('Arial', 'BU', 7);
		$pdf->SetXY(10, 45);
		//$pdf->Cell(24, 5, 'articulo', 0, 1, 'C');
		
		$pdf->SetFont('Arial','',7);
		$gTotalMetros = 0;
		$gTotalKilos = 0;
		$gTotalPiezas	= 0;
		$saldo = 1;
		foreach($entradas as $item)
		{
			$pdf->Cell(40, 5, $this->getDateFormat($item->fechaEntrada), 0, 0, 'L');
			$pdf->Cell(15, 5, 'entrada', 0, 0, 'C');
			$pdf->Cell(15, 5, $item->documentoEntrada, 0, 0, 'C');
			$pdf->Cell(25, 5, '1', 0, 0, 'C');
			$pdf->Cell(15, 5, $saldo, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->metros, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->kilos, 0, 0, 'C');
			$pdf->Cell(35, 5, $item->clavePieza, 0, 1, 'R');
			
			$gTotalMetros += $item->metros;
			$gTotalKilos += $item->kilos;
			//$gTotalPiezas += $item->piezas;
			$saldo++;
		}
		$pdf->Ln();
		$pdf->Cell(40, 5, '', 0, 0, 'L');
		$pdf->Cell(15, 5, '', 0, 0, 'C');
		$pdf->Cell(15, 5, '', 0, 0, 'C');
		$pdf->Cell(25, 5, '', 0, 0, 'C');
		$pdf->Cell(15, 5, $saldo - 1, 'TB', 0, 'R');
		$pdf->Cell(16, 5, number_format($gTotalMetros, 2, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(16, 5, number_format($gTotalKilos, 2, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(35, 5, '', 0, 1, 'R');

		//##construir salidas
		//##columnas
		$pdf->SetFont('Arial','BU', 8);
		$pdf->Ln();
		//$pdf->SetXY(10, 35);
		$pdf->Cell(40, 5, utf8_decode('Fecha entrada'), 0, 0, 'C');
		$pdf->Cell(40, 5, utf8_decode('Fecha salida'), 0, 0, 'C');
		$pdf->Cell(15, 5, 'Documento', 0, 0, 'C');
		$pdf->Cell(15, 5, utf8_decode('Cantidad'), 0, 0, 'C');
		$pdf->Cell(14, 5, 'Existencia', 0, 0, 'C');
		$pdf->Cell(15, 5, 'Metros', 0, 0, 'C');
		$pdf->Cell(15, 5, 'Kilos', 0, 0, 'C');
		$pdf->Cell(35, 5, 'Clave Pieza', 0, 1, 'C');
		

		$pdf->SetFont('Arial', 'BU', 8);
		//$pdf->SetXY(10, 45);
		//$pdf->Cell(24, 5, 'articulo', 0, 1, 'C');
		
		$pdf->SetFont('Arial','',8);
		$gTotalMetros = 0;
		$gTotalKilos = 0;
		$gTotalPiezas	= 0;
		
		foreach($salidas as $item)
		{
			$fechaLImite = date('Y-m-d', strtotime($item->fechaEntrada));

			$saldo = $this->reporteExistenciasModelo->ObtenerPorFecha($producto->id_artref, $fechaLImite);
			$pdf->Cell(40, 5, $this->getDateFormat($item->fechaEntrada), 0, 0, 'L');
			$pdf->Cell(40, 5, $this->getDateFormat($item->fechaSalida), 0, 0, 'L');
			$pdf->Cell(15, 5, $item->documentoEntrada, 0, 0, 'C');
			$pdf->Cell(15, 5, '1', 0, 0, 'C');
			$pdf->Cell(14, 5, $saldo, 0, 0, 'R');
			$pdf->Cell(15, 5, $item->metros, 0, 0, 'R');
			$pdf->Cell(15, 5, $item->kilos, 0, 0, 'C');
			$pdf->Cell(35, 5, $item->clavePieza, 0, 1, 'R');
			
			$gTotalMetros += $item->metros;
			$gTotalKilos += $item->kilos;
			//$gTotalPiezas += $item->piezas;
			
		}
		$pdf->Ln();
		$pdf->Cell(40, 5, '', 0, 0, 'L');
		$pdf->Cell(40, 5, '', 0, 0, 'C');
		$pdf->Cell(15, 5, '', 0, 0, 'C');
		$pdf->Cell(15, 5, '', 0, 0, 'C');
		$pdf->Cell(14, 5, '', 0, 0, 'R');
		$pdf->Cell(15, 5, number_format($gTotalMetros, 2, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(15, 5, number_format($gTotalKilos, 2, '.', ','), 'TB', 0, 'R');
		$pdf->Cell(35, 5, '', 0, 1, 'R');
		$pdf->Ln();

		return $pdf;
	}
}