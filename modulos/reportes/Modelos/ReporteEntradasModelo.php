<?php
class ReporteEntradasModelo extends Modelo
{
	public function Init()
	{

	}
	public function ObtenerResumen($fechaInicial, $fechaFinal = null)
	{
		if( !$fechaInicial )
			throw new Exception('La fecha inicial es requerida');
		

		$fechaInicial 	= sb_format_date($fechaInicial, 'Y-m-d');
		$fechaFinal 	= $fechaFinal ? sb_format_date($fechaFinal, 'Y-m-d') : null;
		
		$query = "SELECT p.documentoSalida, art.clave_prod, art.color, /*art.nombreProducto, p.idPiezas, p.clavePieza*/
					SUM(p.metros) as metros, SUM(p.kilos) as kilos, COUNT(p.documentoEntrada) as piezas, art.marca
				FROM piezas p
				JOIN artref art on art.clave_prod = SUBSTRING_INDEX(p.clavePieza, '-', 1)
				WHERE 1 = 1
				AND p.fechaEntrada is not null
				{where}
				GROUP BY art.clave_prod, art.color, art.marca ";
		
		$where = '';
		

		if( $fechaInicial && $fechaFinal )
			$where .= "AND (DATE(p.fechaEntrada) >= '$fechaInicial' AND DATE(p.fechaEntrada) <= '$fechaFinal') ";
		
		elseif( $fechaInicial )
		{
			$where .= "AND DATE(p.fechaEntrada) = '$fechaInicial' ";
		}
		
		$query = str_replace('{where}', $where, $query);
		//die($query);
		$results = $this->GetResults($query);
		
		return $results;
	}
	/**
	 * 
	 * @param array $items
	 * @return FPDF
	 */
	public function ConstruirPDF($items, $titulo = null)
	{
		$titulo = $titulo ? $titulo : "Resumen de entradas";

		require BASE_DIR . SB_DS . "include/lib/fpdf181/fpdf.php";
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetMargins(10, 20, 9.9);
		//##contruir cabecera
		$pdf->SetFont('Arial','B',16);
		$pdf->SetXY(10, 10);
		$pdf->Cell(196, 10, $titulo, 0, 1, 'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(196, 5, 'Emitido el ' . $this->getDateFormat(date('Y-m-d H:i:s')), 0, 1, 'C');
		$pdf->Cell(196, 5, '' , 0, 1, 'L');
		//##columnas
		$pdf->SetFont('Arial','BU',9);
		$pdf->SetXY(10, 35);
		$pdf->Cell(24, 5, utf8_decode('Artículo'), 0, 0, 'C');
		//$this->Cell(22, 5, 'Referencia', 0, 0, 'C');
		$pdf->Cell(69, 5, 'Color', 0, 0, 'C');
		//$this->Cell(35, 5, utf8_decode('Póliza'), 0, 0, 'C');
		$pdf->Cell(14, 5, 'Metros', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Kilos', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Piezas', 0, 0, 'C');
		$pdf->Cell(16, 5, 'Marca', 0, 1, 'C');

		$pdf->SetFont('Arial', 'BU', 8);
		$pdf->SetXY(10, 45);
		//$pdf->Cell(24, 5, 'articulo', 0, 1, 'C');
		
		$pdf->SetFont('Arial','',8);
		$totalMetros = 0;
		$totalKilos = 0;
		$totalPiezas	= 0;

		foreach($items as $item)
		{
			$pdf->Cell(24, 5, $item->clave_prod, 0, 0, 'L');
			//$this->Cell(22, 5, $rec->referencia, 0, 0, 'C');
			$pdf->Cell(69, 5, $item->color, 0, 0, 'C');
			//$this->Cell(35, 5, $item->poliza, 0, 0, 'C');
			$pdf->Cell(14, 5, $item->metros, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->kilos, 0, 0, 'R');
			$pdf->Cell(16, 5, $item->piezas, 0, 0, 'C');
			$pdf->Cell(16, 5, $item->marca, 0, 1, 'R');
			$totalMetros += $item->metros;
			$totalKilos += $item->kilos;
			$totalPiezas += $item->piezas;
		}
		$pdf->Ln();
		$pdf->Cell(24, 5, 'Gran total', 'TB', 0, 'C');
		$pdf->Cell(69, 5, '', 'TB', 0, 'C');
		$pdf->Cell(14, 5, number_format((float)$totalMetros, 2, '.', ''), 'TB', 0, 'R');
		$pdf->Cell(16, 5, number_format((float)$totalKilos, 3, '.', ''), 'TB', 0, 'R');
		$pdf->Cell(16, 5, $totalPiezas, 'TB', 0, 'C');
		$pdf->Cell(16, 5, '', 0, 1, 'C');

		return $pdf;
	}
}