<?php
class ReporteResumenEntradasControlador extends Controlador
{
	/**
	 * @var ReporteentradasModelo
	 */
	protected	$reporteEntradasModelo;

	public function Init()
	{
		$this->reporteEntradasModelo = $this->CargarModelo('ReporteEntradas');

	}
	public function TaskDefault()
	{
		try
		{
			$this->CargarVista('resumen-entradas');
		}
		catch(Exception $e)
		{

		}
	}
	public function TaskResumen()
	{
		try
		{
			$fechaInicial 		= filter_input(INPUT_POST, 'fechaInicial');
			$fechaFinal 		= filter_input(INPUT_POST, 'fechaFinal');
		

			$titulo = sprintf("Resumen de Entradas del %s %s", 
				$this->reporteEntradasModelo->getDateFormat($fechaInicial, 0, false),
				$fechaFinal ? ' al ' . $this->reporteEntradasModelo->getDateFormat($fechaFinal, 0, false) : ''
			);
			$items = $this->reporteEntradasModelo->ObtenerResumen($fechaInicial, $fechaFinal);
			$pdf = $this->reporteEntradasModelo->ConstruirPDF($items, $titulo);
			$pdf->Output('reporte.pdf', 'I');
			die();
		}
		catch(Exception $e)
		{
			die('ERROR:' . $e->getMessage());
		}
	}
}