<?php
class ReporteOrdenesCompraControlador extends Controlador
{
	protected	$ordenesModelo;
	protected	$reporteSalidasModelo;

	public function Init()
	{
		$this->ordenesModelo = $this->CargarModelo('Ordenes', 'tarimas');
		$this->reporteSalidasModelo	= $this->CargarModelo('ReporteSalidas');
	}
	public function TaskDefault()
	{
		//$_SESSION["datauser"]["menuaccess"] = "02060000";
		//$permisos 	= $this->autPerimisos->regresaPermisosUsuario();
		//$leer		= strstr($permisos, 'L') ? true : false;
		//$escribir	= strstr($permisos, 'E') ? true : false;
		//$borrar		= strstr($permisos, 'B') ? true : false;
		
		$ordenes = $this->ordenesModelo->ObtenerTodas(true);
		$this->CargarVista('reporte-orden-compra', get_defined_vars());
	}
	public function TaskConstruir()
	{
		try
		{
			$id = filter_input(INPUT_GET, 'id');
			if( !$id )
				throw new Exception('Identificador de orden de compra invalido');
			$ordenCompra = $this->ordenesModelo->Obtener($id);
			if( !$ordenCompra )
				throw new Exception('La orden de compra no existe');

			$items = $this->reporteSalidasModelo->ObtenerSalidasPorOrdenCompra($ordenCompra);
			$pdf = $this->reporteSalidasModelo->ConstruirPdfSalidas($ordenCompra, $items);
			$pdf->Output('reporte-salidas-orden-compra.pdf', 'I');
			die;
		}
		catch(Exception $e)
		{
			die('ERROR:' . $e->getMessage());
		}
	}
}