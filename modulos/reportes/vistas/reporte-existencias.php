<div>
	<h2>Reporte de Existencias</h2>
	<form action="" method="post">
		<input type="hidden" name="mod" value="reportes" />
		<input type="hidden" name="task" value="ReporteExistencias.Construir" />
		<p>
			<label>Marca</label>
			<select name="marca">
				<option value="">-- seleccion --</option>
				<?php foreach($marcas as $marca): ?>
				<option value="<?php print $marca->marca ?>"><?php print $marca->marca ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<button type="" class="submit ui-button ui-corner-all ui-widget">Generar Reporte</button>
		</p>
	</form>
</div>