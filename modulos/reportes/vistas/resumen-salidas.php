<?php
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2>Reporte Resumen de Salidas</h2>
			<form action="" method="post" target="_blank">
				<input type="hidden" name="mod" value="reportes" />
				<input type="hidden" name="task" value="ReporteResumenSalidas.Resumen" />
				<!--
				<div class="form-group">
					<label>Fecha Inicial</label>
					<input type="text" name="fechaInicial" value="" class="hasDatepicker" placeholder="dd/mm/aaaa" />
				</div>
				<div class="form-group">
					<label>Fecha Final</label>
					<input type="text" name="fechaFinal" value="" class="hasDatepicker" placeholder="dd/mm/aaaa" />
				</div>
				-->
				<div class="form-group">
					<label>N&uacute;mero de Folio </label>
					<input type="number" min="0" name="documentoSalida" value="" class="" required />
				</div>
				<div class="form-group">
					<button type="" class="submit ui-button ui-corner-all ui-widget">Generar Reporte</button>
				</div>
			</form>
		</div>
	</div>
</div>