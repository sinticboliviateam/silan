<div>
	<h2>Reporte Orden de Compra</h2>
	<div class="tableRecords" style="width: 50%;">
        <table class="">
            <thead class="tableHead">
                <tr class="">
                	<th class="th-col04-col02">Orden de Compra</th>
                    <th class="th-col04-col01">Fecha de registro</th>
                    <th class="th-col04-col04">Total de piezas</th>
                </tr>
            </thead>
            <tbody id="tbl01-records-busqueda">
            	<?php foreach($ordenes as $orden): ?>
                <tr class="registro-orden" data-id="<?php print $orden->idOrdenCompra ?>">
                    <td><?php print $orden->ordenCompra ?></td>
                    <td><?php print $this->ordenesModelo->getDateFormat($orden->fechaRegistro) ?></td>
                    <td><?php print $orden->piezas ?></td>
               	</tr>
               <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<script>
function construirReporte(orden)
{
	let url = 'index.php?mod=reportes&task=ReporteOrdenesCompra.Construir&id=' + orden.id;
	window.location = url;

}
jQuery(function()
{
	jQuery(document).on('click', '.registro-orden', function(e){construirReporte(this.dataset);});
});
</script>