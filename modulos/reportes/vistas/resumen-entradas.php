<?php
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h2>Reporte Resumen de Entradas</h2>
			<form action="" method="post" target="_blank">
				<input type="hidden" name="mod" value="reportes" />
				<input type="hidden" name="task" value="ReporteResumenEntradas.Resumen" />
				
				<div class="form-group">
					<label>Fecha Inicial</label>
					<input type="text" id="fechaInicial" name="fechaInicial" value="" placeholder="dd/mm/aaaa" />
				</div>
				<div class="form-group">
					<label>Fecha Final</label>
					<input type="text" id="fechaFinal" name="fechaFinal" value="" placeholder="dd/mm/aaaa" />
				</div>
				<div class="form-group">
					<button type="" class="submit ui-button ui-corner-all ui-widget">Generar Reporte</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
jQuery(function()
{
	jQuery("#fechaInicial, #fechaFinal").datepicker(jQuery.datepicker.regional[ "es" ] );
});
</script>