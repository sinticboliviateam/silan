<?php
/* 
 * Libreria creada por: Diego Fernández, Manuel Luna
 * MEXICO, 2017
 * SiLan v1.0
 * modificaciones: 2017-03-17
*/
error_reporting(-1);
ini_set('display_errors', 'On');

include (dirname(__FILE__)."/class/Combos.class.php");
require_once(dirname(__FILE__)."/../procesos/Procesos.class.php");

$debug = false;

if(isset($_REQUEST["data"])){
    $datakey = (object)$_REQUEST["data"];
    if ($debug) echo ("<br>" . $datakey->widget);
    switch ($datakey->widget){
        case "listaPiezasSinKg":
            $combos = new Combos();
            echo json_encode($combos->listaPiezasSinKg());
            break;
        case "informacionPieza":
            $combos = new Combos();
            echo json_encode($combos->informacionPieza($datakey->clave_pieza));
            break;
        case "tacomePieza":
            $procesos = new Procesos();
            echo json_encode($procesos->tacomePieza($datakey));
            break;
    /*
        case "imprimePieza":
            $procesos = new Procesos();
            echo json_encode($procesos->imprimePieza($datakey));
            break;
    */
        case "reimprimePieza":
            $procesos = new Procesos();
            echo json_encode($procesos->reimprimePieza($datakey));
            break;
        default:
            break;
    }
}