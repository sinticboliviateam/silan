<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
error_reporting(-1);
ini_set('display_errors', 'On');

include (dirname(__FILE__)."/class/Combos.class.php");
require_once(dirname(__FILE__)."/../procesos/Procesos.class.php");

if(isset($_REQUEST["data"])){
    $datakey = (object)$_REQUEST["data"];
    switch ($datakey->widget){
        case "imageProduct":
            $combos = new Combos();
            echo json_encode($combos->getImageproduct($datakey->keyproduct));
            break;
        case "allWidgetProductOptions":
            $combos = new Combos();
            echo json_encode($combos->getAllWidgetProductOptions());
            break;
        case "allWidgetProductList":
            $combos = new Combos();
            echo json_encode($combos->getAllWidgetProductList());
            break;
        case "guardaPoliza":
            $procesos = new Procesos();
            echo json_encode($procesos->guardaPoliza($datakey));
            break;
        default:
            break;
    }
}