<?php
/* 
 * Libreria creada por: Diego Fernández, Manuel Luna
 * MEXICO, 2017
 * SiLan v1.0
 * modificaciones: 2017-03-17
*/
error_reporting(-1);
ini_set('display_errors', 'On');

require_once (dirname(__FILE__)."/class/Combos.class.php");
require_once(dirname(__FILE__)."/../procesos/Procesos.class.php");

$debug = false;

if(isset($_REQUEST["data"])){
    $datakey = (object)$_REQUEST["data"];
    if ($debug) echo ("<br>" . $datakey->widget);
    switch ($datakey->widget){
        case "validacionUnico":
            $combos = new Combos();
            echo json_encode($combos->validacionUnico($datakey));
            break;
        default:
            break;
    }
}