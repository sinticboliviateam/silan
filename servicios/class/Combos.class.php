<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/


require_once(dirname(__FILE__)."/../../include/dbConnections.inc.php");

class Combos extends Modelo{
    public $msgout = "";
    
    public function __construct(){
        parent::__construct();
    }
//-- para polizas    
    public function getAllWidgetProductOptions() {
        $options = "<option value=0>Selecciona...</option>";
        $sql = "SELECT id_artref, clave_prod FROM ".DB_NAME_SYS.".artref ORDER BY 'idx_claveProducto'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["clave_prod"].'">'.utf8_encode($row["clave_prod"]).'</option>';
        }
        $dataresult = (object) array("error" => "", "options" => $options);
        return $dataresult;
    }

    public function getAllWidgetProductList() {
        $list = "";
        //$sql = "SELECT id_artref, clave_prod FROM " . DB_NAME_SYS . ".artref ORDER BY 'idx_claveProducto'";
        $sql = "SELECT id_artref, clave_prod FROM " . DB_NAME_SYS . ".artref ORDER BY clave_prod";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list .= utf8_encode($row["clave_prod"]) . ",";
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }
//-- para tacome    
public function getAllWidgetPiezaOptions() {
        $options = "<option value=0>Selecciona...</option>";
        // select id_piezas, clave_pieza from silan.piezas order by clave_pieza;
        $sql = "SELECT id_piezas, clave_pieza FROM ".DB_NAME_SYS.".piezas ORDER BY 'clave_pieza'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $options.= '<option value="'.$row["clave_pieza"].'">'.utf8_encode($row["clave_pieza"]).'</option>';
        }
        $dataresult = (object) array("error" => "", "options" => $options);
        return $dataresult;
    }

    public function getAllWidgetPiezaList() {
        $list = "";
        $sql = "SELECT id_piezas, clave_pieza FROM ".DB_NAME_SYS.".piezas ORDER BY 'clave_pieza'";
        //$sql = "SELECT id_artref, clave_prod FROM " . DB_NAME_SYS . ".artref ORDER BY 'idx_claveProducto'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list .= utf8_encode($row["clave_pieza"]) . ",";
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }    
    
    public function getImageproduct($clave_producto=0) {
        $ann = date('Y');
        $mes = date('m');

        $no_poliza = $this->obtieneSiguientePoliza($clave_producto, $ann, $mes);
        $siguientePoliza = $this->construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza);

        $sql = "SELECT ruta FROM ".DB_NAME_SYS.".artref WHERE clave_prod = '$clave_producto' LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        if ($row == NULL) {
            // No existe el producto
            $dataresult = (object) array(
                "error" => "ERROR. El producto no existe.",
                "img" => '',
                "siguientePoliza" => ''
                );
        } else if (!empty($row["ruta"])) {
            $dataresult = (object) array(
                "error" => "",
                "img" => $row["ruta"],
                "siguientePoliza" => $siguientePoliza
                );
        } else {
            $dataresult = (object) array(
                "error" => "ERROR. El producto no cuenta con imágen.",
                "img" => '',
                "siguientePoliza" => $siguientePoliza
                );
        }                
        return $dataresult;
    }

    private function dbCloseobject($result, $oMysqli) {
        $result->free_result;
        $oMysqli->close;
    }

    public function listaPiezasSinKg() {
        $list = "";
        $sql = "SELECT clave_pieza FROM ".DB_NAME_SYS.".piezas WHERE kilos IS NULL ORDER BY 'clave_pieza'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list .= utf8_encode($row["clave_pieza"]) . ",";
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }

    public function listaPiezasSinEntrada() {
        $list = Array();
        $sql = "SELECT numero, clave_pieza FROM ".DB_NAME_SYS.".piezas WHERE fecha_entrada IS NULL ORDER BY 'numero'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }

    public function listaPiezasSinSalida() {
        $list = Array();
        $sql = "SELECT numero, clave_pieza FROM ".DB_NAME_SYS.".piezas ";
        $sql .= "WHERE fecha_entrada IS NOT NULL AND fecha_salida IS NULL ORDER BY 'numero'";
        $result = $this->_db_sys->query($sql);
        while($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        $dataresult = (object) array("error" => "", "list" => $list);
        return $dataresult;
    }

    public function informacionPieza($clave_pieza) {
        $dataresult = (object) array(
                "error" => "",
                "img" => '',
                "metros" => ''
        );

        // Imagen del producto
        $partes = preg_split("/-/", $clave_pieza);
        $clave_prod = $partes[0];

        $sql = "SELECT ruta FROM ".DB_NAME_SYS.".artref WHERE clave_prod = '$clave_prod' LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        if ($row == NULL) {
            // No existe el producto
            $dataresult->error = "ERROR. El producto " . $clave_prod . " no existe.";
        } else if (!empty($row["ruta"])) {
            $dataresult->img = $row["ruta"];
        } else {
            $dataresult->error = "ERROR. El producto " . $clave_prod . " no cuenta con imágen.";
        }                

        // Metros
        $sql = "SELECT metros, kilos FROM ".DB_NAME_SYS.".piezas WHERE clave_pieza = '$clave_pieza' LIMIT 1";
        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        if ($row == NULL) {
            // No existe la pieza (kilos != null)
            $dataresult->error = "ERROR. La pieza no existe.";
            $dataresult->img = '';
        } else if ($row["kilos"] != null) {
            $dataresult->error = "ERROR. Los kilos ya están asignados.";
            $dataresult->img = '';
        } else {
            $dataresult->metros = $row["metros"];
        }

        return $dataresult;
    }

    public function validacionUnico($datakey) {
        $dataresult = (object) array(
                "error" => "",
                "unico" => 'NO'
        );

        if (!property_exists($datakey, 'campo') || !property_exists($datakey, 'valor')) {
            $dataresult->error = 'Se requiere "campo" y "valor"<br>';
            $dataresult->unico = '';
            var_dump($datakey);
        }

        if ($dataresult->error == '') {
            $campo = $datakey->campo;
            $valor = $datakey->valor;

            $sql = "SELECT $campo FROM ".DB_NAME_SYS.".piezas WHERE $campo = '$valor'";
            $result = $this->_db_sys->query($sql);
            $row = $result->fetch_assoc();
            if ($row == NULL) {
                $dataresult->unico = 'SI';
            }
        }

        return $dataresult;
    }

    public function validacionExisteCliente($datakey) {
        $dataresult = (object) array(
                "error" => "",
                "existe" => 'NO'
        );

        if (!property_exists($datakey, 'no_cliente')) {
            $dataresult->error = 'Se requiere "no_cliente"<br>';
            $dataresult->existe = '';
            var_dump($datakey);
        }

        if ($dataresult->error == '') {
            $no_cliente = $datakey->no_cliente;

            $sql = "SELECT no_cliente FROM ".DB_NAME_SYS.".clientes WHERE no_cliente = '$no_cliente'";
            $result = $this->_db_sys->query($sql);
            $row = $result->fetch_assoc();
            if ($row != NULL) {
                $dataresult->existe = 'SI';
            }
        }

        return $dataresult;
    }


}
