<?php
/* 
 * Libreria creada por: Diego Fernández, Manuel Luna
 * MEXICO, 2017
 * SiLan v1.0
 * modificaciones: 2017-03-17
*/
error_reporting(-1);
ini_set('display_errors', 'On');

require_once(dirname(__FILE__)."/class/Combos.class.php");
require_once(dirname(__FILE__)."/../procesos/Procesos.class.php");

$debug = FALSE;

if(isset($_REQUEST["data"])){
    $datakey = (object)$_REQUEST["data"];
    if ($debug) var_dump($datakey);
    switch ($datakey->widget){
        case "listaPiezasSinSalida":
            $combos = new Combos();
            echo json_encode($combos->listaPiezasSinSalida());
            break;
        case "validacionExisteCliente":
            $combos = new Combos();
            echo json_encode($combos->validacionExisteCliente($datakey));
            break;
        case "salidaPiezas":
            $procesos = new Procesos();
            echo json_encode($procesos->salidaPiezas($datakey));
            break;
        default:
            echo 'Valor inesperado: ' . $datakey->widget;
            break;
    }
}