<?php
/* 
 * Libreria creada por: Diego Fernández, Manuel Luna
 * MEXICO, 2017
 * SiLan v1.0
 * modificaciones: 2017-03-17
*/
error_reporting(-1);
ini_set('display_errors', 'On');

require_once(dirname(__FILE__)."/class/Combos.class.php");
require_once(dirname(__FILE__)."/../procesos/Procesos.class.php");

$debug = FALSE;

if(isset($_REQUEST["data"])){
    $datakey = (object)$_REQUEST["data"];
    if ($debug) var_dump($datakey);
    switch ($datakey->widget){
        case "listaPiezasSinEntrada":
            $combos = new Combos();
            echo json_encode($combos->listaPiezasSinEntrada());
            break;
        case "validacionUnico":
            $combos = new Combos();
            echo json_encode($combos->validacionUnico($datakey));
            break;
        case "entradaPiezas":
            $procesos = new Procesos();
            echo json_encode($procesos->entradaPiezas($datakey));
            break;
        default:
            break;
    }
}