<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
//unset($_SESSION["record"]);
//require_once './include/class/menu.class.php';
//$autPermisos = new menu();
//$_SESSION["datauser"]["menuaccess"] = "02040000";
//$permisos = $autPermisos->regresaPermisosUsuario();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Tacome</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/tacome.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/tacome.class.js"></script>
        <script src="./js/class/tacomeValidate.class.js"></script> 
        <script src="./js/class/autocomplete.class.js"></script>
        <script>
            $(document).ready(function(){
                $setmenu.getLinksUser();
                rSisTacome.ready();
//                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div id="dialogTacome" style="display: none;"></div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
