<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
    session_start();
    //var_dump($_SESSION);exit;
    if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" media="screen" href="./css/comm.css">
        <link rel="stylesheet" media="screen" href="./css/menu.css" />
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                //$("nav #menu").find(".active").removeClass();
                //var links = $("nav #menu > li");
                //$(links[0]).find("a").first().addClass("active");
                $setmenu.getLinksUser();
            });
        </script>
    </head>
    <body>
        <?php include('./menu.php'); ?>
        <div class=""></div>
    </body>
</html>