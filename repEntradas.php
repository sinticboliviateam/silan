<?php
/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-04
 * SiLan v1.0
*/
session_start();
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
require_once './include/class/menu.class.php';
$autPermisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02070000";
$permisos = $autPermisos->regresaPermisosUsuario();
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Reporte de Entradas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">

        <link rel="stylesheet" href="./css/repEntradas.css">
        
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/repEntradas.class.js"></script>
        <script>
            
            $(document).ready(function(){
                $setmenu.getLinksUser();
                $("#fechaIni,#fechaFin").datepicker($.datepicker.regional[ "es" ] );

                repEntradas.consulta();
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-periodo">
                <h3>Fecha inicial</h3>
                <input type="text" id="fechaIni" placeholder="dd/mm/aaaa">
                <h3>Fecha final</h3>
                <input type="text" id="fechaFin" placeholder="dd/mm/aaaa">
            </div>
            
            <div class="cleared" style="display: block; width: 50%;"></div>
            <br>
        <!--
        <button class="ui-button ui-widget ui-corner-all ui-icon ui-icon-print" >Consulta</button>
        -->
        <div class="btnConsulta ui-button ui-corner-all ui-widget" role="button" style="font-size: 0.75em;">
           <span class="ui-button-icon ui-icon ui-icon-print"></span>
           <span class="ui-button-icon-space"></span>
           Consulta
        </div>
 
        </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
