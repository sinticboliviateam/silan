<!--/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/-->
<form class="body-menu" style="height: 45px; background-color: #333333;">
    <!--<div style="background-color: #333333; height: 45px;">-->
        <div class="productname"><h2>SiLan</h2></div>
        <div class="menubar">
            <nav>
                <ul id="menu">
                    <li><a href="#" class="" data-menu="01000000" style="">Inventarios</a>
                        <ul class="children">
                            <li><a href="#" class="" onclick="" data-menu="01010000">Entradas H</a></li>
                            <li><a href="#" class="" data-menu="01020000">Salidas H</a></li>
                            <li><a href="#" class="" data-menu="01030000">Kardex H</a></li>
                            <li><a href="#" class="" data-menu="01040000">Rep 1</a></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-menu="02000000" style="">Producción</a>
                        <ul class="children">
                            <li>
                                <a href="#" class="" onclick="" data-menu="02010000">Hilaturas</a>
                                <ul class="children">
                                    <li><a href="" data-menu="02010100">Hilos</a></li>
                                    <li><a href="" data-menu="02010200">Productos</a></li>
                                    <li><a href="" data-menu="02010300">Fórmulas telas</a></li>
                                    <li><a href="" data-menu="02010400">Algo</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="" data-menu="02020000">Pólizas PT</a></li>
                            <li><a href="#" class="" data-menu="02030000">Entradas PT</a></li>
                            <li><a href="#" class="" data-menu="02040000">Tacome</a></li>
                            <li><a href="#" class="" data-menu="02050000" >Salidas PT</a></li>

                            <li><a href="#" class="" data-menu="02060000">Reportes piezas</a>
                                <ul class="children">
                                    <li><a href="#" class="" data-menu="02060100">Rep. Entradas</a></li>
                                    <li><a href="#" class="" data-menu="02060200">Rep. Salidas</a></li>
                                    <li><a href="#" class="" data-menu="02060300">Rep. resumen entradas</a></li>
                                    <li><a href="#" class="" data-menu="02060400">Rep. resumen salidas</a></li>
                                    <li><a href="#" class="" data-menu="02060500">ModaTelas</a></li>
                                    <li><a href="#" class="" data-menu="02060600">Existencias</a></li>
                                    <li><a href="#" class="" data-menu="02060700">Kardex</a></li>
                                    <li><a href="#" class="" data-menu="02060800">Rep. salidas por Orden Compra</a></li>
                                </ul> 
                            </li>

                            <li><a href="#" class="" data-menu="02080000" style="">Tarimas</a>
                                <ul class="children">
                                    <li><a href="#" class="" onclick="" data-menu="02080100">Ordenes de compra</a></li>
                                    <li><a href="#" class="" data-menu="02080200">armado de tarimas</a></li>
                                    <li><a href="#" class="" data-menu="02080300">Destinos</a></li>
                                    <li><a href="#" class="" data-menu="02080400">imprime etiq. LPN</a></li>
                                    <li><a href="#" class="" data-menu="02080500">Reetiq. piezas anteriores</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-menu="03000000" style="">Sistemas</a>
                        <ul class="children">
                            <li><a href="#" class="" data-menu="03010000">Generales</a></li>
                            <li><a href="#" class="" data-menu="03020000">Corte mes</a></li>
                            <li><a href="#" class="" data-menu="03030000">Usuarios</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <nav style="width: 18%;">
            <ul id="menu-2" style="width: 100%;">
                <li style="width: 100%;"><a href="#" class="" data-menu="04000000" style="padding-bottom: 0px;"><?php echo substr(utf8_encode($_SESSION["datauser"]["nombreCompleto"]),0,26); ?></a>
                    <ul class="children">
                        <li><a href="#" class="" data-menu="04010000">Salir</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    <!--</div>-->
</form>