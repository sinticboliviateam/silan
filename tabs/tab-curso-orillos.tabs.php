<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
?>
<form id="form-tab-curso-orillo">
    <div class="flex-modal-produccion">
        <!--*** COLUMNA: CURSO ORILLOS ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab" style="display: flex;">
                <div class="div-oLabel-tab" style="width: 30%; margin-left: 58px;">
                    <label class="oLabel-left">Curso</label></div>
                    <div class="tabs-usuarios-botones" style="justify-content:flex-end;">
                    <div class="agregar-curso"></div>
                </div>
            </div>
            <div class="tableRecords" style="width: 65%; height: 328px;">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col02-col01">Sec</th>
                            <th class="th-col02-col02">Dato</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-orillo">
                        <tr class="" id="">
                            <td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="block-info-tab" style="margin-top: 0.5em;">
                <div class="div-oLabel-tab" style="width: 110px; text-align: right;">
                    <label for="totalHilosOrillo" class="oLabel-left">Total de hilos</label></div>
                <div class="div-dataInput-tab" style="">
                    <input id="totalHilosOrillo" name="totalHilosOrillo" class="oInput campoRequerido" style="width: 126px; text-align: right;" type="text" value="0" readonly></div>
            </div>
        </div>
        <!--*** COLUMNAS: PORCENTAJES Y CODIGOS ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="height: 23.5px; margin-left: 10px;">
                    <label class="oLabel-left">Porcentajes e hilos</label></div>
            </div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col03-col01">Color</th>
                            <th class="th-col03-col02">%</th>
                            <th class="th-col03-col03">Hilo</th>
                        </tr>
                    </thead>
                    <tbody id="tbl02-records-orillo">
                        <tr class="" id="">
                            <td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="block-info-tab" style="margin-top: 0.5em;">
                <div class="div-oLabel-tab" style="width: 70px; text-align: right;">
                    <label for="totalPorcentajeOrillo" class="oLabel-left">Total</label></div>
                <div class="div-dataInput-tab" style="">
                    <input id="totalPorcentajeOrillo" name="totalPorcentajeOrillo" class="oInput" style="width: 100px; text-align: right;" type="text" value="0" readonly></div>
            </div>
        </div>
        <!--*** COLUMNAS: SELECCION DE COLORES ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <!--*** LISTA DE BOTONES ***-->
            <div class="tabs-usuarios-botones">
                <div id="guardar-curso-orillo" class="guarda-datos-curso"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <!--*** SELECCION DE COLORES ***-->
            <div class="block-info-tab" style="margin-bottom: 1em;">
                <div class="div-oLabel-tab">
                    <label class="oLabel-left">Selección</label></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="O87" class="oLabel-left">Color W</label></div>
                <div class="div-dataInput-tab">
                    <input id="O87" name="O87" class="oInput campoRequerido autocomplete-orillo" type="text" value="" maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="O88" class="oLabel-left">Color X</label></div>
                <div class="div-dataInput-tab">
                    <input id="O88" name="O88" class="oInput campoRequerido autocomplete-orillo" type="text" value="" maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="O89" class="oLabel-left">Color Y</label></div>
                <div class="div-dataInput-tab">
                    <input id="O89" name="O89" class="oInput campoRequerido autocomplete-orillo" type="text" value="" maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="O90" class="oLabel-left">Color Z</label></div>
                <div class="div-dataInput-tab">
                    <input id="O90" name="O90" class="oInput campoRequerido autocomplete-orillo" type="text" value="" maxlength="60" tabindex="1"></div>
            </div>
        </div>
    </div>
</form>
