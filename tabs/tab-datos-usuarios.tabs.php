<?php 
// * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
// * SiLan v1.0
// * MEXICO, 2017
session_start();
require_once '../include/combos.php';
?>
<form id="form-tab-datos-usuarios" enctype="multipart/form-data" method="post">
    <div class="flex-modal-produccion">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="nombreCompleto" class="oLabel-left">Nombre completo</label></div>
                <div class="div-dataInput-tab">
                    <input id="nombreCompleto" name="nombreCompleto" class="oInput" type="text" value="" required maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuario" class="oLabel-left">Usuario</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuario" name="usuario" class="oInput" type="text" required maxlength="15" tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="password" class="oLabel-left">Contraseña</label></div>
                <div class="div-dataInput-tab">
                    <input id="password" name="password"  class="oInput" type="password" required maxlength="25" minlength="3" tabindex="3"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="repeatPassword" class="oLabel-left">Confirma contraseña</label></div>
                <div class="div-dataInput-tab">
                    <input id="repeatPassword" name="repeatPassword" class="oInput" type="password" required maxlength="25" minlength="3" tabindex="4"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="rfc" class="oLabel-left">RFC</label></div>
                <div class="div-dataInput-tab">
                    <input id="rfc" name="rfc" class="oInput" type="text" required maxlength="15" tabindex="5"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="tipoUsuario" class="oLabel-left">Tipo de usuario</label></div>
                <div class="div-dataInput-tab">
                    <select id="tipoUsuario" name="tipoUsuario" class="oSelect-normal-tab" tabindex="6"><?php echo $combos->selectTipoUsuario(); ?></select></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="puesto" class="oLabel-left">Puesto</label></div>
                <div class="div-dataInput-tab">
                    <input id="puesto" name="puesto" class="oInput" type="text" tabindex="7"></div>
            </div><div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="horario" class="oLabel-left">Horario</label></div>
                <div class="div-dataInput-tab">
                    <input id="horario" name="horario" class="oInput" type="text" tabindex="8"></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="correos" class="oLabel-left">Correos</label></div>
                <div class="div-dataInput-tab">
                    <input id="correos" name="correos" class="oInput" type="text" tabindex="9"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="celulares"  class="oLabel-left">Celulares</label></div>
                <div class="div-dataInput-tab">
                    <input id="celulares" name="celulares" class="oInput" type="text" tabindex="10"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="telefonos" class="oLabel-left">Teléfonos</label></div>
                <div class="div-dataInput-tab">
                    <input id="telefonos" name="telefonos"class="oInput" type="text" tabindex="11"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="ultimoAcceso" class="oLabel-left">Último acceso</label></div>
                <div class="div-dataInput-tab">
                    <input id="ultimoAcceso" name="ultimoAcceso" class="oInput" type="text" tabindex="12" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="estatus" class="oLabel-left">Estatus</label></div>
                <div class="div-dataInput-tab">
                    <select id="estatus" name="estatus" class="oSelect-normal-tab" tabindex="13"><?php echo $combos->selectEstatusUsuario(1); ?></select></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaEstatus" class="oLabel-left">Fecha estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaEstatus" name="fechaEstatus" class="oInput" type="text" tabindex="14" readonly value="<?php echo $combos->getDateFormat(date("d-m-Y h:i:s"), 1); ?>"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioEstatus" class="oLabel-left">Usuario estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioEstatus" name="usuarioEstatus" class="oInput" type="text" tabindex="15" readonly value="<?php echo ($_SESSION["datauser"]["nombreCompleto"]); ?>"></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px; padding: 0px;">
            <div class="tabs-usuarios-botones">
                <div id="guardar-datos-usuario"></div>
                <div id="borrar-datos-usuario"></div>
                <div id="cerrar-tabs-usuario" class="cerrar-tabs"></div>
            </div>
            <hr>
            <div class="baseImagen" style="margin-top: 10px;">
                <div id="div-imagen" class="imagenRegistro"></div>
            </div>
            <div class="tabs-usuarios-botones" style="justify-content: right; margin-top: 10px;">
                <input type="file" name="file" id="fileupload" style="display: none;">
                <button id="btnAgregaImagen" style="margin-right: 15px;"></button>
                <button id="btnEliminaImagen"></button>
            </div>
        </div>
    </div>
</form>
