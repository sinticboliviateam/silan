<form id="form-datos-tacome">
    <div class="flex-modal-tacome">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="documentoEntrada" class="oLabel-left">Vale de entrada</label></div>
                <div class="div-dataInput-tab">
                    <input id="documentoEntrada" name="documentoEntrada" class="oInput campoRequerido" data-auto="" type="text" value="" required maxlength="45" tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="">
                    <label for="noPieza" class="oLabel-left">Número de pieza</label></div>
                <div class="div-dataInput-tab" style="">
                    <input id="noPieza" name="noPieza" class="oInput campoRequerido" type="number" value="" required tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaEntrada" class="oLabel-left">Fecha de la entrada</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaEntrada" name="fechaEntrada" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioEntrada" class="oLabel-left">Usuario</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioEntrada" name="usuarioEntrada" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="tabs-usuarios-botones" style="">
                <div id="guardaRegistro" class=""></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="listaPiezas" class="oLabel-left">Claves de las piezas</label></div>
                <div id="listaPiezas" class="div-oList"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="width: 100px;">
                    <label for="no_piezas" class="oLabel-left">No. de piezas</label></div>
                <div class="div-dataInput-tab">
                    <input id="no_piezas" name="no_piezas" class="oInput" style="text-align: right; font-weight: bold; width: 80px;" type="text" readonly value="0"></div>
            </div>
        </div>
    </div>
</form>