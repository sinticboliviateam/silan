<?php
session_start();
?>
<form id="form-tab-datos-polizas">
    <div class="flex-modal-polizas">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="claveProducto" class="oLabel-left">Clave del producto</label></div>
                <div class="div-dataInput-tab">
                    <input id="claveProducto" name="claveProducto" class="oInput campoRequerido" style="" data-auto="" type="text" value="" required maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="siguientePoliza" class="oLabel-left">Siguiente póliza</label></div>
                <div class="div-dataInput-tab">
                    <input id="siguientePoliza" name="siguientePoliza" class="oInput campoRequerido" style="text-align: center; font-weight: bold;" data-auto="" type="text" readonly value="" required></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="">
                    <label for="noPiezas" class="oLabel-left">Número de piezas</label></div>
                <div class="div-dataInput-tab" style="width: 97px;">
                    <input id="noPiezas" name="noPiezas" class="oInput campoRequerido" type="number" min="1" max="60" step="1" value="0" required tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="">
                    <label for="metros" class="oLabel-left">Metros por pieza</label></div>
                <div class="div-dataInput-tab" style="width: 97px;">
                    <input id="metros" name="metros" class="oInput campoRequerido" type="number" min="37" max="60" step="any" value="37.00" required tabindex="3"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="">
                    <label for="noTelar" class="oLabel-left">Número de telar</label></div>
                <div class="div-dataInput-tab" style="width: 97px;">
                    <input id="noTelar" name="noTelar" class="oInput campoRequerido" type="number" min="1" max="50" step="1" value="0" required tabindex="4"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="">
                    <label for="noCabos" class="oLabel-left">Número de cabos</label></div>
                <div class="div-dataInput-tab" style="width: 97px;">
                    <input id="noCabos" name="noCabos" class="oInput" type="text" value="" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaRegistro" class="oLabel-left">Fecha de registro</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaRegistro" name="fechaRegistro" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioRegistro" class="oLabel-left">Usuario</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioRegistro" name="usuarioRegistro" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="estatus" class="oLabel-left">Estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="estatus" name="estatus" class="oInput" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaEstatus" class="oLabel-left">Fecha estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaEstatus" name="fechaEstatus" class="oInput" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioEstatus" class="oLabel-left">Usuario estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioEstatus" name="usuarioEstatus" class="oInput" type="text" readonly value=""></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="tabs-usuarios-botones" style="font-size: 1.2em;">
                <div id="guardaPoliza" class="" style="display: none;"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <div class="baseImagen" style="margin-top: 10px;">
                <div id="div-imagen" class="imagenRegistro"></div>
            </div>
        </div>
    </div>
    <div class="flex-modal-left-tab" style="width: 95%;">
        <hr>
        <div class="tabs-botones-reportes" style="font-size: 1.2em; display: none;">
            <div class="btnImprimePoliza"></div>
            <div class="btnImprimeVale"></div>
            <div class="btnImprimeEtiqueta"></div>
            <?php if($_SESSION["datauser"]["tipoUsuario"] == 'A'){ ?>
                    <div class="btnCancelaPoliza"></div>
            <?php } ?>
        </div>
    </div>
</form>