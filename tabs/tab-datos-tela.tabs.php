<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
require_once '../include/combos.php';
?>
<form id="form-tab-datos-tela"> <!--  -->
    <div class="datosFormulasTelas">

    <div class="flex-modal-produccion">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="claveProducto" class="oLabel-left">Clave del producto</label></div>
                <div class="div-dataInput-tab">
                    <input id="claveProducto" name="claveProducto" class="oInput campoRequerido" data-auto="" type="text" value="" required maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="nombreProducto" class="oLabel-left">Nombre</label></div>
                <div class="div-dataInput-tab">
                    <input id="nombreProducto" name="nombreProducto" class="oInput" type="text" value="" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="nombreColor" class="oLabel-left">Color</label></div>
                <div class="div-dataInput-tab">
                    <input id="nombreColor" name="nombreColor" class="oInput" type="text" value="" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="width:180px;">
                    <label for="coloresProducto" class="oLabel-left">Número de colores</label></div>
                <div class="div-dataInput-tab">
                    <input id="coloresProducto" name="coloresProducto" class="oInput campoRequerido" type="number" min="1" max="15" step="1" value="0" required maxlength="2" tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="width: 200px;">
                    <label for="coloresOrillo" class="oLabel-left">Número de colores del orillo</label></div>
                <div class="div-dataInput-tab">
                    <input id="coloresOrillo" name="coloresOrillo" class="oInput campoRequerido" type="number" min="1" max="15" step="1" value="0" required maxlength="2" tabindex="3"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="urdimbreTrama" class="oLabel-left">Curso de urdimbre igual al de trama</label></div>
                <div class="div-dataInput-tab" style="width:50px;">
                    <input id="urdimbreTrama" name="urdimbreTrama" class="oInput" type="checkbox" tabindex="4"></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="claveAnterior" class="oLabel-left">Clave anterior</label></div>
                <div class="div-dataInput-tab">
                    <input id="claveAnterior" name="claveAnterior" class="oInput" type="text" value="" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="nombreMercado" class="oLabel-left">Nombre en el mercado</label></div>
                <div class="div-dataInput-tab">
                    <input id="nombreMercado" name="nombreMercado" class="oInput" type="text" value="" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="estatus" class="oLabel-left">Estatus</label></div>
                <div class="div-dataInput-tab">
                    <select id="estatus" name="estatus" class="oSelect-normal-tab" tabindex="4"><?php echo $combos->selectEstatusUsuario(3); ?></select></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaEstatus" class="oLabel-left">Fecha estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaEstatus" name="fechaEstatus" class="oInput" type="text" readonly value="<?php echo $combos->getDateFormat(date("d-m-Y h:i:s"), 1); ?>"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioEstatus" class="oLabel-left">Usuario estatus</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioEstatus" name="usuarioEstatus" class="oInput" type="text" readonly value="<?php echo utf8_encode($_SESSION["datauser"]["nombreCompleto"]); ?>"></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px; padding: 0px;">
            <div class="tabs-usuarios-botones">
                <div id="guardar-datos-formulas"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <div class="baseImagen" style="margin-top: 10px;">
                <div id="div-imagen" class="imagenRegistro"></div>
            </div>
        </div>
    </div>
    </div>
</form>
