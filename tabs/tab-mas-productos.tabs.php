<?php
/* 
 * 20180810 por Manuel Luna, Juan Marcelo Áviles
 * v1.0
 * 
*/

?>
<form id="form-tab-mas-productos">
    <div class="flex-modal-produccion" style="">
        <div class="flex-modal-left-tab" style="width: 450px; flex-direction: column;">
            <div id="func-ventanas" class="">
                <h3>Ventanas</h3>
            </div>
            <div class="cleared"></div>
            <div id="func-procesos" class="">
                <h3>Procesos</h3>
            </div>
            <div class="cleared"></div>
            <div id="func-reportes" class="">
                <h3>Reportes</h3>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 280px;"></div>
        <div class="flex-modal-left-tab" style="width: 365px;"> 
            <div class="tabs-productos-botones">
                <div id="guardar-mas-producto"></div>
                <div id="borrar-mas-producto"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
        </div>
    </div>
</form>
