<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
?>
<form id="form-tab-datos-tecnicos"> <!--   -->
    <div class="datosFormulasTelas">
    <div class="flex-modal-produccion">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="hilosFondo" class="oLabel-left">Hilos de Fondo</label></div>
                <div class="div-dataInput-tab">
                    <input id="hilosFondo" name="hilosFondo" class="oInput campoRequerido" type="number" min="1" max="9999" step="1" value="0" required tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="hilosOrillo" class="oLabel-left">Hilos de Orillo</label></div>
                <div class="div-dataInput-tab">
                    <input id="hilosOrillo" name="hilosOrillo" class="oInput campoRequerido" type="number" min="1" max="9999" step="1" value="0" required tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="hilosTotales" class="oLabel-left">Hilos Totales</label></div>
                <div class="div-dataInput-tab">
                    <input id="hilosTotales" name="hilosTotales" class="oInput campoRequerido" type="number" value="0" readonly></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="anchoTela" class="oLabel-left">Ancho de la Tela</label></div>
                <div class="div-dataInput-tab">
                    <input id="anchoTela" name="anchoTela" class="oInput campoRequerido" type="number" min="1" max="9999" step="1" value="0" required tabindex="3"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="noCabos" class="oLabel-left">Número de Cabos</label></div>
                <div class="div-dataInput-tab">
                    <input id="noCabos" name="noCabos" class="oInput campoRequerido" type="number" min="1" max="9999" step="1" value="0" required tabindex="4"></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="pasadas2p" class="oLabel-left">Pasada en 2"</label></div>
                <div class="div-dataInput-tab">
                    <input id="pasadas2p" name="pasadas2p" class="oInput campoRequerido" type="number" min="1" max="9999" step="1" value="0" required tabindex="5"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="noRepasoPeine" class="oLabel-left">No. de Repaso de Peine</label></div>
                <div class="div-dataInput-tab">
                    <input id="noRepasoPeine" name="noRepasoPeine" class="oInput campoRequerido upper" type="number" min="1" max="9999" step="1" value="0" maxlength="10" required tabindex="6"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="repasoPeine" class="oLabel-left">Repaso de Peine</label></div>
                <div class="div-dataInput-tab">
                    <input id="repasoPeine" name="repasoPeine" class="oInput campoRequerido upper" type="number" max="9999" step="1" value="0" maxlength="10" required tabindex="7"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="tipoDibujo" class="oLabel-left">Tipo de Dibujo</label></div>
                <div class="div-dataInput-tab">
                    <input id="tipoDibujo" name="tipoDibujo" class="oInput campoRequerido" type="text" value="" required maxlength="20" tabindex="8"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="composicion" class="oLabel-left">Composición</label></div>
                <div class="div-dataInput-tab">
                    <input id="composicion" name="composicion" class="oInput" style="width: 100%; margin: 2px 0 2px 0;" type="text" value="" readonly></div>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 365px; padding: 0px;">
            <div class="tabs-usuarios-botones">
                <div id="guardar-datos-tecnicos"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
        </div>
    </div>
    </div>
</form>
