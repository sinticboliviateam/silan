<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
?>
<form id="form-tab-curso-urdimbre">
    <div class="flex-modal-produccion">
        <!--*** COLUMNA: CURSO DE URDIMBRE ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab" style="display: flex;">
                <div class="div-oLabel-tab" style="width: 30%; margin-left: 58px;">
                    <label class="oLabel-left">Curso</label></div>
                <div class="tabs-usuarios-botones" style="justify-content:flex-end;">
                    <div class="agregar-curso"></div>
                </div>
            </div>
            <div class="tableRecords" style="width: 65%; height: 328px;">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col02-col01">Sec</th>
                            <th class="th-col02-col02">Dato</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-urdimbre">
                        <tr class="" id="">
                            <td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="block-info-tab" style="margin-top: 0.5em;">
                <div class="div-oLabel-tab" style="width: 110px; text-align: right;">
                    <label for="totalHilosUrimbre" class="oLabel-left">Total</label></div>
                <div class="div-dataInput-tab" style="">
                    <input id="totalHilosUrimbre" name="totalHilosUrimbre" class="oInput campoRequerido" style="width: 126px; text-align: right;" type="text" value="0" readonly></div>
            </div>
        </div>
        <!--*** COLUMNAS: PORCENTAJES Y CODIGOS ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="height: 23.5px; margin-left: 10px;">
                    <label class="oLabel-left">Porcentajes e hilos</label></div>
            </div>
            <div class="tableRecords" style="height: 228px;">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col03-col01">Color</th>
                            <th class="th-col03-col02">%</th>
                            <th class="th-col03-col03">Hilo</th>
                        </tr>
                    </thead>
                    <tbody id="tbl02-records-urdimbre">
                        <tr class="" id="">
                            <td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="block-info-tab" style="margin-top: 0.5em;">
                <div class="div-oLabel-tab" style="width: 70px; text-align: right;">
                    <label for="totalPorcentajeUrdimbre" class="oLabel-left">Total</label></div>
                <div class="div-dataInput-tab" style="">
                    <input id="totalPorcentajeUrdimbre" name="totalPorcentajeUrdimbre" class="oInput" style="width: 100px; text-align: right;" type="text" value="0" readonly></div>
            </div>
        </div>
        <!--*** COLUMNAS: SELECCION DE COLORES ***-->
        <div class="flex-modal-left-tab" style="width: 365px;">
            <!--*** LISTA DE BOTONES ***-->
            <div class="tabs-usuarios-botones">
                <div id="guardar-curso-urdimbre" class="guarda-datos-curso"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <!--*** SELECCION DE COLORES ***-->
            <div class="block-info-tab" style="margin-bottom: 1em;">
                <div class="div-oLabel-tab">
                    <label class="oLabel-left">Selección</label></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U65" class="oLabel-left">Color A</label></div>
                <div class="div-dataInput-tab">
                    <input id="U65" name="U65" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U66" class="oLabel-left">Color B</label></div>
                <div class="div-dataInput-tab">
                    <input id="U66" name="U66" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="2"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U67" class="oLabel-left">Color C</label></div>
                <div class="div-dataInput-tab">
                    <input id="U67" name="U67" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="3"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U68" class="oLabel-left">Color D</label></div>
                <div class="div-dataInput-tab">
                    <input id="U68" name="U68" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="4"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U69" class="oLabel-left">Color E</label></div>
                <div class="div-dataInput-tab">
                    <input id="U69" name="U69" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="5"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U70" class="oLabel-left">Color F</label></div>
                <div class="div-dataInput-tab">
                    <input id="U70" name="U70" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="6"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U71" class="oLabel-left">Color G</label></div>
                <div class="div-dataInput-tab">
                    <input id="U71" name="U71" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="7"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U72" class="oLabel-left">Color H</label></div>
                <div class="div-dataInput-tab">
                    <input id="U72" name="U72" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="8"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U73" class="oLabel-left">Color I</label></div>
                <div class="div-dataInput-tab">
                    <input id="U73" name="U73" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="9"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U74" class="oLabel-left">Color J</label></div>
                <div class="div-dataInput-tab">
                    <input id="U74" name="U74" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U75" class="oLabel-left">Color K</label></div>
                <div class="div-dataInput-tab">
                    <input id="U75" name="U75" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U76" class="oLabel-left">Color L</label></div>
                <div class="div-dataInput-tab">
                    <input id="U76" name="U76" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U77" class="oLabel-left">Color M</label></div>
                <div class="div-dataInput-tab">
                    <input id="U77" name="U77" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U78" class="oLabel-left">Color N</label></div>
                <div class="div-dataInput-tab">
                    <input id="U78" name="U78" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
            <div class="block-info-tab div-autocomplete">
                <div class="div-oLabel-tab02">
                    <label for="U79" class="oLabel-left">Color O</label></div>
                <div class="div-dataInput-tab">
                    <input id="U79" name="U79" class="oInput campoRequerido autocomplete-urdimbre" type="text" value="" maxlength="60" tabindex="10"></div>
            </div>
        </div>
    </div>
</form>
