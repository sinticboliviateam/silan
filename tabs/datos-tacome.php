<form id="form-datos-tacome">
    <div class="flex-modal-tacome">
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="clavePieza" class="oLabel-left">Clave de la pieza</label></div>
                <div class="div-dataInput-tab">
                    <input id="clavePieza" name="clavePieza" class="oInput campoRequerido" data-auto="" type="text" value="" required maxlength="60" tabindex="1"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="width: 120px;">
                    <label for="metros" class="oLabel-left">Metros por pieza</label></div>
                <div class="div-dataInput-tab" style="width: 75px; text-align: right;">
                    <input id="metros" name="metros" class="oInput campoRequerido" type="number" min="1" max="100" step="any" value="37.00" required tabindex="2"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab" style="width: 120px;">
                    <label for="kilos" class="oLabel-left">Kilos</label></div>
                <div class="div-dataInput-tab" style="width: 75px;">
                    <input id="kilos" name="kilos" class="oInput campoRequerido" type="number" min="0.500" max="50" step="any" value="1" required tabindex="3"></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="fechaTacome" class="oLabel-left">Fecha tacome</label></div>
                <div class="div-dataInput-tab">
                    <input id="fechaTacome" name="fechaTacome" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>
            <div class="block-info-tab">
                <div class="div-oLabel-tab">
                    <label for="usuarioTacome" class="oLabel-left">Usuario</label></div>
                <div class="div-dataInput-tab">
                    <input id="usuarioTacome" name="usuarioTacome" class="oInput" style="text-align: center; font-weight: bold;" type="text" readonly value=""></div>
            </div>            
        </div>
        <div class="flex-modal-left-tab" style="width: 365px;">
            <div class="tabs-usuarios-botones" style="">
                <div id="guardaRegistro" class=""></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
            <div class="baseImagen" style="margin-top: 10px;">
                <div id="div-imagen" class="imagenRegistro"></div>
            </div>
        </div>
    </div>
</form>