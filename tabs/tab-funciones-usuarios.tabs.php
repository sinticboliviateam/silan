<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/

?>
<form id="form-tab-funciones-usuarios">
    <div class="flex-modal-produccion" style="">
        <div class="flex-modal-left-tab" style="width: 450px; flex-direction: column;">
            <div id="func-ventanas" class="">
                <h3>Ventanas</h3>
            </div>
            <div class="cleared"></div>
            <div id="func-procesos" class="">
                <h3>Procesos</h3>
            </div>
            <div class="cleared"></div>
            <div id="func-reportes" class="">
                <h3>Reportes</h3>
            </div>
        </div>
        <div class="flex-modal-left-tab" style="width: 280px;"></div>
        <div class="flex-modal-left-tab" style="width: 365px;"> 
            <div class="tabs-usuarios-botones">
                <div id="guardar-funciones-usuario"></div>
                <div id="borrar-funciones-usuario"></div>
                <div class="cerrar-tabs"></div>
            </div>
            <hr>
        </div>
    </div>
</form>
