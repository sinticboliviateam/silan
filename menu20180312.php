<!--/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/-->
<form class="body-menu" style="height: 45px; background-color: #333333;">
    <!--<div style="background-color: #333333; height: 45px;">-->
        <div class="productname"><h2>SiLan</h2></div>
        <div class="menubar">
            <nav>
                <ul id="menu">
                    <li><a href="#" class="" data-menu="01000000" style="padding-bottom: 0px;">Inventarios</a>
                        <ul class="children">
                            <li><a href="#" class="" onclick="" data-menu="01010000">Entradas H</a></li>
                            <li><a href="#" class="" data-menu="01020000">Salidas H</a></li>
                            <li><a href="#" class="" data-menu="01030000">Kardex H</a></li>
                            <li><a href="#" class="" data-menu="01040000">Rep 1</a></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-menu="02000000" style="padding-bottom: 0px;">Producción</a>
                        <ul class="children">
                            <li><a href="#" class="" onclick="" data-menu="02010000">Fórmulas telas</a></li>
                            <li><a href="#" class="" data-menu="02020000">Pólizas PT</a></li>
                            <li><a href="#" class="" data-menu="02030000">Entradas PT</a></li>
                            <li><a href="#" class="" data-menu="02040000">Tacome</a></li>
                            <li><a href="#" class="" data-menu="02050000">Salidas PT</a></li>
                            <li><a href="#" class="" data-menu="02060000">Imp. salidas</a></li>
                        </ul>
                    </li>
                    <li><a href="#" class="" data-menu="03000000" style="padding-bottom: 0px;">Sistemas</a>
                        <ul class="children">
                            <li><a href="#" class="" data-menu="03010000">Generales</a></li>
                            <li><a href="#" class="" data-menu="03020000">Corte mes</a></li>
                            <li><a href="#" class="" data-menu="03030000">Usuarios</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <nav style="width: 260px;">
            <ul id="menu-2" style="width: 100%;">
                <li style="width: 100%;"><a href="#" class="" data-menu="04000000" style="padding-bottom: 0px;"><?php echo substr(utf8_encode($_SESSION["datauser"]["nombreCompleto"]),0,26); ?></a>
                    <ul class="children">
                        <li><a href="#" class="" data-menu="04010000">Salir</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    <!--</div>-->
</form>