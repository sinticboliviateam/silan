<?php
/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-30
 * SiLan v1.0
*/
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('html_errors', false);
ini_set('display_startup_errors', 1);

if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
require_once './include/class/menu.class.php';
require_once './include/combos.php';
$autPerimisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02080100";
$permisos = $autPerimisos->regresaPermisosUsuario();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Ordenes de compra</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/ordenesCompra.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/ordenesCompra.class.js"></script>
        <script src="./js/class/ordenesCompraValidate.class.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                $setmenu.getLinksUser();
                $("#fechaOrden").datepicker($.datepicker.regional[ "es" ] );
                //$silan_ordenesComp.widgetEventOrdenesComp();
                $silan_ordenesComp._initform();
                $silan_ordenesComp.buscarRegistro();
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Órdenes de compra</h3>
                    <input id="filter" name="filter" type="text" placeholder="Escribe aquí" class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <button id="btnNuevaOrden" class=""></button>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col06-col01">Orden compra</th>
                            <th class="th-col06-col02">fecha</th>
                            <th class="th-col06-col03">#destino</th>
                            <th class="th-col06-col03">Destino</th>

                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogOrdenCompra" style="display: none;">
                <div id="tabs-ordenComp" style="width: 95%; height: auto; margin: 0 auto;">
                        <form id="form-ordenComp" enctype="multipart/form-data" method="post">
                        	<input type="hidden" id="idOrdenCompra" name="idOrdenCompra" value="" />
                            <div class="flex-modal-ordenesComp">
                                <div class="flex-modal-left-tab" style="width: 365px;">
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="noOrdenCompra" class="oLabel-left">No. orden de compra</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="noOrdenCompra" name="noOrdenCompra" class="oInput" type="text" value="" 
                                            	required maxlength="60" tabindex="1"></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="fechaOrden" class="oLabel-left">Fecha</label></div>
                                        <div class="div-dataInput-tab">
                                            <input id="fechaOrden" name="fechaOrden" class="oInput" type="text" tabindex="2" readonly value=""></div>
                                    </div>
                                    <div class="block-info-tab">
                                        <div class="div-oLabel-tab">
                                            <label for="destinoOrden" class="oLabel-left">Destino</label></div>
                                        <div class="div-dataInput-tab">
                                            <select id="destinoOrden" name="destinoOrden" class="oSelect-normal-tab" tabindex="3"><?php echo $combos->selectDestinosOC(); ?></select></div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="ordenes-compra-botones" style="clear:both;width:100%;">
                                        <div id="guardar-ordenes-compra" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-disk"></span><span class="ui-button-icon-space"> </span>Guardar</div>
                                        <div id="borrar-ordenes-compra" class="ui-button ui-corner-all ui-widget"><span class="ui-button-icon ui-icon ui-icon-trash"></span><span class="ui-button-icon-space"> </span>Borrar</div>
                                        <div id="cerrar-ordenes-compra" class="cerrar-tabs ui-button ui-corner-all ui-widget">
                                        	<span class="ui-button-icon ui-icon ui-icon-arrowreturnthick-1-w"></span>
                                        	<span class="ui-button-icon-space"> </span>
                                        	Regresar
                                        </div>
                                        <a href="javascript:;" id="btn-imprimir-csv" class="ui-button ui-corner-all ui-widget" style="display:none;">
                                        	<span class="ui-button-icon ui-icon ui-icon-print"></span>
                                        	Imprimir CSV
                                        </a>
							</div>
                        </form>
 
                </div>
            </div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>