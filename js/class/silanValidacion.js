function rsValidacion(){
    this.zonas = function(){
        $zonasTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtNombre : { required : true, minlength : 1, maxlength : 25 },
                            txtBreve : { required : true, minlength : 1, maxlength : 10 },
                            numOrden : { required : true, number : true, minlength : 1, min : 1, maxlength : 4 }
                        },
                        messages: {
                        }
                    });
    };

    this.empresas = function(){
        $empresasTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtNombre : { required : true, minlength : 2, maxlength : 80 },
                            areaDomicilio : { required : true, minlength : 10, maxlength : 100 },
                            txtContacto : { required : true, minlength : 10, maxlength : 100 },
                            txtTelefonos : { required : true, minlength : 10, maxlength : 100 },
                            txtCelulares : { required : true, minlength : 10, maxlength : 100 },
                            txtCorreos : { required : true, minlength : 10, maxlength : 100 }
                        }
                    });
    };

    this.areas = function(){
        $areasTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtNombre : { required : true, minlength : 2, maxlength : 200 },
                            txtInfoAdicional : { required : true, minlength : 10, maxlength : 200 },
                            numOrden : { required : false, number : true, minlength : 1, min : 1, maxlength : 4 }
                        }
                    });
    };

    this.ubicaciones = function(){
        $ubicacionesTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtNombre : { required : true, minlength : 2, maxlength : 50 },
                            txtObservaciones : { required : true, minlength : 10, maxlength : 300 },
                            numOrden : { required : false, number : true, minlength : 1, min : 1, maxlength : 4 }
                        }
                    });
    };

    this.sensores = function(){
        $sensoresTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtDescripcion: { required : true, minlength : 1, maxlength : 50 },
                            numFrecuencia: { required : true, number : true, minlength : 1, min : 0 },
                            numValorMaximo: { required : false, number : true },
                            numValorMinimo : { required : false, number : true },
                            numValorUmbral : { required : false, number : true },
                            numSiguienteAlerta : { required : true, number : true, minlength : 1, min : 0 },
                            txtCast : { required : false, minlength : 1, maxlength : 50 }
                        },
                        messages: {
                        }
                    });
        $sensoresTabConfiguracion = $("#tabConfiguracion").validate({
                        rules: {
                            txtURLoIP: { required : true, minlength : 7, maxlength : 100 },
                            txtPuerto : { required : true, minlength : 1, maxlength : 50, min : 1 },
                            numID : { required : true, number : true, min : 1, minlength : 1, maxlength : 4 },
                            txtReferencia : { required : true, minlength : 1, maxlength : 30 },
                            txtURLSensor: { required : false, minlength : 7, maxlength : 100 },
                            txtURLVideo: { required : false, minlength : 7, maxlength : 100 }
                        },
                        messages: {
                        }
                    });
    };   

    this.unidades = function(){
        $unidadesTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtUnidad : { required : true, minlength : 1, maxlength : 15 },
                            txtSigla : { required : true, minlength : 1, maxlength : 10 },
                            numOrden : { required : false, number : true, minlength : 1, min : 1, maxlength : 4 }
                        }
                    });
    };

    this.doctosExternos = function(){
        $doctosExternosTabDatos = $("#tabDatos").validate({
                        rules: {
                            txtfechaDocto : { required : true, date : true },
                            txtNumDocumento : { required : true, minlength : 3, maxlength : 20 },
                            numCantidad : { required : true, number : true }
                        }
                    });
    };

    this.usuarios = function(){
        $usuariosTabDatos = $("#div-edition").validate({
                        rules: {
                            txtNombreCompleto : { required : true, minlength : 3, maxlength : 100 },
                            txtUsuario : { required : true, minlength : 3, maxlength : 45 },
                            pwd : { required : true, minlength : 3, maxlength : 45 },
                            pwd2 : { required : true, equalTo : "#pwd" },
                            txtRFC : { required : true, minlength : 10, maxlength : 15 }
                        }
                    });
    };
}
$rsValidaTabs = new rsValidacion();

