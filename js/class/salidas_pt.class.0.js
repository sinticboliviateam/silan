/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna
 * MEXICO, 2017
 * SiLan v1.0
 * Modificaciones: 2017-03-17
*/

var Salidas_pt = {
    clave_pieza: null,

    ready: function() {
        Salidas_pt.init();
        Clave_Pieza.init();
        Kilos.init();
        SalidaPieza.init();
    },

    init: function(){
        $("#modal-salidas_pt").dialog({
            modal: false,
            title: "Salidas PT",
            dialogClass: 'hide-close',
            width: 1000,
            height: 490,
            resizable: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
    },

    actualizaPieza: function() {
        var clave_pieza =  $("#" + Clave_Pieza.id).val();
        if(clave_pieza != "" && Salidas_pt.clave_pieza != clave_pieza) {
            //console.log('actualizaProducto: ' + clave_producto);
            Salidas_pt.clave_pieza = clave_pieza;
            Salidas_pt.obtieneInformacionPieza(clave_pieza);

        }
    },

    obtieneInformacionPieza: function(clave_pieza) {
        //console.log('obtiene information del producto ' + keyProduct);
        $("#imagenProducto").empty();
        $.post('./servicios/salidas_pt.php',
            {data:{widget:"informacionPieza", clave_pieza:clave_pieza}}, function(data){
                if (data.metros != "") {
                    $("#" + Metros.id).val(data.metros);
                    if (data.img != "")  {
                        $("#imagenProducto").append("<img src='" + data.img + "' width='530' height='380' />");
                    } else {
                        $("#imagenProducto").text("Producto: " + $("#clave_producto").val());
                    }
                } else {
                    Salidas_pt.clave_pieza = null;
                }
                if (data.error != "") {
                    alert(data.error);
                }
            },"json").fail(function(data){
                alert("ERROR.-> "+data);
            }
        );
    },

    valida: function() {
        var valido = true;
        var focus = false;

        if (!Clave_Pieza.valida()) {
            if(!focus) focus = $("#" + Clave_Pieza.id);
            valido = false;
        }

        if (!Metros.valida()) {
            if(!focus) focus = $("#" + Metros.id);
            valido = false;
        }

        if (!Kilos.valida()) {
            if(!focus) focus = $("#" + Kilos.id);
            valido = false;
        }

        if (focus) focus.focus();
        return valido;
    }

}

var Clave_Pieza = {
    id: 'clave_pieza',
    init: function() {
        $("#" + Clave_Pieza.id).empty();
        $.post('./servicios/salidas_pt.php',
            {data:{widget:"listaPiezasSinKg"}}, function(data){
                if (data.error == "") {
                    var list = data.list.split(',');
                    $("#" + Clave_Pieza.id).autocomplete({
                        source: list,
                        select: function(event, ui){
                                    $("#" + Clave_Pieza.id).val(ui.item.value);
                                    //console.log('select: ' + $("#clave_producto").val());
                                    Salidas_pt.actualizaPieza();
                                    $("#" + Metros.id).focus();
                                    event.preventDefault();
                                },
                        change: function(){
                                    //console.log('change: ' + $("#clave_producto").val());
                                    Salidas_pt.actualizaPieza();
                                    $("#" + Metros.id).focus();
                                }
                    });
                } else {
                    alert(data.error);
                }
            },"json").fail(function(data){
                console.log(data);
                alert("ERROR.-> "+data);
            }
        );
    },
    valida: function() {
        if (Salidas_pt.clave_pieza == null) {
            alert('Clave de pieza inválida');
            return false;            
        }
        return true;
    },
    val: function() {
        return $("#" + Clave_Pieza.id).val();
    }
};

var Metros = {
    id: 'metros',
    init: function() {},
    valida: function() {
        return SiLan.validacionNumero(Metros.id, 35, 60, 'metros', 2);
    },
    val: function() {
        return $("#" + Metros.id).val();
    }

};

var Kilos = {
    id: 'kilos',
    init: function() {},
    valida: function() {
        return SiLan.validacionNumero(Kilos.id, .001, 80, 'kilos', 3);
    },
    val: function() {
        return $("#" + Kilos.id).val();
    }

};

var SalidaPieza = {
    id: "salidaPieza",
    init: function(){
        $("#" + SalidaPieza.id).on("click", function(e){
            if (!Salidas_pt.valida()) return;

            //console.log('Guarda');
            var dataKey = {
                widget:"salidaPieza",
                clave_pieza: Clave_Pieza.val(),
                metros: Metros.val(),
                kilos: Kilos.val()
            };
            //console.log(dataKey);

            $.post('./servicios/salidas_pt.php',
            {data: dataKey}, function(data){
                if (data.poliza != "")  {
                    alert("Se actualizó la pieza: " + Clave_Pieza.val());
                    $(":input").val("");
                    $("#imagenProducto").empty();
                    Clave_Pieza.init();
                    $("#clave_pieza").focus();
                }
                if (data.error != "") {
                    console.log(data);
                    alert(data.error);
                }
            },"json").fail(function(data){
                console.log(data);
                alert("ERROR.-> "+data);
            });

        });
    },

}