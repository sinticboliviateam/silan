/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
var rSisTacomeValidate = {
    _init : function(){
        this.formValidateTacome();
    },
    formValidateTacome : function(){
        $("form#form-datos-tacome").validate({
            onfocusout: function(element) {
                if($(element).valid())
                    rSisTacome.parametros[$(element).attr("name")] = $(element).val();
            },
            rules : {
                "clavePieza" : { required: true },
                "metros" : { required: true, range : [1.00,100.00]},
                "kilos" : { required: true, range : [0.500,50.000]}
            },
            messages: {
                clavePieza : { required : "Este campo es obligatorio" },
                metros : { required : "Este campo es obligatorio", range : "Rango válido entre 37 hasta 60" },
                kilos : { required : "Este campo es obligatorio", range : "Rango válido entre 1 hasta 80" }
            },
            submitHandler: function(form)
            {
            	rSisTacomeValidate.obtenValores(this.currentElements);
                
                $silan_msg.showmessagecondition("¿Se encuentra preparada la impresora?",
                ()=>
            	{
            		rSisTacomeValidate.submitTacome();
                    //alert('sigue la impresión');
                }, 
                () => 
                {
                	console.log('cerrando');
                });
                /*
                setTimeout(function()
                {
                        
                }, 125);
                */
            }
        });
    },
    obtenValores : function(elementos)
    {
        for(var i=0; i<=(elementos.length)-1; ++i)
        {
            rSisTacome.parametros[elementos[i].name] = elementos[i].value; 
        }
        rSisTacome.parametros.idPiezas = rSisAutocomplete.key;
        rSisTacome.parametros.evento = "insertaTacome";
       
    },
    submitTacome : function()
    {
    	//var params = 'data=' + JSON.stringify(rSisTacome.parametros);
    	//console.log(rSisTacome.parametros);
    	var params = {'data': rSisTacome.parametros};
        //jQuery.post('./include/tacome.silan.php', params, function(data)
    	jQuery.post('index.php?mod=tacome&task=Guardar', params, function(data)
        {
    		/*
            if(data.error !== 0)
                $silan_msg.showmessage(data.msg || data.mensaje, 'closemodal', data.title);
            else*/
            	$silan_msg.showmessage('Valores actualizado correctamente', 'closemodal', 'VALORES ACTUALIZADOS');
        }, 'json').fail(function(data)
        {
        	alert(data.responseJSON.error || 'Error desconocido al guardar tacome');
        }).always(function(data)
        {
            //location.reload();
        });
    }
};
