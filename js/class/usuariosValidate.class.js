/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
function usuariosValidate(){
    var dataform = {
        event : "usuarios",
        functions : new Array()
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    
    this.clearobjvar = function(){
        dataform = {
            event : "usuarios"
        };
    };
    
    this.formValidateDatos = function(){
        $("#form-tab-datos-usuarios").validate({
            onfocusout: function(element) { 
                var context = $(element).val();
                if($(element).attr("name") === "usuario")
                    $(element).val(context.replace(/\s/g,""));
                ($(element).valid())
                    $silan_validUsuarios.setobjvar($(element).attr("name"),$(element).val());
            },
            rules : {
                repeatPassword : { required : true, equalTo : "#password" }
            },
            messages: {
                nombreCompleto : { required : "Este campo es obligatorio" },
                usuario : { required : "Este campo es obligatorio" },
                password : { required : "Este campo es obligatorio" },
                repeatPassword : { required : "Este campo es obligatorio" },
                rfc : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form){
                if(!$silan_validUsuarios.validFieldUserdat()){
                    $silan_msg.showmessage("Existen datos obligatorios dentro de la pestaña Datos.",
                                            "closemodal","Catálogo de usuarios");
                    $("#tabs-usuarios").tabs("option", "active", 0);
                    return;
                }
                $.post('./include/usuarios.silan.php',
                    {data:$silan_validUsuarios.getobjvar()}, function(data){
                        (data.error !== 0)
                            ? $silan_msg.showmessage(data.msg,data.action,data.title)
                            : location.reload();
                    },"json").fail(function(data){
                        alert("ERROR 0 formValidate.-> "+data);
                    });
            }
        });
    };
    
    this.validFieldUserdat = function(){
        if($("#nombreCompleto").val() === "" ||
            $("#usuario").val() === "" ||
            $("#password").val() === "" || 
            $("#rfc").val() === ""){
            return false;
        }
        $silan_validUsuarios.setobjvar("nombreCompleto", $("#nombreCompleto").val());
        $silan_validUsuarios.setobjvar("usuario", $("#usuario").val());
        $silan_validUsuarios.setobjvar("password", $("#password").val());
        $silan_validUsuarios.setobjvar("rfc", $("#rfc").val());
        $silan_validUsuarios.setobjvar("tipoUsuario", $("#tipoUsuario").val());
        $silan_validUsuarios.setobjvar("puesto", $("#puesto").val());
        $silan_validUsuarios.setobjvar("horario", $("#horario").val());
        $silan_validUsuarios.setobjvar("correos", $("#correos").val());
        $silan_validUsuarios.setobjvar("celulares", $("#celulares").val());
        $silan_validUsuarios.setobjvar("telefonos", $("#telefonos").val());
        $silan_validUsuarios.setobjvar("ultimoAcceso", $("#ultimoAcceso").val());
        $silan_validUsuarios.setobjvar("estatus", $("#estatus").val());
        $silan_validUsuarios.setobjvar("fechaEstatus", $("#fechaEstatus").val());
        $silan_validUsuarios.setobjvar("usuarioEstatus", $("#usuarioEstatus").val());
        $silan_validUsuarios.setobjvar("imagen", "");
        var checkFunctions = $("form#form-tab-funciones-usuarios :checkbox:checked");
        if(checkFunctions.length === 0)
            return false;
        var permisos = $silan_validUsuarios.validFieldUserfun(checkFunctions);
        return true;
    };
    
    this.validFieldUserfun = function(functions){
        var permisos = new Array();
        var func = ""; var value = "";
        functions.each(function(idx, ele){
            if(idx === 0) func = (ele.name).substr(5,3);
            if((ele.name).substr(5,3) !== func){
                //if(value.substr(0,1) === 'L')
                if(value.length >= 0)
                    permisos.push({item : func, val : value});
                func = (ele.name).substr(5,3);
                value = "";
            }
            value+= ele.value;
        });
        //if(value.substr(0,1) === 'L')
        if(value.length >= 0)
            permisos.push({item : func, val : value});
        $silan_validUsuarios.setobjvar("functions",permisos);
    }
}
$silan_validUsuarios = new usuariosValidate();