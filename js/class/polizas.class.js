/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna, Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
rSisPolizas = {
    parametros : {},
    read : function(){
        this.ventanaPrincipal._init();
        this.tabsPolizas.cargaCampos();
    },
    ventanaPrincipal : {
        _init : function(){
            this.campoBuscar();
            this.botones();
            this.tablaLlenado();
        },
        campoBuscar : function(){
            $("input#filter").keyup(function(){
                $("tr.renglonesGrid").remove();
                $("span#status-busca").html("Buscando...");
                rSisPolizas.ventanaPrincipal.tablaLlenado();
                rSisPolizas.ventanaPrincipal.muestraPoliza();
            });
        },
        filtro : function(){
            return $("input#filter").val();
        },
        botones : function(){
            $("div#btnNuevaPoliza")
                .button({ 
                    icons: {
                        primary: "ui-icon-disk"
                    },
                    label: "Nuevo"
                })
                .on("click",function(e){
                    e.preventDefault();
                    rSisPolizas.parametros.evento = "nuevaPoliza";
                    rSisPolizas.tabsPolizas._init();
                    rSisPolizas.tabsPolizas.muestraDialog();
                    $("div#guardaPoliza").show();
                });
        },
        tablaLlenado : function(){
            $.post('./include/tablasSistemas.silan.php',
                {data: {opt:"polizas", filter:rSisPolizas.ventanaPrincipal.filtro()}}, function(data){
                    $("tbody#tbl01-records-busqueda").append(data);
                    $("span#status-busca").html("");
                }).always(function(data){
                    rSisPolizas.ventanaPrincipal.tablaEventos();
                });
        },
        tablaEventos : function(){
            $("tr.renglonesGrid").on("click",function(e){
                e.preventDefault();
                rSisPolizas.parametros.evento = "editaPoliza";
                rSisPolizas.parametros.key = this.id;
                $.post('./include/polizas.silan.php',
                    {data:rSisPolizas.parametros}, function(data){
                        if(data.error !== 0)
                            $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json").fail(function(data){
                        alert("ERROR 0 tablaEventos: renglonesGrid.-> "+data);
                    }).always(function(data){
                        rSisPolizas.tabsPolizas._init();
                        rSisPolizas.ventanaPrincipal.muestraPoliza(data.data);
                    });
            });
        },
        muestraPoliza : function(data){
            $("input#claveProducto").val(data.claveProducto);
            $("input#siguientePoliza").val(data.noPoliza);
            $("input#noPiezas").val(data.noPiezas);
            $("input#metros").val(data.metros);
            $("input#noTelar").val(data.noTelar);
            $("input#noCabos").val(data.noCabos);
            $("input#fechaRegistro").val(data.fechaRegistro);
            $("input#usuarioRegistro").val(data.usuarioRegistro);
            $("input#estatus").val(data.nombreEstatus);
            $("input#fechaEstatus").val(data.fechaEstatus);
            $("input#usuarioEstatus").val(data.usuarioEstatus);
            if(data.img !== "")
                $('#div-imagen').css({"background":"url('." + data.img + "') center no-repeat", "background-size": "contain"});
            $("form#form-tab-datos-polizas").find(":input").attr("readonly","readonly");
            $("div.tabs-botones-reportes").show();
            $("div#guardaPoliza").hide();
            rSisPolizas.tabsPolizas.muestraDialog();
        }
    },
    tabsPolizas : {
        _init : function(){
            this.cargaDialog();
        },
        cargaCampos : function(){
//            $("div#tabs-datos-poliza").load("./tabs/tab-datos-poliza.tabs.php", function(){
            $("div#dialogPolizas").load("./tabs/tab-datos-poliza.tabs.php", function(){
                rSisPolizas.tabsPolizas.botones();
                rSisPolizas.tabsPolizas.datosRegistro();
                rSisPolizas.tabsPolizas.eventoAutocomplete();
                rSisPolizasValidate._init();
            });
        },
        cargaDialog : function(){
            $("div#dialogPolizas").dialog({
                modal: true,
                autoOpen: false,
                title: "Generación de pólizas",
                dialogClass: 'hide-close',
                width: 750,
//                maxHeight: 800,
                resizable: false,
                position: { my: "center top", at: "top+52px", of: window },
                draggable: false,
                closeOnEscape: true,
                beforeClose: function( event, ui ){
                    rSisPolizas.parametros = {};
                },
                close: function(event, ui){
                    location.reload();
                }
            });
        },
        muestraDialog : function(){
            $("div#dialogPolizas").dialog({
                autoOpen: true,
                open: function(event,ui){
//                    rSisPolizas.tabsPolizas.activaTabs();
                    $("input#claveProducto").focus();
//                    $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
                }
            });
            $(document).find(".ui-widget, .ui-button").css({"font-size":"0.75em"});
        },
//        activaTabs : function(){
//            $("div#tabs-polizas").tabs({
//                activate : function( event, ui ) {
//                    if(ui.newPanel.index() === 1)
//                        $("input#claveProducto").focus();
//                }
//            });
//        },
        botones : function(){
            $("div#guardaPoliza")
                .button({ 
                    icons: { primary: "ui-icon-disk" },
                    label: "Guardar"
                })
                .on("click",function(e){
                    e.preventDefault();
                    $("form#form-tab-datos-polizas").submit();
                });
            
            $("div.cerrar-tabs")
                .button({ 
                    icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                    label: "Regresar"
                })
                .on("click",function(e){
                    e.preventDefault();
                    $("div#dialogPolizas").dialog("close");
                });
            
            $("div.btnImprimePoliza")
                .button({ 
                    icons: { primary: "ui-icon-print" },
                    label: "Imp. Póliza"
                })
                .on("click",function(e){
                    e.preventDefault();
                    $.get('./include/polizasPDF.silan.php',
                        {keyPoliza:rSisPolizas.parametros.key}, function(data){
                            window.open(data, '_blank');
                        });
                });
            $("div.btnImprimeVale")
                .button({ 
                    icons: { primary: "ui-icon-print" },
                    label: "Imp. Vale"
                })
                .on("click",function(e){
                    e.preventDefault();
                    $.get('./include/valePolizasPDF.silan.php',
                        {keyPoliza:rSisPolizas.parametros.key}, function(data){
                            window.open(data, '_blank');
                        });
                });
            $("div.btnImprimeEtiqueta")
                .button({ 
                    icons: { primary: "ui-icon-print" },
                    label: "Imp. Etiqueta"
                })
                .on("click",function(e){
                    e.preventDefault();
                    $.get('./include/etiquetasZPLII.silan.php',
                        {keyPoliza:rSisPolizas.parametros.key});
                });
            $("div.btnCancelaPoliza")
                .button({ 
                    icons: { primary: "ui-icon-close" },
                    label: "Cancelar"
                })
                .on("click",function(e){
                    e.preventDefault();
                    rSisPolizas.parametros.evento = "cancelaPoliza";
                    $.post('./include/polizas.silan.php',
                        {data:rSisPolizas.parametros}, function(data){
                            if(data.error !== 0)
                                $silan_msg.showmessage(data.msg,data.action,data.title);
                        },"json").fail(function(data){
                            alert("ERROR 0 tablaEventos: renglonesGrid.-> "+data);
                        }).always(function(data){
                            location.reload();
                        });
                    });
        },
        datosRegistro : function(){
            $.post('./include/polizas.silan.php',
                {data: {evento:"usuarioRegistro"}}, function(data){
                $("input#fechaRegistro").prop("value",data.fechaRegistro);
                $("input#usuarioRegistro").prop("value",data.usuarioRegistro);
                $("input#estatus").prop("value",data.estatus);
                $("input#fechaEstatus").prop("value",data.fechaEstatus);
                $("input#usuarioEstatus").prop("value",data.usuarioEstatus);
            },"json");
        },
        eventoAutocomplete : function(){
            $("input#claveProducto").focusin(function(){
                rSisAutocomplete.polizas(this.id);
            });
        }
    }
};