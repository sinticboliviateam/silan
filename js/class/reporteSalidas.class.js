/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna / Francisco Gonzalez
 * MEXICO, 2017
 * SiLan v1.0
*/
reporteSalida = {
    idSalida : 0,
    read : function(){
        reporteSalida.init();
    },
    init : function(){
        $.post('./include/tablasSistemas.silan.php',
            {data: {opt:"reporteSalidas"}}, function(data){
                $("tbody#tbl01-records-busqueda").append(data);
            }).always(function(data){
                reporteSalida.tablaEventos();
            });
    },
    tablaEventos : function(){
        $("tr.renglonesGrid").on("click",function(e){
            e.preventDefault();
            reporteSalida.idSalida = this.id;
            $.get('./include/salidasPDF.silan.php',
                {keyRecord:reporteSalida.idSalida}, function(data){
                    window.open(data, '_blank');
                });
        });
    }
};