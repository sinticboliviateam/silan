/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna, Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/

var SiLan = {

    countDecimals:function (value) {
        if(isNaN(value)) return 0;
        if(Math.floor(value) === value) return 0;
        return value.toString().split(".")[1].length || 0; 
    },

    validacionNumero: function(valor, inferior, superior, deQue, decimales) { //'no_piezas', 1, 60, 'piezas', 0
        var valido = true;
        var cantidad = Number(valor);
        if (isNaN(cantidad) || cantidad < inferior || cantidad > superior) {
            valido = false;
            alert('Número de ' + deQue + ' inválido (' + inferior + ' a ' + superior + ')');
        }
        if (SiLan.countDecimals(cantidad) > decimales) {
            valido = false;
            alert('Máximo de decimales en ' + deQue + ' es ' + decimales);
        }
        return valido;
    },

    validacionUnico: function(valor, campo, deQue, esUnicoFunc, noEsUnicoFunc) {
        var valido = true;
        var dataKey = {
                widget: 'validacionUnico',
                campo: campo,
                valor: valor
            };

        $.post('./servicios/entradas_pt.php',
            {data:dataKey}, function(data){
                if (data.error != "") {
                    alert(data.error);
                }
                if (data.unico == 'NO') {
                    noEsUnicoFunc();
                } else {
                    esUnicoFunc();
                }

            },"json").fail(function(data){
                console.log('-- ERROR --');
                console.log(data);
                alert("ERROR.-> "+data);
            }
        );
    },

    validacionExisteCliente: function(no_cliente, existe, noExiste) {
        var valido = true;
        var dataKey = {
                widget: 'validacionExisteCliente',
                no_cliente: no_cliente
            };

        $.post('./servicios/salidas_pt.php',
            {data:dataKey}, function(data){
                if (data.error != "") {
                    alert(data.error);
                }
                if (data.existe == 'SI') {
                    existe();
                } else {
                    noExiste();
                }

            },"json").fail(function(data){
                console.log('-- ERROR --');
                console.log(data);
                alert("ERROR.-> "+data);
            }
        );
    }

};