rSisTacome = {
    parametros : {},
    ready : function(){
        this.cargaCampos();
    },
    cargaCampos : function(){
        $("div#dialogTacome").load("./tabs/datos-tacome.php", function(){
            rSisTacome.datosRegistro();
            rSisTacome.eventoAutocomplete();
            rSisTacomeValidate._init();
        });
    },
    datosRegistro : function(){
        $.post('./include/polizas.silan.php',
            {data: {evento:"usuarioRegistro"}}, function(data){
                $("input#fechaTacome").prop("value",data.fechaRegistro);
                $("input#usuarioTacome").prop("value",data.usuarioRegistro);
                rSisTacome.muestraDialog();
                rSisTacome.botones();
            },"json");
    },
    eventoAutocomplete : function(){
        $("input#clavePieza").focusin(function(){
            rSisAutocomplete.tacome(this.id);
        });
    },
    
    muestraDialog : function(){
        $("div#dialogTacome").dialog({
            modal: true,
            autoOpen: true,
            title: "Tacome",
            dialogClass: 'hide-close',
            width: 750,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true,
            close: function(event, ui)
            {
            	//window.location.replace('./inicio.php');
            	window.location = 'index.php';
            }
        }).show();
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
    },
    botones : function(){
        $("div#guardaRegistro")
            .button({ 
                icons: { primary: "ui-icon-disk" },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("form#form-datos-tacome").submit();
            });
            
        $("div.cerrar-tabs")
            .button({ 
                icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                label: "Cerrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("div#dialogTacome").dialog("close");
            });
    }
};



