/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
$.datepicker.regional['es'] = {
     closeText: 'Cerrar',
     prevText: '< Ant',
     nextText: 'Sig >',
     currentText: 'Hoy',
     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
     monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
     dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
     dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
     dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
     weekHeader: 'Sm',
     dateFormat: 'dd/mm/yy',
     firstDay: 1,
     isRTL: false,
     showMonthAfterYear: false,
     yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);

function messagedialog(){

     var self =this;

    this.showmessage = function(msg, action, title){
        $("#modal-message").empty();
        $("#modal-message").append("<p>"+msg+"</p>");
        $("#modal-message").dialog({
            modal: true,
            title: title,
            dialogClass: 'hide-close',
            buttons:{
                "Aceptar": function(){
                    $silan_msg[action]($(this));
                }
            }
        });
//        $(".ui-dialog-titlebar").css({"background":"#671578 none repeat scroll 0 0","color":"#fcfcfc"});
        $(document).find(".ui-widget").css({"font-size":"0.8em"});
    };
    
    this.showmessagecondition = function(msg,actionOK, actionNOT, title){
       
        $("#modal-message").empty();
        $("#modal-message").append(msg);
        $("#modal-message").dialog({
            modal: true,
            title: title,
            dialogClass: 'hide-close',
            buttons:{
                "Cancelar": function(){                                     
                    actionNOT(self);
                     $("#modal-message").empty();
                    $("#modal-message").dialog("close");
                },
                "Aceptar": function(){
                    actionOK(self);
                    $("#modal-message").empty();
                    $("#modal-message").dialog("close");
                }  }
        });
//        $(".ui-dialog-titlebar").css({"background":"#671578 none repeat scroll 0 0","color":"#fcfcfc"});
        $(document).find(".ui-widget").css({"font-size":"0.8em"});
    };
    
    this.reload = function(widget){
        $silan_msg.closemodal(widget);
        location.reload();
    };
    
    this.closemodal = function(widget){
        widget.empty();
        widget.dialog("close");
        widget.dialog("destroy");
    };
    
    this.closeprocess = function(filename){
        window.location.replace(filename);
    };
    this.conditionOK_Tacome = function(){
        rSisTacomeValidate.submitTacome();
    };
    
    this.conditionNOT_Tacome = function(widget){
        $silan_msg.reload(widget);
        return false;
    };
}
$silan_msg = new messagedialog();