/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna / frank
 * MEXICO, 2017
 * SiLan v1.0
 * Modificaciones: 2017-03-17
*/

var Entradas_pt = {
    numero_pieza: null,
    listaPiezasSinEntrada: [],
    mapaPiezas: {},
    focus: false,
    debug: false,

    ready: function() {
        if (this.debug) console.log('Entradas_pt.ready()');
        Entradas_pt.cargaCampos();
    },
    
    cargaCampos : function(){
        $("div#dialogEntradasPt").load("./tabs/datos-entradasPt.php", function(){
            Entradas_pt.datosRegistro();
        });
    },
    datosRegistro : function(){
        $.post('./include/entradasPt.silan.php',
            {data: {evento:"usuarioRegistro"}}, function(data){
                $("input#fechaEntrada").prop("value",data.fechaRegistro);
                $("input#usuarioEntrada").prop("value",data.usuarioRegistro);
                Entradas_pt.init();
                EntradaPieza.init();
                Numero_Pieza.init();
                DocumentoEntrada.init();
            },"json");
    },
    init: function(){
        $("div#dialogEntradasPt").dialog({
            modal: true,
            autoOpen: true,
            title: "Entradas Pt",
            dialogClass: 'hide-close',
            width: 750,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true,
            close: function(event, ui){
                window.location.replace('./inicio.php');
            }
        }).show();
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
    },

    actualizaPieza: function() {
        var numero_pieza =  Numero_Pieza.val();
        if(numero_pieza !== "" && Entradas_pt.numero_pieza !== numero_pieza) {
            if (this.debug) console.log('actualizaPieza: ' + numero_pieza);
            if (Entradas_pt.existeNumero_Pieza(numero_pieza)) {
                Entradas_pt.numero_pieza = numero_pieza;
            } else {
                $silan_msg.showmessage('Clave de pieza inválida: ' + numero_pieza,"closemodal","Entradas Pt");
                Entradas_pt.numero_pieza = null;
            }
        }
    },

    existeNumero_Pieza: function(numero_pieza) {
        if (this.debug) console.log('existeNumero_Pieza: ' + numero_pieza);
        return ($.inArray(numero_pieza, Entradas_pt.listaPiezasSinEntrada) !== -1);
    },

    valida: function() {
        var focus = false;
        var valido = true;

        if (!DocumentoEntrada.valida()) {
            if(!focus) focus = $(DocumentoEntrada.id);
            valido = false;
        }

        if (!ListaPiezas.valida()) {
            if(!focus) focus = $(ListaPiezas.id);
            valido = false;
        }

        if (!valido) {
            if (focus) focus.focus();
        }
        return valido;
    },

    guarda: function() {
        $.post('./include/entradasPt.silan.php',
            {data:{evento:"entradasPt", lista_numero_pieza: ListaPiezas.lista, documentoEntrada: DocumentoEntrada.val()}}, function(data){
            if(data.error !== 0)
                    $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    console.log("ERROR 0 Entradas_pt.guarda() -> "+data);
            }).always(function(data){
                $silan_msg.showmessage(data.msg,data.action,data.title);
//                DocumentoEntrada.limpia();
//                ListaPiezas.limpia();
//                Numero_Pieza.init();
//                $(DocumentoEntrada.id).focus();
            });
    }
};

var Numero_Pieza = {
    id: 'input#noPieza',
    debug: false,
    init: function() {
        $(Numero_Pieza.id).empty();
        $.post('./include/entradasPt.silan.php',
            {data:{evento:"listaPiezasSinEntrada"}}, function(data){
            if(data.error !== 0)
                    $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                console.log("ERROR 0 Nuemro_Pieza.init() -> "+data);
            }).always(function(data){
                var list = [];
                var pieza;
                for (var l in data.list) {
                    pieza = data.list[l];
                    if (parseInt(pieza.noPieza) !== 0) {
                        list.push(pieza.noPieza);
                    }
                    Entradas_pt.mapaPiezas[pieza.noPieza] = pieza;
                }
                Entradas_pt.listaPiezasSinEntrada = list;
            });
        
        $(Numero_Pieza.id).keydown(function(event) {
            if ( event.which === 13 ) {
                if (this.debug) console.log('enter: ' + $("#numero_pieza").val());
                event.preventDefault();
                Entradas_pt.actualizaPieza();
                if (Entradas_pt.numero_pieza !== null) {                    
                    if (ListaPiezas.agrega(Entradas_pt.numero_pieza)) {
                        if (this.debug) console.log('si agrega');
                        ListaPiezas.dibuja();
                    }
                    $(Numero_Pieza.id).val('');
                }
            }
        });
    },
    valida: function() {
        if (Entradas_pt.numero_pieza === null) {
            alert('Clave de pieza inválida');
            return false;            
        }
        return true;
    },
    val: function() {
        return $(Numero_Pieza.id).val();
    }
};

var DocumentoEntrada = {
    id: 'input#documentoEntrada',
    valido: false,
    debug: false,
    init: function() {
        $(this.id).on('change',function(){
            $.post('./include/entradasPt.silan.php',
                {data: {evento:"validacionUnico", campo:"documentoEntrada", valor:DocumentoEntrada.val()}}, function(data){
                    if(data.error !== 0)
                        $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json").fail(function(data){
                    console.log("ERROR 0 DocumentoEntrada.init() -> "+data);
                }).always(function(data){
                    (data.unico === 'NO' ? DocumentoEntrada.noEsUnico() : DocumentoEntrada.esUnico() );
                });
        });
    },
    limpia: function() {
        $(this.id).val('');
        this.valido = false;
    },
    valida: function() {
        DocumentoEntrada.valido = true;
        if (DocumentoEntrada.val() === "") {
            DocumentoEntrada.valido = false;
            $silan_msg.showmessage('Documento de entrada inválido',"closemodal","Entradas Pt");
        }
        return DocumentoEntrada.valido;
    },
    val: function() {
        return $(DocumentoEntrada.id).val();
    },
    esUnico: function() {
        if (DocumentoEntrada.debug) console.log('esUnico');
        DocumentoEntrada.valido = true;
    },
    noEsUnico:function() {
        if (DocumentoEntrada.debug) console.log('noEsUnico');
        DocumentoEntrada.valido = false;
        $silan_msg.showmessage('El documento de entrada ' + DocumentoEntrada.val() + ' ya existe',"closemodal","Entradas Pt");
        $(this.id).val('').focus();
    }
};

var EntradaPieza = {
    id: "entradaPieza",
    debug: false,
    init: function(){
        $("div#guardaRegistro")
            .button({ 
                icons: { primary: "ui-icon-disk" },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                if (!Entradas_pt.valida()) return;
                Entradas_pt.guarda();
            });
            
        $("div.cerrar-tabs")
            .button({ 
                icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                label: "Cerrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("div#dialogEntradasPt").dialog("close");
            });
    }
};

var ListaPiezas = {
    id: 'div#listaPiezas',
    debug: false,
    lista: [],
    init: function(){

    },
    limpia: function() {
        this.lista = [];
        this.dibuja();
    },
    val: function() {
        return this.lista.join();
    },
    agrega: function(numero_pieza) {
        if ($.inArray(numero_pieza, ListaPiezas.lista) === -1) {
            ListaPiezas.lista.push(numero_pieza);
            return true;
        } else {
            var sound = document.getElementById("error");
            sound.play();
            return false;
        }
    },
    dibuja: function() {
        $(ListaPiezas.id).empty();
        var div, numero, clave_pieza;
        for (var p in ListaPiezas.lista) {
            numero = ListaPiezas.lista[p];
            clave_pieza = Entradas_pt.mapaPiezas[numero].clavePieza;
            if (this.debug) console.log(numero);
            div = $(document.createElement('div'))
                .text('(' + numero + ') ' + clave_pieza)
                .addClass('listaPiezas')
                .attr('title', 'Quitar')
                .attr('numero', numero)
                .on('click',function(){
                    if (this.debug) console.log('clic en ' + $(this).text());
                    var listaNueva = [];
                    var numero;
                    for (var p in ListaPiezas.lista) {
                        numero = ListaPiezas.lista[p];
                        if (numero !== $(this).attr('numero')) {
                            listaNueva.push(numero);
                        }
                    }
                    ListaPiezas.lista = listaNueva;
                    ListaPiezas.dibuja();
                })
            ;
            $(ListaPiezas.id).append(div);
        }
        $('input#no_piezas').val(ListaPiezas.lista.length);
    },
    valida: function() {
        var valido = true;
        if (ListaPiezas.lista.length === 0) {
            valido = false;
            $silan_msg.showmessage('No hay piezas seleccionadas',"closemodal","Etiquetas Pt");
        }
        return valido;
    }
};
