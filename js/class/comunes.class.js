function requestData(url,data,error, success)
{
	$.ajax({
      	url:url, 
      	data:data,
      	statusCode: {
		    401: error,
		    500: error
		  },
		  success: success,
		  dataType:"json",
		  type:"POST"
	});
}

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getPatternFor(v)
{
	var len = v.indexOf(".");
	var str = "";
	for(i=0;i<len;i++)
	{
		str+="#";
	}
	str+=".##";
	return str;
}


//***************
function jsRS(){
	var $this = this;
	var $datSession = "";
	var $action = "search";
	this.init = function(){
		$("input[name=txtUltimoAcceso], input[name=txtFechaEstatus], input[name=txtUsuarioEstatus]").prop("readonly","readonly");
		$("#botonesGB").hide();
		$this.getDataSession();
		$this.controlEvents();
		$this.fillGridUser();
		$this.fillCombos({cmb:'status', type:4});
		$this.fillTabFunctions();
		$this.addEventsElements();
	};

	this.addEventsElements = function(){
		$("#btnGuardar").attr("onClick","javascript:$rs.onSaveRecords()");
		$("#btnBorrar").attr("onClick","javascript:$rs.onDeleteRecords()");
		$("input[name='filter']").attr("onKeyUp","javascript:$rs.onSearchRecords()");
		$("#cmbEstatus").attr("onClick","javascript:$rs.eventSelectStatus()");
		$("#btnNuevo").attr("onClick","javascript:$rs.onNewRecord()");
	};

	this.setdatSession = function(field, value){
		$datSession[field] = value;
	};

	this.getdatSession = function(field){
		return (field === "") ? $datSession : $datSession[field];
	};

	this.getAction = function(){
		return $action;
	};

	this.fillGridUser = function(){
		var datakey = JSON.stringify({name : 'usuarios', filter:''});
		requestData('./php/rsFillGrids.php',{data : datakey}, onUserError, fillUsers);
	};

	this.getDataSession = function(){
		$.ajax({
            type : "POST",
            url : './php/rsGetDataSession.php',
            success : function(data){
            	$datSession = JSON.parse(data);
            	$.each($datSession.permisos, function(idx, ele){
            		if(ele === "L")
            			$(".tableRecords").show();
            		if(ele === "E")
            			$("#btnNuevo, .btnGuardarRegistro").show();
            		if(ele === "B")
            			$(".btnBorrarRegistro").show();
            	});
            }
        });
	};

	this.fillTabFunctions = function(){
		//*** LISTA TODAS LAS FUNCIONES
		var datakey = JSON.stringify({action : "listFunctions"});
		$.ajax({
			type : "POST",
			url : "./php/rsRecordUsers.php",
			data : "data="+datakey,
			contentType : "application/x-www-form-urlencoded",
			async : false,
			error : function(data){
				alert("ERROR. "+data, "0");
			},
			success : function(data){
				var result = JSON.parse(data);
				$("#func-ventanas").append(result.data.win);
				$("#func-procesos").append(result.data.pro);
				$("#func-reportes").append(result.data.rep);
			}
		});
	};

	this.getDateFormat = function(data){
		var fullMonthNames = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septimbre','Octubre','Novimbre','Dicimbre');
		var shortMonthNames = new Array('Ene','Feb','Mar','Abr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dic');
		var fullDayNames = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
		var shortDayNames = new Array('Dom','Lun','Mar','Mie','Jue','Vie','Sab');
	//******
		var month = (data.month === "full") ? fullMonthNames[data.fecha.getMonth()] : shortMonthNames[data.fecha.getMonth()];
		var day = (data.day === "full") ? fullDayNames[data.fecha.getDay()] : shortMonthNames[data.fecha.getDay()];
	//******
		var fecha = String(data.fecha.getDate()+100).substring(1)+" "+month+" "+data.fecha.getFullYear();
		var hora = String(data.fecha.getHours()+100).substring(1)+":"+
				   String(data.fecha.getMinutes()+100).substring(1)+":"+
				   String(data.fecha.getSeconds()+100).substring(1);
		return (data.time) ? fecha+" "+hora : fecha;
	};

	this.fillCombos = function(data){
		var dataKey = JSON.stringify(data);
        $.ajax({
            type : "POST",
            url : './php/rsFillCombos.php',
            data : "datakey="+dataKey,
            async :  false,
            error : function(data){
                console.log("ERROR. fillCombos-> "+data)
            },
            success : function(data){
                $("#cmbEstatus").append(data);
            }
        });
	};

	this.controlEvents = function(){
    //*** DETECTA LA EDICION DE REGISTROS
        $(".rs-editrow").on("click", function(e){
        	e.preventDefault();
        	$action = "edit";
        	$datSession.actual = { id : $(this).prop("id")};
        	$("tbody > tr").removeClass("registroSelecionado");
	    	$(this).addClass("registroSelecionado");
    	//***
        	$("#div-edition input").prop("value","");
        	$("#div-edition [name*='portrait']").attr("src",'/images/noimage.jpg');
    	//*** OBTIENE LA INFORMACION PERSONAL DEL USUARIO
    		var dataKey = JSON.stringify({action : $action, id : $(this).prop("id")});
    		cleanAll(); // LIMPIA PESTAÑA: SENSORES - FUNCIONES
    		$.ajax({
	            type : "POST",
	            url : './php/rsRecordUsers.php',
	            data : "data="+dataKey,
	            error : function(data){
	            	alert(data);
	            },
	            success : function(data){
	            	var result = JSON.parse(data);
	            	if(result.error !== 0){
	            		alert(result.msg)
	            		return false;
	            	}
	            	onUserData(result.data.user);
	            	if((result.data.sensors).length > 0) $this.userSensorsData(result.data.sensors);
	            	if((result.data.functions).length > 0) $this.userFunctionsData(result.data.functions);
	            	//$("#btnGuardar, #btnBorrar").show();
    				$action = "update";
	            }
	        });
        });
	};


	//*** EXAMINA RELACION SENSORES => USUARIOS
	this.userSensorsData = function(data){
		var treeApi = $('#tree').aciTree('api');
        data.forEach(function(val,idx,arr){
	        treeApi.searchId(false,true,{
	            id : val.SENSOR+"",
	                success : function(item,options){
						treeApi.check($(item));
                    },
                    fail : function(item,options){
                        console.log("ERROR. userSensorsData-> "+val.ID);
                    }
            });
        });
	};

	//*** EXAMINA RELACION FUNCIONES => USUARIOS
	this.userFunctionsData = function(data){
		data.forEach(function(ele,idx, arr){
			//** FUNCIONES - VENTANAS
			$("[name^=func_"+ele.func+"]").each(function(){
            	if(this.value.indexOf('L') >= 0 && ele.permiso.indexOf('L') >= 0)
                	$(this).attr('checked', true);
	            if (this.value.indexOf('E') >= 0 && ele.permiso.indexOf('E') >= 0)
	                $(this).attr('checked', true);
	            if (this.value.indexOf('B') >= 0 && ele.permiso.indexOf('B') >=0)
	                $(this).attr('checked', true);
        	});
			//** FUNCIONES - PROCESOS
			$("[name^=proc_"+ele.func+"]").each(function(){
            	if(this.value.indexOf('X') >= 0 && ele.permiso.indexOf('X') >= 0)
                	$(this).attr('checked', true);
        	});
			//** FUNCIONES - REPORTES
			$("[name^=rep_"+ele.func+"]").each(function(){
            	if(this.value.indexOf('X') >= 0 && ele.permiso.indexOf('X') >= 0)
                	$(this).attr('checked', true);
        	});
		});
	};

	this.clearForms = function(){
		$("#resMsg").html("");
		$("#div-edition input").prop("value","");
    	$("#div-edition [name*='portrait']").attr("src",'/images/noimage.jpg');
    	$("#tipoUsuario").prop("selectedIndex",0).trigger("click");
    	$("#cmbEstatus").prop("selectedIndex",0).trigger("click");
    	showDatos();
    	cleanAll(); // LIMPIA PESTAÑA: SENSORES - FUNCIONES
    	$datSession.actual = { id : 0 };
	};

	//*** BOTON GUARDAR
	this.onSaveRecords = function(){
		if(!$usuariosTabDatos.form()) return;
		//if(!form.form()) return;
        if($("#div-edition input[name*='pwd']").val().localeCompare($("#div-edition input[name*='pwd2']").val()))
        {
            $("#resMsg").html("El password no coincide!");
            return;
        }
    //** PESTAÑA DATOS
        var data = $("#div-edition").serialize();
        data += "&id="+$datSession.actual.id;
        data += "&sessionFechaEstatus="+$datSession.fechaEstatus;
        data += "&image="+""; // COLOCAR AQUI LA INFORMACION DE LA IMAGEN
    //** PESTAÑA SENSORES
        $("#resMsg").html("En Progreso...");
        var treeApi = $('#tree').aciTree('api');
        var _sels = $(treeApi.children(null,true,true));
        var sensores = "";
        for(i=0;i<_sels.length;i++)
			if(treeApi.isLeaf(($(_sels[i])))){
				(sensores==="")
					? sensores += "sens_"+treeApi.getId($(_sels[i]))+"="+treeApi.isChecked($(_sels[i]))
					: sensores += "&sens_"+treeApi.getId($(_sels[i]))+"="+treeApi.isChecked($(_sels[i]));
			}
	//** PESTAÑA FUNCIONES
		var permisos = new Array();
		var func = ""; var value = "";
		var checkFunctions = $("#div-funciones :checkbox:checked");
		checkFunctions.each(function(idx, ele){
			if(idx == 0) func = (ele.name).substr(5,3);
			if((ele.name).substr(5,3) != func){
				if(value.substr(0,1) === 'L')
					permisos.push({item : func, val : value});
				func = func = (ele.name).substr(5,3);
				value = "";
			}
			value+= ele.value;
		});
		if(value.substr(0,1) === 'L')
			permisos.push({item : func, val : value});
	//**
    	var dataKey = JSON.stringify({action : $action, fields : data, sensors : sensores, functions : permisos});
    	requestData('./php/rsRecordUsers.php',{data : dataKey}, onGuardaError, $this.refreshForms);
	};

	//*** ELIMNA REGISTROS DE LA TABLA
	this.onDeleteRecords = function(){
    	var r = confirm("Realmente quieres borrar este usuario?");
    	if (r == true) {
        	$action = "delete";
        	var dataKey = JSON.stringify({action : $action, id : $datSession.actual.id});
        	requestData('./php/rsRecordUsers.php',{data : dataKey}, onUserError, $this.refreshForms);
    	}
	};

	//*** CAMPO CONSULTAR O BUSCAR
	this.onSearchRecords = function(){
        $("#status-busca").html("Buscando...");
		var datakey = JSON.stringify({name : 'usuarios', filter:$("input[name='filter']").prop("value")});
        requestData('./php/rsFillGrids.php',{data : datakey}, onUserError, fillUsers);
    };

	//*** SELECT STATUS
	this.eventSelectStatus = function(){
        var data = { fecha :  new Date(), month : "short", day : "short", time : true };
        $datSession.fechaEstatus = 1;
        $("input[name=txtFechaEstatus]").prop("value",$this.getDateFormat(data));
        $("INPUT[name=txtUsuarioEstatus]").prop("value",$datSession.fullName);
    };

    //*** BOTON NUEVO
	this.onNewRecord = function(){
		$this.clearForms();
		$action = "insert";
	};

	//*** ADICIONA UNA IMAGEN
	//$("#addImage").on("click", function(e){ e.preventDefault();
	this.onAddImage = function(){
		//$("#photoImage").trigger("click");
		alert("ok");
	};

	this.refreshForms = function(data){
		$this.fillGridUser();
		$this.onNewRecord();
	};
}
$rs = new jsRS();