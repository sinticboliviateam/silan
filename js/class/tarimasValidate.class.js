/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-30
 * SiLan v1.0
*/
function ordenesCompValidate(){
    var dataform = {
        event : "usuarios",
        functions : new Array()
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    
    this.clearobjvar = function(){
        dataform = {
            event : "usuarios"
        };
    };
    
    this.formValidateDatos = function(){

        $("#form-ordenComp").validate({

            
            rules : {
                noOrdenCompra : { required : true },
                fechaOrden : { required : true },
                destinoOrden : { required : true }
            },
            messages: {
                noOrdenCompra : { required : "Este campo es obligatorio" },
                fechaOrden : { required : "Este campo es obligatorio" },
                destinoOrden : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form){
                if(!$silan_validOrdenCompra.validaCampos()){
                    $silan_msg.showmessage("Existen datos obligatorios.",
                                            "closemodal","Tabla de órdenes de compra.");
                    return;
                }
                $.post('./include/ordenesCompra.php',
                    {data:$silan_validOrdenCompra.getobjvar()}, function(data){
                        (data.error !== 0)
                            ? $silan_msg.showmessage(data.msg,data.action,data.title)
                            : location.reload();
                    },"json").fail(function(data){
                        alert("ERROR LINE 53 formValidate.-> "+data);
                    });
            }
        });
        return false;
    };
    
    this.validaCampos = function(){
        if($("#noOrdenCompra").val() === "" ||
            $("#fechaOrden").val() === "" ||
            $("#destinoOrden").val() === "" 
        ){
            return false;
        }
        $silan_validOrdenCompra.setobjvar("noOrdenCompra", $("#noOrdenCompra").val());
        $silan_validOrdenCompra.setobjvar("fechaOrden", $("#fechaOrden").val());
        $silan_validOrdenCompra.setobjvar("destinoOrden", $("#destinoOrden").val());
        
        return true;
    };
    
}
$silan_validOrdenCompra = new ordenesCompValidate();