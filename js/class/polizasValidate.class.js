/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego Fernandez
 * SiLan v1.0
 * MEXICO, 2017
*/
rSisPolizasValidate = {
    _init : function(){
        this.formValidatePolizas();
    },
    formValidatePolizas : function(){
        $("form#form-tab-datos-polizas").validate({
            onfocusout: function(element) { 
                if($(element).valid())
                    rSisPolizas.parametros[$(element).attr("name")] = $(element).val();
            },
            rules : {
                "claveProducto" : { required: true },
                "noPiezas" : { required: true, range : [1,60]},
                "metros" : { required: true, range : [37.00,60.00]},
                "noTelas" : { required: true, range : [1,50]}
            },
            messages: {
                claveProducto : { required : "Este campo es obligatorio" },
                noPiezas : { required : "Este campo es obligatorio", range : "Rango válido entre 1 hasta 60" },
                metros : { required : "Este campo es obligatorio", range : "Rango válido entre 37 hasta 60" },
                noTelar : { required : "Este campo es obligatorio", range : "Rango válido entre 1 hasta 50" }
            },
            submitHandler: function(form){
                rSisPolizasValidate.obtenValores(this.currentElements);
                $.post('./include/polizas.silan.php',
                    {data:rSisPolizas.parametros}, function(data){
                        if(data.error !== 0)
                            $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json").fail(function(data){
                        console.log("ERROR 0 formValidatePolizas.-> "+data);
                    }).always(function(data){
                        $silan_msg.showmessage(data.msg,data.action,data.title);
//                        location.reload();
                    });
            }
        });
    },
    obtenValores : function(elementos){
        for(var i=0; i<=(elementos.length)-1; ++i){
            if(elementos[i].name === "claveProducto"){
                rSisPolizas.parametros.claveProducto = elementos[i].dataset.key;
            } else { rSisPolizas.parametros[elementos[i].name] = elementos[i].value; }
        }
        rSisPolizas.parametros.idFormulas = rSisAutocomplete.id;
        rSisPolizas.parametros.evento = "insertaPoliza";
    }
};
