/* 
 * por: Rsistemas, Manuel Luna
 * 2018-04-04
 * SiLan v1.0
*/
repEntradas = {
    consulta : function(){
        
            $("div.btnConsulta")
                .button({ 
                    icons: { primary: "ui-icon-print" },
                    label: "Imp. consulta"
                })
                .on("click",function(e){
                    fi= $("#fechaIni").val();
                    ff= $("#fechaFin").val();

                    if((ff>=fi) && (fi !="" && ff!="")) {
                        fi= fi.substring(6)+'-'+fi.substring(3,5)+'-'+fi.substring(0,2);
                        ff= ff.substring(6)+'-'+ff.substring(3,5)+'-'+ff.substring(0,2);
                        e.preventDefault();
                        //alert("fi "+ fi+"\nff "+ff);
                        //console.log(fi);console.log(ff);
                        $.get('./include/repEntradasPDF.silan.php',
                            {fechaIni: fi, fechaFin: ff}, function(data){
                                window.open(data, '_blank');
                            });
                    }
                });
    }
};
