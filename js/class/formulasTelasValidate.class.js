/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
function formulasValidate(){
    var dataform = {
        event : "formulas",
        letras : "",
        urdimbre : { hilos : "", curso : "", porcentaje : "", totalCurso : "" },
        trama : { hilos : "", curso : "", porcentaje : "", total : "" },
        orillo : { hilos : "", curso : "", porcentaje : "", total : "" },
        valid : false
    };
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    this.setsubobject = function(pfield, sfield, value){
        dataform[pfield][sfield] = value;
    };
    this.clearobjvar = function(){
        dataform = {
            event : "usuarios",
            letras : ""
        };
    };
    this.clearsubobject = function(field){
        dataform.urdimbre = { hilos : "", curso : "", porcentaje : "", totalCurso : "" };
        dataform.trama = { hilos : "", curso : "", porcentaje : "", totalCurso : "" };
        dataform.orillo = { hilos : "", curso : "", porcentaje : "", totalCurso : "" };
    };
//****
    this.metodos = function(){
        $.validator.addMethod("datosCurso", function(value, element) {
//            var l = $silan_validFormulas.getobjvar("letras");
            var expreg = new RegExp("^(?:[\\d]{1,3}[A-O]{1}|[\\d]{1,3}[\\(]{1}[\\d]{1,3}[A-O]{1}|[\\(]{1}[\\d]{1,3}[A-O]{1})$","i");
            return expreg.test(value);
        }, "Datos incorrectos.");
        
        $.validator.addMethod("datosCursoOrillo", function(value, element) {
            var expreg = new RegExp("^(?:[\\d]{1,3}[W-Z]{1}|[\\d]{1,3}[\\(]{1}[\\d]{1,3}[W-Z]{1}|[\\(]{1}[\\d]{1,3}[W-Z]{1})$","i");
            return expreg.test(value);
        }, "Datos incorrectos.");
    };
    
    this.formValidateTelas = function(){
        $("#form-tab-datos-tela").validate({
            onfocusout: function(element) { 
                ($(element).valid())
                    $silan_validFormulas.setobjvar($(element).attr("name"),$(element).val());
            },
            rules : {
                //"coloresProducto" : { number : true },
                "coloresOrillo" : { number : true }
            },
            messages: {
                claveProducto : { required : "Este campo es obligatorio" },
//                coloresProducto : { required : "Este campo es obligatorio" },
                coloresOrillo : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form){
                $silan_validFormulas.getTabsData(form);
                $.post('./include/formulasTelas.silan.php',
                    {data:$silan_validFormulas.getobjvar()}, function(data){
                        if(data.error !== 0)
                            $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json").fail(function(data){
                        console.log("ERROR 0 formValidateTelas.-> "+data);
                    }).always(function(){
                        location.reload();
                    });
            }
        });
    };
    
    this.formValidateTecnicos = function(){
        $("#form-tab-datos-tecnicos").validate({ //#form-tab-datos-tecnicos
            onfocusout: function(element) { 
                ($(element).valid())
                    $silan_validFormulas.setobjvar($(element).attr("name"),$(element).val());
            },
            rules : { 
                "hilosFondo" : { number : true },
                "hilosOrillo" : { number : true },
                "anchoTela" : { number : true },
                "noCabos" : { number : true },
                "pasadas2p" : { number : true },
                "noRepasoPeine" : { number : true },
                "repasoPeine" : { number : true }
            },
            messages: {
                hilosFondo : { required : "Este campo es obligatorio" },
                hilosOrillo : { required : "Este campo es obligatorio" },
                anchoTela : { required : "Este campo es obligatorio" },
                noCabos : { required : "Este campo es obligatorio" },
                pasadas2p : { required : "Este campo es obligatorio" },
                noRepasoPeine : { required : "Este campo es obligatorio" },
                repasoPeine : { required : "Este campo es obligatorio" },
                tipoDibujo : { required : "Este campo es obligatorio" },
            },
            submitHandler: function(form){
                $silan_validFormulas.getTabsData("div.datosFormulasTelas");
                $("form#form-tab-curso-urdimbre").submit();
            }
        });
    };
    
    this.formValidateCursoUrdimbre = function() {
        $silan_validFormulas.metodos();
        $("form#form-tab-curso-urdimbre").validate({
            rules: {
                u1: {
                    required: true,
                    datosCurso: true
                }
            },
            submitHandler: function(form){
                if($silan_validFormulas.getobjvar("valid") === false)
                    return false;
                var param = {
                    gridPorcent : "tbody#tbl02-records-urdimbre",
                    gridCurso : "tbody#tbl01-records-urdimbre",
                    total : "input#totalHilosUrimbre",
                    tab : "urdimbre"
                };
                var result = $silan_validFormulas.getTabsDataCursos(this.currentElements,param);
                if(!result){
                    $silan_msg.showmessage("Existen datos obligatorios, que no han sido capturados.","closemodal","Curso de urdimbre");
                    return false;
                }
                if(parseInt($("input#totalPorcentajeUrdimbre")) < 100){
                    $silan_msg.showmessage("El porcentaje total es incorrecto, revise los valores del curso.","closemodal","Curso de urdimbre");
                    return false;
                }
                $("form#form-tab-curso-trama").submit();
            }
        });
    };
    
    this.formValidateCursoTrama = function() {
        $silan_validFormulas.metodos();
        $("form#form-tab-curso-trama").validate({
            rules: {
                u1: {
                    required: true,
                    datosCurso: true
                }
            },
            submitHandler: function(form){
                if($silan_validFormulas.getobjvar("valid") === false)
                    return false;
                var param = {
                    gridPorcent : "tbody#tbl02-records-trama",
                    gridCurso : "tbody#tbl01-records-trama",
                    total : "input#totalHilosTrama",
                    tab : "trama"
                };
                if($("input#urdimbreTrama").is(":checked")){
                    $silan_validFormulas.setobjvar("trama", $silan_validFormulas.getobjvar("urdimbre"));
                } else {
                    var result = $silan_validFormulas.getTabsDataCursos(this.currentElements,param);
                    if(!result){
                        $silan_msg.showmessage("Existen datos obligatorios, que no han sido capturados.","closemodal","Curso de la trama");
                        return false;
                    }
                    if(parseInt($("input#totalPorcentajeTrama")) < 100){
                        $silan_msg.showmessage("El porcentaje total es incorrecto, revise los valores del curso.","closemodal","Curso de la trama");
                        return false;
                    }
                }
                $("form#form-tab-curso-orillo").submit();
            }
        });
    };
    
    this.formValidateCursoOrillo = function() {
        $silan_validFormulas.metodos();
        $("form#form-tab-curso-orillo").validate({
            rules: {
                u1: {
                    required: true,
                    datosCursoOrillo: true
                }
            },
            submitHandler: function(form){
                if($silan_validFormulas.getobjvar("valid") === false)
                    return false;
                var param = {
                    gridPorcent : "tbody#tbl02-records-orillo",
                    gridCurso : "tbody#tbl01-records-orillo",
                    total : "input#totalHilosOrillo",
                    tab : "orillo"
                };
                var result = $silan_validFormulas.getTabsDataCursos(this.currentElements,param);
                if(!result){
                    $silan_msg.showmessage("Existen datos obligatorios, que no han sido capturados.","closemodal","Curso de orillo");
                    return false;
                }
                if(parseInt($("input#totalPorcentajeOrillo")) < 100){
                    $silan_msg.showmessage("El porcentaje total es incorrecto, revise los valores del curso.","closemodal","Curso de orillo");
                    return false;
                }
                $.post('./include/formulasTelas.silan.php',
                    {data:$silan_validFormulas.getobjvar()}, function(data){
                        if(data.error !== 0)
                            $silan_msg.showmessage(data.msg,data.action,data.title);
                    },"json").fail(function(data){
                        console.log("ERROR 0 formValidateCursoUrdimbre.-> "+data);
                    }).always(function(){
                        location.reload();
                    });
            }
        });
    };
    
    this.getTabsData = function(form){
        var oInput = $(form).find(":input.campoRequerido");
        for(var i=0; i<=oInput.length-1; ++i){
            if(oInput[i].name === "claveProducto"){
                $silan_validFormulas.setobjvar(oInput[i].name,oInput[i].dataset.key);
            } else { $silan_validFormulas.setobjvar(oInput[i].name,oInput[i].value); }
        }
        var oSelect = $(form).find("select");
        for(var i=0; i<=oSelect.length-1; ++i){
            $silan_validFormulas.setobjvar(oSelect[i].name,oSelect[i].value);
        }
        var oCheckbox = $(form).find(":checkbox");
        for(var i=0; i<=oCheckbox.length-1; ++i){
            $silan_validFormulas.setobjvar(oCheckbox[i].name,oCheckbox[i].checked);
        }
    };
    
    this.getTabsDataCursos = function(formElements,nameObjects){
        var temp = new Object(); var porcent = new Array(); var curso = new Array();
//        hilos = new Array();
//        for(var i=0; i<=(formElements.length)-1; ++i){
//            temp = { letra : String.fromCharCode(formElements[i].name.substr(1,2)), idHilos : formElements[i].dataset.key, tipo : 'T' };
//            hilos.push(temp);
//        }
        temp = {};
        var tableElements = $(nameObjects.gridPorcent).find("tr.renglonesGridPorcentajes");
        for(var i=0; i<=(tableElements.length)-1; ++i){
            if(parseFloat($($(tableElements)[i]).find("td").eq(1).text()) === 0)
                return false;
            temp = { letra : $($(tableElements)[i]).find("td").eq(0).text(), idHilos : tableElements[i].dataset.key, porcentaje : parseFloat($($(tableElements)[i]).find("td").eq(1).text()) };
            porcent.push(temp);
        }
        temp = {};
        tableElements = $(nameObjects.gridCurso).find("tr.renglonesGridCurso");
        for(var i=0; i<=(tableElements.length)-1; ++i){
            temp = { secuencia : parseInt($($(tableElements)[i]).find("td").eq(0).text()), dato : $($(tableElements)[i]).find("td").eq(1).text() };
            curso.push(temp);
        }
//        *** $silan_validFormulas.setsubobject(nameObjects.tab, "hilos", hilos);
        $silan_validFormulas.setsubobject(nameObjects.tab, "curso", curso);
        $silan_validFormulas.setsubobject(nameObjects.tab, "porcentaje", porcent);
        $silan_validFormulas.setsubobject(nameObjects.tab, "totalCurso", $(nameObjects.total).val());
        return true;
    };
}
$silan_validFormulas = new formulasValidate();