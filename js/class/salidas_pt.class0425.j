/* 
 * Libreria creada por: Diego Fernánez, Manuel Luna / frank
 * MEXICO, 2017
 * SiLan v1.0
 * Modificaciones: 2017-03-17
*/

var Salidas_pt = {
    numero_pieza: null,
    listaPiezasSinSalida: [],
    mapaPiezas: {},
    focus: false,
    debug: false,

    ready: function() {
        if (this.debug) console.log('Salidas_pt.ready()');
        Salidas_pt.cargaCampos();
    },

    cargaCampos : function(){
        $("div#dialogSalidasPt").load("./tabs/datos-salidasPt.php", function(){
            Salidas_pt.datosRegistro();
        });
    },
    datosRegistro : function(){
        $.post('./include/salidasPt.silan.php',
            {data: {evento:"usuarioRegistro"}}, function(data){
                $("input#fechaSalida").prop("value",data.fechaRegistro);
                $("input#usuarioSalida").prop("value",data.usuarioRegistro);
                Salidas_pt.init();
                SalidaPieza.init();
                Numero_Pieza.init();
                NumeroCliente.init();
            },"json");
    },
    init: function(){
        $("div#dialogSalidasPt").dialog({
            modal: true,
            autoOpen: true,
            title: "Salidas Pt",
            dialogClass: 'hide-close',
            width: 750,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: true,
            close: function(event, ui){
                window.location.replace('./inicio.php');
            }
        }).show();
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
    },

    actualizaPieza: function() {
        var numero_pieza =  Numero_Pieza.val();
        if(numero_pieza !== "" && Salidas_pt.numero_pieza !== numero_pieza) {
            if (this.debug) console.log('actualizaPieza: ' + numero_pieza);
            if (Salidas_pt.existeNumero_Pieza(numero_pieza)) {
                Salidas_pt.numero_pieza = numero_pieza;
            } else {
                $silan_msg.showmessage('Clave de pieza inválida: ' + numero_pieza,"closemodal","Salidas Pt");
                Salidas_pt.numero_pieza = null;
            }
        }
    },
    
    

    existeNumero_Pieza: function(numero_pieza) {
        if (this.debug) console.log('existeNumero_Pieza: ' + numero_pieza);
        return ($.inArray(numero_pieza, Salidas_pt.listaPiezasSinSalida) !== -1);
    },

    valida: function() {
        if (this.debug) console.log('valida');
        var focus = false;
        var valido = true;

        if (!NumeroCliente.valida()) {
            if(!focus) focus = $(NumeroCliente.id);
            valido = false;
        }

        if (!ListaPiezas.valida()) {
            if(!focus) focus = $(ListaPiezas.id);
            valido = false;
        }

        if (!valido) {
            if (focus) focus.focus();
        }

        return valido;
    },

    guarda: function() {
        if (this.debug) console.log('Guarda');
        $.post('./include/salidasPt.silan.php',
            {data:{evento:"salidaPiezas", lista_numero_pieza: ListaPiezas.lista, noCliente: NumeroCliente.val()}}, function(data){
            if(data.error !== 0)
                    $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    console.log("ERROR 0 Salidas_pt.guarda() -> "+data);
            }).always(function(data){
                $silan_msg.showmessage(data.msg,data.action,data.title);
            });
    }
};

var Numero_Pieza = {
    id: 'input#noPieza',
    debug: true,
    init: function() {
        $(Numero_Pieza.id).empty();
        $.post('./include/salidasPt.silan.php',
            {data:{evento:"listaPiezasSinSalida"}}, function(data){
            if(data.error !== 0)
                    $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                console.log("ERROR 0 Nuemro_Pieza.init() -> "+data);
            }).always(function(data){
                var list = [];
                var pieza;
                for (var l in data.list) {
                    pieza = data.list[l];
                    if (parseInt(pieza.noPieza) !== 0) {
                        list.push(pieza.noPieza);
                    }
                    Salidas_pt.mapaPiezas[pieza.noPieza] = pieza;
                }
                Salidas_pt.listaPiezasSinSalida = list;
            });

        $(Numero_Pieza.id).keydown(function(event) {
            if ( event.which === 13 ) {
                if (this.debug) console.log('enter: ' + $("#numero_pieza").val());
                event.preventDefault();
                Salidas_pt.actualizaPieza();                
                if (Salidas_pt.numero_pieza !== null) {                    
                    if (ListaPiezas.agrega(Salidas_pt.numero_pieza)) {
                        if (this.debug) console.log('si agrega');
                        ListaPiezas.dibuja();
                    }
                    $(Numero_Pieza.id).val('');
                }
            }
        });
    },
    valida: function() {
        if (Salidas_pt.numero_pieza === null) {
            alert('Clave de pieza inválida');
            return false;            
        }
        return true;
    },
    val: function() {
        return $(Numero_Pieza.id).val();
    }
};

var NumeroCliente = {
    id: 'input#noCliente',
    valido: false,
    debug: false,
    init: function() {
        $(this.id).on('change',function(){
            $.post('./include/salidasPt.silan.php',
                {data: {evento:"validacionExisteCliente", cliente:NumeroCliente.val()}}, function(data){
                    if(data.error !== 0)
                        $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    console.log("ERROR 0 NumeroCliente.init() -> "+data);
                }).always(function(data){
                    (data.existe === 'NO' ? NumeroCliente.noExiste() : NumeroCliente.existe() );
            });
        });
    },
    limpia: function() {
        $("#" + this.id).val('');
        this.valido = false;
    },
    valida: function() {
        var valido = true;

        if (!NumeroCliente.valido) {
            valido = false;
            alert('Número de cliente inválido');
        }

        return valido;
    },
    val: function() {
        return $(NumeroCliente.id).val();
    },
    existe: function() {
        if (NumeroCliente.debug) console.log('existe');
        NumeroCliente.valido = true;
    },
    noExiste:function() {
        if (NumeroCliente.debug) console.log('noExiste');
        NumeroCliente.valido = false;
        $silan_msg.showmessage("El cliente número " + NumeroCliente.val() + " no existe","closemodal","Salidas Pt");
        $(NumeroCliente.id).val('').focus();
    }

};

var SalidaPieza = {
    id: "salidaPieza",
    debug: false,
    init: function(){
        $(SalidaPieza.id).on("click", function(e){
            if (!Salidas_pt.valida()) return;
            Salidas_pt.guarda();
        });
        
        $("div#guardaRegistro")
            .button({ 
                icons: { primary: "ui-icon-disk" },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                if (!Salidas_pt.valida()) return;
                Salidas_pt.guarda();
            });
            
        $("div.cerrar-tabs")
            .button({ 
                icons: { primary: "ui-icon-arrowreturnthick-1-w" },
                label: "Cerrar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("div#dialogSalidasPt").dialog("close");
            });
    }
};

var ListaPiezas = {
    id: 'div#listaPiezas',
    debug: false,
    lista: [],
    init: function(){

    },
    limpia: function() {
        this.lista = [];
        this.dibuja();
    },
    val: function() {
        return this.lista.join();
    },
    agrega: function(numero_pieza) {
        if ($.inArray(numero_pieza, ListaPiezas.lista) === -1) {
            ListaPiezas.lista.push(numero_pieza);
            return true;
        } else {
            var sound = document.getElementById("error");
            sound.play();
            return false;
        }
        return false;
    },    
    
    dibuja: function() {
        $(ListaPiezas.id).empty();
        var div, numero, clave_pieza;
        for (var p in ListaPiezas.lista) {
            numero = ListaPiezas.lista[p];
            clave_pieza = Salidas_pt.mapaPiezas[numero].clavePieza;
            if (this.debug) console.log(numero);
            div = $(document.createElement('div'))
                .text('(' + numero + ') ' + clave_pieza)
                .addClass('listaPiezas')
                .attr('title', 'Quitar')
                .attr('numero', numero)
                .on('click',function(){
                    if (this.debug) console.log('clic en ' + $(this).text());
                    var listaNueva = [];
                    var numero;
                    for (var p in ListaPiezas.lista) {
                        numero = ListaPiezas.lista[p];
                        if (numero !== $(this).attr('numero')) {
                            listaNueva.push(numero);
                        }
                    }
                    ListaPiezas.lista = listaNueva;
                    ListaPiezas.dibuja();
                })
            ;
            $(ListaPiezas.id).append(div);
        }
        $('input#no_piezas').val(ListaPiezas.lista.length);
    },
    valida: function() {
        var valido = true;

        if (ListaPiezas.lista.length === 0) {
            valido = false;
            alert('No hay piezas seleccionadas');
        }

        return valido;
    }
};
