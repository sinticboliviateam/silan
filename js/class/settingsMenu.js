function settingsmenu(){
	var $this = this;
	var $arrLinks = new Object();
	
	this.createmenu = function(){
		$arrLinks.m = new Array("","dashboard","#","#","#","login");
		$arrLinks.sm = new Array("",
					   [""],
					   [""],
					   ["","vZonas","vEmpresas","vAreas","vUbicaciones","vSensores","vUnidades","vDoctosExternos","vAlertas","vEstatus","vControles"],
					   ["","vGenerales","vUsuarios","vActividades"],
					   [""]
					   );
	};

	this.getLinksUser = function(){
		$.ajax({
            type : "POST",
            url : './php/class/settingsmenu.class.php',
            data : "",
            error : function(data){
            	alert(data);
            },
            success : function(data){
            	var result = JSON.parse(data);
            	$this.createmenu();
            	$this.createlinks(result);
            }
        });
	};

	this.createlinks = function(listmenu){
		var links = $("nav a");
		$("nav a").hide();
		var m = ""; var sm = ""; var head="";
		$.each(listmenu, function(idx,obj){
			for (var i=0; i<=links.length; i++) {
				if(obj.MENU === links[i].dataset.menu){
					m = parseInt(obj.MENU.substr(0,3));
					sm = parseInt(obj.MENU.substr(3,3));
					(sm === 0)
						? links[i].href = "./"+$arrLinks.m[m]+".php"
						: links[i].href = "./"+$arrLinks.sm[m][sm]+".php";
					if(m===3 && sm >= 1)
						$("a[data-menu=003000]").show();
					if(m===4 && sm >= 1)
						$("a[data-menu=004000]").show();
					$($(links)[i]).show();
					break;
				}
			}
		});
		$("a[data-menu='005000']").prop("href","./"+$arrLinks.m[5]+".php");
		$("a[data-menu='005000']").show(); //*** SALIR
	};
}
$setmenu_1 = new settingsmenu();