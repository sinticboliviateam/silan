rSisAutocomplete = {
    id : "",
    key : 0,
    init : function(){
        $("#"+rSisAutocomplete.elemento).autocomplete({
//                min_length: 3,
//                delay: 300,
            limit : 50,
            source: function(request, response) {
                $.post("./include/autocomplete.silan.php",
                    {data : {term : request.term, opcion : rSisAutocomplete.opcion, limit : 250 }}, function(data){
                        response(data);
                    },"json");
            },
            select: function(event, ui){
                event.preventDefault();
                $(this).val(ui.item.label);
                $(this).attr("data-key",ui.item.value);
                rSisAutocomplete.id = ui.item.label;
                rSisAutocomplete.key = ui.item.value;
                if(typeof(rSisAutocomplete.evento) !== "undefined")
                   rSisAutocomplete[rSisAutocomplete.evento]();
            },
            open: function(event, ui){
                if(rSisAutocomplete.opcion === "hilosTenidos"){
                    ($silan_formulasTelas.getobjvar("viewAutocomplete"))
                        ? $(this).autocomplete("widget").show()
                        : $(this).autocomplete("widget").hide();
                }
            }
        });
    },
    getdata: function(){
        $.post('./include/formulasTelas.silan.php',
            {data: {event:"getdata",clave:rSisAutocomplete.key}}, function(data){
                $("#nombreProducto").val(data.getdata.nombreProducto);
                $("#nombreColor").val(data.getdata.color);
                $("#claveAnterior").val(data.getdata.claveAnterior);
                $("#nombreMercado").val(data.getdata.nombreMercado);
                $('#div-imagen').css({"background":"url('."+data.getdata.ruta+"') center no-repeat", "background-size": "contain"});
            },"json").fail(function(data){
                console.log(data);
            });
    },
    hilos : function(oInput){
        rSisAutocomplete.elemento = oInput;
        rSisAutocomplete.opcion = "hilosTenidos";
        rSisAutocomplete.evento = "modificaColorPorcentajes";
        rSisAutocomplete.init();
//        ***$($("#"+oInput).parent().parent()).show();
    },
    productos : function(oInput){
        rSisAutocomplete.elemento = oInput;
        rSisAutocomplete.opcion = "productos";
        rSisAutocomplete.evento = "getdata";
        rSisAutocomplete.init();
    },
    polizas : function(oInput){
        rSisAutocomplete.elemento = oInput;
        rSisAutocomplete.opcion = "formulasTelas";
        rSisAutocomplete.evento = "muestraDatosProducto";
        rSisAutocomplete.init();
    },
    tacome : function(oInput){
        rSisAutocomplete.elemento = oInput;
        rSisAutocomplete.opcion = "piezas";
        rSisAutocomplete.evento = "muestraDatosPieza";
        rSisAutocomplete.init();
    },
    //*** EVENTOS FORMULAS TELAS
    extraeletra : function(letra,tab){
//        ***var letra = String.fromCharCode(rSisAutocomplete.elemento.slice(1,3).toUpperCase());
//        ***var tab = rSisAutocomplete.elemento.slice(0,1);
        for(l=65; l<=79; ++l){
            if(letra.charCodeAt() >= l){
                if($silan_formulasTelas.getobjvar("letras")[tab].indexOf(String.fromCharCode(l)) === -1){
                    var letras = $silan_formulasTelas.getobjvar("letras")[tab]+String.fromCharCode(l);
                    $silan_validFormulas.setobjvar("letras",letras);
                    $silan_formulasTelas.setobjletras(tab,letras);
                    rSisAutocomplete.creaTablaPorcentaje(String.fromCharCode(l));
                }
            } else { break; }
        }
        $silan_formulasTelas.calculaPorcentajes();
    },
    extraeletraorillo : function(letra,tab){
        console.log(letra.charCodeAt());
        for(l=87; l<=90; ++l){
            if(letra.charCodeAt() >= l){
                if($silan_formulasTelas.getobjvar("letras")[tab].indexOf(String.fromCharCode(l)) === -1){
                    var letras = $silan_formulasTelas.getobjvar("letras")[tab]+String.fromCharCode(l);
                    $silan_validFormulas.setobjvar("letras",letras);
                    $silan_formulasTelas.setobjletras(tab,letras);
                    rSisAutocomplete.creaTablaPorcentaje(String.fromCharCode(l));
                }
            } else { break; }
        }
        $silan_formulasTelas.calculaPorcentajes();
    },
    creaTablaPorcentaje : function(letra){
        var oGridPorcentaje = $silan_formulasTelas.getobjvar("gridPorcentaje");
        var nuevoRenglo = '<tr class="renglonesGridPorcentajes" id="'+letra+'" data-key="'+rSisAutocomplete.key+'">';
        nuevoRenglo+= '<td class="td-col03-col01 cel-readonly">'+letra+'</td>';
        nuevoRenglo+= '<td class="td-col03-col02">0.000</td>';
        nuevoRenglo+= '<td class="td-col03-col03">'+rSisAutocomplete.id.substr(0,12).trim()+'</td>';
        nuevoRenglo+= '</tr>';
        $(oGridPorcentaje).append(nuevoRenglo);
    },
    modificaColorPorcentajes : function(){
        var letra = String.fromCharCode(rSisAutocomplete.elemento.slice(1,3).toUpperCase());
        var oGridProcentajes = $silan_formulasTelas.getobjvar("gridPorcentaje");
        var oTable = $("tbody"+oGridProcentajes).find("tr");
        for(var i=1; i<=(oTable.length)-1; ++i){
            if($(oTable)[i].id !== letra)
                continue;
            oTable[i].dataset.key = rSisAutocomplete.key;
            $($(oTable)[i]).find("td").eq(2).text(rSisAutocomplete.id.substr(0,12).trim());
            break;
        }
        rSisAutocomplete.id = "";
        rSisAutocomplete.key = 0;
    },
    modificaColorVacio : function(letra, oElement){
        var oGridProcentajes = $silan_formulasTelas.getobjvar("gridPorcentaje");
        var oTable = $("tbody"+oGridProcentajes).find("tr");
        for(var i=1; i<=(oTable.length)-1; ++i){
            if($(oTable)[i].id !== letra)
                continue;
            oTable[i].dataset.key = 0;
            $($(oTable)[i]).find("td").eq(2).text("");
            oElement.removeAttr("data-key");
            break;
        }
    },
    muestraInputColores : function(letra,tab){
        for(l=65; l<=79; ++l){
            if(letra.charCodeAt() >= l)
                $("input#"+tab+l).parent().parent().show();
            else break;
        }
    },
    actualizaDatos : function(dato){
        $("#"+rSisAutocomplete.elemento).val(dato);
        $("#"+rSisAutocomplete.elemento).bind('autocompletechange', $silan_formulasTelas.setAutocomplete("#"+rSisAutocomplete.elemento,dato));
        $("#"+rSisAutocomplete.elemento).trigger("change");
    },
    //*** EVENTOS POLIZAS
    muestraDatosProducto: function(){
        $("div.imagenRegistro").empty();
        $("input#siguientePoliza").val("");
        $.post('./include/polizas.silan.php',
            {data:{evento:"nuevaPoliza", keyproduct:rSisAutocomplete.key}}, function(data){
                $("input#siguientePoliza").val(data.result.siguientePoliza);
                $("input#noCabos").val(data.result.noCabos);
                rSisAutocomplete.id = data.result.id;
                if(data.result.img !== "")
                    $('#div-imagen').css({"background":"url('." + data.result.img + "') center no-repeat", "background-size": "contain"});
                if(data.error !== "")
                    alert(data.result.error);
            },"json").fail(function(data){
                alert("ERROR.-> actualizaDatosPoliza " + data);
            });
    },
    //*** EVENTOS TACOME
    muestraDatosPieza: function(){
        $("div.imagenRegistro").empty();
        $.post('./include/tacome.silan.php',
            {data:{evento:"nuevoRegistro", keyproduct:rSisAutocomplete.key}}, function(data){
                if(data.result.img !== "")
                    $('#div-imagen').css({"background":"url('." + data.result.img + "') center no-repeat", "background-size": "contain"});
                if(data.error !== "")
                    alert(data.result.error);
            },"json").fail(function(data){
                alert("ERROR.-> muestraDatosPieza " + data);
            });
    }
};