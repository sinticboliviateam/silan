/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
Grupos = {
    tValor : 0,
    tParcial : 0,
    tEntero : 0,
    tIndex : 0,
    arrGrupos : [],
    edit : false,
    grid : "",
    creaGrupos : function(gridCurso,rowGrid){
        if(Grupos.edit){
            Grupos.edit = false;
            Grupos.modificaDatos(gridCurso,rowGrid);
        } else {
            var partida = Number(Grupos.tValor.slice(Grupos.tParcial+1,-1));
            if(Grupos.tParcial !== 0){
                Grupos.tIndex = rowGrid;
                Grupos.tEntero = Number(Grupos.tValor.slice(0,Grupos.tParcial));
                var multiplo = Grupos.tEntero/partida;
                var pObject = {t:Grupos.tEntero, p:[partida], m:multiplo, l:[Grupos.tValor.slice(-1).toUpperCase()], i:rowGrid};
                if(Grupos.arrGrupos[Grupos.grid] === undefined)
                    Grupos.arrGrupos[Grupos.grid] = new Array();
                Grupos.arrGrupos[Grupos.grid][rowGrid] = pObject;
            } else {
                var suma = 0;
                Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].p.push(partida);
                for(var i=0; i<=(Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].p.length)-1; ++i)
                    suma+= Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].p[i];
                Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].m = Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].t/suma;
                Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].l.push(Grupos.tValor.slice(-1).toUpperCase());
                $silan_formulasTelas.setobjvar("cTotal",$silan_formulasTelas.getobjvar("cTotal") - Grupos.tEntero);
            }
            Grupos.insertaDatoedicion(gridCurso,rowGrid);
        }
        return Grupos.tEntero;
    },
    recalculaPorcentajes : function(items){
        var letra = ""; var valor = 0;
        for(var i=0; i<=(Grupos.arrGrupos[Grupos.grid].length)-1; ++i){
            if(typeof(Grupos.arrGrupos[Grupos.grid][i]) === "undefined")
                continue;
            for(var j=0; j<=(Grupos.arrGrupos[Grupos.grid][i].p.length)-1; ++j){
                letra = Grupos.arrGrupos[Grupos.grid][i].l[j];
                valor = Grupos.arrGrupos[Grupos.grid][i].p[j] * Grupos.arrGrupos[Grupos.grid][i].m;
                (typeof(items[letra]) === "undefined")
                    ? items[letra] = valor
                    : items[letra]+= valor;
            }
        }
        $silan_formulasTelas.actualizaPorcentajes(items);
    },
    insertaDatoedicion : function(gridCurso,rowGrid){
        var data = String(Grupos.tIndex).padStart(3,"0")+String(Grupos.arrGrupos[Grupos.grid][Grupos.tIndex].p.length-1).padStart(2,"0");
        $("tbody"+gridCurso).find("tr#"+rowGrid).attr("data-edit",data);
    },
    modificaDatos : function(gridCurso, rowGrid){
        var suma = 0;
        $silan_formulasTelas.setobjvar("cTotal",$silan_formulasTelas.getobjvar("cTotal") - Grupos.tEntero);
        var idxGrupo = Number($("tbody"+gridCurso).find("tr#"+rowGrid).attr("data-edit").slice(0,3));
        var idxPartida = Number($("tbody"+gridCurso).find("tr#"+rowGrid).attr("data-edit").slice(3));
        var parcial = Grupos.tValor.indexOf('(');
        Grupos.arrGrupos[Grupos.grid][idxGrupo].l[idxPartida] = Grupos.tValor.slice(-1).toUpperCase();
        Grupos.arrGrupos[Grupos.grid][idxGrupo].p[idxPartida] = Number(Grupos.tValor.slice(parcial+1,-1));
        if(parcial !== 0){
            var total = Number(Grupos.tValor.slice(0,parcial));
            Grupos.arrGrupos[Grupos.grid][idxGrupo].t = total;
            Grupos.tEntero = total;
        }
        for(var i=0; i<=(Grupos.arrGrupos[Grupos.grid][idxGrupo].p.length)-1; ++i)
            suma+= Grupos.arrGrupos[Grupos.grid][idxGrupo].p[i];
        Grupos.arrGrupos[Grupos.grid][idxGrupo].m = Grupos.arrGrupos[Grupos.grid][idxGrupo].t/suma;
    }
};

function formulasTelas() {
    var dataformulas = {
        event : "formulas",
        hilosTotales : 0,
        cTotal : 0,
        oTotal : "",
        datoCurso : "",
        paso : false,
        letras : {U:"",T:"",O:""},
        formValidate : ""
    };
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataformulas : dataformulas[field];
    };
    this.setobjvar = function(field, value){
        dataformulas[field] = value;
    };
    this.setobjletras = function(field, value){
        dataformulas.letras[field] = value;
    };
    this.delobjvar = function(field){
        delete dataformulas[field];
    }
    this.clearobjvar = function(){
        dataformulas = {
            event : "formulas",
            hilosTotales : 0,
            cTotal : 0,
            oTotal : "",
            datoCurso : "",
            paso : false,
            letras : {U:"",T:"",O:""},
            formValidate : ""
        };
    };
//*** INICIALIZA LOS ELEMENTOS - FORM
    this._initformFormulas = function(){
        $silan_formulasTelas.setobjvar("delInput",false);
        $silan_formulasTelas.buscarRegistroFormulas();
        $silan_formulasTelas.getRecordsTableFormula();
        $("#tabs-datos-formula").load("./tabs/tab-datos-tela.tabs.php", function(){
//            $silan_formulasTelas.widgetEventsTabsFormulas();
            $silan_validFormulas.formValidateTelas();
        });
        $("#tabs-tecnicos-formula").load("./tabs/tab-datos-tecnicos.tabs.php", function(){
//            $silan_formulasTelas.widgetEventsTabsFormulas();
            $silan_validFormulas.formValidateTecnicos();
        });
        $("#tabs-urdimbre-formula").load("./tabs/tab-curso-urdimbre.tabs.php", function(){
            $silan_validFormulas.formValidateCursoUrdimbre();
        });
        $("#tabs-trama-formula").load("./tabs/tab-curso-trama.tabs.php", function(){
            $silan_validFormulas.formValidateCursoTrama();
        });
        $("#tabs-orillos-formula").load("./tabs/tab-curso-orillos.tabs.php", function(){
            $silan_validFormulas.formValidateCursoOrillo();
            $silan_formulasTelas.showDialogFormulas();
        });
        $silan_formulasTelas.widgetEventFormulas();
    };
//*** OPCIONES PARA BUSCAR REGISTROS DENTRO DEL GRID
    this.getRecordsTableFormula = function(){
        var filter = $("#filter").val();
        $.post('./include/tablasProduccion.silan.php',
            {data: {opt:"formulasTablas", filter:filter}}, function(data){
                $("#tbl01-records-busqueda").append(data);
                $("#status-busca").html("");
            }).always(function(){
                $silan_formulasTelas.controlaEventoGridTelas();
            });
    };
    this.buscarRegistroFormulas = function(){
        $("#filter").keyup(function(){
            $(".renglonesGrid").remove();
            $("#status-busca").html("Buscando...");
            $silan_formulasTelas.getRecordsTableFormula();
        });
    };
    this.controlaEventoGridTelas = function(){
        $("tr.renglonesGrid").on("click",function(e){
            e.preventDefault();
            $silan_validFormulas.setobjvar("event","editFormula");
            $silan_validFormulas.setobjvar("key",this.id);
            $.post('./include/formulasTelas.silan.php',
                {data:$silan_validFormulas.getobjvar()}, function(data){
                    if(data.error !== 0)
                        $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    alert("ERROR 0 renglonesGrid.-> "+data);
                }).always(function(data){
                    $silan_formulasTelas.editaDatosTela(data.data);
                });
        });
    };
//*** BOTONES FORM PRINCIPAL
    this.widgetEventFormulas = function(){
        $("div#btnNuevoProducto")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validFormulas.setobjvar("event","insertDatos");
                $silan_formulasTelas.setobjvar("viewAutocomplete", true);
                for(var i=1; i<=4; ++i)
                    $($("div#tabs-formulas").find("li")[i]).hide();
                $("div.ocultaTabs").hide();
                //***
                $("div#dialogFormulas").dialog({
                    autoOpen: true,
                    open: function(e,ui){
                        $silan_formulasTelas.activaTabs();
                        $silan_formulasTelas.widgetEventsTabsFormulas();
                        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
                    }
                });
            }).show();
//        $(document).find(".ui-widget").css({"font-size":"0.8em"});
    };
    this.showDialogFormulas = function(){
        $("div#dialogFormulas").dialog({
            modal: true,
            autoOpen: false,
            title: "Actualización de Fórmulas",
            dialogClass: 'hide-close',
            width: 1140,
            maxHeight: 600,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false,
            closeOnEscape: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
        /*$(".tabs-usuarios-botones").css({"font-size":"1.4em"});*/
    };
//*** CONTROL DEL ESTATUS
    this.cambiaEstatusFormulas = function(){ 
        $("#estatus").on("change", function(e){
            e.preventDefault();
            $.post('./include/phpcomm.php',
                {}, function(data){
                    $("#fechaEstatus").prop("value", data.date);
                    $("#usuarioEstatus").prop("value", data.name);
                },"json").fail(function(data){
                    consolo.log("ERROR 0 renglonesGrid.-> "+data);
                });
        });
    };
//*** BOTONES FORM TABS
    this.widgetEventsTabsFormulas = function(){
        //** TAB - DATOS
        $("div#guardar-datos-formulas")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validFormulas.setobjvar("valid",true);
                ($silan_validFormulas.getobjvar("event") === "updateFormula")
                    ? $("form#form-tab-datos-tecnicos").submit()
                    : $("form#form-tab-datos-tela").submit();
            });
            
        $("div.cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#dialogFormulas").dialog("close");
                location.reload();
            });
            
        $("input#claveProducto").focusin(function(){
            rSisAutocomplete.productos(this.id);
        });
        $("input#urdimbreTrama").on("change", function(){
            if($(this).is(":checked")){
                $("div#tabs-trama-formula").find("div").children().attr("disabled","disabled");
                $("form#form-tab-curso-trama").find("div.agregar-curso, div.guarda-datos-curso").hide();
                
            } else { 
                $("div#tabs-trama-formula").find("div").children().removeAttr("disabled"); 
                $("form#form-tab-curso-trama").find("div.agregar-curso, div.guarda-datos-curso").show();
            }
        });
        //*** TAB - TECNICOS
        $("div#guardar-datos-tecnicos")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validFormulas.setobjvar("valid",true);
                $("#form-tab-datos-tecnicos").submit(); //#form-tab-datos-tecnicos
            });
        $("input#hilosFondo").on("focusout", function(){
            if($(this).valid()){
                var htot = Number.parseInt(this.value)+Number.parseInt($("#hilosOrillo").val());
                $("#hilosTotales").prop("value", htot);
            }
        });
        $("input#hilosOrillo").on("focusout", function(){
            if($(this).valid()){
                var htot = Number.parseInt(this.value)+Number.parseInt($("#hilosFondo").val());
                $("#hilosTotales").prop("value", htot);
            }
        });
    //** TAB - CURSO URDIMBRE
        $("input.autocomplete-urdimbre").focusin(function(){
            rSisAutocomplete.hilos(this.id);
        });
        $("input.autocomplete-urdimbre").on("change", function(){
            (this.value.trim() === "")
                ? rSisAutocomplete.modificaColorVacio(String.fromCharCode(rSisAutocomplete.elemento.slice(1,3)),$(this))
                : $("input.autocomplete-urdimbre").trigger($.Event('keypress', { keyCode: 9 }));
        });
        $("div.guarda-datos-curso")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validFormulas.setobjvar("valid",true);
                $("#form-tab-datos-tecnicos").submit();
//                $("form#form-tab-curso-urdimbre").submit();
            });
        //** AGREGAR Y EDITAR DATOS EN LA TABLA DE CURSO
        $("div.agregar-curso")
            .button({ 
                icons: { primary: "ui-icon-plusthick" },
                label: "Agregar curso"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_formulasTelas.agregaInput();
            });
    //** TAB - CURSO TRAMA
        $("input.autocomplete-trama").focusin(function(){
            rSisAutocomplete.hilos(this.id);
        });
        $("input.autocomplete-trama").on("change", function(){
            (this.value.trim() === "")
                ? rSisAutocomplete.modificaColorVacio(String.fromCharCode(rSisAutocomplete.elemento.slice(1,3)),$(this))
                : $("input.autocomplete-trama").trigger($.Event('keypress', { keyCode: 9 }));
        });
    //** TAB - CURSO ORILLO 
        $("input.autocomplete-orillo").focusin(function(){
            rSisAutocomplete.hilos(this.id);
        });
        $("input.autocomplete-orillo").on("change", function(){
            (this.value.trim() === "")
                ? rSisAutocomplete.modificaColorVacio(String.fromCharCode(rSisAutocomplete.elemento.slice(1,3)),$(this))
                : $("input.autocomplete-orillo").trigger($.Event('keypress', { keyCode: 9 }));
        });
    };
    this.agregaInput = function(){
        var oGrid = $silan_formulasTelas.getobjvar("gridCurso");
        $silan_formulasTelas.setobjvar("datoCurso","");
        $("tr.renglonesGridCurso").find("input.input-table-curso").remove();
        var items = parseInt($(oGrid).find("tr.renglonesGridCurso").length) + 1;
        var nuevoRenglo = '<tr class="renglonesGridCurso" id="'+items+'">';
        nuevoRenglo+= '<td class="td-col02-col01 cel-readonly">'+items+'</td>';
        nuevoRenglo+= '<td class="td-col02-col02 upper"><input class="input-table-curso upper" id="u1" name="u1" type="text" value="" required /></td>';
        nuevoRenglo+= '</tr>';
        $(oGrid).append(nuevoRenglo);
        $silan_formulasTelas.setobjvar("idCurso",items);
        $silan_formulasTelas.eventosInputTabla();
        $("input.input-table-curso:last").focus();
    };
    this.eventosInputTabla = function(){
        //** VALIDA LOS DATOS CAPTURADOS EN CURSO
        $("input.input-table-curso").keypress(function(e){
            if(e.keyCode === 13){
                if(this.value === ""){
                    $(this).trigger("focusout");
                    return false;
                }
                $silan_validFormulas.setobjvar("valid",false);
                if($(this).valid()){
                    var dInput = $(this).val();
                    $silan_formulasTelas.setobjvar("delInput",false);
                    $($silan_formulasTelas.getobjvar("gridCurso")+" tr#"+$silan_formulasTelas.getobjvar("idCurso")).find("td").eq(1).text(dInput);
                    var oTotal = $silan_formulasTelas.extraeValorHilos(dInput);
                    var hTotal = $silan_formulasTelas.getobjvar("cTotal") + parseInt(oTotal);
                    $silan_formulasTelas.setobjvar("cTotal",hTotal);
                    $($silan_formulasTelas.getobjvar("oTotal")).val(hTotal);
//                    $silan_formulasTelas.calculaPorcentajes();
                    (dInput.slice(-1).toUpperCase().charCodeAt() < 87)
                        ? rSisAutocomplete.extraeletra(dInput.slice(-1).toUpperCase(),$silan_validFormulas.getobjvar("tab"))
                        : rSisAutocomplete.extraeletraorillo(dInput.slice(-1).toUpperCase(),$silan_validFormulas.getobjvar("tab"));
                    $silan_formulasTelas.setobjvar("paso",false);
                    $(this).remove();
                    (typeof($silan_formulasTelas.getobjvar("edicion")) === "undefined")
                        ? setTimeout(function(){ $silan_formulasTelas.agregaInput(); },125)
                        : $silan_formulasTelas.delobjvar("edicion");
                }
            }
        });
        $("input.input-table-curso").focusin(function(e){
            e.preventDefault();
            this.value = $silan_formulasTelas.getobjvar("datoCurso");
            return true;
        });
        $("input.input-table-curso").focusout(function(){
            if(this.value === ""){
                $($silan_formulasTelas.getobjvar("gridCurso")).find("tr#"+$silan_formulasTelas.getobjvar("idCurso")).remove();
                $silan_formulasTelas.setobjvar("idCurso",$silan_formulasTelas.getobjvar("idCurso")-1);
                $($silan_formulasTelas.getobjvar("oTotal")).val($silan_formulasTelas.getobjvar("cTotal"));
                var oGrid = $silan_formulasTelas.getobjvar("gridCurso");
                var oTable = $("tbody"+oGrid).find("tr.renglonesGridCurso");
                for(var i=0; i<=oTable.length-1; ++i){
                    $(oTable[i]).attr("id",i+1);
                    $(oTable[i]).find("td").eq(0).text(i+1);
                }
                $(this).remove();
                Grupos.edit = false;
                $silan_formulasTelas.setobjvar("delInput",true);
                $silan_formulasTelas.setobjvar("paso",false);
                $silan_formulasTelas.calculaPorcentajes();
            } else { $("input.input-table-curso").trigger($.Event('keypress', { keyCode: 13 })); }
        });
        $("tr.renglonesGridCurso").on("dblclick", function(e){
            e.preventDefault();
            if($silan_formulasTelas.getobjvar("autorizado") === false || 
                ($("div#tabs-formulas").tabs("option", "active") === 3 && $("input#urdimbreTrama").is(":checked")))
                return false;
            if($silan_formulasTelas.getobjvar("paso") === true)
                return false;
            $silan_formulasTelas.setobjvar("edicion",true);
            var datoCurso = $(this).find("td").eq(1).text();
            $silan_formulasTelas.setobjvar("idCurso",parseInt(this.id));
            $silan_formulasTelas.setobjvar("datoCurso",datoCurso);
            (datoCurso.indexOf('(') === -1)
                ? $silan_formulasTelas.setobjvar("cTotal",$silan_formulasTelas.getobjvar("cTotal")-$silan_formulasTelas.extraeValorHilos(datoCurso))
                : Grupos.edit = true;
            var input = '<input class="input-table-curso upper" id="u1" name="u1" type="text"  value="" required />';
            $(this).find("td").eq(1).text("");
            $(this).find("td").eq(1).append(input);
            $silan_formulasTelas.eventosInputTabla();
            $("input.input-table-curso").focus();
            $silan_formulasTelas.setobjvar("paso",true);
        });
    };
    this.extraeValorHilos = function(valor){
        var parcial = valor.indexOf('('); var nuevoValor = 0;
        if(parcial === -1){
            nuevoValor = Number(valor.slice(0,-1));
        } else {
            Grupos.tValor = valor;
            Grupos.tParcial = parcial;
            nuevoValor = Grupos.creaGrupos($silan_formulasTelas.getobjvar("gridCurso"),$silan_formulasTelas.getobjvar("idCurso"));
        }
//        rSisAutocomplete.muestraInputColores(valor.slice(-1).toUpperCase(),$silan_validFormulas.getobjvar("tab"));
        return nuevoValor;
    };
    this.setAutocomplete = function(oElement,item){
        if(item === "")
            return false;
        $(oElement).val(item);
        $(oElement).autocomplete("search", item);
        var list = $(oElement).autocomplete("widget");
//        $(list[0].children[0]).click();
        $(list[0].lastChild).click();
//        setTimeout(function(){ $(list[0].lastChild).click(); }, 125);
    };
//*** EDITA LOS DATOS DEL PRODUCTO SELECCIONADO
    this.editaDatosTela = function(data){
        $silan_formulasTelas.delobjvar("eventedit");
        $silan_formulasTelas.setobjvar("viewAutocomplete", false);
        $silan_formulasTelas.setobjvar("autorizado", data.autorizado);
        //** TAB - DATOS
        $("#claveProducto").prop("value", data.claveProducto).attr("readonly","readonly");
        $("#nombreProducto").prop("value", data.nombreProducto);
        $("#nombreColor").prop("value", data.color);
        $("#coloresProducto").prop("value", data.noColores);
        $("#coloresOrillo").prop("value", data.noColoresOrillo);
        if(data.urdimbreTramaIguales === 'S')
            $("input[name=urdimbreTrama]").prop("checked", true);
        $("#claveAnterior").prop("value", data.claveAnterior);
        $("#nombreMercado").prop("value", data.nombreMercado);
        $("select[name=estatus]").prop("value", data.idEstatus);
        $("#fechaEstatus").prop("value", data.fechaEstatus);
        $("#usuarioEstatus").prop("value", data.usuarioEstatus);
        $('#div-imagen').css({"background":"url('."+data.imagen+"') center no-repeat", "background-size": "contain"});
        //** TAB - TECNICOS
        $("input#hilosFondo").prop("value", data.hilosFondo);
        $("input#hilosOrillo").prop("value", data.hilosOrillo);
        $("input#hilosTotales").prop("value", data.hilosTotales);
        $("input#anchoTela").prop("value", data.anchoTela);
        $("input#noCabos").prop("value", data.noCabos);
        $("input#pasadas2p").prop("value", data.pasadas2p);
        $("input#noRepasoPeine").prop("value", data.noRepasoPeine);
        $("input#repasoPeine").prop("value", data.repasoPeine);
        $("input#tipoDibujo").prop("value", data.tipoDibujo);
        $("input#composicion").prop("value", data.composicion);
    //*** TAB - URDIMBRE
        setTimeout(function(){
            if(typeof(data.cursoUrdimbre) !== "undefined"){
                $silan_validFormulas.setobjvar("tab","U");
                $silan_formulasTelas.setobjvar("eventedit",true);
                Grupos.grid = "U";
                var oGrid = $("tbody#tbl01-records-urdimbre"); var oTotal = 0;
                for(var i=0; i<=data.cursoUrdimbre.length-1; ++i){
                    
                    $silan_formulasTelas.setobjvar("idCurso",i);
                    
                    oTotal+= $silan_formulasTelas.extraeValorHilos(data.cursoUrdimbre[i]["dato"]);
                    var nuevoRenglo = '<tr class="renglonesGridCurso" id="'+data.cursoUrdimbre[i]["secuencia"]+'">';
                    nuevoRenglo+= '<td class="td-col02-col01 cel-readonly">'+data.cursoUrdimbre[i]["secuencia"]+'</td>';
                    nuevoRenglo+= '<td class="td-col02-col02 upper">'+data.cursoUrdimbre[i]["dato"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGrid).append(nuevoRenglo);
                }
                var hTotal = $silan_formulasTelas.getobjvar("cTotal") + parseInt(data.totalUrdido);
                $silan_formulasTelas.setobjvar("cTotal",hTotal);
                $("input#totalHilosUrimbre").val(parseInt(data.totalUrdido));
                var oGridPorcentaje = $("tbody#tbl02-records-urdimbre");
                var tPorcentaje = 0; var pPorcentaje = 0; var letras = "";
                for(var j=0; j<=data.colorHilosUrdimbre.length-1; ++j){
                    pPorcentaje = parseFloat(data.colorHilosUrdimbre[j]["porcentaje"]);
                    var nuevoRenglo = '<tr class="renglonesGridPorcentajes" id="'+data.colorHilosUrdimbre[j]["letra"]+'" data-key="'+data.colorHilosUrdimbre[j]["idHilo"]+'">';
                    nuevoRenglo+= '<td class="td-col03-col01 cel-readonly">'+data.colorHilosUrdimbre[j]["letra"]+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col02">'+pPorcentaje.toFixed(3)+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col03">'+data.colorHilosUrdimbre[j]["claveHilo"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGridPorcentaje).append(nuevoRenglo);
                    tPorcentaje+= pPorcentaje;
                    letras+= data.colorHilosUrdimbre[j]["letra"];
    //                $silan_formulasTelas.setobjvar("event","editFormulas");
    //                letras+= data.hilosUrdimbre[j]["letra"];
                }
                $("input#totalPorcentajeUrdimbre").val(tPorcentaje.toFixed(3));
                for(var i=0; i<=data.colorHilosUrdimbre.length-1; ++i){
                    var oElemento = "input#U"+data.colorHilosUrdimbre[i]["letra"].charCodeAt();
                    rSisAutocomplete.hilos($(oElemento).attr("id"));
                    rSisAutocomplete.actualizaDatos(data.colorHilosUrdimbre[i]["claveHilo"]);
                }
                $silan_formulasTelas.setobjvar("hilosUrdimbre",data.colorHilosUrdimbre);
                $silan_formulasTelas.setobjletras("U",letras);
                $silan_validFormulas.setobjvar("letras", letras);
//                $silan_formulasTelas.eventosInputTabla();
            }
        },125);
    //*** TAB - TRAMA
        setTimeout(function(){
            if(typeof(data.cursoTrama) !== "undefined"){
                $silan_validFormulas.setobjvar("tab","T");
                $silan_formulasTelas.setobjvar("eventedit",true);
                var oGrid = $("tbody#tbl01-records-trama"); var oTotal = 0;
                Grupos.grid = "T";
                for(var i=0; i<=data.cursoTrama.length-1; ++i){
                    
                    $silan_formulasTelas.setobjvar("idCurso",i);
                    
                    oTotal+= $silan_formulasTelas.extraeValorHilos(data.cursoTrama[i]["dato"]);
                    var nuevoRenglo = '<tr class="renglonesGridCurso" id="'+data.cursoTrama[i]["secuencia"]+'">';
                    nuevoRenglo+= '<td class="td-col02-col01 cel-readonly">'+data.cursoTrama[i]["secuencia"]+'</td>';
                    nuevoRenglo+= '<td class="td-col02-col02 upper">'+data.cursoTrama[i]["dato"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGrid).append(nuevoRenglo);
                }
                var hTotal = $silan_formulasTelas.getobjvar("cTotal") + parseInt(data.totalTrama);
                $silan_formulasTelas.setobjvar("cTotal",hTotal);
                $("input#totalHilosTrama").val(parseInt(data.totalTrama));
                var oGridPorcentaje = $("tbody#tbl02-records-trama");
                var tPorcentaje = 0; var pPorcentaje = 0; var letras = "";
                for(var j=0; j<=data.colorHilosTrama.length-1; ++j){
                    pPorcentaje = parseFloat(data.colorHilosTrama[j]["porcentaje"]);
                    var nuevoRenglo = '<tr class="renglonesGridPorcentajes" id="'+data.colorHilosTrama[j]["letra"]+'" data-key="'+data.colorHilosTrama[j]["idHilo"]+'">';
                    nuevoRenglo+= '<td class="td-col03-col01 cel-readonly">'+data.colorHilosTrama[j]["letra"]+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col02">'+pPorcentaje.toFixed(3)+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col03">'+data.colorHilosTrama[j]["claveHilo"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGridPorcentaje).append(nuevoRenglo);
                    tPorcentaje+= pPorcentaje;
                    letras+= data.colorHilosTrama[j]["letra"];
                }
                $("input#totalPorcentajeTrama").val(tPorcentaje.toFixed(3));
                for(var i=0; i<=data.colorHilosTrama.length-1; ++i){
                    var oElemento = "input#T"+data.colorHilosTrama[i]["letra"].charCodeAt();
                    rSisAutocomplete.hilos($(oElemento).attr("id"));
                    rSisAutocomplete.actualizaDatos(data.colorHilosTrama[i]["claveHilo"]);
                }
                $silan_formulasTelas.setobjvar("hilosTrama",data.colorHilosTrama);
                $silan_formulasTelas.setobjletras("T",letras);
                $silan_validFormulas.setobjvar("letras", letras);
//                $silan_formulasTelas.eventosInputTabla();
            }
        },125);
    //*** TAB - ORILLO
        setTimeout(function(){
            if(typeof(data.cursoOrillo) !== "undefined"){
                $silan_validFormulas.setobjvar("tab","O");
                $silan_formulasTelas.setobjvar("eventedit",true);
                var oGrid = $("tbody#tbl01-records-orillo"); var oTotal = 0;
                for(var i=0; i<=data.cursoOrillo.length-1; ++i){
                    
//                    $silan_formulasTelas.setobjvar("idCurso",i);
                    
                    oTotal+= $silan_formulasTelas.extraeValorHilos(data.cursoOrillo[i]["dato"]);
                    var nuevoRenglo = '<tr class="renglonesGridCurso" id="'+data.cursoOrillo[i]["secuencia"]+'">';
                    nuevoRenglo+= '<td class="td-col02-col01 cel-readonly">'+data.cursoOrillo[i]["secuencia"]+'</td>';
                    nuevoRenglo+= '<td class="td-col02-col02 upper">'+data.cursoOrillo[i]["dato"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGrid).append(nuevoRenglo);
                }
                var hTotal = $silan_formulasTelas.getobjvar("cTotal") + parseInt(oTotal);
                $silan_formulasTelas.setobjvar("cTotal",hTotal);
                $("input#totalHilosOrillo").val(parseInt(oTotal));
                var oGridPorcentaje = $("tbody#tbl02-records-orillo");
                var tPorcentaje = 0; var pPorcentaje = 0; var letras = "";
                for(var j=0; j<=data.colorHilosOrillo.length-1; ++j){
                    pPorcentaje = parseFloat(data.colorHilosOrillo[j]["porcentaje"]);
                    var nuevoRenglo = '<tr class="renglonesGridPorcentajes" id="'+data.colorHilosOrillo[j]["letra"]+'" data-key="'+data.colorHilosOrillo[j]["idHilo"]+'">';
                    nuevoRenglo+= '<td class="td-col03-col01 cel-readonly">'+data.colorHilosOrillo[j]["letra"]+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col02">'+pPorcentaje.toFixed(3)+'</td>';
                    nuevoRenglo+= '<td class="td-col03-col03">'+data.colorHilosOrillo[j]["claveHilo"]+'</td>';
                    nuevoRenglo+= '</tr>';
                    $(oGridPorcentaje).append(nuevoRenglo);
                    tPorcentaje+= pPorcentaje;
                    letras+= data.colorHilosOrillo[j]["letra"];
                }
                $("input#totalPorcentajeOrillo").val(tPorcentaje.toFixed(3));
                for(var i=0; i<=data.colorHilosOrillo.length-1; ++i){
                    var oElemento = "input#O"+data.colorHilosOrillo[i]["letra"].charCodeAt();
                    rSisAutocomplete.hilos($(oElemento).attr("id"));
                    rSisAutocomplete.actualizaDatos(data.colorHilosOrillo[i]["claveHilo"]);
                }
                $silan_formulasTelas.setobjvar("hilosOrillo",data.colorHilosOrillo);
                $silan_formulasTelas.setobjletras("O",letras);
                $silan_validFormulas.setobjvar("letras", letras);
            }
            $silan_formulasTelas.eventosInputTabla();
        },125);
    //*** VERIFICA PERMISOS
        if(!data.autorizado){
            $("div#guardar-datos-formulas").hide();
            $("div#tabs-datos-formula").find("div").children().attr("disabled","disabled");
            //****
            $("div#guardar-datos-tecnicos").hide();
            $("div#tabs-tecnicos-formula").find("div").children().attr("disabled","disabled");
            //****
            $("div.agregar-curso").hide();
            $("div.guarda-datos-curso").hide();
            $("div#tabs-urdimbre-formula").find("div").children().attr("disabled","disabled");
            $("div#tabs-trama-formula").find("div").children().attr("disabled","disabled");
            $("div#tabs-orillos-formula").find("div").children().attr("disabled","disabled");
        }
        if(data.urdimbreTramaIguales === 'S'){
            $("div#tabs-trama-formula").find("div").children().attr("disabled","disabled");
            $("form#form-tab-curso-trama").find("div.agregar-curso, div.guarda-datos-curso").hide();
        }
    //***
        $("#dialogFormulas").dialog({
            autoOpen: true,
            open: function(e,ui){
                $silan_formulasTelas.activaTabs();
                $silan_formulasTelas.widgetEventsTabsFormulas();
                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.83em"});
            }
        });
        $silan_validFormulas.setobjvar("event","updateFormula");
        
        // $("input[name=tipoUsuario][value='"+data.tipoUsuario+"']").prop("checked", true); CASO: OPTION
    };
//*** PERSONALIZA LOS DATOS DE TABS
    this.activaTabs = function(){
        $("div#tabs-formulas").tabs({
                beforeActive : function(event, ui){},
                activate : function( event, ui ) {
                    $silan_formulasTelas.setobjvar("viewAutocomplete", true);
                    if(ui.newPanel.index() === 1)
                        $("input#claveProducto").focus();
                }
            });
        $("div#tabs-formulas").on("tabsbeforeactivate", function(event, ui){
//             *** $("div.div-autocomplete").hide();
//            if(ui.newPanel.index() === 1 || ui.newPanel.index() === 2){
//                if($silan_validFormulas.getobjvar("event") === "updateCursos")
//                   $silan_validFormulas.setobjvar("event","updateTecnicos");
//            } else { $silan_validFormulas.setobjvar("event","updateCursos"); }
            if(ui.newPanel.index() === 3){
//                var oColores = $("#form-tab-curso-urdimbre div.div-autocomplete");
//                for(var i=0; i<=Number($("#coloresProducto").val()); ++i)
//                    $(oColores[i]).show();
                var param = {
                    oGrid : "#tbl01-records-urdimbre",
                    totItems : parseInt($("tbody#tbl01-records-urdimbre").find("tr.renglonesGridCurso").length),
                    oTotal : "#totalHilosUrimbre",
                    oTotalPorcentaje : "#totalPorcentajeUrdimbre",
                    cTotal : parseInt($("input#totalHilosUrimbre").val()),
//                    colores : $silan_formulasTelas.getobjvar("letras")["U"],
                    oGridPorcent : "#tbl02-records-urdimbre",
                    tab : "U"
                };
                $silan_formulasTelas.actualizaVariablesCursos(param);
                if($silan_formulasTelas.getobjvar("eventedit") !== undefined && $silan_formulasTelas.getobjvar("hilosUrdimbre") !== undefined){
                    var data = $silan_formulasTelas.getobjvar("hilosUrdimbre");
                    var oElemento = "";
                    for(var i=0; i<=data.length-1; ++i){
                        oElemento = "input#U"+data[i]["letra"].charCodeAt();
                        rSisAutocomplete.hilos($(oElemento).attr("id"));
                        rSisAutocomplete.actualizaDatos(data[i]["claveHilo"]);
                    }
                }
            }
            if(ui.newPanel.index() === 4){
                var param = {
                    oGrid : "#tbl01-records-trama",
                    totItems : parseInt($("tbody#tbl01-records-trama").find("tr.renglonesGridCurso").length),
                    oTotal : "#totalHilosTrama",
                    oTotalPorcentaje : "#totalPorcentajeTrama",
                    cTotal : parseInt($("input#totalHilosTrama").val()),
                    oGridPorcent : "#tbl02-records-trama",
                    tab : "T"
                };
                $silan_formulasTelas.actualizaVariablesCursos(param);
                if($silan_formulasTelas.getobjvar("eventedit") !== undefined && $silan_formulasTelas.getobjvar("hilosTrama") !== undefined){
                    var data = $silan_formulasTelas.getobjvar("hilosTrama");
                    var oElemento = "";
                    for(var i=0; i<=data.length-1; ++i){
                        oElemento = "input#T"+data[i]["letra"].charCodeAt();
                        rSisAutocomplete.hilos($(oElemento).attr("id"));
                        rSisAutocomplete.actualizaDatos(data[i]["claveHilo"]);
                    }
                }
            }
            if(ui.newPanel.index() === 5){
                var param = {
                    oGrid : "#tbl01-records-orillo",
                    totItems : parseInt($("tbody#tbl01-records-orillo").find("tr.renglonesGridCurso").length),
                    oTotal : "#totalHilosOrillo",
                    oTotalPorcentaje : "#totalPorcentajeOrillo",
                    cTotal : parseInt($("input#totalHilosOrillo").val()),
                    oGridPorcent : "#tbl02-records-orillo",
                    tab : "O"
                };
                $silan_formulasTelas.actualizaVariablesCursos(param);
                if($silan_formulasTelas.getobjvar("eventedit") !== undefined && $silan_formulasTelas.getobjvar("hilosOrillo") !== undefined){
                    var data = $silan_formulasTelas.getobjvar("hilosOrillo");
                    var oElemento = "";
                    for(var i=0; i<=data.length-1; ++i){
                        oElemento = "input#O"+data[i]["letra"].charCodeAt();
                        rSisAutocomplete.hilos($(oElemento).attr("id"));
                        rSisAutocomplete.actualizaDatos(data[i]["claveHilo"]);
                    }
                }
            }
        });
    };
    this.actualizaVariablesCursos = function(param){
        $silan_formulasTelas.setobjvar("idCurso",param.totItems);
        $silan_formulasTelas.setobjvar("gridCurso",param.oGrid);
        $silan_formulasTelas.setobjvar("oTotal",param.oTotal);
        $silan_formulasTelas.setobjvar("oTotalPorcentaje",param.oTotalPorcentaje);
        $silan_formulasTelas.setobjvar("cTotal",param.cTotal);
        $silan_formulasTelas.setobjvar("gridPorcentaje",param.oGridPorcent);
//        $silan_validFormulas.setobjvar("letras",param.colores);
        $silan_validFormulas.setobjvar("tab",param.tab);
        Grupos.grid = param.tab;
    };
//*** CALCULA - ACTUALIZA TABLA DE PORCENTAJES
    this.calculaPorcentajes = function(){
        var items = new Object(); var datoCurso = ""; var valor = 0; var letra = "";
        var oGrid = $silan_formulasTelas.getobjvar("gridCurso");
        var oTable = $("tbody"+oGrid).find("tr.renglonesGridCurso");
        for(var i=0; i<=oTable.length-1; ++i){
            datoCurso = $(oTable[i]).find("td").eq(1).text();
            if(datoCurso.indexOf('(') !== -1)
                continue;
            valor = $silan_formulasTelas.extraeValorHilos(datoCurso);
            letra = datoCurso.substr(-1).toUpperCase();
            (typeof(items[letra]) === "undefined")
                ? items[letra] = valor
                : items[letra]+= valor;
        }
        (Grupos.arrGrupos[Grupos.grid] === undefined)
            ? $silan_formulasTelas.actualizaPorcentajes(items)
            : Grupos.recalculaPorcentajes(items);
    };
    this.actualizaPorcentajes = function(items){
        var total = $silan_formulasTelas.getobjvar("cTotal");
        var oGridPorcentaje = $silan_formulasTelas.getobjvar("gridPorcentaje");
        var oRow = ""; var porcentaje = 0;
        var oTable = $("tbody"+oGridPorcentaje).find("tr");
        var pTotal = 0;
        for(var j=1; j<=(oTable.length)-1; ++j)
            $($(oTable)[j]).find("td").eq(1).text("0.000");
        Object.getOwnPropertyNames(items).forEach(
            function (val, idx, array) {
                oRow = $("tbody"+oGridPorcentaje).find("tr#"+val);
                if($(oRow).text() !== ""){
                    porcentaje = (items[val] / total) * 100;
                    $(oRow).find("td").eq(1).text(porcentaje.toFixed(3));
                    pTotal+= parseFloat(porcentaje.toFixed(3));
                }
            }
        );
        if($silan_formulasTelas.getobjvar("delInput") === true){
            var ultimoRegistro = (oTable.length)-1;
            if($($(oTable)[ultimoRegistro]).find("td").eq(1).text() === "0.000"){
                $($(oTable)[ultimoRegistro]).remove();
                $silan_formulasTelas.actualizaPorcentajes(items);
            } else { $silan_formulasTelas.setobjvar("delInput", false); }
        }
        $("input"+$silan_formulasTelas.getobjvar("oTotalPorcentaje")).val(pTotal.toFixed(3));
    };
}
$silan_formulasTelas = new formulasTelas();