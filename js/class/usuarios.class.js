/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
function usuarios(){
    var datauser = {
        event : ""
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  datauser : datauser[field];
    };
    
    this.setobjvar = function(field, value){
        datauser[field] = value;
    };
    
    this.clearobjvar = function(){
        datauser = {
            event : ""
        };
    };
//*** INICIALIZA LOS ELEMENTOS DOM DEL FORM
    this._initform = function(){
        //$silan_usuarios.buscarRegistro();
        $silan_usuarios.getRecordsTableUser();
        $("div#tabs-datos-usuarios").load("./tabs/tab-datos-usuarios.tabs.php", function(){
            $("#tabs-funciones-usuarios").load("./tabs/tab-funciones-usuarios.tabs.php", function(){
                $silan_usuarios.setobjvar("event","listFunctions");
                $silan_usuarios.getTabFunctionsUser();
                $("#tabs-usuarios").tabs();
                $silan_usuarios.widgetEventsTabsUsuarios();
                setTimeout(function(){
                    $silan_usuarios.showDialogUser();
                    $silan_validUsuarios.formValidateDatos();
                    $silan_validUsuarios.validFieldUserfun(); }, 125);
                $silan_usuarios.widgetEventUsuarios();
            });
        });
    };
    
//*** OPCIONES PARA BUSCAR REGISTROS DENTRO DEL GRID
    this.getRecordsTableUser = function(){
        var filter = $("#filter").val();
//        filter = filter.replace(/\s/g,"");
        $.post('./include/tablasSistemas.silan.php',
            {data: {opt:"usuarios", filter:filter}}, function(data){
                $("#tbl01-records-busqueda").append(data);
                $("#status-busca").html("");
            }).always(function(){
                $silan_usuarios.widgetEventUsuarios();
            });
    };
    
    this.buscarRegistro = function(){
        $("#filter").keyup(function(){
            $(".renglonesGrid").remove();
            $("#status-busca").html("Buscando...");
            $silan_usuarios.getRecordsTableUser();
        });
    };
//*** BOTONES FORM PRINCIPAL
    this.widgetEventUsuarios = function(){
        $("button#btnNuevoUsuario")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Nuevo"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validUsuarios.setobjvar("event","insert");
                $("#dialogUser").dialog("open");
            });
            
        $("tr.renglonesGrid").on("click",function(e){
            e.preventDefault();
            $silan_validUsuarios.setobjvar("event","edit");
            $silan_validUsuarios.setobjvar("key",this.id);
            $.post('./include/usuarios.silan.php',
                {data:$silan_validUsuarios.getobjvar()}, function(data){
                    if(data.error !== 0)
                        $silan_msg.showmessage(data.msg,data.action,data.title);
                },"json").fail(function(data){
                    alert("ERROR 0 renglonesGrid.-> "+data);
                }).always(function(data){
                    $silan_usuarios.editDatauser(data.result);
                });;
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
    };
//*** BOTONES FORM TABS
    this.widgetEventsTabsUsuarios = function(){
    //*** USUARIOS
        $("#estatus").on("change", function(e){
            e.preventDefault();
            $.post('./include/phpcomm.php',
                {}, function(data){
                    $("#fechaEstatus").prop("value", data.date);
                    $("#usuarioEstatus").prop("value", data.name);
                },"json").fail(function(data){
                    alert("ERROR 0 renglonesGrid.-> "+data);
                });
        });
        
        $("#guardar-datos-usuario")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#form-tab-datos-usuarios").submit();
            });
            
        $("#borrar-datos-usuario")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                
            });
            
        $(".cerrar-tabs")
            .button({ 
                icons: {
                    primary: "ui-icon-arrowreturnthick-1-w"
                },
                label: "Regresar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#dialogUser").dialog("close");
                location.reload();
            });
            
        $("#btnAgregaImagen")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Agregar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#fileupload").trigger("click");
            });
            
        $("#btnEliminaImagen")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Eliminar"
            })
            .on("click",function(e){
                e.preventDefault();
                $silan_validUsuarios.setobjvar("event","delImage");
                $.post('./include/usuarios.silan.php',
                    {data : $silan_validUsuarios.getobjvar()}, function(data){
                        if(data.error !== 0)
                            $silan_msg.showmessage(data.msg,data.action,data.title);
                        $("#div-imagen").css({"background" : ""});
                    },"json").fail(function(data){
                        alert("ERROR 0 btnEliminaImagen.-> "+data);
                    });
            });
            
        $("#fileupload").on("change", function(){
            $silan_usuarios.uploadfiles();
        });
    //*** FUNCIONES
        $("#guardar-funciones-usuario")
            .button({ 
                icons: {
                    primary: "ui-icon-disk"
                },
                label: "Guardar"
            })
            .on("click",function(e){
                e.preventDefault();
                $("#guardar-datos-usuario").trigger("click");
            });
            
        $("#borrar-funciones-usuario")
            .button({ 
                icons: {
                    primary: "ui-icon-trash"
                },
                label: "Borrar"
            })
            .on("click",function(e){
                e.preventDefault();
                
            });
    };
    
    this.uploadfiles = function(){
        var formData = new FormData($("#form-tab-datos-usuarios")[0]);
        //formData.append("kp","user");
        $.ajax({
            url: './include/uploadfiles.silan.php',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){
                $('#div-imagen').css({"background":"url('"+data+"') center no-repeat", "background-size": "contain"});
            }
        });  
    };
        
    this.showDialogUser = function(){
        $("#dialogUser").dialog({
            modal: true,
            autoOpen: false,
            title: "Actualización de Usuarios",
            dialogClass: 'hide-close',
            width: 1140,
            maxHeight: 600,
            resizable: false,
            position: { my: "center top", at: "top+52px", of: window },
            draggable: false
        });
        $(document).find(".ui-widget, .ui-button").css({"font-size":"0.8em"});
        $(".tabs-usuarios-botones").css({"font-size":"1.4em"});
    };
    
    this.getTabFunctionsUser = function(){
        //*** LISTA TODAS LAS FUNCIONES
        $.post('./include/usuarios.silan.php',
            {data: $silan_usuarios.getobjvar()}, function(data){
            if(data.error !== 0)
                $silan_msg.showmessage(data.msg,data.action,data.title);
            },"json").fail(function(data){
                alert("ERROR 0 fillTabFunctions.-> "+data);
            }).always(function(data){
                $silan_usuarios.appendCheckBox(data.dataresult);
            });
    };
    
    this.appendCheckBox = function(data){
        $("#func-ventanas").append(data.win);
        $("#func-procesos").append(data.pro);
        $("#func-reportes").append(data.rep);
    };
//*** EDITA LOS DATOS DEL USUARIO SELECCIONADO
    this.editDatauser = function(data){
        $("#nombreCompleto").prop("value", data.nombreCompleto);
        $("#usuario").prop("value", data.usuario);
        $("#password").prop("value", data.password);
        $("#repeatPassword").prop("value", data.password);
        $("#rfc").prop("value", data.rfc);
        $("#tipoUsuario").prop("value", data.rfc);
//        $("input[name=tipoUsuario][value='"+data.tipoUsuario+"']").prop("checked", true); CASO: OPTION o CHECKBOX
        $("select[name=tipoUsuario]").prop("value", data.tipoUsuario);
        $("#puesto").prop("value", data.puesto);
        $("#horario").prop("value", data.horario);
        $("#correos").prop("value", data.correos);
        $("#celulares").prop("value", data.celulares);
        $("#telefonos").prop("value", data.telefonos);
        $("#ultimoAcceso").prop("value", data.ultimoAcceso);
        $("select[name=estatus]").prop("value", data.idEstatus);
//        $("input[name=estatus][value="+data.estatus+"]").prop("checked", true); CASO: OPTION o CHECKBOX
        $("#fechaEstatus").prop("value", data.fechaEstatus);
        $("#usuarioEstatus").prop("value", data.usuarioEstatus);
        
        $('#div-imagen').css({"background":"url('"+data.imagen+"') center no-repeat", "background-size": "contain"});
        $silan_usuarios.editUserFunctions(data.functions);

        //$("#dialogUser").dialog("open");
        //$silan_validUsuarios.setobjvar("event","update");
    };
//*** EXAMINA RELACION FUNCIONES => USUARIOS
    this.editUserFunctions = function(data){
        data.forEach(function(ele,idx, arr){
            //** FUNCIONES - VENTANAS
            $("[name^=func_"+ele.func+"]").each(function(){
                if(this.value.indexOf('L') >= 0 && ele.permiso.indexOf('L') >= 0)
                    $(this).attr('checked', true);
                if (this.value.indexOf('E') >= 0 && ele.permiso.indexOf('E') >= 0)
                    $(this).attr('checked', true);
                if (this.value.indexOf('B') >= 0 && ele.permiso.indexOf('B') >=0)
                    $(this).attr('checked', true);
            });
            //** FUNCIONES - PROCESOS
            $("[name^=proc_"+ele.func+"]").each(function(){
                if(this.value.indexOf('X') >= 0 && ele.permiso.indexOf('X') >= 0)
                    $(this).attr('checked', true);
            });
            //** FUNCIONES - REPORTES
            $("[name^=rep_"+ele.func+"]").each(function(){
                if(this.value.indexOf('X') >= 0 && ele.permiso.indexOf('X') >= 0)
                    $(this).attr('checked', true);
            });
        });
        $("#dialogUser").dialog("open");
        $silan_validUsuarios.setobjvar("event","update");
    };
}
$silan_usuarios = new usuarios();