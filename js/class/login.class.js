/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua
 * SiLan v1.0
 * MEXICO, 2017
*/
function silanLogin() {
    var dataform = {
        event : "login"
    };
    
    this.getobjvar = function(field){
        return (typeof(field) === "undefined") ?  dataform : dataform[field];
    };
    
    this.setobjvar = function(field, value){
        dataform[field] = value;
    };
    
    this.clearobjvar = function(){
        dataform = {
            event : "login"
        };
    };
    
    this.formValidate = function(){
        $("#formLogin").validate({
            onfocusout: function(element) { 
                var context = $(element).val();
                $(element).val(context.replace(/\s/g,""));
                ($(element).valid())
                    $silan_login.setobjvar($(element).attr("name"),$(element).val());
            },
            messages: {
                username : { required : "Este campo es obligatorio" },
                password : { required : "Este campo es obligatorio" }
            },
            submitHandler: function(form){
                $silan_login.setobjvar("username",$("#username").val());
                $silan_login.setobjvar("password",$("#password").val());
                $.post('./include/login.silan.php',
                    {data:$silan_login.getobjvar()}, function(data){
                        (data.error !== 0)
                            ? $silan_msg.showmessage(data.msg,data.action,data.title)
                            : window.location.replace(data.path);
                    },"json").fail(function(data){
                        alert("ERROR 0 formValidate.-> "+data);
                    });
            }
        });
    };




    this.widgetEventForm = function(){
        $("#username, #password").keypress(function(e){
            if(e.keyCode === 13){
                $silan_login.sendValidate();
            }
        });
    };

    this.sendValidate = function(){
        $("#formLogin").submit();
    };
}
$silan_login = new silanLogin();