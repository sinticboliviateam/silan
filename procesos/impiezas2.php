<?php
/* importa piezas 20190408
 *
 * requiere definir:
 * sinespacios.csv    <-- 12 campos y compas, copiarlo en carpeta del proceso
 * 
 * $fechaImportación
 * $serieAnterior   último consecutivo en piezas[noPieza]
 *
*/

echo ("Inicio</br>");

$pruebaConexion = False;
if ($pruebaConexion) {
	echo ("Prueba conexion</br>");
	$mysqli = new mysqli("localhost", "root", "raiz", "silan");

	/* verificar la conexión */
	if (mysqli_connect_errno()) {
	    printf("Falló la conexión failed: %s\n", $mysqli->connect_error);
	    exit();
	}

	$query = "SELECT * FROM silan.piezas LIMIT 1";
	$result = $mysqli->query($query);

	if($result->num_rows === 0) {
		echo ("VACIO</br>");
	} else {
		$row = $result->fetch_array(MYSQLI_NUM);
		printf ("%s %s %s %s\n", $row[0], $row[1], $row[2], $row[3]);
	}

	exit();
}


$servidor="localhost";
$dbusuario="root";
$dbpass="raiz";
$dbnombre="silan";
$tabla= "silan.piezas";
$conex= new mysqli($servidor,$dbusuario,$dbpass,$dbnombre);

if($conex->connect_error) {
	die("conexión fallida: ".$conex->connect_error);
}

$fechaImportacion= "2019-04-14 00:01:00";

/* campos excel 20190410 -Erick Mendieta
[0] cve_Articulo
[1] cve_Poliza 
[2] etqta_No
[3] etqta_Medida
[4] etqta_Peso
[5] etqta_Registro_Entrada
[6] etqta_Salio
[7] Cat_Articulo
[8] Cat_Referencia
[9] Cat_Color
[10] SKU
[11] Codigo_nuevo
*/
// se requiere etqta_no en forma de número, codigo_nuevo sin caracter 'al inicio

$linea = -1;
$serieAnterior= 25453;   //  <-- poner último consecutivo en la BD
$contadorSinNoPieza= 1;
$archivo = fopen("sinespacios.csv", "r");
while (($datos = fgetcsv($archivo, ",")) == true) 
{
	$linea++;

	if($linea >= 0){

    	//$idPiezas= $linea;
    	$idPiezas= 'null';
    	$idPoliza= 1; // sólo para esta importación 20190410

		$clavePolizaAnterior= trim($datos[1]);

		$claveArticuloNueva= trim($datos[11]);
    	if($claveArticuloNueva != ''){  // si tiene clave nueva de producto
	    	$partesClavePolizaAnterior= explode("-",$clavePolizaAnterior);
	    	
	    	$partesClavePolizaAnterior[0]= $claveArticuloNueva;
	    	$nuevaPoliza= implode("-", $partesClavePolizaAnterior);

    		$clavePieza= $nuevaPoliza;
    	} else {  //si NO tiene clave nueva de producto
    		$clavePieza= $clavePolizaAnterior;
    	}
    	$clavePieza= $clavePieza."-".(int)trim($datos[2]);
    	

    	$metros= trim($datos[3]);
		$kilos= trim($datos[4]);   //que con los tres decimales?

		/* renumerar apartir de 1 20180115 Ya no es necesario
		if ($datos[5]=='') {
			$datos[5]= $serieAnterior;
			$serieAnterior++;
		} 
		if (is_nan($datos[5]) ) {
			$datos[5]= $serieAnterior;
			$serieAnterior++;
		} 
		echo "serieAnterior: $serieAnterior <br/>";		
		*/

		$serieAnterior++;
		$noPieza= $serieAnterior;  // consecutivo único para la pieza
		$etiquetaPt= '';
		$fechaTacome= $fechaImportacion;
		$usuarioTacome= 'Admin';
		$documentoEntrada= 'CA'.trim($datos[5]);   //20190408 el consecutivo anterior en SCOP
		$fechaEntrada= $fechaImportacion;
		$usuarioEntrada= 'Admin';
		$documentoSalida= 'null';
		$noCliente= 'null';
		$fechaSalida= 'null';
		$usuarioSalida= 'null';
		$idTarima= 'null';
		

		$valores="$idPiezas,$idPoliza,'$clavePieza',$metros,$kilos,$noPieza,";
		$valores.="'$etiquetaPt','$fechaTacome','$usuarioTacome','$documentoEntrada',";
		$valores.="'$fechaEntrada','$usuarioEntrada',$documentoSalida,$noCliente,";
		$valores.="$fechaSalida,$usuarioSalida,$idTarima";

		$sql="INSERT INTO $tabla VALUES($valores)";

		echo "--> ".$linea." ". $sql;
		echo "<br/>";

		$activo = true;
		if ($activo) {
				$conex->query($sql);
				if($conex->errno) {
					echo $conex->errno."> ".$conex->error." NO se pudo afectar el registro LINEA->".$linea;
					echo "<br/>";
					print_r($datos);
					echo $sql;
					die($conex->errno);
				}
		}
	}
}
echo "ya salí del loop <br/>";
fclose($archivo);
mysqli_close($conex);
echo("fin de proceso -> ".$linea." LINEAS PROCESADAS");

?>