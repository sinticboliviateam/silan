<?php
/* pruebas general
 *
 *
*/

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$servidor="localhost";
$dbusuario="root";
$dbpass="raiz";
$dbnombre="silan";
$tabla= "silan.piezas";
$conex= new mysqli($servidor,$dbusuario,$dbpass,$dbnombre);

$fechaSaldoInicial= "2018-01-01 00:01:00";

/* campos excel Dic. 2017
[0] cve_Articulo
[1] cve_Poliza 
[2] etqta_No
[3] etqta_Medida
[4] etqta_Peso
[5] etqta_Registro_Entrada
[6] etqta_Salio
[7] Cat_Articulo
[8] Cat_Referencia
[9] Cat_Color
[10] Codigo_nuevo
[11] SKU
*/
// se requiere etqta_no en forma de número, codigo_nuevo sin caracter 'al inicio

$linea = -1;
$serieAnterior= 1;
$contadorSinNoPieza= 1;
$archivo = fopen("sinespacios.csv", "r");
while (($datos = fgetcsv($archivo, ",")) == true) 
{
	$corte = !(empty($datos[7].$datos[8].$datos[9]));
	$linea++;

	if($corte && $linea >= 1){

    	$idPiezas= $linea;
    	$idPoliza= 1; //sólo para esta existencia inicial
		$clavePolizaAnterior= $datos[1];

    	if($datos[10] != ''){  // si tiene clave nueva de producto
	    	$partesClavePolizaAnterior= explode("-",$clavePolizaAnterior);
	    	$claveArticuloNueva= $datos[10];
	    	$partesClavePolizaAnterior[0]= $claveArticuloNueva;
	    	$nuevaPoliza= implode("-", $partesClavePolizaAnterior);

    		$clavePieza= $nuevaPoliza;
    	} else {  //si NO tiene clave nueva de producto
    		$clavePieza= $clavePolizaAnterior;
    	}
    	$clavePieza= $clavePieza."-".$datos[2];
    	

    	$metros= $datos[3];
		$kilos= $datos[4];   //que con los tres decimales?

		/* renumerar apartir de 1 20180115 Ya no es necesario
		if ($datos[5]=='') {
			$datos[5]= $serieAnterior;
			$serieAnterior++;
		} 
		if (is_nan($datos[5]) ) {
			$datos[5]= $serieAnterior;
			$serieAnterior++;
		} 
		echo "serieAnterior: $serieAnterior <br/>";		
		*/

		$noPieza= $datos[5];  // SE refiere al número de serie único, es un entero

		$etiquetaPt= "R";
		$fechaTacome= $fechaSaldoInicial;
		$usuarioTacome= 'Admin';
		$documentoEntrada= 'saldo inicial 2018';
		$fechaEntrada= $fechaSaldoInicial;
		$usuarioEntrada= 'Admin';
		$documentoSalida= null;
		$noCliente= null;
		$fechaSalida= '';
		$usuarioSalida= null;
		

		$valores="$idPiezas,$idPoliza,'$clavePieza',$metros,$kilos,$noPieza,
		'$etiquetaPt','$fechaTacome','$usuarioTacome','$documentoEntrada',
		'$fechaEntrada','$usuarioEntrada','$documentoSalida','$noCliente',
		'$fechaSalida','$usuarioSalida'";

		$sql="INSERT INTO $tabla VALUES($valores)";


		$conex->query($sql);

		if($conex->errno) {
			echo $conex->errno."> ".$conex->error." NO se pudo afectar el registro LINEA->".$linea;
			echo "<br/>";
			print_r($datos);
			echo $sql;
			die($conex->errno);
		}

		//echo "<br/>";
	}
}
fclose($archivo);
mysqli_close($conex);
echo("fin de proceso -> ".$linea." LINEAS PROCESADAS");

?>