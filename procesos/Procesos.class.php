<?php

require_once(dirname(__FILE__)."/../include/dbConnections.inc.php");

class Procesos extends Modelo{
    public $msgout = "";
    public $debug = FALSE;
    
    public function __construct(){
        parent::__construct();
    }
    
    public function guardaPoliza($datakey) {
// Genera tantos registros en la tabla "piezas" como lo indique el campo Numero de piezas
// Recalcula el valor del campo Siguiente póliza considerando el mes del campo fecha_registro.
// Para cada Clave de artículo y mes nuevo el número de póliza inicia en uno,
// para el siguiente registro es dos etc.
// Para cada registro guarda en los campos:

// id_piezas: consecutivo automático de la tabla PIEZAS
// no_poliza: para cada clave_de_producto y mes es un consecutivo que inicia en 1. (ver si es necesario ya que clave_pieza contiene este dato en la 4ta posición)
//
// clave_pieza: ejem.: 77803-08-16-003-18   // es necesario guardar la cadena ya que tiene un equivalente en código de barras para la entrada y salida de la pieza
//       77803-08-16-003= no_poliza
//       77803= clave de producto o tela
//       08= sería el mes del campo FECHA_REGISTRO   
//       16= sería el año del campo FECHA_REGISTRO
//       003= número siguiente de póliza del mes 8 (mismo dato que en el campo no_poliza, ver si es necesario redundar)
//       18= número de pieza. Inicia de 1…n, donde n= al valor en el campo Número_de_piezas
// 
// metros   <-- 35.00 como default en la ventana de pólizas, el usuario podrá modificarlo. Rango posibles (35 a 60)
// no_telar
// no_cabos
// fecha_registro
// usuario_registro
// 
// Campos que no se graban aquí (se graban en Tacome):
// metros    <-- el usuario podrá modificar el valor previamente registrado en la ventana de pólizas
// kilos
// fecha_tacome
// usuario_tacome
//
// polizas.php?data[widget]=guardar&data[clave_producto]=11604&data[no_piezas]=5&data[metros]=35&data[no_telar]=2&data[no_cabos]=180

        if ($this->debug) var_dump($datakey);

        $clave_producto = $datakey->clave_producto;
        if ($this->debug) echo "<br> clave_producto: " . $clave_producto;

        $no_piezas = $datakey->no_piezas;
        if ($this->debug) echo "<br> no_piezas: " . $no_piezas;

        // DB
        $metros = $datakey->metros;
        if ($this->debug) echo "<br> metros: " . $metros;

        $no_telar = $datakey->no_telar;
        if ($this->debug) echo "<br> no_telar: " . $no_telar;

        $no_cabos = $datakey->no_cabos;
        if ($this->debug) echo "<br> no_cabos: " . $no_cabos;

        $id_piezas = null; // auto generado
        $no_poliza = null; // tal vez no se use
        $clave_pieza = null; // calculado para cada pieza en base a la clave del producto
        $kilos = null; // Se llena en Tacome
        $fecha_registro = null; // se calcula al momento
        $fecha_tacome = null; // Se llena en Tacome
        $usuario_tacome = null; // Se llena en Tacome

        //$datakey->usuario_registro = 'sin usuario';
        $datakey->fecha_registro = date('Y-m-d H:i:s');
        if ($this->debug) echo "<br> fecha_registro: " . $fecha_registro;

        // Determinar poliza
        $ann = date('Y');
        $mes = date('m');

        $no_poliza = $this->obtieneSiguientePoliza($clave_producto, $ann, $mes);
        // echo "<br> no_poliza: " . $no_poliza;

        // Insertar piezas
        for ($no_pieza = 1; $no_pieza <= $no_piezas; $no_pieza++) {
            $clave_pieza = $this->construyeClavePieza($clave_producto, $ann, $mes, $no_poliza, $no_pieza);
            if ($this->debug) echo "<br> clave_pieza: " . $clave_pieza;
            $this->insertaPieza($datakey, $clave_pieza);
        }

        $poliza = $this->construyeClavePoliza($clave_producto, $ann, $mes, $no_poliza);
        $dataresult = (object) array("error" => "", "poliza" => $poliza);

        return $dataresult;

    }

    private function insertaPieza($datakey, $clave_pieza) {
        $id_piezas = null; // auto generado

        $metros = $datakey->metros;
        $no_telar = $datakey->no_telar;
        $no_cabos = $datakey->no_cabos;

        $no_poliza = null; // tal vez no se use
        //$clave_pieza = $datakey->clave_pieza;
        $fecha_registro = $datakey->fecha_registro;
        $usuario_registro = $datakey->usuario_registro;

        $kilos = null; // Se llena en Tacome
        $fecha_tacome = null; // Se llena en Tacome
        $usuario_tacome = null; // Se llena en Tacome

        $sql = "INSERT INTO piezas (clave_pieza, metros, no_telar, no_cabos, fecha_registro, usuario_registro)
        VALUES ('" . $clave_pieza . "'," . $metros . "," . $no_telar . "," . $no_cabos . ",'" . $fecha_registro . "','" . $usuario_registro . "')";

        if ($this->debug) echo '<br>sql: ' . $sql;

        if ($this->_db_sys->query($sql) === TRUE) {
            if ($this->debug) echo "New record created successfully";
        } else {
            if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
        }

    }

    public function tacomePieza($datakey) {
        $id_piezas = null; // auto generado

        $clave_pieza = $datakey->clave_pieza;
        $metros = $datakey->metros;
        $kilos = $datakey->kilos;
        $usuario_tacome = $datakey->usuario_tacome;

        $fecha_tacome = date('Y-m-d H:i:s');

        // Calcula número
        $sql = "SELECT ifnull(MAX(numero),0) as maxnum FROM piezas;";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        $numero = intval($row['maxnum']) + 1;

        // Actualiza el registro con información Tacome
        $sql = "UPDATE piezas ";
        $sql .= "SET metros = $metros, ";
        $sql .= "kilos = $kilos, ";
        $sql .= "usuario_tacome = '$usuario_tacome', ";
        $sql .= "fecha_tacome = '$fecha_tacome', ";
        $sql .= "numero = '$numero', ";
        $sql .= "etiqueta = 'I' ";
        $sql .= "WHERE clave_pieza = '$clave_pieza'";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $dataresult = (object) array("error" => "");
        if ($this->_db_sys->query($sql) === TRUE) {
            if ($this->debug) echo "Record updated successfully";
        } else {
            if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
            $dataresult->error = "Error: " . $sql . "<br>" . $conn->error;
            return dataresult;
        }

        //return $dataresult;
        return $this->imprimePieza($datakey);
    }

    public function reimprimePieza($datakey) {
        $clave_pieza = $datakey->clave_pieza;

        // Actualiza el registro con información Tacome
        $sql = "UPDATE piezas ";
        $sql .= "SET etiqueta = 'R' ";
        $sql .= "WHERE clave_pieza = '$clave_pieza'";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $dataresult = (object) array("error" => "");
        if ($this->_db_sys->query($sql) === TRUE) {
            if ($this->debug) echo "Record updated successfully";
        } else {
            if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
            $dataresult->error = "Error: " . $sql . "<br>" . $conn->error;
            return dataresult;
        }

        //return $dataresult;
        return $this->imprimePieza($datakey);
    }

    public function imprimePieza($datakey) {
        $clave_pieza = $datakey->clave_pieza;

        $dataresult = (object) array("error" => "");

        // recuperar datos de pieza de la db
        $sql = "SELECT * FROM piezas WHERE clave_pieza = '$clave_pieza' LIMIT 1";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);

        $pieza = $result->fetch_assoc();
        if ($pieza == NULL) {
            $dataresult->error = 'No se eoncontró la pieza ' . $clave_pieza;
            return $dataresult;            
        }

        // determinar clave_prod
        $partesClavePieza = preg_split('/-/', $clave_pieza);
        $clave_prod = $partesClavePieza[0];

        // Recuperar datos de producto de db
        $sql = "SELECT * FROM artref WHERE clave_prod = '$clave_prod' LIMIT 1";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);

        $artref = $result->fetch_assoc();
        if ($pieza == NULL) {
            $dataresult->error = 'No se eoncontró el producto ' . $clave_prod;
            return $dataresult;            
        }


        // Organizar datos de la etiqueta
        $printdata = (object) array( 'data' => array(
            "servicio" => 'etiqueta',
            "sku" => $artref['sku'],
            "marca" => $artref['marca'],
            "clave_prod" => $artref['clave_prod'],
            "color" => $artref['color'],
            "ancho" => $artref['ancho'],
            "composicion01" => $artref['composicion01'],
            "composicion02" => $artref['composicion02'],
            "metros" => $pieza['metros'],
            "clave_pieza" => $pieza['clave_pieza'],
            "numero" => $pieza['numero']
        ));

        $printdata = http_build_query($printdata);
        //var_dump($printdata);


        //$rutaServidorImpresion = 'localhost';
        $rutaServidorImpresion = '10.0.0.23'; // CAMBIAR: Direccion IP de ZebraPrint

        // Llamar servicio de impresiom
        $url = 'http://' . $rutaServidorImpresion . '/ZebraPrint/zebraprint.php';

        //echo $url;
        //var_dump($printdata);

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $printdata);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );
        curl_close( $ch );

        //echo ($response);
        $dataresult = (object) array("error" => "", "response" => $response);
        return $dataresult;

        // Evaluar el resultado de la impresion

        // Registrar impresion en db

    }

    public function entradaPiezas($datakey) {
        $documento_entrada = $datakey->documento_entrada;
        $lista_numero_pieza = $datakey->lista_numero_pieza;
        $usuario_entrada = $datakey->usuario_entradas_pt;

        $fecha_entrada = date('Y-m-d H:i:s');

        $piezas = preg_split("/,/", $lista_numero_pieza);

        $dataresult = (object) array("error" => "");
        foreach ($piezas as $numero) {
            $sql = "UPDATE piezas ";
            $sql .= "SET documento_entrada = '$documento_entrada', usuario_entrada = '$usuario_entrada', fecha_entrada = '$fecha_entrada' ";
            $sql .= "WHERE numero = '$numero'";

            if ($this->debug) echo '<br>sql: ' . $sql;

            if ($this->_db_sys->query($sql) === TRUE) {
                if ($this->debug) echo "Record updated successfully";
            } else {
                if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
                $dataresult->error = "SQL: " . $sql . "<br>Error: " . $conn->error . "<br>";
                break;
            }
        }
      
        return $dataresult;
    }

    public function salidaPiezas($datakey) {
        $no_cliente = $datakey->no_cliente;
        $lista_numero_pieza = $datakey->lista_numero_pieza;
        $usuario_salida = $datakey->usuario_salidas_pt;

        $fecha_salida = date('Y-m-d H:i:s');

        $piezas = preg_split("/,/", $lista_numero_pieza);

        // Calcula documento_salida
        $sql = "SELECT ifnull(MAX(documento_salida),0) as ultimoDocumento FROM piezas;";

        if ($this->debug) echo '<br>sql: ' . $sql;

        $result = $this->_db_sys->query($sql);
        $row = $result->fetch_assoc();
        $documento_salida = intval($row['ultimoDocumento']) + 1;

        $dataresult = (object) array(
            "error" => "",
            "documento_salida" => $documento_salida
        );

        foreach ($piezas as $numero) {
            $sql = "UPDATE piezas ";
            $sql .= "SET documento_salida = '$documento_salida', ";
            $sql .= "no_cliente = '$no_cliente', ";
            $sql .= "usuario_salida = '$usuario_salida', ";
            $sql .= "fecha_salida = '$fecha_salida' ";
            $sql .= "WHERE numero = '$numero'";

            if ($this->debug) echo '<br>sql: ' . $sql;

            if ($this->_db_sys->query($sql) === TRUE) {
                if ($this->debug) echo "Record updated successfully";
            } else {
                if ($this->debug) echo "Error: " . $sql . "<br>" . $conn->error;
                $dataresult->error = "SQL: " . $sql . "<br>Error: " . $conn->error . "<br>";
                break;
            }
        }
      
        return $dataresult;
    }
}
?>