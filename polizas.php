<?php
/* 
 * Libreria creada por: Manuel Luna /  Francisco J Gonzalez Zarazua / Diego
 * SiLan v1.0
 * MEXICO, 2017
*/
session_start();
if(!array_key_exists("idUsuario", $_SESSION['datauser'])) header("location:login.php");
//unset($_SESSION["record"]);
require_once './include/class/menu.class.php';
$autPermisos = new menu();
$_SESSION["datauser"]["menuaccess"] = "02020000";
$permisos = $autPermisos->regresaPermisosUsuario();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pólizas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/comm.css">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="./css/polizas.css">
        <link rel="stylesheet" href="./css/tableViewRecords.css">
        <link rel="stylesheet" media="screen" href="./css/cupertino.1.12/jquery-ui.css" />
        <script src="./js/jquery-3.2.0.js"></script>
        <script src="./css/cupertino.1.12/jquery-ui.js"></script>
        <script src="./js/jquery-validation-1.17.0/dist/jquery.validate.js"></script>
        <script src="./js/jquery-validation-1.17.0/src/localization/messages_es.js"></script>
        <script src="./js/class/msg_silan.class.js"></script>
        <script src="./js/class/settingmenu.class.js"></script>
        <script src="./js/class/polizas.class.js"></script>
        <script src="./js/class/polizasValidate.class.js"></script> 
        <script src="./js/class/autocomplete.class.js"></script>
        <script>
            $(document).ready(function(){
                $setmenu.getLinksUser();
                rSisPolizas.read();
                $(document).find(".ui-widget, .ui-button").css({"font-size":"0.75em"});
            });
        </script>
    </head>
    <body>
        <div class="header-menu">
            <?php include('./menu.php'); ?>
        </div>
        <div class="cleared" style="height: 50px;"></div>
        <div class="container-main">
            <div class="div-buscaRegistros">
                <div class="buscaRegistros">
                    <h3>Pólizas</h3>
                    <input id="filter" name="filter" type="text" placeholder="dato a buscar..." class="oInput" maxlength="20" value="">
                    <span id="status-busca"></span>
                </div>
                <div class="boton-nuevoRegistro">
                    <?php 
                        $autorizado = strpos($permisos, 'E');
                        if($autorizado){ ?> 
                            <div id="btnNuevaPoliza" class="" style=""></div>
                    <?php } ?>
                </div>
            </div>
            <div class="cleared" style="display: block; width: 95%;"></div>
            <div class="tableRecords">
                <table class="">
                    <thead class="tableHead">
                        <tr class="">
                            <th class="th-col05-col01-p">No. póliza</th>
                            <th class="th-col05-col02-p">Fecha</th>
                            <th class="th-col05-col03-p">No. piezas</th>
                            <th class="th-col05-col04-p">Piezas terminadas</th>
                            <th class="th-col05-col05-p">Estatus</th>
                        </tr>
                    </thead>
                    <tbody id="tbl01-records-busqueda">
                        <tr class="" id="">
                            <td></td><td></td><td></td><td></td><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="cleared" style="width: 95%; height: 10px; display: block;"></div>
            <div id="dialogPolizas" style="display: none;">
<!--                <div id="tabs-polizas" style="height: auto; margin: 0 auto;">
                    <ul>
                        <li><a href="div#tabs-datos-poliza">Póliza</a></li>
                    </ul>
                    <div id="tabs-datos-poliza"></div>
                </div>-->
            </div>
        </div>
        <div id="modal-message"></div>
    </body>
</html>
