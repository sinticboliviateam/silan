<?php
defined('SB_DS') or define('SB_DS', DIRECTORY_SEPARATOR);
define('MODULOS_DIR', dirname(__FILE__) . SB_DS . 'modulos');
define('BASE_DIR', dirname(__FILE__));
define('BASE_URL', ((isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['HTTP_HOST']) . '/SiLan');
define('TMP_DIR', BASE_DIR . SB_DS . 'tmp');
define('USUARIOS_DIR', BASE_DIR . SB_DS . 'img' . SB_DS . 'usuarios');
define('USUARIOS_URL', BASE_URL . '/img/usuarios');
define('PRODUCTOS_DIR', BASE_DIR . SB_DS . 'img' . SB_DS . 'clavesprods');
define('PRODUCTOS_URL', BASE_URL . '/img/clavesprods');
define('IP_IMPRESORA', "10.0.0.35");
define('IP_IMPRESORA_2', "10.0.0.17");
define('PUERTO_IMPRESORA', '515');
define('NOMBRE_IMPRESORA', 'ZT410-203dpi_ZPL');

ini_set('display_errors', 1);error_reporting(E_ALL);
//require_once BASE_DIR . SB_DS . "/include/class/config.inc.php";
require_once BASE_DIR . SB_DS . 'include' . SB_DS . 'functions.php';
if( defined('CFG_ONLY') )
	return true;
require_once BASE_DIR . SB_DS . "/include/class/dbConnections.inc.php";
require_once BASE_DIR . SB_DS . '/include/class/menu.class.php';
class Controlador
{
	/**
	 *
	 * @var Applicacion
	 */
	protected	$app;
	protected 	$viewHTML;
	protected	$viewFile;
	protected	$viewVars 	= [];
	protected	static $modelsPool = [];
	
	public function Init()
	{
		
	}
	public function SetApp($app)
	{
		$this->app = $app;
	}
	/**
	 * Carga una vista del modulo
	 * @param unknown $vista
	 */
	public function CargarVista($vista, $vars = [])
	{
		if( !strstr($vista, '.php') )
			$vista .= '.php';
		$mod = $this->app->GetModulo();
		
		$this->viewFile = $mod['path'] . SB_DS . 'vistas' . SB_DS . $vista;
		$this->viewVars = $vars;
	}
	public function MostrarVista()
	{
		if( !$this->viewFile )
			return false;
		if( !is_file($this->viewFile) )
			throw new Exception('La vista no existe');
		
		//##extrae las variables para que esten disponibles en la vista
		extract($this->viewVars);
		include $this->viewFile;
	}
	/**
	 * @var Modelo
	 * @param string $nombre
	 * @return NULL|Modelo
	 */
	public function CargarModelo($nombre = null, $modulo = null)
	{
		
		$mod = $this->app->GetModulo();
		if( $modulo )
		{
			$mod = ['nombre' => $modulo, 'path' => $this->app->ObtenerRutaModulo($modulo)];
		}
		if( !$nombre )
			$nombre 	= $mod['nombre'];
		if( !$nombre )
			return null;
		
		if( isset(self::$modelsPool[$nombre]) )
		{
			return self::$modelsPool[$nombre];
		}
		
		$nombre 		= ucfirst($nombre);
		$modeloClass 	= $nombre . 'Modelo';
		$modeloFile		= $mod['path'] . SB_DS . 'Modelos' . SB_DS . $modeloClass . '.php';
		
		if( !is_file($modeloFile) )
			return null;
	
		require_once $modeloFile;
		
		self::$modelsPool[$nombre] = new $modeloClass();
		self::$modelsPool[$nombre]->SetApp($this->app);
		self::$modelsPool[$nombre]->Init();
		
		//die($nombre);
		return self::$modelsPool[$nombre];
	}
	/**
	 * Send a JSON response and ends execution
	 * 
	 * @param array|object $data
	 */
	public function ResponseJSON($data)
	{
		//$data = (array)$data;
		$json = sb_json_encode($data);
		if( !$json )
		{
			http_response_code(500);
			header('Content-type: application/json');
			die(json_encode(['status' => 'error', 'error' => json_last_error_msg()]));
		}
		if( (is_array($data) && isset($data['error'])) || (is_object($data) && isset($data->error)) )
			http_response_code(500);
		else 
			http_response_code(200);
		header('Content-type: application/json; charset=utf-8');
		//eader('Content-type: application/json');
		die($json);
	}
}
class Applicacion
{
	protected $modulo;
	protected $ctrl;

	public function __construct()
	{

	}
	public function Iniciar()
	{
		$this->ConectarDB();
		session_start();
		if( !isset($_SESSION['datauser']) || !array_key_exists("idUsuario", $_SESSION['datauser']) )
		{
			if( !headers_sent() )
			{
				header("Location:login.php");
				die();
			}
			header('Content-Type: application/json');
			http_response_code(500);
			$json = json_encode(['status' => 'error', 'error' => 'Session invalida']);
			die($json);
		}
		
		$modulo = isset($_REQUEST['mod']) ? $_REQUEST['mod'] : null;
		if( !$modulo )
			return false;
		$this->CargarModulo($modulo);

	}
	public function ConectarDB()
	{
		
	}
	public function ObtenerRutaModulo($modulo)
	{
		$moduloPath 		= MODULOS_DIR . SB_DS . $modulo;
		if( !is_dir($moduloPath) )
			return null;
		
		return $moduloPath;
	}
	protected function CargarModulo($modulo)
	{
		$moduloPath 		= MODULOS_DIR . SB_DS . $modulo;
		$claseControlador 	= ucfirst($modulo) . 'Controlador';
		$archivoControlador = $moduloPath . SB_DS . $claseControlador . '.php';
		
		//##verificar si se necesita un controlador especifico
		$task	= isset($_REQUEST['task']) ? $_REQUEST['task'] : null;

		if( $task && strstr($task, '.') )
		{
			list($controller_class, ) 	= array_map('trim', explode('.', $task));
			
			if( is_file( $moduloPath . SB_DS . $controller_class . 'Controlador.php' ) )
			{
				$archivoControlador = $moduloPath . SB_DS . $controller_class . 'Controlador.php';
				$claseControlador	= $controller_class . 'Controlador';
			}
			elseif( is_file( $moduloPath . SB_DS . 'Controladores' . SB_DS . $controller_class . 'Controlador.php' ) )
			{
				$archivoControlador = $moduloPath . SB_DS . 'Controladores' . SB_DS . $controller_class . 'Controlador.php';
				$claseControlador	= $controller_class . 'Controlador';
			}
		}

		//##buscar controllador
		if( !is_file($archivoControlador) )
			return false;
		
		$this->modulo = ['path' => $moduloPath, 'archivo_controlador' => $archivoControlador, 'nombre' => $modulo];
		require_once $archivoControlador;
		$this->ctrl = new $claseControlador();
		$this->ctrl->SetApp($this);
		$this->ctrl->Init();
		return $this->ctrl;
	}
	public function Dispatch()
	{
		if( !$this->ctrl )
			return false;
		$task	= isset($_REQUEST['task']) ? $_REQUEST['task'] : null;
		if( !$task )
			$task = 'Default';
		if( strstr($task, '.') )
		{
			list($controller_class, $_task) = explode('.', $task);
			$task = $_task;
		}
		$method_name = 'Task' . sb_str2camelcase($task);

		//##verificar si existe el metodo en el controlador
		if( !method_exists($this->ctrl, $method_name) )
			return false;
		call_user_func([$this->ctrl, $method_name]);
	}
	/**
	 * Devuelve el modulo actual
	 *
	 */
	public function GetModulo()
	{
		return $this->modulo;
	}
	/**
	 * Returna la instancia del controlador actual
	 * 
	 * @return Controlador
	 */
	public function GetControllador(){return $this->ctrl;}
	public function Show()
	{
		try
		{
			$this->ctrl && $this->ctrl->MostrarVista();
		}
		catch(Exception $e)
		{
			print "<h2>". $e->getMessage()."</h2>";
		}
	}
}
$app = new Applicacion();
try
{
	$app->Iniciar();
	$app->Dispatch();
}
catch(Exception $e)
{
	die('APP ERROR:' . $e->getMessage());
}