<?php
    session_start();
?>
<head>
    <script src="./js/settingsmenu.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            //$("nav #menu").find(".active").removeClass();
            //var links = $("nav #menu > li");
            //$(links[0]).find("a").first().addClass("active");
            $setmenu.getLinksUser();
        });
    </script>
</head>
<body>
    <div class="productname"><h2>SiLan</h2></div>
    <div class="menubar">
        <nav>
            <ul id="menu">
                <li><a href="#" class="" data-menu="001000">Medidores</a></li>
                <li><a href="#" class="" data-menu="002000">An&aacute;lisis</a></li>
                <li><a href="#" class="" data-menu="003000">Cat&aacute;logos</a>
                    <ul class="children">
                        <li><a href="#" class="" data-menu="003001">Zonas</a></li>
                        <li><a href="#" class="" data-menu="003002">Empresas</a></li>
                        <li><a href="#" class="" data-menu="003003">Áreas</a></li>
                        <li><a href="#" class="" data-menu="003004">Ubicaciones</a></li>
                        <li><a href="#" class="" data-menu="003005">Sensores</a></li>
                        <li><a href="#" class="" data-menu="003006">Unidades</a></li>
                        <li><a href="#" class="" data-menu="003007">Doctos. Externos</a></li>
                        <li><a href="#" class="" data-menu="003008">Alertas</a></li>
                        <li><a href="#" class="" data-menu="003009">Estatus</a></li>
                        <li><a href="#" class="" data-menu="003010">Controles</a></li>
                    </ul>
                </li>
                <li><a href="#" class="" data-menu="004000">Admin</a>
                    <ul class="children">
                        <li><a href="#" class="" data-menu="004001">Generales</a></li>
                        <li><a href="#" class="" data-menu="004002">Usuarios</a></li>
                        <li><a href="#" class="" data-menu="004003">Actividades</a></li>
                    </ul>
                </li>   
                <li><a href="#" class="" data-menu="005000">Salida</a></li>
            </ul>
        </nav>
    </div>
    <div class="username"><span><?php echo $_SESSION["usuario"]["nombre_completo"]; ?></span></div>
</body>