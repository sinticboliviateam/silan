CREATE DATABASE  IF NOT EXISTS `silan` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `silan`;
-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: silan
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nombreCompleto` varchar(100) NOT NULL,
  `rfc` varchar(15) DEFAULT NULL,
  `puesto` varchar(20) DEFAULT NULL,
  `telefonos` varchar(70) DEFAULT NULL,
  `celulares` varchar(70) DEFAULT NULL,
  `correos` varchar(100) DEFAULT NULL,
  `idEstatus` int(10) unsigned NOT NULL,
  `usuarioEstatus` varchar(100) DEFAULT NULL,
  `fechaEstatus` datetime NOT NULL COMMENT '	',
  `ultimoAcceso` datetime DEFAULT NULL,
  `ipp` varchar(15) DEFAULT NULL,
  `horario` varchar(15) DEFAULT NULL,
  `imagen` varchar(15) DEFAULT NULL,
  `tipoUsuario` enum('A','U') NOT NULL DEFAULT 'U',
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `idUsuario_UNIQUE` (`idUsuario`),
  UNIQUE KEY `rfc_UNIQUE` (`rfc`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'frank','admin','Francisco Javier González ','ASGVS','Programador','55','44','frank@gmail.com',1,'Francisco','2017-08-10 20:42:17','2017-08-21 09:40:54',NULL,'9 - 6','','A'),(3,'luisa','123','Luisa González Marín','rago601010','confección','55','044','cuenta@hotmail.com',2,'Francisco Javier González ','2017-08-18 11:42:12','2017-08-19 04:28:45','','9 a 18:30','luisa.jpeg','U'),(4,'oscar','123','Oscar Dávalos Manrriquez','ABCDEF010101BJ0','Jefe de planta','','','',1,'Francisco Javier González ','2017-08-18 02:54:51','0000-00-00 00:00:00','','9 - 6','','U'),(5,'jaime','123','Jaime Solorzano','asasas020202ab2','','','','',1,'Francisco Javier González ','2017-08-18 04:33:31','2017-08-19 10:53:39','','','','U'),(6,'pablo','123','Pablo Gómez Alcantara','goap121212bj0','','','','',1,'Francisco Javier González ','2017-08-18 04:36:31','0000-00-00 00:00:00','','','','U'),(7,'alicia','123','alicia carolina Rojas','ali','psicología','','','',1,'Francisco Javier González ','2017-08-18 08:12:09','0000-00-00 00:00:00','','9 a 18:30','luisa.jpeg','U'),(8,'mario','123','Mario de la Fuente','mar','apoyo','','','',1,'Francisco Javier González ','2017-08-18 08:17:16','0000-00-00 00:00:00','','9 a 18:30','mario.jpg','U'),(9,'felipe','123','Felipe Contreras','fel','mantenimiento','','','',1,'Francisco Javier González ','2017-08-18 08:41:37','0000-00-00 00:00:00','','9 a 18:30','felipe.jpg','U');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-22 14:28:18
