CREATE DATABASE  IF NOT EXISTS `silan` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `silan`;
-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: silan
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuariosFunciones`
--

DROP TABLE IF EXISTS `usuariosFunciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariosFunciones` (
  `idFuncion` int(10) unsigned NOT NULL,
  `idUsuario` int(10) unsigned NOT NULL,
  `permisos` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariosFunciones`
--

LOCK TABLES `usuariosFunciones` WRITE;
/*!40000 ALTER TABLE `usuariosFunciones` DISABLE KEYS */;
INSERT INTO `usuariosFunciones` VALUES (2,4,'LE'),(3,4,'LE'),(14,6,'LEB'),(15,6,'LEB'),(16,6,'LEB'),(2,5,'L'),(2,7,'L'),(4,7,'E'),(7,7,'B'),(8,7,'LE'),(8,9,'L'),(9,9,'E'),(10,9,'B'),(4,8,'E'),(5,8,'L'),(8,8,'B'),(11,8,'L'),(14,8,'E'),(15,8,'L'),(16,8,'B'),(2,10,'L'),(2,3,'LEB'),(3,3,'E'),(7,3,'L'),(8,3,'L'),(9,3,'LB'),(10,3,'L');
/*!40000 ALTER TABLE `usuariosFunciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-22 14:30:16
