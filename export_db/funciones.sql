CREATE DATABASE  IF NOT EXISTS `silan` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `silan`;
-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: silan
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `funciones`
--

DROP TABLE IF EXISTS `funciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funciones` (
  `idFuncion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modulo` varchar(45) DEFAULT NULL,
  `tipo` enum('V','P') NOT NULL DEFAULT 'V',
  `funcion` varchar(45) DEFAULT NULL,
  `programa` varchar(45) DEFAULT NULL,
  `campos` varchar(50) DEFAULT NULL,
  `valores` varchar(50) DEFAULT NULL,
  `orden` tinyint(3) unsigned DEFAULT NULL,
  `menu` varchar(8) DEFAULT NULL,
  `mostrar` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`idFuncion`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funciones`
--

LOCK TABLES `funciones` WRITE;
/*!40000 ALTER TABLE `funciones` DISABLE KEYS */;
INSERT INTO `funciones` VALUES (1,'Produccion','V','Inventario','inicio.php','','',101,'01000000',1),(2,'Produccion','V','Entradas H','entradasH.php','','',102,'01010000',1),(3,'Produccion','V','Salidas H','salidasH.php','','',103,'01020000',1),(4,'Produccion','V','Kardex','kardex.php','','',104,'01030000',1),(5,'Produccion','V','Rep 1','rep1.php','','',105,'01040000',1),(6,'Produccion','V','Producción','inicio.php','','',106,'02000000',1),(7,'Produccion','V','Fórmulas telas','formulasTelas.php','','',107,'02010000',1),(8,'Produccion','V','Pólizas','polizas.php','','',108,'02020000',1),(9,'Produccion','V','Entradas PT','entradasPt.php','','',109,'02030000',1),(10,'Produccion','V','Tacome','tacome.php','','',110,'02040000',1),(11,'Produccion','V','Salidas PT','salidasPt.php','','',111,'02050000',1),(12,'Produccion','V','Rep P1','repP1.php','','',112,'02060000',1),(13,'Produccion','V','Sistemas','inicio.php','','',113,'03000000',1),(14,'Produccion','V','Generales','generales.php','','',114,'03010000',1),(15,'Produccion','V','Corte mes','corteMes.php','','',115,'03020000',1),(16,'Produccion','V','Usuarios','usuarios.php','','',116,'03030000',1);
/*!40000 ALTER TABLE `funciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-22 14:18:09
