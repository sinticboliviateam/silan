CREATE DATABASE  IF NOT EXISTS `silan` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `silan`;
-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: silan
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `formulasTelas`
--

DROP TABLE IF EXISTS `formulasTelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formulasTelas` (
  `idFormulas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `claveProducto` varchar(15) NOT NULL,
  `noColores` int(10) unsigned NOT NULL DEFAULT '1',
  `noColoresOrillo` int(10) unsigned NOT NULL DEFAULT '1',
  `urdimbreTramaIguales` enum('S','N') NOT NULL DEFAULT 'N',
  `idEstatus` int(10) unsigned NOT NULL,
  `usuarioEstatus` varchar(25) NOT NULL,
  `fechaEstatus` datetime NOT NULL,
  `hilosFondo` int(10) unsigned NOT NULL DEFAULT '0',
  `hilosOrillo` int(10) unsigned NOT NULL DEFAULT '0',
  `hilosTotales` int(10) unsigned NOT NULL DEFAULT '0',
  `anchoTela` int(10) unsigned NOT NULL DEFAULT '0',
  `noCabos` int(10) unsigned NOT NULL DEFAULT '0',
  `pasadas2p` int(10) unsigned NOT NULL DEFAULT '0',
  `noRepasoPeine` int(10) unsigned NOT NULL DEFAULT '0',
  `tipoDibujo` varchar(20) NOT NULL,
  `composicion` varchar(20) NOT NULL,
  PRIMARY KEY (`idFormulas`),
  UNIQUE KEY `claveProducto_UNIQUE` (`claveProducto`),
  KEY `idx_claveProducto` (`claveProducto`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formulasTelas`
--

LOCK TABLES `formulasTelas` WRITE;
/*!40000 ALTER TABLE `formulasTelas` DISABLE KEYS */;
INSERT INTO `formulasTelas` VALUES (1,'00116T004',81,51,'S',3,'Francisco Javier González','2017-08-21 05:29:06',1000,20,1020,21,31,41,51,'Tafetán','100% ACR');
/*!40000 ALTER TABLE `formulasTelas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-22 14:23:46
