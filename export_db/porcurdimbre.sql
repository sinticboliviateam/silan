/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.0.45-community-nt-log : Database - silan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`silan` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `silan`;

/*Table structure for table `porcurdimbre` */

DROP TABLE IF EXISTS `porcurdimbre`;

CREATE TABLE `porcurdimbre` (
  `idPorcUrdimbre` int(10) unsigned NOT NULL auto_increment,
  `idFormulaTela` int(10) unsigned NOT NULL,
  `letra` varchar(1) NOT NULL,
  `idHilo` int(10) unsigned NOT NULL,
  `porcentaje` float unsigned NOT NULL,
  PRIMARY KEY  (`idPorcUrdimbre`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
